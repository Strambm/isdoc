<?php

namespace Isdoc\Enums;

use Isdoc\Exceptions\UnvalidTaxScheme;

/**
 * Způsob výpočtu DPH 
 */
class TaxScheme
{
    const VAT = "VAT";
    const TIN = "TIN";

    const ALL = [
        self::VAT,
        self::TIN,
    ];

    public static function validate(?string $taxScheme): ?string
    {
        if (in_array($taxScheme, self::ALL) || $taxScheme == null) {
            return $taxScheme;
        } else {
            throw new UnvalidTaxScheme();
        }
    }
}
