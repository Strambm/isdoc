<?php

namespace Isdoc\Traits;

use DateTime;
use Isdoc\Exceptions\InvalidDateFormat;

trait DateValidation
{
    /**
     * Validuje date string na požadovaný formát
     * @see \Isdoc\Tests\Traits\DateValidation\ValidateDateTest
     */
    protected function validateDate(string $date, string  $format = 'Y-m-d'): void
    {
        $d = DateTime::createFromFormat($format, $date);
        if (!($d && $d->format($format) === $date)) {
            throw new InvalidDateFormat($date);
        }
    }

    /**
     * @see \Isdoc\Tests\Traits\DateValidation\ValidateDateTest
     */
    protected function validateDateNullable(string|null $date, string $format = 'Y-m-d'): void
    {
        if(!is_null($date)) {
            $this->validateDate($date, $format);
        }
    }
}
