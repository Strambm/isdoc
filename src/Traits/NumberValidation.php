<?php

namespace Isdoc\Traits;

use Isdoc\Exceptions\InvalidNumericFormat;

trait NumberValidation
{
    /**
     * @see \Isdoc\Tests\Traits\NumberValidation\ValidateNumericTest
     */
    protected function validateNumeric(string $value): void
    {
        if(!ctype_digit($value))
        {
            throw new InvalidNumericFormat($value);
        }
    }

    /**
     * @see \Isdoc\Tests\Traits\NumberValidation\ValidateNumericTest
     */
    protected function validateNumericNullable(string|null $value): void
    {
        if(!is_null($value)) {
            $this->validateNumeric($value);
        }
    }
}
