<?php

namespace Isdoc\Traits;

trait BoolValidation
{
    /**
     * @see \Isdoc\Tests\Traits\BoolValidation\StrToBoolTest
     */
    protected function strToBool(string|null $value): bool|null
    {
        if(is_null($value) || $value == '') {
            return null;
        }
        if($value == 'false') {
            return false;
        } elseif($value == 'true') {
            return true;
        }
    }

    /**
     * @see \Isdoc\Tests\Traits\BoolValidation\BoolToStrTest
     */
    protected function boolToStr(bool|null $value): string|null
    {
        if(is_null($value)) {
            return null;
        }
        if($value) {
            return 'true';
        } else {
            return 'false';
        }
    }
}
