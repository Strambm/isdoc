<?php

namespace Isdoc\Traits;

use Isdoc\Exceptions\InvalidUuidFormat;

trait UUIDValidation
{
    /**
     * Validuje date string na požadovaný formát
     * @see \Isdoc\Tests\Traits\UUIDValidation\ValidateUuidTest
     */
    protected function validateUuid(string $uuid): void
    {
        if(!preg_match($this->getPatern(), $uuid)) {
            throw new InvalidUuidFormat();
        }
    }

    /**
     * @see \Isdoc\Tests\Traits\UUIDValidation\ValidateUuidTest
     */
    protected function validateUuidNullable(string|null $uuid): void
    {
        if(!is_null($uuid)) {
            $this->validateUuid($uuid);
        }
    }

    private function getPatern(): string
    {
        return '/^[0-9A-Fa-f]{8}-[0-9A-Fa-f]{4}-[0-9A-Fa-f]{4}-[0-9A-Fa-f]{4}-[0-9A-Fa-f]{12}$/';
    }
}
