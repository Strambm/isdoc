<?php

namespace Isdoc\Traits;

use Isdoc\Models\ExchangeRate;
use Isdoc\Exceptions\ManuallySettedForeignCurrencyValueWithoutExchangeRate;

trait HasForeignCurrencyAttribute
{
    /**
     * @var null|ExchangeRate $exchangeRate
     */
    protected $exchangeRate = null;
    protected $throwManuallySettedForeignCurrencyValueWithoutExchangeRateEnable = true;

    /**
     * @see \Isdoc\Tests\Traits\HasForeignCurrencyAttribute\GetSetExchangeRateTest
     */
    public function setExchangeRate(ExchangeRate|null $rate): static
    {
        $this->exchangeRate = $rate;
        return $this;
    }

    /**
     * @see \Isdoc\Tests\Traits\HasForeignCurrencyAttribute\GetSetExchangeRateTest
     */
    public function getExchangeRate(): ExchangeRate|null
    {
        return $this->exchangeRate;
    }

    /**
     * @see \Isdoc\Tests\Traits\HasForeignCurrencyAttribute\HasForeignCurrencyInDocumentTest
     */
    public function hasForeignCurrencyInDocument(): bool
    {
        if(is_null($this->getExchangeRate())) {
            return false;
        }
        return $this->exchangeRate->hasForeignCurrency();
    }

    /**
     * @see \Isdoc\Tests\Traits\HasForeignCurrencyAttribute\GetForeignAmountForMethodTest
     */
    protected function getForeignAmountForMethod(string $getMethodName): float|null
    {
        $methodName = substr($getMethodName, 0, -4);
        $attributeName = mb_strtolower($getMethodName[3]) . substr($getMethodName, 4);
        if(!$this->throwManuallySettedForeignCurrencyValueWithoutExchangeRateEnable && !is_null($this->$attributeName)) {
            return $this->$attributeName;
        }
        elseif($this->hasForeignCurrencyInDocument()) {
            if(!is_null($this->$attributeName)) {
                return $this->$attributeName;
            }
            return $this->getExchangeRate()->getExchangeAmount($this->$methodName());
        } else {
            if(!is_null($this->$attributeName)) {
                throw new ManuallySettedForeignCurrencyValueWithoutExchangeRate();
            }
            return null;
        }
    }

    /**
     * @see \Isdoc\Tests\Traits\HasForeignCurrencyAttribute\SetThrowManuallySettedForeignCurrencyValueWithoutExchangeRateTest
     */
    public function setThrowManuallySettedForeignCurrencyValueWithoutExchangeRate(bool $value): static
    {
        $this->throwManuallySettedForeignCurrencyValueWithoutExchangeRateEnable = $value;
        return $this;
    }
}
