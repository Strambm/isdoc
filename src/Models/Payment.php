<?php

namespace Isdoc\Models;

use Isdoc\Enums\PaymentMeansCode;
use Isdoc\Traits\StringConversion;
use Isdoc\Exceptions\MissingPaidAmount;
use Isdoc\Exceptions\MissingPaymentMeansCode;
use Isdoc\Exceptions\UnvalidPaymentDetailsForPaymentMeansCode;
use Isdoc\Interfaces\RenderableInterface;
use Isdoc\Exceptions\UnvalidPaymentMeansCode;
use Isdoc\Traits\BoolValidation;

/**
 * Platba
 */
class Payment implements RenderableInterface
{
    use StringConversion;
    use BoolValidation;

    protected $partialPayment = false;
    protected $paidAmount = null;
    protected $paymentMeansCode = null;
    protected $details = null;

    /** 
     * Částka k zaplacení 
     * @see \Isdoc\Tests\Models\Payment\GetSetPaidAmountTest
     */
    public function setPaidAmount(float $paidAmount): static
    {
        $this->paidAmount = $paidAmount;
        return $this;
    }

    /** 
     * Částka k zaplacení 
     * @see \Isdoc\Tests\Models\Payment\GetSetPaidAmountTest
     * @throws MissingPaidAmount
     */
    public function getPaidAmount(): float
    {
        if(is_null($this->paidAmount)) {
            throw new MissingPaidAmount();
        }
        return $this->paidAmount;
    }

    /** 
     * Kód způsobu platby 
     * @see \Isdoc\Tests\Models\Payment\GetSetPaymentMeansCodeTest
     * @throws UnvalidPaymentMeansCode
     */
    public function setPaymentMeansCode(int $paymentMeansCode): static
    {
        $this->paymentMeansCode = PaymentMeansCode::validate($paymentMeansCode);
        return $this;
    }

    /** 
     * Kód způsobu platby 
     * @see \Isdoc\Tests\Models\Payment\GetSetPaymentMeansCodeTest
     * @throws MissingPaymentMeansCode
     */
    public function getPaymentMeansCode(): int
    {
        if(is_null($this->paymentMeansCode)) {
            throw new MissingPaymentMeansCode();
        }
        return $this->paymentMeansCode;
    }

    /** 
     * Podrobnosti o platbě
     * @see \Isdoc\Tests\Models\Payment\GetSetDetailsTest
     */
    public function setDetails(PaymentDetails|null $details): static
    {
        $this->details = $details;
        return $this;
    }

    /** 
     * Podrobnosti o platbě
     * @see \Isdoc\Tests\Models\Payment\GetSetDetailsTest
     * @throws UnvalidPaymentDetailsForPaymentMeansCode
     */
    public function getDetails(): PaymentDetails|null
    {
        $this->validateDetailForCode();
        return $this->details;
    }

    private function validateDetailForCode(): void
    {
        if(is_null($this->details)) {
            return;
        }
        if(
            $this->details instanceof PaymentDetailCash
                && !in_array($this->getPaymentMeansCode(), [
                    PaymentMeansCode::CASH
                ])
        ) {
            throw new UnvalidPaymentDetailsForPaymentMeansCode();
        } elseif(
            $this->details instanceof PaymentDetailBankTransaction
            && !in_array($this->getPaymentMeansCode(), [
                PaymentMeansCode::BANK_TRANSFER,
            ])
        ) {
            throw new UnvalidPaymentDetailsForPaymentMeansCode();
        }
    }

    /**
     * Je povolena částečná platba
     * @see \Isdoc\Tests\Models\Payment\GetSetHasPartialPaymentTest
     */
    public function getHasPartialPayment(): bool|null
    {
        return $this->partialPayment;
    }

    /**
     * Je povolena částečná platba
     * @see \Isdoc\Tests\Models\Payment\GetSetHasPartialPaymentTest
     */
    public function setHasPartialPayment(bool|null $value): static
    {
        $this->partialPayment = $value;
        return $this;
    }

    /**
     * @see \Isdoc\Tests\Models\Payment\ToXmlElementTest
     */
    public function toXmlElement(): IsdocSimpleXMLElement
    {
        $payment = new IsdocSimpleXMLElement('<Payment></Payment>');
        if(!is_null($this->getHasPartialPayment())) {
            $payment->addAttribute('partialPayment', $this->boolToStr($this->getHasPartialPayment()));
        }
        $payment->addChild('PaidAmount', $this->getPaidAmount());
        $payment->addChild('PaymentMeansCode', $this->getPaymentMeansCode());
        $payment->appendSimpleXMLElementOptional($this->getDetails());
        return $payment;
    }
}
