<?php

namespace Isdoc\Models;

use Isdoc\Models\TaxTotal;
use Isdoc\Enums\CurrencyCode;
use Isdoc\Enums\DocumentType;
use Isdoc\Traits\BoolValidation;
use Isdoc\Traits\DateValidation;
use Isdoc\Traits\UUIDValidation;
use Isdoc\Traits\StringConversion;
use Isdoc\Models\LegalMonetaryTotal;
use Isdoc\Exceptions\MissingIssueDate;
use Isdoc\Exceptions\InvalidUuidFormat;
use Isdoc\Models\IsdocSimpleXMLElement;
use Isdoc\Exceptions\UnvalidDocumentType;
use Isdoc\Interfaces\RenderableInterface;
use Isdoc\Exceptions\MissingVATApplicable;
use Isdoc\Traits\HasForeignCurrencyAttribute;
use Isdoc\Exceptions\MissingAccountingSupplierParty;

/**
 * Hlavní element dokumentu, jeho subtyp viz položka DocumentType
 */
class Invoice
implements
    RenderableInterface
{
    use StringConversion;
    use DateValidation;
    use UUIDValidation;
    use HasForeignCurrencyAttribute;
    use BoolValidation;

    protected $documentType = DocumentType::INVOICE;
    protected $id = null;
    protected $uuid = null;
    protected $issueDate = null;
    protected $taxPointDate = null;
    protected $vatApplicable = null;
    protected $electronicPossibilityAgreementReference = null;
    protected $note = null;
    protected $originalDocumentReferences = [];
    protected $accountingSupplierParty = null;
    protected $accountingCustomerParty = null;
    protected $paymentMeans = null;
    protected $taxTotal = null;
    protected $legalMonetaryTotal = null;
    protected $invoiceLines = [];
    protected $issuingSystem = null;
    protected $deliveryNoteReferences = [];
    protected $orderReferences = [];
    protected $deliveryAddress = null;
    protected $taxedDeposits = [];
    protected $nonTaxedDeposits = [];
    protected $_cached_TaxTotal = null;

    public function __construct()
    {
        $this->setExchangeRate(new ExchangeRate(CurrencyCode::CZK));
    }

    /**
     * Typ dokumentu
     * @see \Isdoc\Tests\Models\Invoice\GetSetDocumentTypeTest
     * @throws UnvalidDocumentType
     */
    public function setDocumentType(int $documentType): static
    {
        $this->documentType = DocumentType::validate($documentType);
        return $this;
    }

    /**
     * Typ dokumentu
     * @see \Isdoc\Tests\Models\Invoice\GetSetDocumentTypeTest
     */
    public function getDocumentType(): int
    {
        return $this->documentType;
    }

    /**
     * Lidsky čitelné číslo dokladu
     * @see \Isdoc\Tests\Models\Invoice\GetSetIdTest
     */
    public function setId(string $id): static
    {
        $this->id = $id;
        return $this;
    }

    /**
     * Lidsky čitelné číslo dokladu
     * @see \Isdoc\Tests\Models\Invoice\GetSetIdTest
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * GUID identifikace od emitujícího systému
     * @see \Isdoc\Tests\Models\Invoice\GenerateUuidTest
     * @throws InvalidUuidFormat
     */
    public function setUuid(string|null $uuid): static
    {
        $this->validateUuidNullable($uuid);
        $this->uuid = $uuid;
        return $this;
    }

    /**
     * GUID identifikace od emitujícího systému
     * @see \Isdoc\Tests\Models\Invoice\GenerateUuidTest
     */
    public function getUuid(): string
    {
        if(is_null($this->uuid)) {
            $uuid = $this->generateUuid();
            $this->setUuid($uuid);
        }
        return $this->uuid;
    }

    /**
     * @see \Isdoc\Tests\Models\Invoice\GenerateUuidTest
     */
    protected function generateUuid(): string
    {
        if (function_exists('com_create_guid') === true) {
            return trim(com_create_guid(), '{}');
        }

        return sprintf('%04X%04X-%04X-%04X-%04X-%04X%04X%04X', mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(16384, 20479), mt_rand(32768, 49151), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535));
    }

    /**
     * Identifikace systému, který odesílá/generuje fakturu
     * @see \Isdoc\Tests\Models\Invoice\GetSetIssuingSystemTest
     */
    public function setIssuingSystem(string|null $issuingSystem): static
    {
        $this->issuingSystem = $issuingSystem;
        return $this;
    }

    /**
     * Identifikace systému, který odesílá/generuje fakturu
     * @see \Isdoc\Tests\Models\Invoice\GetSetIssuingSystemTest
     */
    public function getIssuingSystem(): string
    {
        if(is_null($this->issuingSystem)) {
            return $this->getDefaultIssuringSystem();
        }
        return $this->issuingSystem;
    }

    /**
     * Defaultní název systému
     * @see \Isdoc\Tests\Models\Invoice\GetSetIssuingSystemTest
     */
    protected function getDefaultIssuringSystem(): string
    {
        return 'Cetria/isdoc';
    }

    /**
     * Datum vystavení
     * @see \Isdoc\Tests\Models\Invoice\GetSetIssueDateTest
     * @throws InvalidDateFormat
     */
    public function setIssueDate(string $issueDate): static
    {
        $this->validateDate($issueDate);
        $this->issueDate = $issueDate;
        return $this;
    }

    /**
     * Datum vystavení
     * @see \Isdoc\Tests\Models\Invoice\GetSetIssueDateTest
     * @throws MissingIssueDate
     */
    public function getIssueDate(): string
    {
        if(is_null($this->issueDate)) {
            throw new MissingIssueDate();
        }
        return $this->issueDate;
    }

    /**
     * Datum zdanitelného plnění
     * @see \Isdoc\Tests\Models\Invoice\GetSetTaxPointDateTest
     * @throws InvalidDateFormat
     */
    public function setTaxPointDate(string|null $taxPointDate): static
    {
        $this->validateDateNullable($taxPointDate);
        $this->taxPointDate = $taxPointDate;
        return $this;
    }

    /**
     * Datum zdanitelného plnění
     * @see \Isdoc\Tests\Models\Invoice\GetSetTaxPointDateTest
     */
    public function getTaxPointDate(): null|string
    {
        return $this->taxPointDate;
    }

    /**
     * Je předmětem DPH
     * @see \Isdoc\Tests\Models\Invoice\GetSetVATAplicableTest
     */
    public function setVATApplicable(bool $vatApplicable): static
    {
        $this->vatApplicable = $vatApplicable;
        return $this;
    }

    /**
     * Je předmětem DPH
     * @see \Isdoc\Tests\Models\Invoice\GetSetVATAplicableTest
     * @throws MissingVATApplicable
     */
    public function getVATApplicable(): bool
    {
        if(is_null($this->vatApplicable)) {
            throw new MissingVATApplicable();
        }
        return $this->vatApplicable;
    }

    /**
     * Označení dokumentu, kterým dal příjemce vystaviteli souhlas s elektronickou formou faktury
     * @see \Isdoc\Tests\Models\Invoice\GetSetElectronicPossibilityAgreementReferenceTest
     */
    public function setElectronicPossibilityAgreementReference(ElectronicPossibilityAgreementReference|null $electronicPossibilityAgreementReference): static
    {
        $this->electronicPossibilityAgreementReference = $electronicPossibilityAgreementReference;
        return $this;
    }

    /**
     * Označení dokumentu, kterým dal příjemce vystaviteli souhlas s elektronickou formou faktury
     * @see \Isdoc\Tests\Models\Invoice\GetSetElectronicPossibilityAgreementReferenceTest
     */
    public function getElectronicPossibilityAgreementReference(): ElectronicPossibilityAgreementReference|null
    {
        if(is_null($this->electronicPossibilityAgreementReference)) {
            return (new ElectronicPossibilityAgreementReference())->setValue('');
        }
        return $this->electronicPossibilityAgreementReference;
    }

    /**
     * Poznámka
     * @see \Isdoc\Tests\Models\Invoice\GetSetNoteTest
     */
    public function setNote(Note|null $note): static
    {
        $this->note = $note;
        return $this;
    }

    /**
     * Poznámka
     * @see \Isdoc\Tests\Models\Invoice\GetSetNoteTest
     */
    public function getNote(): Note|null
    {
        return $this->note;
    }

    /**
     * Kód měny
     * @see \Isdoc\Tests\Models\Invoice\GetSetExcahngeRateTest
     */
    public function getLocalCurrencyCode(): string
    {
        return $this->exchangeRate->getLocalCurrencyCode();
    }

    /**
     * Kód cizí měny
     * @see \Isdoc\Tests\Models\Invoice\GetSetExcahngeRateTest
     */
    public function getForeignCurrencyCode(): string|null
    {
        return $this->exchangeRate->getForeignCurrencyCode();
    }

    /**
     * Kurz cizí měny, pokud je použita, jinak 1
     * @see \Isdoc\Tests\Models\Invoice\GetSetExcahngeRateTest
     */
    public function getCurrRate(): float
    {
        return $this->exchangeRate->getCurrRate();
    }

    /**
     * Vztažný kurz cizí měny, většinou 1
     * @see \Isdoc\Tests\Models\Invoice\GetSetExcahngeRateTest
     */
    public function getRefCurrRate(): float
    {
        return $this->exchangeRate->getRefCurrRate();
    }

    /**
     * Dodavatel, účetní jednotka
     * @see \Isdoc\Tests\Models\Invoice\GetSetAccountingSupplierPartyTest
     */
    public function setAccountingSupplierParty(PartyContact $party): static
    {
        $this->accountingSupplierParty = $party;
        return $this;
    }

    /**
     * Dodavatel, účetní jednotka
     * @see \Isdoc\Tests\Models\Invoice\GetSetAccountingSupplierPartyTest
     * @throws MissingAccountingSupplierParty
     */
    public function getAccountingSupplierParty(): PartyContact
    {
        if(is_null($this->accountingSupplierParty)) {
            throw new MissingAccountingSupplierParty();
        }
        return $this->accountingSupplierParty;
    }

    /**
     * Příjemce, účetní jednotka
     * @see \Isdoc\Tests\Models\Invoice\GetSetAccountingCustomerPartyTest
     */
    public function setAccountingCustomerParty(PartyContact|null $party): static
    {
        $this->accountingCustomerParty = $party;
        return $this;
    }

    /**
     * Příjemce, účetní jednotka
     * @see \Isdoc\Tests\Models\Invoice\GetSetAccountingCustomerPartyTest
     */
    public function getAccountingCustomerParty(): PartyContact|null
    {
        return $this->accountingCustomerParty;
    }

    /**
     * Odkaz na související objednávku
     * @see \Isdoc\Tests\Models\Invoice\GetAddOrderReferenceTest
     */
    public function addOrderReference(OrderReference $orderReference): static
    {
        $this->orderReferences[] = $orderReference;
        return $this;
    }

    /**
     * Odkaz na související objednávku
     * @return OrderReference[]|null
     * @see \Isdoc\Tests\Models\Invoice\GetAddOrderReferenceTest
     */
    public function getOrderReferences(): array|null
    {
        if(count($this->orderReferences) == 0) {
            return null;
        }
        return $this->orderReferences;
    }

    /**
     * Odkaz na související dodací list
     * @see \Isdoc\Tests\Models\Invoice\GetAddDeliveryNoteReferenceTest
     */
    public function addDeliveryNoteReference(DeliveryNoteReference $reference): static
    {
        $this->deliveryNoteReferences[] = $reference;
        return $this;
    }

    /**
     * Odkaz na související dodací list
     * @return DeliveryNoteReference[]|null
     * @see \Isdoc\Tests\Models\Invoice\GetAddDeliveryNoteReferenceTest
     */
    public function getDeliveryNoteReferences(): array|null
    {
        if(count($this->deliveryNoteReferences) == 0) {
            return null;
        }
        return $this->deliveryNoteReferences;
    }

    /**
     * Odkaz na původní doklad, který tento aktuální doklad opravuje (jen pro typy dokumentů 2, 3 a 6)
     * Jen u dokumentů:
     *  - Opravný daňový doklad (dobropis)
     *  - Opravný daňový doklad (vrubopis)
     *  - Opravný daňový doklad při přijetí platby (dobropis DZL)
     * @see \Isdoc\Tests\Models\Invoice\GetAddOriginalDocumentReferenceTest
     */
    public function addOriginalDocumentReference(OriginalDocumentReference $originalDocumentReference): static
    {
        $this->originalDocumentReferences[] = $originalDocumentReference;
        return $this;
    }

    /**
     * Odkaz na původní doklad, který tento aktuální doklad opravuje (jen pro typy dokumentů 2, 3 a 6)
     * Jen u dokumentů:
     *  - Opravný daňový doklad (dobropis)
     *  - Opravný daňový doklad (vrubopis)
     *  - Opravný daňový doklad při přijetí platby (dobropis DZL)
     * @return OriginalDocumentReference[]|null
     * @see \Isdoc\Tests\Models\Invoice\GetAddOriginalDocumentReferenceTest
     */
    public function getOriginalDocumentReferences(): array|null
    {
        if(count($this->originalDocumentReferences) == 0) {
            return null;
        }
        return $this->originalDocumentReferences;
    }

    /**
     * Místo určení, adresa dodání
     * @see \Isdoc\Tests\Models\Invoice\GetSetDeliveryAddressTest
     */
    public function setDeliveryAddress(PartyContact|null $partyContact): static
    {
        $this->deliveryAddress = $partyContact;
        return $this;
    }

    /**
     * Místo určení, adresa dodání
     * @see \Isdoc\Tests\Models\Invoice\GetSetDeliveryAddressTest
     */
    public function getDeliveryAddress(): PartyContact|null
    {
        return $this->deliveryAddress;
    }

    /**
     * Řádek faktury
     * @see \Isdoc\Tests\Models\Invoice\GetAddInvoiceLineTest
     */
    public function addInvoiceLine(InvoiceLine $invoiceLine): static
    {
        $this->_cached_TaxTotal = null;
        $invoiceLine->setExchangeRate($this->getExchangeRate());
        $this->invoiceLines[] = $invoiceLine;
        return $this;
    }

    /**
     * Řádek faktury
     * @return InvoiceLine[]
     * @see \Isdoc\Tests\Models\Invoice\GetAddInvoiceLineTest
     */
    public function getInvoiceLines(): array
    {
        return $this->invoiceLines;
    }

    /**
     * Informace o konkrétní částce v sazbě na odúčtovaném daňovém zálohovém listu
     * @see \Isdoc\Tests\Models\Invoice\GetAddTaxedDepositTest
     */
    public function addTaxedDeposit(TaxedDeposit $taxedDeposit): static
    {
        $this->_cached_TaxTotal = null;
        $this->taxedDeposits[] = $taxedDeposit;
        return $this;
    }

    /**
     * Informace o konkrétním zaplaceném nedaňovém zálohovém listu
     * @see \Isdoc\Tests\Models\Invoice\GetAddNonTaxedDepositTest
     */
    public function addNonTaxedDeposit(NonTaxedDeposit $nonTaxedDeposit): static
    {
        $this->nonTaxedDeposits[] = $nonTaxedDeposit;
        return $this;
    }

    /**
     * Informace o konkrétním zaplaceném nedaňovém zálohovém listu
     * @return NonTaxedDeposit[]|null
     * @see \Isdoc\Tests\Models\Invoice\GetAddNonTaxedDepositTest
     */
    public function getNonTaxedDeposits(): array|null
    {
        if(count($this->nonTaxedDeposits) == 0) {
            return null;
        }
        return $this->nonTaxedDeposits;
    }

    /**
     * Informace o konkrétní částce v sazbě na odúčtovaném daňovém zálohovém listu
     * @return TaxedDeposit[]|null
     * @see \Isdoc\Tests\Models\Invoice\GetAddTaxedDepositTest
     */
    public function getTaxedDeposits(): array|null
    {
        if(count($this->taxedDeposits) == 0) {
            return null;
        }
        return $this->taxedDeposits;
    }

    /**
     * Daňová rekapitulace
     * @see \Isdoc\Tests\Models\Invoice\GetSetTaxTotalTest
     */
    public function setTaxTotal(TaxTotal|null $taxTotal): static
    {
        $this->taxTotal = $taxTotal;
        return $this;
    }

    /**
     * Daňová rekapitulace
     * @see \Isdoc\Tests\Models\Invoice\GetSetTaxTotalTest
     */
    public function getTaxTotal(): TaxTotal
    {
        if(!is_null($this->_cached_TaxTotal)) {
            return $this->_cached_TaxTotal;
        }

        if(is_null($this->taxTotal)) {
            $taxTotal = new TaxTotal();
            foreach($this->getInvoiceLines() as $invoiceLine) {
                $taxTotal->addInvoiceLine($invoiceLine);
            }
            if(!is_null($this->getTaxedDeposits())) {
                foreach($this->getTaxedDeposits() as $taxedDeposit) {
                    $taxTotal->addTaxedDeposit($taxedDeposit);
                }
            }
            $this->_cached_TaxTotal = $taxTotal;
            return $this->_cached_TaxTotal;
        }

        return $this->taxTotal;
    }

    /**
     * Kolekce celkových částek na dokladu končící položkou částka k zaplacení
     * @see \Isdoc\Tests\Models\Invoice\GetSetLegalMonetaryTotalTest
     */
    public function setLegalMonetaryTotal(LegalMonetaryTotal $legalMonetaryTotal): static
    {
        $this->legalMonetaryTotal = $legalMonetaryTotal;
        return $this;
    }

    /**
     * Kolekce celkových částek na dokladu končící položkou částka k zaplacení
     * @see \Isdoc\Tests\Models\Invoice\GetSetLegalMonetaryTotalTest
     */
    public function getLegalMonetaryTotal(): LegalMonetaryTotal
    {
        if(is_null($this->legalMonetaryTotal)) {
            /** @var TaxSubTotal[] $taxSubTotals */
            $taxSubTotals = $this->getTaxTotal()->getTaxSubTotals();
            $model = new LegalMonetaryTotal();
            foreach($taxSubTotals as $taxSubTotal) {
                $model->setTaxExclusiveAmount($model->getTaxExclusiveAmount() + $taxSubTotal->getTaxableAmount());
                $model->setTaxInclusiveAmount($model->getTaxInclusiveAmount() + $taxSubTotal->getTaxInclusiveAmount());
                $model->setAlreadyClaimedTaxExclusiveAmount($model->getAlreadyClaimedTaxExclusiveAmount() + $taxSubTotal->getAlreadyClaimedTaxableAmount());
                $model->setAlreadyClaimedTaxInclusiveAmount($model->getAlreadyClaimedTaxInclusiveAmount() + $taxSubTotal->getAlreadyClaimedTaxInclusiveAmount());
                $model->setExchangeRate($this->getExchangeRate());
            }
            if(!is_null($this->getNonTaxedDeposits())) {
                foreach($this->getNonTaxedDeposits() as $nonTaxedDeposit) {
                    $model->setPaidDepositsAmount($model->getPaidDepositsAmount() + $nonTaxedDeposit->getDepositAmount());
                }
            }
            return $model;
        }

        return $this->legalMonetaryTotal;
    }

    /**
     * Kolekce plateb
     * @see \Isdoc\Tests\Models\Invoice\GetSetPaymentMeansTest
     */
    public function setPaymentMeans(PaymentMeans|null $payment): static
    {
        $this->paymentMeans = $payment;
        return $this;
    }

    /**
     * Kolekce plateb
     * @see \Isdoc\Tests\Models\Invoice\GetSetPaymentMeansTest
     */
    public function getPaymentMeans(): PaymentMeans|null
    {
        return $this->paymentMeans;
    }

    /**
     * @see \Isdoc\Tests\Models\Invoice\ToXmlElementTest
     */
    public function toXmlElement(): IsdocSimpleXMLElement
    {
        $invoice = new IsdocSimpleXMLElement('<?xml version="1.0" encoding="UTF-8"?><Invoice></Invoice>');
        $invoice->addAttribute('version', '6.0.2');
        $invoice->addAttribute('xmlns', 'http://isdoc.cz/namespace/2013');
        $invoice->addChild('DocumentType', $this->getDocumentType());
        $invoice->addChild('ID', $this->getId());
        $invoice->addChild('UUID', $this->getUuid());
        $invoice->addChildOptional('IssuingSystem', $this->getIssuingSystem());
        $invoice->addChild('IssueDate', $this->getIssueDate());
        $invoice->addChildOptional('TaxPointDate', $this->getTaxPointDate());
        $invoice->addChild('VATApplicable', $this->boolToStr($this->getVATApplicable()));
        $invoice->appendSimpleXMLElementOptional($this->getElectronicPossibilityAgreementReference());
        $invoice->appendSimpleXMLElementOptional($this->getNote());
        $invoice->addChild('LocalCurrencyCode', $this->getLocalCurrencyCode());
        $invoice->addChildOptional('ForeignCurrencyCode', $this->getForeignCurrencyCode());
        $invoice->addChild('CurrRate', $this->getCurrRate());
        $invoice->addChild('RefCurrRate', $this->getRefCurrRate());

        $accountingSupplierPartyChild = $invoice->addChild('AccountingSupplierParty');
        $accountingSupplierPartyChild->appendSimpleXMLElement($this->getAccountingSupplierParty());

        $accountingCustomerPartyChild = $invoice->addChild('AccountingCustomerParty');
        $accountingCustomerPartyChild->appendSimpleXMLElement($this->getAccountingCustomerParty());

        $invoice->appendXmlArray('OrderReferences', $this->getOrderReferences());
        $invoice->appendXmlArray('DeliveryNoteReferences', $this->getDeliveryNoteReferences());
        $invoice->appendXmlArray('OriginalDocumentReferences', $this->getOriginalDocumentReferences());

        if(!is_null($this->getDeliveryAddress())) {
            $deliveryAddress = $invoice->addChild('Delivery');
            $deliveryAddress->appendSimpleXMLElement($this->getDeliveryAddress());
        }

        $invoice->appendXmlArray('InvoiceLines', $this->getInvoiceLines());
        $invoice->appendXmlArray('NonTaxedDeposits', $this->getNonTaxedDeposits());
        $invoice->appendXmlArray('TaxedDeposits', $this->getTaxedDeposits());
        $invoice->appendSimpleXMLElement($this->getTaxTotal());
        $invoice->appendSimpleXMLElement($this->getLegalMonetaryTotal());
        $invoice->appendSimpleXMLElementOptional($this->getPaymentMeans());

        return $invoice;
    }

    public function toIsdocFile(string $fullFilePath): void
    {
        $dom = $this->formatXml();
        $filePath = $this->validateFilePath($fullFilePath);
        $dom->save($filePath, LIBXML_NOEMPTYTAG); //LIBXML_NOEMPTYTAG - dela oba tagy "<foo></foo>" pro prazdny element (misto <foo/>)
    }

    private function validateFilePath(string $fullFilePath): string
    {
        if (!str_ends_with(($fullFilePath), '.isdoc')) return "$fullFilePath.isdoc";
        return $fullFilePath;
    }
}
