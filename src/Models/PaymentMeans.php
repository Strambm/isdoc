<?php

namespace Isdoc\Models;

use Isdoc\Exceptions\MissingPayment;
use Isdoc\Models\BankAccount;
use Isdoc\Traits\StringConversion;
use Isdoc\Interfaces\RenderableInterface;

/**
 * Platba
 */
class PaymentMeans implements RenderableInterface
{
    use StringConversion;

    protected $payments = [];
    protected $alternativeBankAccounts = [];

    /**
     * Platba (Musí být alespoň jedna)
     * @see \Isdoc\Tests\Models\PaymentMeans\GetAddPaymentsTest
     */
    public function addPayment(Payment $payment): static
    {
        $this->payments[] = $payment;
        return $this;
    }

    /**
     * Platba
     * @see \Isdoc\Tests\Models\PaymentMeans\GetAddPaymentsTest
     * @throws MissingPayment
     */
    public function getPayments(): array
    {
        if(count($this->payments) == 0) {
            throw new MissingPayment();
        }
        return $this->payments;
    }

    /**
     * Kolekce dalších bankovních účtů, na které je možno také platit
     * @see \Isdoc\Tests\Models\PaymentMeans\GetAddAlternativeBankAccountsTest
     */
    public function addAlternativeBankAccount(BankAccount $bankAccount): static
    {
        $this->alternativeBankAccounts[] = $bankAccount;
        return $this;
    }

    /**
     * Kolekce dalších bankovních účtů, na které je možno také platit
     * @return BankAccount[]
     * @see \Isdoc\Tests\Models\PaymentMeans\GetAddAlternativeBankAccountsTest
     */
    public function getAlternativeBankAccounts(): array
    {
        return $this->alternativeBankAccounts;
    }

    /**
     * @see \Isdoc\Tests\Models\PaymentMeans\ToXmlElementTest
     */
    public function toXmlElement(): IsdocSimpleXMLElement
    {
        $paymentMeans = new IsdocSimpleXMLElement('<PaymentMeans></PaymentMeans>');
        foreach($this->getPayments() as $payment) {
            $paymentMeans->appendSimpleXMLElementOptional($payment);
        }
        if(count($this->getAlternativeBankAccounts()) > 0) {
            $child = $paymentMeans->addChild('AlternateBankAccounts');
            foreach($this->getAlternativeBankAccounts() as $bankAccount) {
                $child->appendSimpleXMLElement($bankAccount->toXmlElement());
            }
        }
        return $paymentMeans;
    }
}
