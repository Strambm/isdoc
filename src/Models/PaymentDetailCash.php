<?php

namespace Isdoc\Models;

use Isdoc\Traits\DateValidation;
use Isdoc\Exceptions\MissingIssueDate;

/**
 * Detaily o platbě v Hotovosti
 */
class PaymentDetailCash extends PaymentDetails
{
    use DateValidation;

    protected $documentId = '';
    protected $issueDate = null;

    /** 
     * Identifikátor svázaného dokladu, například pokladní účtenky 
     * @see \Isdoc\Tests\Models\PaymentDetailCash\GetSetDocumentIdTest
     */
    public function setDocumentId(string $documentId): static
    {
        $this->documentId = $documentId;
        return $this;
    }

    /** 
     * Identifikátor svázaného dokladu, například pokladní účtenky 
     * @see \Isdoc\Tests\Models\PaymentDetailCash\GetSetDocumentIdTest
     */
    public function getDocumentId(): string
    {
        return $this->documentId;
    }

    /** 
     * Datum vystavení
     * @param string $issueDate (formát YYYY-MM-DD)
     * @see \Isdoc\Tests\Models\PaymentDetailCash\GetSetIssueDateTest
     * @throws InvalidDateFormat
     */
    public function setIssueDate(string $issueDate): static
    {
        $this->validateDate($issueDate);
        $this->issueDate = $issueDate;
        return $this;
    }

    /** 
     * Datum vystavení
     * @return string $issueDate (formát YYYY-MM-DD)
     * @see \Isdoc\Tests\Models\PaymentDetailCash\GetSetIssueDateTest
     * @throws MissingIssueDate
     */
    public function getIssueDate(): string
    {
        if(is_null($this->issueDate)) {
            throw new MissingIssueDate();
        }
        return $this->issueDate;
    }

    /**
     * @see \Isdoc\Tests\Models\PaymentDetailCash\ToXmlElementTest
     * @throws MissingIssueDate
     */
    public function toXmlElement(): IsdocSimpleXMLElement
    {
        $details = new IsdocSimpleXMLElement('<Details></Details>');
        $details->addChild('DocumentID', $this->getDocumentId());
        $details->addChild('IssueDate', $this->getIssueDate());
        return $details;
    }
}
