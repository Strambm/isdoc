<?php

namespace Isdoc\Models;

use Isdoc\Enums\TaxScheme;
use Isdoc\Traits\StringConversion;
use Isdoc\Exceptions\UnvalidTaxScheme;
use Isdoc\Interfaces\RenderableInterface;

/**
 * Daňové údaje. Element je možné použít vícekrát a určit více identifikátorů, např. DIČ a IČ DPH na Slovensku.
 */
class PartyTaxScheme implements RenderableInterface
{
    use StringConversion;

    protected $companyId = null;
    protected $taxScheme = null;

    /**
     * @param companyId Daňové schéma. Nejpoužívanější schémata jsou: VAT (daň z přidané hodnoty, používá se v ČR pro DIČ a na Slovensku pro IČ DPH) a TIN (používá se na Slovensku pro DIČ).
     * @param taxScheme Hodnota pro TaxScheme dané společnosti (např. v ČR pro VAT: hodnota DPH)
     * @see \Isdoc\Tests\Models\PartyTaxScheme\ConstructorTest
     * @throws UnvalidTaxScheme
     */
    public function __construct(string $companyId, string $taxScheme)
    {
        $this->setCompanyId($companyId);
        $this->setTaxScheme($taxScheme);
    }

    private function setCompanyId(string $companyId): static
    {
        $this->companyId = $companyId;
        return $this;
    }

    private function setTaxScheme(string $taxScheme): static
    {
        $this->taxScheme = TaxScheme::validate($taxScheme);
        return $this;
    }

    /**
     * Hodnota pro TaxScheme dané společnosti (např. v ČR pro VAT: hodnota DPH)
     * @see \Isdoc\Tests\Models\PartyTaxScheme\GetCompanyIdTest
     */
    public function getCompanyId(): string
    {
        return $this->companyId;
    }

    /** 
     * Daňové schéma. Nejpoužívanější schémata jsou: VAT (daň z přidané hodnoty, používá se v ČR pro DIČ a na Slovensku pro IČ DPH) a TIN (používá se na Slovensku pro DIČ).
     * @see \Isdoc\Tests\Models\PartyTaxScheme\GetTaxSchemeTest
     */
    public function getTaxScheme(): string
    {
        return $this->taxScheme;
    }

    /**
     * @see \Isdoc\Tests\Models\PartyTaxScheme\ToXmlElementTest
     */
    public function toXmlElement(): IsdocSimpleXMLElement
    {
        $contact = new IsdocSimpleXMLElement('<PartyTaxScheme></PartyTaxScheme>');
        $contact->addChild('CompanyID',$this->getCompanyId()); 
        $contact->addChild('TaxScheme',$this->getTaxScheme()); 
        return $contact;
    }
}
