<?php

namespace Isdoc\Models;

use Isdoc\Traits\StringConversion;
use Isdoc\Interfaces\RenderableInterface;

/**
 * Informace o položce faktury
 */
class Item implements RenderableInterface
 { 
    use StringConversion; 

    protected $description = null;
    protected $sellersItemIdentification = null;
    protected $catalogueItemIdentification = null;
    protected $secondarySellersItemIdentification = null;
    protected $tertiarySellersItemIdentification = null;
    protected $buyersItemIdentification = null;

    /** 
     * Popis položky
     * @see \Isdoc\Tests\Models\Item\GetSetDescriptionTest
     */
    public function setDescription(string|null $description): static
    {
        $this->description = $description;
        return $this;
    }

    /** 
     * Popis položky
     * @see \Isdoc\Tests\Models\Item\GetSetDescriptionTest
     */
    public function getDescription(): string|null
    {
        return $this->description;
    }

    /**
     * Kód EAN
     * @see \Isdoc\Tests\Models\Item\GetSetCatalogueItemIdentificationTest
     */
    public function setCatalogueItemIdentification(string|null $catalogueIdentification): static
    {
        $this->catalogueItemIdentification = $catalogueIdentification;
        return $this;
    }

    /**
     * Kód EAN
     * @see \Isdoc\Tests\Models\Item\GetSetCatalogueItemIdentificationTest
     */
    public function getCatalogueItemIdentification(): string|null
    {
        return $this->catalogueItemIdentification;
    }

    /**
     * Identifikace zboží dle prodejce
     * @see \Isdoc\Tests\Models\Item\GetSetSellersItemIdentificationTest
     */
    public function setSellersItemIdentification(string|null $sellerIdentification): static
    {
        $this->sellersItemIdentification = $sellerIdentification;
        return $this;
    }

    /**
     * Identifikace zboží dle prodejce
     * @see \Isdoc\Tests\Models\Item\GetSetSellersItemIdentificationTest
     */
    public function getSellersItemIdentification(): string|null
    {
        return $this->sellersItemIdentification;
    }

    /**
     * Sekundární identifikace zboží dle prodejce
     * @see \Isdoc\Tests\Models\Item\GetSetSellersItemIdentificationTest
     */
    public function setSecondarySellersItemIdentification(string|null $seccondaryItemIdentification): static
    {
        $this->secondarySellersItemIdentification = $seccondaryItemIdentification;
        return $this;
    }

    /**
     * Sekundární identifikace zboží dle prodejce
     * @see \Isdoc\Tests\Models\Item\GetSetSellersItemIdentificationTest
     */
    public function getSecondarySellersItemIdentification(): null|string
    {
        return $this->secondarySellersItemIdentification;
    }

    /**
     * Terciální identifikace zboží dle prodejce
     * @see \Isdoc\Tests\Models\Item\GetSetTertiarySellersItemIdentificationTest
     */
    public function setTertiarySellersItemIdentification(string|null $tertiarySellersItemIdentification): static
    {
        $this->tertiarySellersItemIdentification = $tertiarySellersItemIdentification;
        return $this;
    }

    /**
     * Terciální identifikace zboží dle prodejce
     * @see \Isdoc\Tests\Models\Item\GetSetTertiarySellersItemIdentificationTest
     */
    public function getTertiarySellersItemIdentification(): string|null
    {
        return $this->tertiarySellersItemIdentification;
    }

    /**
     * Identifikace zboží dle kupujícího
     * @see \Isdoc\Tests\Models\Item\GetSetBuyersItemIdentificationTest
     */
    public function setBuyersItemIdentification(string|null $buyersIItemIdentification): static
    {
        $this->buyersItemIdentification = $buyersIItemIdentification;
        return $this;
    }

    /**
     * Identifikace zboží dle kupujícího
     * @see \Isdoc\Tests\Models\Item\GetSetBuyersItemIdentificationTest
     */
    public function getBuyersItemIdentification(): string|null
    {
        return $this->buyersItemIdentification;
    }

    /**
     * @see \Isdoc\Tests\Models\Item\ToXmlElementTest
     */
    public function toXmlElement(): IsdocSimpleXMLElement
    {
        $item = new IsdocSimpleXMLElement('<Item></Item>');
        $item->addChildOptional('Description', $this->getDescription());
        $item = $this->appendXmlElementId($item, 'CatalogueItemIdentification', 'getCatalogueItemIdentification');
        $item = $this->appendXmlElementId($item, 'SellersItemIdentification', 'getSellersItemIdentification'); 
        $item = $this->appendXmlElementId($item, 'SecondarySellersItemIdentification', 'getSecondarySellersItemIdentification');
        $item = $this->appendXmlElementId($item, 'TertiarySellersItemIdentification', 'getTertiarySellersItemIdentification');
        $item = $this->appendXmlElementId($item, 'BuyersItemIdentification', 'getBuyersItemIdentification');

        return $item;
    }

    private function appendXmlElementId(IsdocSimpleXMLElement $xml, string $name, string $getter): IsdocSimpleXMLElement
    {
        if(!is_null($this->$getter())) {
            $child = $xml->addChild($name);
            $child->appendSimpleXMLElement(new IsdocSimpleXMLElement('<ID>' . $this->$getter() . '</ID>'));
        }
        return $xml;
    }
}