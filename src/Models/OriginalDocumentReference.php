<?php

namespace Isdoc\Models;

/**
 * Odkaz na původní doklad, který tento aktuální doklad opravuje
 * jen pro typy dokumentů:
 *  2:  (CORRECTING_INVOICE == dobropis),
 *  3:  (CORRECTING_INVOICE_REVERSE == vrubopis),
 *  6:  (CORRECTION_PAYMENT == dobropis DZL)
 */
class OriginalDocumentReference extends DocumentReference
{
    protected function getTagName(): string
    {
        return 'OriginalDocumentReference';
    }
}
