<?php

namespace Isdoc\Models;

use Isdoc\Traits\StringConversion;
use Isdoc\Exceptions\MissingPercent;
use Isdoc\Exceptions\MissingTaxCategory;
use Isdoc\Interfaces\RenderableInterface;
use Isdoc\Traits\HasForeignCurrencyAttribute;
use Isdoc\Exceptions\MissingClassifiedTaxCategory;
use Isdoc\Exceptions\TaxCategoryPercentNotEqualsWithInvoiceLineTaxCategoryPercent;


/**
 * Sumář jedné daňové sazby
 */
class TaxSubTotal implements RenderableInterface
{
    use StringConversion;
    use HasForeignCurrencyAttribute;

    protected $taxCategory = null;
    protected $alreadyClaimedTaxableAmount = 0;
    protected $alreadyClaimedTaxableAmountCurr = null;
    protected $alreadyClaimedTaxAmount = 0;
    protected $alreadyClaimedTaxAmountCurr = null;
    protected $alreadyClaimedTaxInclusive = 0;
    protected $alreadyClaimedTaxInclusiveCurr = null;
    protected $differenceTaxableAmount = null;
    protected $differenceTaxableAmountCurr = null;
    protected $differenceTaxAmount = null;
    protected $differenceTaxAmountCurr = null;
    protected $differenceTaxInclusiveAmountCurr = null;
    protected $differenceTaxInclusiveAmount = null;
    protected $taxableAmount = 0;
    protected $taxableAmountCurr = null;
    protected $taxAmount = 0;
    protected $taxAmountCurr = null;
    protected $taxInclusiveAmount = 0;
    protected $taxInclusiveAmountCurr = null;

    /** 
     * Základ v sazbě v cizí měně 
     */
    public function setTaxableAmountCurr(float|null $value): static
    {
        $this->taxableAmountCurr = $value;
        return $this;
    }

    /** 
     * Základ v sazbě v cizí měně 
     */
    public function getTaxableAmountCurr(): float|null
    {
        return $this->taxableAmountCurr;
    }

    /** 
     * Základ v sazbě v tuzemské měně
     * @see \Isdoc\Tests\Models\TaxSubTotal\GetSetLineExtensionAmountTest
     */
    public function setTaxableAmount(float $value): static
    {
        $this->taxableAmount = $value;
        return $this;
    }

    /** 
     * Základ v sazbě v tuzemské měně
     * @see \Isdoc\Tests\Models\TaxSubTotal\GetSetLineExtensionAmountTest
     **/
    public function getTaxableAmount(): float
    {
        return $this->taxableAmount;
    }

    /** 
     * Daň v sazbě v cizí měně 
     * @see \Isdoc\Tests\Models\TaxSubTotal\GetSetTaxAmountCurrTest
     */
    public function getTaxAmountCurr(): float|null
    {
        return $this->taxAmountCurr;
    }

    /** 
     * Daň v sazbě v cizí měně 
     * @see \Isdoc\Tests\Models\TaxSubTotal\GetSetTaxAmountCurrTest
     */
    public function setTaxAmountCurr(float|null $value): static
    {
        $this->taxAmountCurr = $value;
        return $this;
    }

    /** 
     * Daň v sazbě v tuzemské měně 
     * @see \Isdoc\Tests\Models\TaxSubTotal\GetSetTaxAmountTest
     */
    public function getTaxAmount(): float
    {
        return $this->taxAmount;
    }

    /** 
     * Daň v sazbě v tuzemské měně 
     * @see \Isdoc\Tests\Models\TaxSubTotal\GetSetTaxAmountTest
     */
    public function setTaxAmount(float $value): static
    {
        $this->taxAmount = $value;
        return $this;
    }

    /** 
     * Částka s daní v sazbě v cizí měně 
     * @see \Isdoc\Tests\Models\TaxSubTotal\GetSetTaxInclusiveAmountCurrTest
     */
    public function getTaxInclusiveAmountCurr(): float|null
    {
        return $this->taxInclusiveAmountCurr;
    }

    /** 
     * Částka s daní v sazbě v cizí měně 
     * @see \Isdoc\Tests\Models\TaxSubTotal\GetSetTaxInclusiveAmountCurrTest
     */
    public function setTaxInclusiveAmountCurr(float|null $value): static
    {
        $this->taxInclusiveAmountCurr = $value;
        return $this;
    }

    /** 
     * Částka s daní v sazbě v tuzemské měně 
     * @see \Isdoc\Tests\Models\TaxSubTotal\GetSetTaxInclusiveAmountTest
     */
    public function getTaxInclusiveAmount(): float
    {
        return $this->taxInclusiveAmount;
    }

    /** 
     * Částka s daní v sazbě v tuzemské měně 
     * @see \Isdoc\Tests\Models\TaxSubTotal\GetSetTaxInclusiveAmountTest
     */
    public function setTaxInclusiveAmount(float $value): static
    {
        $this->taxInclusiveAmount = $value;
        return $this;
    }

    /**  
     * Na záloze již uplatněno, základ v sazbě v cizí měně 
     * @see \Isdoc\Tests\Models\TaxSubTotal\GetSetAlreadyClaimedTaxableAmountCurrTest
     */
    public function getAlreadyClaimedTaxableAmountCurr(): float|null
    {
        return $this->alreadyClaimedTaxableAmountCurr;
    }

    /**  
     * Na záloze již uplatněno, základ v sazbě v cizí měně 
     * @see \Isdoc\Tests\Models\TaxSubTotal\GetSetAlreadyClaimedTaxableAmountCurrTest
     */
    public function setAlreadyClaimedTaxableAmountCurr(float|null $value): static
    {
        $this->alreadyClaimedTaxableAmountCurr = $value;
        return $this;
    }

    /**  
     * Na záloze již uplatněno, základ v sazbě v tuzemské měně 
     * @see \Isdoc\Tests\Models\TaxSubTotal\GetSetAlreadyClaimedTaxableAmountTest
     */
    public function getAlreadyClaimedTaxableAmount(): float
    {
        return $this->alreadyClaimedTaxableAmount;
    }

    /**  
     * Na záloze již uplatněno, základ v sazbě v tuzemské měně 
     * @see \Isdoc\Tests\Models\TaxSubTotal\GetSetAlreadyClaimedTaxableAmountTest
     */
    public function setAlreadyClaimedTaxableAmount(float $value): static
    {
       $this->alreadyClaimedTaxableAmount = $value;
       return $this;
    }

    /**  
     * Na záloze již uplatněno, daň v sazbě v cizí měně 
     * @see \Isdoc\Tests\Models\TaxSubTotal\GetSetAlreadyClaimedTaxAmountCurrTest
     */
    public function getAlreadyClaimedTaxAmountCurr(): float|null
    {
        return $this->alreadyClaimedTaxAmountCurr;
    }

    /**  
     * Na záloze již uplatněno, daň v sazbě v cizí měně 
     * @see \Isdoc\Tests\Models\TaxSubTotal\GetSetAlreadyClaimedTaxAmountCurrTest
     */
    public function setAlreadyClaimedTaxAmountCurr(float|null $value): static
    {
        $this->alreadyClaimedTaxAmountCurr = $value;
        return $this;
    }

    /**  
     * Na záloze již uplatněno, daň v sazbě v tuzemské měně 
     * @see \Isdoc\Tests\Models\TaxSubTotal\GetSetAlreadyClaimedTaxAmountTest
     */
    public function getAlreadyClaimedTaxAmount(): float
    {
        return $this->alreadyClaimedTaxAmount;
    }

    /**  
     * Na záloze již uplatněno, daň v sazbě v tuzemské měně 
     * @see \Isdoc\Tests\Models\TaxSubTotal\GetSetAlreadyClaimedTaxAmountTest
     */
    public function setAlreadyClaimedTaxAmount(float $value): static
    {
        $this->alreadyClaimedTaxAmount = $value;
        return $this;
    }

    /**  
     * Na záloze již uplatněno, s daní v sazbě v cizí měně 
     * @see \Isdoc\Tests\Models\TaxSubTotal\GetSetAlreadyClaimedTaxInclusiveAmountCurrTest
     */
    public function getAlreadyClaimedTaxInclusiveAmountCurr(): float|null
    {
        return $this->alreadyClaimedTaxInclusiveCurr;
    }

    /**  
     * Na záloze již uplatněno, s daní v sazbě v cizí měně
     * @see \Isdoc\Tests\Models\TaxSubTotal\GetSetAlreadyClaimedTaxInclusiveAmountCurrTest 
     */
    public function setAlreadyClaimedTaxInclusiveAmountCurr(float|null $value): static
    {
        $this->alreadyClaimedTaxInclusiveCurr = $value;
        return $this;
    }

    /**  
     * Na záloze již uplatněno, s daní v sazbě v tuzemské měně 
     * @see \Isdoc\Tests\Models\TaxSubTotal\GetSetAlreadyClaimedTaxInclusiveAmountTest
     */
    public function getAlreadyClaimedTaxInclusiveAmount(): float
    {
        return $this->alreadyClaimedTaxInclusive;
    }

    /**  
     * Na záloze již uplatněno, s daní v sazbě v tuzemské měně 
     * @see \Isdoc\Tests\Models\TaxSubTotal\GetSetAlreadyClaimedTaxInclusiveAmountTest
     */
    public function setAlreadyClaimedTaxInclusiveAmount(float $value): static
    {
        $this->alreadyClaimedTaxInclusive = $value;
        return $this;
    }

    /**  
     * Rozdíl, základ v cizí měně 
     * @see \Isdoc\Tests\Models\TaxSubTotal\GetSetDifferenceTaxableAmountCurrTest
     */
    public function getDifferenceTaxableAmountCurr(): float|null
    {
        if(!is_null($this->differenceTaxableAmountCurr)) {
            return $this->differenceTaxableAmountCurr;
        }

        if(!is_null($this->getTaxableAmountCurr()) && !is_null($this->getAlreadyClaimedTaxableAmountCurr())) {
            return $this->getTaxableAmountCurr() - $this->getAlreadyClaimedTaxableAmountCurr();
        }
        return null;
    }

    /**  
     * Rozdíl, základ v cizí měně 
     * @see \Isdoc\Tests\Models\TaxSubTotal\GetSetDifferenceTaxableAmountCurrTest
     */
    public function setDifferenceTaxableAmountCurr(float|null $value): static
    {
        $this->differenceTaxableAmountCurr = $value;
        return $this;
    }

    /**  
     * Rozdíl, základ v tuzemské měně 
     * @see \Isdoc\Tests\Models\TaxSubTotal\GetSetDifferenceTaxableAmountTest
     */
    public function getDifferenceTaxableAmount(): float
    {
        if(!is_null($this->differenceTaxableAmount)) {
            return $this->differenceTaxableAmount;
        }
        return $this->getTaxableAmount() - $this->getAlreadyClaimedTaxableAmount();
    }

    /**  
     * Rozdíl, základ v tuzemské měně 
     * @see \Isdoc\Tests\Models\TaxSubTotal\GetSetDifferenceTaxableAmountTest
     */
    public function setDifferenceTaxableAmount(float|null $value): static
    {
        $this->differenceTaxableAmount = $value;
        return $this;
    }

    /**  
     * Rozdíl, daň v cizí měně 
     * @see \Isdoc\Tests\Models\TaxSubTotal\GetSetDifferenceTaxAmountCurrTest
     */
    public function getDifferenceTaxAmountCurr(): float|null
    {
        if(!is_null($this->differenceTaxAmountCurr)) {
            return $this->differenceTaxAmountCurr;
        }

        if(!is_null($this->getTaxAmountCurr()) && !is_null($this->getAlreadyClaimedTaxAmountCurr())) {
            return $this->getTaxAmountCurr() - $this->getAlreadyClaimedTaxAmountCurr();
        }
        return null;
    }

    /**  
     * Rozdíl, daň v cizí měně 
     * @see \Isdoc\Tests\Models\TaxSubTotal\GetSetDifferenceTaxAmountCurrTest
     */
    public function setDifferenceTaxAmountCurr(float|null $value): static
    {
        $this->differenceTaxAmountCurr = $value;
        return $this;
    }

    /**  
     * Rozdíl, daň v tuzemské měně 
     * @see \Isdoc\Tests\Models\TaxSubTotal\GetSetDifferenceTaxAmountTest
     */
    public function getDifferenceTaxAmount(): float
    {
        if(!is_null($this->differenceTaxAmount)) {
            return $this->differenceTaxAmount;
        }
        return $this->getTaxAmount() - $this->getAlreadyClaimedTaxAmount();
    }

    /**  
     * Rozdíl, daň v tuzemské měně 
     * @see \Isdoc\Tests\Models\TaxSubTotal\GetSetDifferenceTaxAmountTest
     */
    public function setDifferenceTaxAmount(float|null $value): static
    {
        $this->differenceTaxAmount = $value;
        return $this;
    }

    /**  
     * Rozdíl, s daní v cizí měně 
     * @see \Isdoc\Tests\Models\TaxSubTotal\GetSetDifferenceTaxInclusiveAmountCurrTest
     */
    public function getDifferenceTaxInclusiveAmountCurr(): float|null
    {
        if(!is_null($this->differenceTaxInclusiveAmountCurr)) {
            return $this->differenceTaxInclusiveAmountCurr;
        }

        if(!is_null($this->getTaxInclusiveAmountCurr()) && !is_null($this->getAlreadyClaimedTaxInclusiveAmountCurr())) {
            return $this->getTaxInclusiveAmountCurr() - $this->getAlreadyClaimedTaxInclusiveAmountCurr();
        }
        return null;
    }

    /**  
     * Rozdíl, s daní v cizí měně 
     * @see \Isdoc\Tests\Models\TaxSubTotal\GetSetDifferenceTaxInclusiveAmountCurrTest
     */
    public function setDifferenceTaxInclusiveAmountCurr(float|null $value): static
    {
        $this->differenceTaxInclusiveAmountCurr = $value;
        return $this;
    }

    /**  
     * Rozdíl, s daní v tuzemské měně 
     * @see \Isdoc\Tests\Models\TaxSubTotal\GetSetDifferenceTaxInclusiveAmountTest
     */
    public function getDifferenceTaxInclusiveAmount(): float
    {
        if(!is_null($this->differenceTaxInclusiveAmount)) {
            return $this->differenceTaxInclusiveAmount;
        }
        return $this->getTaxInclusiveAmount() - $this->getAlreadyClaimedTaxInclusiveAmount();
    }

    /**  
     * Rozdíl, s daní v tuzemské měně 
     * @see \Isdoc\Tests\Models\TaxSubTotal\GetSetDifferenceTaxInclusiveAmountTest
     */
    public function setDifferenceTaxInclusiveAmount(float|null $value): static
    {
        $this->differenceTaxInclusiveAmount = $value;
        return $this;
    }

    /**  
     * Daňová sazba 
     * @see \Isdoc\Tests\Models\TaxSubTotal\GetSetTaxCategoryTest
     */
    public function setTaxCategory(TaxCategory $taxCategory): static
    {
        $this->taxCategory = $taxCategory;
        return $this;
    }

    /**  
     * Daňová sazba 
     * @see \Isdoc\Tests\Models\TaxSubTotal\GetSetTaxCategoryTest
     * @throws MissingTaxCategory
     */
    public function getTaxCategory(): TaxCategory
    {
        if(is_null($this->taxCategory)) {
            throw new MissingTaxCategory();
        }
        return $this->taxCategory;
    }

    /**
     * @see \Isdoc\Tests\Models\TaxSubTotal\ToXmlElementTest
     */
    public function toXmlElement(): IsdocSimpleXMLElement
    {
        $taxSubTotal = new IsdocSimpleXMLElement('<TaxSubTotal></TaxSubTotal>');
        $taxSubTotal->addChildOptional('TaxableAmountCurr', $this->getTaxableAmountCurr());
        $taxSubTotal->addChild('TaxableAmount', $this->getTaxableAmount());
        $taxSubTotal->addChildOptional('TaxAmountCurr', $this->getTaxAmountCurr());
        $taxSubTotal->addChild('TaxAmount', $this->getTaxAmount());
        $taxSubTotal->addChildOptional('TaxInclusiveAmountCurr', $this->getTaxInclusiveAmountCurr());
        $taxSubTotal->addChild('TaxInclusiveAmount', $this->getTaxInclusiveAmount());
        $taxSubTotal->addChildOptional('AlreadyClaimedTaxableAmountCurr', $this->getAlreadyClaimedTaxableAmountCurr());
        $taxSubTotal->addChild('AlreadyClaimedTaxableAmount', $this->getAlreadyClaimedTaxableAmount());
        $taxSubTotal->addChildOptional('AlreadyClaimedTaxAmountCurr', $this->getAlreadyClaimedTaxAmountCurr());
        $taxSubTotal->addChild('AlreadyClaimedTaxAmount', $this->getAlreadyClaimedTaxAmount());
        $taxSubTotal->addChildOptional('AlreadyClaimedTaxInclusiveAmountCurr', $this->getAlreadyClaimedTaxInclusiveAmountCurr());
        $taxSubTotal->addChild('AlreadyClaimedTaxInclusiveAmount', $this->getAlreadyClaimedTaxInclusiveAmount());
        $taxSubTotal->addChildOptional('DifferenceTaxableAmountCurr', $this->getDifferenceTaxableAmountCurr());
        $taxSubTotal->addChild('DifferenceTaxableAmount', $this->getDifferenceTaxableAmount());
        $taxSubTotal->addChildOptional('DifferenceTaxAmountCurr', $this->getDifferenceTaxAmountCurr());
        $taxSubTotal->addChild('DifferenceTaxAmount', $this->getDifferenceTaxAmount());
        $taxSubTotal->addChildOptional('DifferenceTaxInclusiveAmountCurr', $this->getDifferenceTaxInclusiveAmountCurr());
        $taxSubTotal->addChild('DifferenceTaxInclusiveAmount', $this->getDifferenceTaxInclusiveAmount());
        $taxSubTotal->appendSimpleXMLElement($this->getTaxCategory());
        return $taxSubTotal;
    }

    /**
     * Přičte Hodnoty z InvoiceLine do celkového součtu pokud zapadá do stejné TaxCategory
     * @throws TaxCategoryPercentNotEqualsWithInvoiceLineTaxCategoryPercent
     * @throws MissingPercent
     * @throws MissingTaxCategory
     * @throws MissingClassifiedTaxCategory
     * @see \Isdoc\Tests\Models\TaxSubTotal\AddInvoiceLineTest
     */
    public function addInvoiceLine(InvoiceLine $invoiceLine): static
    {
        $this->throwIfTaxPercentNotEquals($invoiceLine->getClassifiedTaxCategory());
        $this->taxableAmount += $invoiceLine->getLineExtensionAmount();
        $this->taxAmount += $invoiceLine->getLineExtensionAmountTaxInclusive() - $invoiceLine->getLineExtensionAmount();
        $this->taxInclusiveAmount += $invoiceLine->getLineExtensionAmountTaxInclusive();
        if(!is_null($invoiceLine->getLineExtensionAmountCurr()) && !is_null($invoiceLine->getLineExtensionAmountTaxInclusiveCurr())) {
            $this->taxableAmountCurr += $invoiceLine->getLineExtensionAmountCurr();
            $this->taxAmountCurr += $invoiceLine->getLineExtensionAmountTaxInclusiveCurr() - $invoiceLine->getLineExtensionAmountCurr();
            $this->taxInclusiveAmountCurr += $invoiceLine->getLineExtensionAmountTaxInclusiveCurr();
        }
        return $this;
    }

    /**
     * Přičte Hodnoty z TaxedDeposit do celkového součtu pokud zapadá do stejné TaxCategory
     * @throws TaxCategoryPercentNotEqualsWithInvoiceLineTaxCategoryPercent
     * @throws MissingPercent
     * @throws MissingTaxCategory
     * @throws MissingClassifiedTaxCategory
     * @see \Isdoc\Tests\Models\TaxSubTotal\AddTaxedDepositTest
     */
    public function addTaxedDeposit(TaxedDeposit $taxedDeposit): static
    {
        $this->throwIfTaxPercentNotEquals($taxedDeposit->getClassifiedTaxCategory());
        $this->alreadyClaimedTaxableAmount += $taxedDeposit->getTaxableDepositAmount();
        $this->alreadyClaimedTaxAmount += $taxedDeposit->getTaxInclusiveDepositAmount() - $taxedDeposit->getTaxableDepositAmount();
        $this->alreadyClaimedTaxInclusive += $taxedDeposit->getTaxInclusiveDepositAmount();
        if(!is_null($taxedDeposit->getTaxableDepositAmountCurr()) && !is_null($taxedDeposit->getTaxInclusiveDepositAmountCurr())) {
            $this->alreadyClaimedTaxableAmountCurr += $taxedDeposit->getTaxableDepositAmountCurr();
            $this->alreadyClaimedTaxAmountCurr += $taxedDeposit->getTaxInclusiveDepositAmountCurr() - $taxedDeposit->getTaxableDepositAmountCurr();
            $this->alreadyClaimedTaxInclusiveCurr += $taxedDeposit->getTaxInclusiveDepositAmountCurr();
        }
        return $this;
    }

    private function throwIfTaxPercentNotEquals(ClassifiedTaxCategory $classifiedTaxCategory): void
    {
        if($this->getTaxCategory()->getPercent() != $classifiedTaxCategory->getPercent()) {
            throw new TaxCategoryPercentNotEqualsWithInvoiceLineTaxCategoryPercent();
        }
    }
}
