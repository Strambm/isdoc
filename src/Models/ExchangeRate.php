<?php

namespace Isdoc\Models;

use Isdoc\Enums\CurrencyCode;
use Isdoc\Exceptions\UnvalidCurrencyCode;
use Isdoc\Exceptions\ExcahngeRateInLocalTransaction;

class ExchangeRate
{
    protected $localCurrencyCode = null;
    protected $foreignCurrencyCode = null;
    protected $currRate = 1;
    protected $refCurrRate = 1;

    /**
     * @throws UnvalidCurrencyCode
     * @see \Isdoc\Tests\Models\ExchangeRate\ConstructorTest
     */
    public function __construct(string $localCurrencyCode, string|null $foreignCurrencyCode = null, float $currRate = 1, float $refCurrRate = 1) 
    {
        $this->setLocalCurrencyCode($localCurrencyCode);
        $this->setForeignCurrencyCode($foreignCurrencyCode);
        $this->setCurrRate($currRate);
        $this->setRefCurrRate($refCurrRate);
    }

    /**
     * Kód měny
     * @see \Isdoc\Enums\CountryCode
     * @see \Isdoc\Tests\Models\ExchangeRate\GetSetLocalCurrencyCodeTest
     * @throws UnvalidCurrencyCode
     */
    public function setLocalCurrencyCode(string $fromCurrencyCode): static
    {
        $this->localCurrencyCode = CurrencyCode::validate($fromCurrencyCode);
        return $this;
    }

    /**
     * Kód měny
     * @see \Isdoc\Enums\CountryCode
     * @see \Isdoc\Tests\Models\ExchangeRate\GetSetForeignCurrencyCodeTest
     * @throws UnvalidCurrencyCode
     */
    public function setForeignCurrencyCode(string|null $foreignCurrencyCode): static
    {
        if($foreignCurrencyCode == '') {
            $foreignCurrencyCode = null;
        }
        if(!is_null($foreignCurrencyCode)) {
            $foreignCurrencyCode = CurrencyCode::validate($foreignCurrencyCode);
        }
        $this->foreignCurrencyCode = $foreignCurrencyCode;
        return $this;
    }

    /**
     * Kurz cizí měny, pokud je použita, jinak 1 (Kurz se kterým se obchoduje)
     * @see \Isdoc\Tests\Models\ExchangeRate\GetSetCurrRateTest
     */
    public function setCurrRate(float $rate): static
    {
        $this->currRate = $rate;
        return $this;
    }

    /**
     * Vztažný kurz cizí měny, většinou 1 (Stanovený kurz)
     * @see \Isdoc\Tests\Models\ExchangeRate\GetSetRefCurrRateTest
     */
    public function setRefCurrRate(float $rate): static
    {
        $this->refCurrRate = $rate;
        return $this;
    }

    /**
     * Kód měny
     * @see \Isdoc\Tests\Models\ExchangeRate\GetSetLocalCurrencyCodeTest
     */
    public function getLocalCurrencyCode(): string
    {
        return $this->localCurrencyCode;
    }

    /**
     * Kód měny
     * @see \Isdoc\Tests\Models\ExchangeRate\GetSetForeignCurrencyCodeTest
     */
    public function getForeignCurrencyCode(): string|null
    {
        return $this->foreignCurrencyCode;
    }

    /**
     * Kurz cizí měny, pokud je použita, jinak 1 (Kurz se kterým se obchoduje)
     * @see \Isdoc\Tests\Models\ExchangeRate\GetSetCurrRateTest
     * @throws ExcahngeRateInLocalTransaction
     */
    public function getCurrRate(): float
    {
        $this->throwIfExchangeRateIsChangedButForeignCurrencyIsNull($this->currRate);
        return $this->currRate;
    }

    private function throwIfExchangeRateIsChangedButForeignCurrencyIsNull(float $value): void
    {
        if(
            !$this->hasForeignCurrency()
                && $value != 1
        ) {
            throw new ExcahngeRateInLocalTransaction();
        }
    }

    /**
     * Vztažný kurz cizí měny, většinou 1 (Stanovený kurz)
     * @see \Isdoc\Tests\Models\ExchangeRate\GetSetRefCurrRateTest
     * @throws ExcahngeRateInLocalTransaction
     */
    public function getRefCurrRate(): float
    {
        $this->throwIfExchangeRateIsChangedButForeignCurrencyIsNull($this->refCurrRate);
        return $this->refCurrRate;
    }

    /**
     * Funkční kurz
     * @see \Isdoc\Tests\Models\ExchangeRate\GetExchangeRateTest
     */
    public function getExchangeRate(): float
    {
        return $this->getCurrRate();
    }

    /**
     * Funkční hodnota po převodu
     * @see \Isdoc\Tests\Models\ExchangeRate\GetExchangeAmountTest
     */
    public function getExchangeAmount(float $valueInLocalCurrency): float|null
    {
        if($this->hasForeignCurrency()) {
            return $valueInLocalCurrency / $this->getExchangeRate();
        } else {
            return null;
        }
    }

    /**
     * Vrací true prokud je vyžadován převod
     * @see \Isdoc\Tests\Models\ExchangeRate\HasForeignCurrencyTest
     */
    public function hasForeignCurrency(): bool
    {
        return !is_null($this->foreignCurrencyCode);
    }
}
