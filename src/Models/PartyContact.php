<?php

namespace Isdoc\Models;

use Isdoc\Exceptions\MissingPartyName;
use Isdoc\Exceptions\MissingPostalAddress;
use Isdoc\Traits\StringConversion;
use Isdoc\Interfaces\RenderableInterface;
use Isdoc\Exceptions\MissingPartyIdentifivation;

/**
 * Identifikace subjektu
 */
class PartyContact implements RenderableInterface
{
    use StringConversion;

    protected $partyIdentification = null;
    protected $name = null;
    protected $postalAddress = null;
    protected $partyTaxSchemes = [];
    protected $contact = null;


    /**
     * Element identifikačních položek subjektu (firmy)
     * @see \Isdoc\Tests\Models\PartyContact\GetSetPartyIdentificationTest
     */
    public function setPartyIdentification(PartyIdentification $partyIdentification): static
    {
        $this->partyIdentification = $partyIdentification;
        return $this;
    }

    /**
     * Element identifikačních položek subjektu (firmy)
     * @see \Isdoc\Tests\Models\PartyContact\GetSetPartyIdentificationTest
     * @throws MissingPartyIdentifivation
     */
    public function getPartyIdentification(): PartyIdentification
    {
        if(is_null($this->partyIdentification)) {
            throw new MissingPartyIdentifivation();
        }
        return $this->partyIdentification;
    }

    /** 
     * Název subjektu
     * @see \Isdoc\Tests\Models\PartyContact\GetSetNameTest
     */
    public function setName(string $name): static
    {
        $this->name = $name;
        return $this;
    }

    /** 
     * Název subjektu
     * @see \Isdoc\Tests\Models\PartyContact\GetSetNameTest
     * @throws MissingPartyName
     */
    public function getName(): string
    {
        if(is_null($this->name)) {
            throw new MissingPartyName();
        }
        return $this->name;
    }

    /** 
     * Poštovní adresa
     * @see \Isdoc\Tests\Models\PartyContact\GetSetPostalAddressTest
     */
    public function setPostalAddress(PostalAddress $postalAddress): static
    {
        $this->postalAddress = $postalAddress;
        return $this;
    }

    /** 
     * Poštovní adresa
     * @see \Isdoc\Tests\Models\PartyContact\GetSetPostalAddressTest
     * @throws MissingPostalAddress
     */
    public function getPostalAddress(): PostalAddress
    {
        if(is_null($this->postalAddress)) {
            throw new MissingPostalAddress();
        }
        return $this->postalAddress;
    }

    /** 
     * Daňové údaje. Element je možné použít vícekrát a určit více identifikátorů, např. DIČ a IČ DPH na Slovensku.
     * @see \Isdoc\Tests\Models\PartyContact\AddGetPartyTaxSchemeTest
     */
    public function addPartyTaxScheme(PartyTaxScheme $partyTaxScheme): static
    {
        $this->partyTaxSchemes[] = $partyTaxScheme;
        return $this;
    }

    /**
     * @return PartyTaxScheme[]
     * @see \Isdoc\Tests\Models\PartyContact\AddGetPartyTaxSchemeTest
     */
    public function getPartyTaxScheme(): array
    {
        return $this->partyTaxSchemes;
    }

    /** 
     * Kontaktní osoba
     * @see \Isdoc\Tests\Models\PartyContact\GetSetContactTest
     */
    public function setContact(null|Contact $contact): static
    {
        $this->contact = $contact;
        return $this;
    }

    /**
     * Kontaktní osoba
     * @see \Isdoc\Tests\Models\PartyContact\GetSetContactTest
     */
    public function getContact(): Contact|null
    {
        return $this->contact;
    }

    /**
     * @see \Isdoc\Tests\Models\PartyContact\ToXmlElementTest
     * @throws MissingPartyIdentifivation
     * @throws MissingPartyName
     * @throws MissingPostalAddress
     */
    public function toXmlElement(): IsdocSimpleXMLElement
    {
        $party = new IsdocSimpleXMLElement('<Party></Party>');
        $party->appendSimpleXMLElement($this->getPartyIdentification());

        $partyName = $party->addChild('PartyName');
        $partyName->addChild('Name', $this->getName());

        $party->appendSimpleXMLElement($this->getPostalAddress());

        foreach ($this->getPartyTaxScheme() as $partyTaxScheme) {
            $party->appendSimpleXMLElement($partyTaxScheme);
        }

        $party->appendSimpleXMLElementOptional($this->getContact());

        return $party;
    }
}
