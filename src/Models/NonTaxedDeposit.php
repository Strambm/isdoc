<?php

namespace Isdoc\Models;

use Isdoc\Traits\StringConversion;
use Isdoc\Traits\HasForeignCurrencyAttribute;

/**
 * Informace o konkrétní částce v sazbě na odúčtovaném daňovém zálohovém listu
 */
class NonTaxedDeposit extends Deposit
{
    use StringConversion;
    use HasForeignCurrencyAttribute;
    
    protected $depositAmountCurr = null;
    protected $depositAmount = 0;

    /**
     * Kladná částka zálohy v cizí měně
     * @see \Isdoc\Tests\Models\NonTaxedDeposit\GetSetDepositAmountCurrTest
     */
    public function getDepositAmountCurr(): float|null
    {
        return $this->getForeignAmountForMethod(__FUNCTION__);
    }

    /**
     * Kladná částka zálohy v cizí měně
     * @see \Isdoc\Tests\Models\NonTaxedDeposit\GetSetDepositAmountCurrTest
     */
    public function setDepositAmountCurr(float|null $value): static
    {
        $this->depositAmountCurr = $value;
        return $this;
    }

    /**
     * Kladná část zálohy
     * @see \Isdoc\Tests\Models\NonTaxedDeposit\GetSetDepositAmountTest
     */
    public function setDepositAmount(float $value): static
    {
        $this->depositAmount = $value;
        return $this;
    }

    /**
     * Kladná část zálohy
     * @see \Isdoc\Tests\Models\NonTaxedDeposit\GetSetDepositAmountTest
     */
    public function getDepositAmount(): float
    {
        return $this->depositAmount;
    }

    /**
     * @see \Isdoc\Tests\Models\NonTaxedDeposit\ToXmlElementTest
     */
    public function toXmlElement(): IsdocSimpleXMLElement
    {
        $xml = new IsdocSimpleXMLElement('<NonTaxedDeposit></NonTaxedDeposit>');
        $xml->addChild('ID', $this->getId());
        $xml->addChild('VariableSymbol', $this->getVariableSymbol());
        $xml->addChildOptional('DepositAmountCurr', $this->getDepositAmountCurr());
        $xml->addChild('DepositAmount', $this->getDepositAmount());
        return $xml;
    }
}