<?php

namespace Isdoc\Models;

use Isdoc\Traits\StringConversion;
use Isdoc\Interfaces\RenderableInterface;

/**
 * Detaily o platbě
 */
abstract class PaymentDetails implements RenderableInterface
{
    use StringConversion;
}
