<?php

namespace Isdoc\Models;

/**
 * Odkaz na související dodací list
 */
class DeliveryNoteReference extends DocumentReference
{
    protected function getTagName(): string
    {
        return 'DeliveryNoteReference';
    }
}
