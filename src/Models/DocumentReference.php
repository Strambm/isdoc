<?php

namespace Isdoc\Models;

use Isdoc\Traits\DateValidation;
use Isdoc\Traits\UUIDValidation;
use Isdoc\Traits\StringConversion;
use Isdoc\Exceptions\InvalidDateFormat;
use Isdoc\Exceptions\InvalidUuidFormat;
use Isdoc\Interfaces\RenderableInterface;
use Isdoc\Exceptions\MissingDeliveryNoteReferenceId;

abstract class DocumentReference implements RenderableInterface
{
    use StringConversion;
    use DateValidation;
    use UUIDValidation;
    
    protected $id = null;
    protected $issueDate = null;
    protected $uuid = null;

    /** 
     * Vlastní identifikátor dodacího listu u dodavatele 
     * @see \Isdoc\Tests\Models\DocumentReference\GetSetIdTest
     **/
    public function setId(string $id): static
    {
        $this->id = $id;
        return $this;
    }

    /** 
     * Vlastní identifikátor dodacího listu u dodavatele 
     * @see \Isdoc\Tests\Models\DocumentReference\GetSetIdTest
     * @throws MissingDeliveryNoteReferenceId
     **/
    public function getId(): string
    {
        if(is_null($this->id)) {
            throw new MissingDeliveryNoteReferenceId();
        }
        return $this->id;
    }

    /** 
     * Datum vystavení
     * @see \Isdoc\Tests\Models\DocumentReference\GetSetIssueDateTest
     * @throws InvalidDateFormat
     **/
    public function setIssueDate(string|null $issueDate): static
    {
        $this->validateDateNullable($issueDate);
        $this->issueDate = $issueDate;
        return $this;
    }

    /** 
     * Datum vystavení
     * @see \Isdoc\Tests\Models\DocumentReference\GetSetIssueDateTest
     **/
    public function getIssueDate(): string|null
    {
        return $this->issueDate;
    }

    /** 
     * Unikátní identifikátor GUID
     * @see \Isdoc\Tests\Models\DocumentReference\GetSetUuidTest
     * @throws InvalidUuidFormat
     **/
    public function setUuid(string|null $uuid): static
    {
        $this->validateUuidNullable($uuid);
        $this->uuid = $uuid;
        return $this;
    }

    /** 
     * Unikátní identifikátor GUID
     * @see \Isdoc\Tests\Models\DocumentReference\GetSetUuidTest
     **/
    public function getUuid(): string|null
    {
        return $this->uuid;
    }

    /**
     * @see \Isdoc\Tests\Models\DocumentReference\ToXmlElementTest
     */
    public function toXmlElement(): IsdocSimpleXMLElement
    {
        $reference = new IsdocSimpleXMLElement('<' . $this->getTagName() . '></' . $this->getTagName() . '>');
        $reference->addAttribute('id', $this->getId());
        $reference->addChild('ID', $this->getId());
        $reference->addChildOptional('IssueDate', $this->getIssueDate());
        $reference->addChildOptional('UUID', $this->getUuid());
        return $reference;
    }

    protected abstract function getTagName(): string;
}
