<?php

namespace Isdoc\Models;

use Isdoc\Enums\CountryCode;
use Isdoc\Traits\StringConversion;
use Isdoc\Interfaces\RenderableInterface;

/**
 * Poštovní adresa
 */
class PostalAddress implements RenderableInterface
{
    use StringConversion;

    protected $streetName = '';
    protected $buildingNumber = '';
    protected $cityName = '';
    protected $postalZone = '';
    protected $country = null;

    /**
     * @see \Isdoc\Tests\Models\PostalAddress\ConstructorTest
     */
    public function __construct()
    {
        $this->country = (new Country)
            ->setIdentificationCode(CountryCode::CZ);
    }

    /**
     * Ulice
     * @see \Isdoc\Tests\Models\PostalAddress\GetSetStreetNameTest
     */
    public function setStreetName(string $streetName): static
    {
        $this->streetName = $streetName;
        return $this;
    }

    /**
     * Ulice
     * @see \Isdoc\Tests\Models\PostalAddress\GetSetStreetNameTest
     */
    public function getStreetName(): string
    {
        return $this->streetName;
    }

    /** 
     * Číslo popisné/orientační
     * @see \Isdoc\Tests\Models\PostalAddress\GetSetBuildingNumberTest
     */
    public function setBuildingNumber(string $buildingNumber): static
    {
        $this->buildingNumber = $buildingNumber;
        return $this;
    }

    /** 
     * Číslo popisné/orientační
     * @see \Isdoc\Tests\Models\PostalAddress\GetSetBuildingNumberTest
     */
    public function getBuildingNumber(): string
    {
        return $this->buildingNumber;
    }

    /** 
     * Město
     * @see \Isdoc\Tests\Models\PostalAddress\GetSetCityNameTest
     */
    public function setCityName(string $cityName): static
    {
        $this->cityName = $cityName;
        return $this;
    }

    /** 
     * Město
     * @see \Isdoc\Tests\Models\PostalAddress\GetSetCityNameTest
     */
    public function getCityName(): string
    {
        return $this->cityName;
    }

    /** 
     * PSČ
     * @see \Isdoc\Tests\Models\PostalAddress\GetSetPostalZoneTest
     */
    public function setPostalZone(string $postalZone): static
    {
        $this->postalZone = $postalZone;
        return $this;
    }

    /** 
     * PSČ
     * @see \Isdoc\Tests\Models\PostalAddress\GetSetPostalZoneTest
     */
    public function getPostalZone(): string
    {
        return $this->postalZone;
    }

    /** 
     * Země
     * @see \Isdoc\Tests\Models\PostalAddress\GetSetCountryTest
     */
    public function setCountry(Country $country): static
    {
        $this->country = $country;
        return $this;
    }

    /** 
     * Země
     * @see \Isdoc\Tests\Models\PostalAddress\GetSetCountryTest
     */
    public function getCountry(): Country
    {
        return $this->country;
    }

    /**
     * @see \Isdoc\Tests\Models\PostalAddress\ToXmlElementTest
     */
    public function toXmlElement(): IsdocSimpleXMLElement
    {
        $postalAddress = new IsdocSimpleXMLElement('<PostalAddress></PostalAddress>');
        $postalAddress->addChild('StreetName', $this->getStreetName());
        $postalAddress->addChild('BuildingNumber', $this->getBuildingNumber());
        $postalAddress->addChild('CityName', $this->getCityName());
        $postalAddress->addChild('PostalZone', $this->getPostalZone());
        $postalAddress->appendSimpleXMLElement($this->getCountry()->toXmlElement());
        return $postalAddress;
    }
}
