<?php

namespace Isdoc\Models;

use Isdoc\Enums\CountryCode;
use Isdoc\Traits\StringConversion;
use Isdoc\Exceptions\UnvalidCountryCode;
use Isdoc\Interfaces\RenderableInterface;
use Isdoc\Exceptions\MissingIdentificationCode;

class Country implements RenderableInterface
{
    use StringConversion;

    protected $identificationCode = null;
    protected $name = null;

    /** 
     * Kód země podle ISO 3166-1 alpha-2
     * @see \Isdoc\Enums\CountryCode
     * @see \Isdoc\Tests\Models\Country\GetSetIdentificationCodeTest
     * @throws UnvalidCountryCode
     */
    public function setIdentificationCode(string|null $identificationCode): static
    {
        if(!is_null($identificationCode)) {
            CountryCode::validate($identificationCode);
        }
        $this->identificationCode = $identificationCode;
        return $this;
    }

    /** 
     * Kód země podle ISO 3166-1 alpha-2
     * @see \Isdoc\Tests\Models\Country\GetSetIdentificationCodeTest
     */
    public function getIdentificationCode(): string
    {
        return (string) $this->identificationCode;
    }

    /** 
     * Název země
     * @see \Isdoc\Tests\Models\Country\GetSetNameTest
     */
    public function setName(string|null $name): static
    {
        $this->name = $name;
        return $this;
    }

    /**
     * Název země
     * @throws MissingIdentificationCode
     * @see \Isdoc\Tests\Models\Country\GetSetNameTest
     */
    public function getName(): string
    {
        if(!is_null($this->name)) {
            return $this->name;
        } elseif($this->getIdentificationCode() != '') {
            return CountryCode::getNameFromCode($this->getIdentificationCode());
        }
        return '';
    }

    /** 
     * @return static
     * @see \Isdoc\Tests\Models\Country\ToXmlElementTest
     */
    public function toXmlElement(): IsdocSimpleXMLElement
    {
        $country = new IsdocSimpleXMLElement('<Country></Country>');
        $country->addChild('IdentificationCode', $this->getIdentificationCode());
        $country->addChild('Name', $this->getName());
        return $country;
    }
}
