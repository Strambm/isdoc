<?php

namespace Isdoc\Models;

use Isdoc\Traits\StringConversion;
use Isdoc\Interfaces\RenderableInterface;
use Isdoc\Exceptions\TaxSubTotalWasCreatedBefore;

/**
 * Daňová rekapitulace
 */
class TaxTotal implements RenderableInterface
{
    use StringConversion;

    protected $taxSubTotals = [];
    protected $taxAmountCurr = null;
    protected $taxAmount = null;

    /**
     * Sumář jedné daňové sazby
     * @throws TaxSubTotalWasCreatedBefore
     * @see \Isdoc\Tests\Models\TaxTotal\AddGetTaxTotalTest
     */
    public function addTaxSubTotal(TaxSubTotal $taxSubTotal): static
    {
        $percent = $taxSubTotal->getTaxCategory()->getPercent();
        $this->throwIfTaxSubTotalWithSamePercentExists($percent);

        $this->taxSubTotals[] = $taxSubTotal;
        return $this;
    }

    private function throwIfTaxSubTotalWithSamePercentExists(float $percent): void
    {
        $result = array_filter($this->taxSubTotals, function(TaxSubTotal $item) use ($percent): bool {
            return $item->getTaxCategory()->getPercent() == $percent;
        });
        if(count($result) != 0) {
            throw new TaxSubTotalWasCreatedBefore();
        }
    }

    /**
     * Sumář jedné daňové sazby
     * @return TaxSubTotal[]
     * @see \Isdoc\Tests\Models\TaxTotal\AddGetTaxTotalTest
     */
    public function getTaxSubTotals(): array
    {
        return $this->taxSubTotals;
    }

    /**
     * Přičte k rekapitulaci hodnoty z jednoho řádku na faktuře
     * @see \Isdoc\Tests\Models\TaxTotal\AddInvoiceLineTest
     */
    public function addInvoiceLine(InvoiceLine $invoiceLine): static
    {
        $taxSubTotal = $this->findOrCreateTaxSubTotal($invoiceLine->getClassifiedTaxCategory());
        $taxSubTotal->addInvoiceLine($invoiceLine);
        return $this;
    }

    private function findOrCreateTaxSubTotal(ClassifiedTaxCategory $classifiedTaxCategory): TaxSubTotal
    {
        $taxSubTotal = null;
        for($a = 0; $a < count($this->taxSubTotals); $a ++) {
            if($classifiedTaxCategory->getPercent() == $this->taxSubTotals[$a]->getTaxCategory()->getPercent()) {
                $taxSubTotal  = &$this->taxSubTotals[$a];
            }
        }
        if(is_null($taxSubTotal)) {
            $this->taxSubTotals[] = (new TaxSubTotal())
                ->setTaxCategory((new TaxCategory)->setPercent($classifiedTaxCategory->getPercent())
                ->setVATAplicable($classifiedTaxCategory->getVatApplicable())
                ->setLocalReverseChargeFlag($classifiedTaxCategory->getLocalReverseCharge())
            );
            $taxSubTotal = &$this->taxSubTotals[count($this->taxSubTotals) - 1];
        }
        return $taxSubTotal;
    }

    /**
     * Přičte k rekapitulaci hodnoty z jednoho záznamu TaxedDeposit
     * @see \Isdoc\Tests\Models\TaxTotal\AddTaxedDepositTest
     */
    public function addTaxedDeposit(TaxedDeposit $taxedDeposit): static
    {
        $taxSubTotal = $this->findOrCreateTaxSubTotal($taxedDeposit->getClassifiedTaxCategory());
        $taxSubTotal->addTaxedDeposit($taxedDeposit);
        return $this;
    }

    /** 
     * Částka v cizí měně 
     * @see \Isdoc\Tests\Models\TaxTotal\GetSetTaxAmountCurrTest
     */
    public function getTaxAmountCurr(): float|null
    {
        if(!is_null($this->taxAmountCurr)) {
            return $this->taxAmountCurr;
        }
        return $this->getSumOfParameterOfTaxSubTotal('getTaxAmountCurr');
    }

    private function getSumOfParameterOfTaxSubTotal(string $parameterGetMethodName): float|null
    {
        $value = null;
        foreach($this->taxSubTotals as $taxSubTotal) {
            $increment = $taxSubTotal->$parameterGetMethodName();
            if(!is_null($increment)) {
                $value += $taxSubTotal->$parameterGetMethodName();
            }
        }
        return $value;
    }

    /** 
     * Částka v cizí měně 
     * @see \Isdoc\Tests\Models\TaxTotal\GetSetTaxAmountCurrTest
     */
    public function setTaxAmountCurr(float|null $value): static
    {
        $this->taxAmountCurr = $value;
        return $this;
    }

    /** 
     * Částka 
     * @see \Isdoc\Tests\Models\TaxTotal\GetSetTaxAmountTest
     */
    public function getTaxAmount(): float
    {
        if(!is_null($this->taxAmount)) {
            return $this->taxAmount;
        }
        return (float) $this->getSumOfParameterOfTaxSubTotal('getTaxAmount');
    }

    /** 
     * Částka 
     * @see \Isdoc\Tests\Models\TaxTotal\GetSetTaxAmountTest
     */
    public function setTaxAmount(float|null $value): static
    {
        $this->taxAmount = $value;
        return $this;
    }

    /**
     * @see \Isdoc\Tests\Models\TaxTotal\ToXmlElementTest
     */
    public function toXmlElement(): IsdocSimpleXMLElement
    {
        $taxTotal = new IsdocSimpleXMLElement('<TaxTotal></TaxTotal>');

        foreach ($this->taxSubTotals as $taxSubTotal) {
            $taxTotal->appendSimpleXMLElement($taxSubTotal->toXmlElement());
        }

        $taxTotal->addChildOptional('TaxAmountCurr', $this->getTaxAmountCurr());
        $taxTotal->addChild('TaxAmount', $this->getTaxAmount());
        return $taxTotal;
    }

    
}
