<?php

namespace Isdoc\Models;

use SimpleXMLElement;
use Isdoc\Interfaces\RenderableInterface;

class IsdocSimpleXMLElement extends SimpleXMLElement
{
    /**
     * Přidává XML do SimpleXMLElement objektu
     *
     * @param string $xml
     * @return bool XML string přidán
     * @see \Isdoc\Tests\Models\IsdocSimpleXMLElement\AppendXMLTest
     */
    public function appendXML(string $xml): bool
    {
        // ověření, zdali je co přidat
        if ($nodata = !strlen($xml) or $this[0] == NULL) {
            return $nodata;
        }

        // přidání XML
        $node     = dom_import_simplexml($this);
        $fragment = $node->ownerDocument->createDocumentFragment();
        $fragment->appendXML($xml);

        return (bool)$node->appendChild($fragment);
    }

    /**
     * Přidává SimpleXMLElement jako XML do SimpleXMLElement objektu
     *
     * @param SimpleXMLElement|IsdocSimpleXMLElement|RenderableInterface $child
     * @return bool SimpleXMLElement přidán
     * @see \Isdoc\Tests\Models\IsdocSimpleXMLElement\AppendSimpleXMLElementTest
     */
    public function appendSimpleXMLElement(SimpleXMLElement|IsdocSimpleXMLElement|RenderableInterface $child): bool
    {
        $child = $this->convertRenderableToXml($child);
        // vložení attributu
        if ($child->xpath('.') != array($child)) {
            $this[$child->getName()] = (string)$child;
            return true;
        }
        
        $xml = $child->asXML();

        // odstranění XML deklarace z hlavičky
        if ($child->xpath('/*') == array($child)) {
            $pos = strpos($xml, "\n");
            $xml = substr($xml, $pos + 1);
        }

        return $this->appendXML($xml);
    }

    /**
     * Přidává child <element> do SimpleXMLElement objektu pouze pokud parametr $value není null. 
     * 
     * @param string $qualifiedName Jméno child elementu k přidání
     * @param string $value Hodnota child <element>
     * @param string $namespace Namespace, do kterého child <element> náleží
     * @return static|null
     * @see \Isdoc\Tests\Models\IsdocSimpleXMLElement\AddChildOptionalTest
     */
    public function addChildOptional(string $qualifiedName, ?string $value = null, ?string $namespace = null)
    {
        if ($value != null) {
            $child = $this->addChild($qualifiedName, $value, $namespace);
            return $child;
        }
        return $this;
    }

    /**
     * Přidává SimpleXMLElement jako XML do SimpleXMLElement objektu pokud není parametr $child null.
     * @return bool SimpleXMLElement přidán
     * @see \Isdoc\Tests\Models\IsdocSimpleXMLElement\AppendSimpleXMLElementOptionalTest
     */
    public function appendSimpleXMLElementOptional(SimpleXMLElement|null|RenderableInterface $child): bool
    {
        if(!is_null($child)) {
            $child = $this->convertRenderableToXml($child);
            return $this->appendSimpleXMLElement($child);
        }
        return false;
    }

    private function convertRenderableToXml(RenderableInterface|SimpleXMLElement|null $value): null|SimpleXMLElement
    {
        if($value instanceof RenderableInterface) {
            return $value->toXmlElement();
        }
        return $value;
    }

    /**
     * @see \Isdoc\Tests\Models\IsdocSimpleXMLElement\AppendXmlArrayTest
     */
    public function appendXmlArray(string $key, array|null $values): void
    {
        if (!empty($values)) {
            $parent = $this->addChild($key);
            foreach ($values as $value) {
                if($value instanceof RenderableInterface) {
                    $parent->appendSimpleXMLElement($value->toXmlElement());
                } elseif($value instanceof SimpleXMLElement) {
                    $parent->appendSimpleXMLElement($value);
                }
            }
        }
    }
}
