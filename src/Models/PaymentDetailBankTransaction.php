<?php

namespace Isdoc\Models;

use Isdoc\Exceptions\MissingBic;
use Isdoc\Traits\DateValidation;
use Isdoc\Exceptions\MissingIban;
use Isdoc\Traits\NumberValidation;
use Isdoc\Exceptions\MissingBankId;
use Isdoc\Exceptions\MissingDueDate;
use Isdoc\Exceptions\MissingBankCode;
use Isdoc\Exceptions\MissingBankAccount;
use Isdoc\Exceptions\InvalidNumericFormat;

/**
 * Detaily o platbě převodem
 */
class PaymentDetailBankTransaction extends PaymentDetails
{
    use DateValidation;
    use NumberValidation;
    
    protected $paymentDueDate = null;
    protected $bankAccount = null;
    protected $vs = null;
    protected $ks = null;
    protected $ss = null;
    

    /** 
     * Datum splatnosti
     * @param string $paymentDueDate (formát YYYY-MM-DD)
     * @see \Isdoc\Tests\Models\PaymentDetailBankTransaction\GetSetIssueDateTest
     * @throws InvalidDateFormat
     */
    public function setPaymentDueDate(string $paymentDueDate): static
    {
        $this->validateDate($paymentDueDate);
        $this->paymentDueDate = $paymentDueDate;
        return $this;
    }

    /** 
     * Datum splatnosti
     * @param string $paymentDueDate (formát YYYY-MM-DD)
     * @see \Isdoc\Tests\Models\PaymentDetailBankTransaction\GetSetIssueDateTest
     * @throws MissingDueDate
     */
    public function getPaymentDueDate(): string
    {
        if(is_null($this->paymentDueDate)) {
            throw new MissingDueDate();
        }
        return $this->paymentDueDate;
    }

    /**
     * Bankovní účet
     */
    public function setBankAccount(BankAccount $bankAccount): static
    {
        $this->bankAccount = $bankAccount;
        return $this;
    }

    /**
     * Bankovní účet
     * @see \Isdoc\Tests\Models\PaymentDetailBankTransaction\GetSetBankCodeTest
     * @throws MissingBankAccount
     */
    public function getBankAccount(): BankAccount
    {
        if(is_null($this->bankAccount)) {
            throw new MissingBankAccount();
        }
        return $this->bankAccount;
    }

    /**
     * Variabilní symbol
     * @see \Isdoc\Tests\Models\PaymentDetailBankTransaction\GetSetVsTest
     */
    public function getVs(): string|null
    {
        return $this->vs;
    }

    /**
     * Variabilní symbol
     * @see \Isdoc\Tests\Models\PaymentDetailBankTransaction\GetSetVsTest
     * @throws InvalidNumericFormat
     */
    public function setVs(string|null $value): static
    {
        $this->validateNumericNullable($value);
        $this->vs = $value;
        return $this;
    }

    /** 
     * Konstantní symbol
     * @see \Isdoc\Tests\Models\PaymentDetailBankTransaction\GetSetConstantSymbolTest
     * @throws InvalidNumericFormat
     */
    public function setConstantSymbol(null|string $constantSymbol): static
    {
        $this->validateNumericNullable($constantSymbol);
        $this->ks = $constantSymbol;
        return $this;
    }

    /** 
     * Konstantní symbol
     * @see \Isdoc\Tests\Models\PaymentDetailBankTransaction\GetSetConstantSymbolTest
     */
    public function getConstantSymbol(): null|string
    {
        return $this->ks;
    }

    /**
     * Specificiký symbol
     * @see \Isdoc\Tests\Models\PaymentDetailBankTransaction\GetSetSpecificSymbolTest
     */
    public function getSpecificSymbol(): string|null
    {
        return $this->ss;
    }

    /**
     * Specificiký symbol
     * @see \Isdoc\Tests\Models\PaymentDetailBankTransaction\GetSetSpecificSymbolTest
     * @throws InvalidNumericFormat
     */
    public function setSpecificSymbol(string|null $value): static
    {
        $this->validateNumericNullable($value);
        $this->ss = $value;
        return $this;
    }

    /**
     * @see \Isdoc\Tests\Models\PaymentDetailBankTransaction\ToXmlElementTest
     * @throws MissingDueDate
     * @throws MissingBankAccount
     * @throws MissingBankId
     * @throws MissingBankCode
     * @throws MissingIban
     * @throws MissingBic
     */
    public function toXmlElement(): IsdocSimpleXMLElement
    {
        $details = new IsdocSimpleXMLElement('<Details></Details>');

        $details->addChild('PaymentDueDate', $this->getPaymentDueDate());
        $details->addChild('ID', $this->getBankAccount()->getId());
        $details->addChild('BankCode', $this->getBankAccount()->getBankCode());
        $details->addChild('Name', $this->getBankAccount()->getName());
        $details->addChild('IBAN', $this->getBankAccount()->getIban());
        $details->addChild('BIC', $this->getBankAccount()->getBic());
        $details->addChildOptional('VariableSymbol', $this->getVs());
        $details->addChildOptional('ConstantSymbol', $this->getConstantSymbol());
        $details->addChildOptional('SpecificSymbol', $this->getSpecificSymbol());
        return $details;
    }
}
