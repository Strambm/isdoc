<?php

namespace Isdoc\Models;

use Isdoc\Enums\VatCalculationMethod;

class Price
{   
    protected $vatPercent = null;
    protected $value = 0;
    protected $vatCalculationMethod = null;

    /**
     * @see \Isdoc\Tests\Models\Price\ConstructorTest
     */
    public function __construct(float $vatPercent, int $vatCalculationMethod)
    {
        $this->vatPercent = $vatPercent;
        $this->vatCalculationMethod = VatCalculationMethod::validate($vatCalculationMethod);
    }

    /**
     * @param $value Pokud je vatCalculationMethod = 0 cena BezDPH; pokud = 1 cena s DPH
     * @see \Isdoc\Tests\Models\Price\SetValueTest
     */
    public function setValue(float $value): static
    {
        $this->value = $value;
        return $this;
    }

    /**
     * @see \Isdoc\Tests\Models\Price\GetValueWithVatTest
     */
    public function getValueWithVat(): float
    {
        switch($this->vatCalculationMethod) {
            case VatCalculationMethod::ZDOLA:
                return $this->value * $this->getVatMultiplier();
            case VatCalculationMethod::SHORA:
                return $this->value;
        }
    }

    /**
     * @see \Isdoc\Tests\Models\Price\GetValueOfVatTest
     */
    public function getValueOfVat(): float
    {
        return $this->getValueWithVat() - $this->getValueWithoutVat();
    }

    /**
     * @see \Isdoc\Tests\Models\Price\GetValueWithoutVatTest
     */
    public function getValueWithoutVat(): float
    {
        switch($this->vatCalculationMethod) {
            case VatCalculationMethod::ZDOLA:
                return $this->value;
            case VatCalculationMethod::SHORA:
                return $this->value / $this->getVatMultiplier();
        }
    }

    private function getVatMultiplier(): float
    {
        return 1 + ($this->vatPercent / 100);
    }
}
