<?php

namespace Isdoc\Models;

use Isdoc\Enums\TaxScheme;
use Isdoc\Traits\BoolValidation;
use Isdoc\Traits\StringConversion;
use Isdoc\Exceptions\MissingPercent;
use Isdoc\Exceptions\UnvalidTaxScheme;
use Isdoc\Interfaces\RenderableInterface;


/**
 * Daňová sazba
 */
class TaxCategory implements RenderableInterface
{
    use StringConversion;
    use BoolValidation;

    protected $percent = null;
    protected $taxScheme = null;
    protected $vatApplicable = null;
    protected $localReverseChargeFlag = null;

    /** 
     * Procentní sazba daně
     * @see \Isdoc\Tests\Models\TaxCategory\GetSetPercentTest
     */
    public function setPercent(float $percent): static
    {
        $this->percent = $percent;
        return $this;
    }

    /** 
     * Procentní sazba daně
     * @see \Isdoc\Tests\Models\TaxCategory\GetSetPercentTest
     * @throws MissingPercent
     */
    public function getPercent(): float
    {
        if(is_null($this->percent)) {
            throw new MissingPercent();
        }
        return $this->percent;
    }

    /**
     * Daňové schéma. Nejpoužívanější schémata jsou: VAT (daň z přidané hodnoty, používá se v ČR pro DIČ a na Slovensku pro IČ DPH) a TIN (používá se na Slovensku pro DIČ).
     * @see \Isdoc\Tests\Models\TaxCategory\GetSetTaxSchemeTest
     * @throws UnvalidTaxScheme
     */
    public function setTaxScheme(null|string $taxScheme): static
    {
        $this->taxScheme = TaxScheme::validate($taxScheme);
        return $this;
    }

    /**
     * Daňové schéma. Nejpoužívanější schémata jsou: VAT (daň z přidané hodnoty, používá se v ČR pro DIČ a na Slovensku pro IČ DPH) a TIN (používá se na Slovensku pro DIČ).
     * @see \Isdoc\Tests\Models\TaxCategory\GetSetTaxSchemeTest
     */
    public function getTaxScheme(): null|string
    {
        return $this->taxScheme;
    }

    /**
     * Je předmětem DPH
     * @see \Isdoc\Tests\Models\TaxCategory\GetSetVATAplicableTest
     */
    public function setVATAplicable(bool|null $vatAplicable): static
    {
        $this->vatApplicable = $vatAplicable;
        return $this;
    }

    /**
     * Je předmětem DPH
     * @see \Isdoc\Tests\Models\TaxCategory\GetSetVATAplicableTest
     */
    public function getVATAplicable(): bool|null
    {
        return $this->vatApplicable;
    }

    /**
     * Je daňová sazba v režimu přenesení daňové povinnosti
     * @see \Isdoc\Tests\Models\TaxCategory\GetSetLocalReverseChargeFlagTest
     */
    public function setLocalReverseChargeFlag(bool|null $localReverseChargeFlag): static
    {
        $this->localReverseChargeFlag = $localReverseChargeFlag;
        return $this;
    }

    /**
     * Je daňová sazba v režimu přenesení daňové povinnosti
     * @see \Isdoc\Tests\Models\TaxCategory\GetSetLocalReverseChargeFlagTest
     */
    public function getLocalReverseChargeFlag(): bool|null
    {
        return $this->localReverseChargeFlag;
    }
    
    /**
     * @see \Isdoc\Tests\Models\TaxCategory\ToXmlElementTest
     */
    public function toXmlElement(): IsdocSimpleXMLElement
    {
        $taxCategory = new IsdocSimpleXMLElement('<TaxCategory></TaxCategory>');
        $taxCategory->addChild('Percent', $this->getPercent());
        $taxCategory->addChildOptional('TaxScheme', $this->getTaxScheme());
        $taxCategory->addChildOptional('VATApplicable', $this->boolToStr($this->getVATAplicable()));
        $taxCategory->addChildOptional('LocalReverseChargeFlag', $this->boolToStr($this->getLocalReverseChargeFlag()));
        return $taxCategory;
    }
}