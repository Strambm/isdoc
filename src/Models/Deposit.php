<?php

namespace Isdoc\Models;

use Isdoc\Interfaces\RenderableInterface;

abstract class Deposit implements RenderableInterface
{
    protected $id = null;
    protected $vs = null;

    /**
     * Jméno dokladu, identifikace daňového zálohového listu u vystavitele
     * @see \Isdoc\Tests\Models\Deposit\GetSetIdTest
     */
    public function setId(string $id): static
    {
        $this->id = $id;
        return $this;
    }

    /**
     * Jméno dokladu, identifikace daňového zálohového listu u vystavitele
     * @see \Isdoc\Tests\Models\Deposit\GetSetIdTest
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * Variabilní symbol
     * @see \Isdoc\Tests\Models\Deposit\GetSetVariableSymbolTest
     */
    public function setVariableSymbol(string $vs): static
    {
        $this->vs = $vs;
        return $this;
    } 

    /**
     * Variabilní symbol
     * @see \Isdoc\Tests\Models\Deposit\GetSetVariableSymbolTest
     */
    public function getVariableSymbol(): string
    {
        return $this->vs;
    } 
}