<?php

namespace Isdoc\Models;

use Isdoc\Enums\LanguageCode;
use Isdoc\Traits\StringConversion;
use Isdoc\Exceptions\MissingNoteValue;
use Isdoc\Exceptions\MissingLanguageId;
use Isdoc\Exceptions\UnvalidLanguageId;
use Isdoc\Interfaces\RenderableInterface;

abstract class NoteType implements RenderableInterface
{
    use StringConversion;

    protected $value = null;
    protected $languageId = null;

    /**
     * Poznámka
     * @see \Isdoc\Tests\Models\NoteType\GetSetValueTest
     */
    public function setValue(string $note): static
    {
        $this->value = $note;
        return $this;
    }

    /**
     * Poznámka
     * @see \Isdoc\Tests\Models\NoteType\GetSetValueTest
     * @throws MissingNoteValue
     */
    public function getValue(): string
    {
        if(is_null($this->value)) {
            throw new MissingNoteValue();
        }
        return $this->value;
    }

    /**
     * Identifikátor použitého jazyka (např. "cs" pro češtinu)
     * @param string $languageId Kód z množiny jazykových kódů (ISO 639-1 language codes)
     * @throws UnvalidLanguageId
     */
    public function setLanguageId(string|null $languageId): static
    {
        if(!is_null($languageId)) {
            LanguageCode::validate($languageId);
        }
        $this->languageId = $languageId;
        return $this;
    }
    
    /**
     * Identifikátor použitého jazyka (např. "cs" pro češtinu)
     * @return string $languageId Kód z množiny jazykových kódů (ISO 639-1 language codes)
     */
    public function getLanguageId(): string|null
    {
        return $this->languageId;
    }

    /**
     * @see \Isdoc\Tests\Models\NoteType\ToXmlElementTest
     */
    public function toXmlElement(): IsdocSimpleXMLElement
    {
        $note = new IsdocSimpleXMLElement('<' . $this->getTagName() . '>' . $this->getValue() . '</' . $this->getTagName() . '>');
        if(!is_null($this->getLanguageId())) {
            $note->addAttribute('languageID', $this->getLanguageId());
        }
        return $note;
    }

    protected abstract function getTagName(): string;
}