<?php

namespace Isdoc\Models;

use Isdoc\Models\Price;
use Isdoc\Traits\StringConversion;
use Isdoc\Enums\VatCalculationMethod;
use Isdoc\Interfaces\RenderableInterface;
use Isdoc\Traits\HasForeignCurrencyAttribute;
use Isdoc\Exceptions\MissingClassifiedTaxCategory;

/**
 * Informace o konkrétní částce v sazbě na odúčtovaném daňovém zálohovém listu
 */
class TaxedDeposit implements RenderableInterface
{
    use StringConversion;
    use HasForeignCurrencyAttribute;
    
    protected $id = null;
    protected $vs = null;
    protected $classifiedTaxCategory = null;
    protected $taxableDepositAmount = null;
    protected $taxableDepositAmountCurr = null;
    protected $taxInclusiveDepositAmount = null;
    protected $taxInclusiveDepositAmountCurr = null;

    /**
     * Jméno dokladu, identifikace daňového zálohového listu u vystavitele
     * @see \Isdoc\Tests\Models\TaxedDeposit\GetSetIdTest
     */
    public function setId(string $id): static
    {
        $this->id = $id;
        return $this;
    }

    /**
     * Jméno dokladu, identifikace daňového zálohového listu u vystavitele
     * @see \Isdoc\Tests\Models\TaxedDeposit\GetSetIdTest
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * Variabilní symbol
     * @see \Isdoc\Tests\Models\TaxedDeposit\GetSetVariableSymbolTest
     */
    public function setVariableSymbol(string $vs): static
    {
        $this->vs = $vs;
        return $this;
    } 

    /**
     * Variabilní symbol
     * @see \Isdoc\Tests\Models\TaxedDeposit\GetSetVariableSymbolTest
     */
    public function getVariableSymbol(): string
    {
        return $this->vs;
    } 

    /**
     * Kladná část zálohy bez daně v cizí měně
     * @see \Isdoc\Tests\Models\TaxedDeposit\GetSetTaxableDepositAmountCurrTest
     */
    public function getTaxableDepositAmountCurr(): float|null
    {
        return $this->getForeignAmountForMethod(__FUNCTION__);
    }

    /**
     * Kladná část zálohy bez daně v cizí měně
     * @see \Isdoc\Tests\Models\TaxedDeposit\GetSetTaxableDepositAmountCurrTest
     */
    public function setTaxableDepositAmountCurr(float|null $value): static
    {
        $this->taxableDepositAmountCurr = $value;
        return $this;
    }

    /**
     * Kladná část zálohy bez daně v tuzemské měně
     * @see \Isdoc\Tests\Models\TaxedDeposit\GetSetTaxableDepositAmountTest
     */
    public function setTaxableDepositAmount(float|null $value): static
    {
        $this->taxableDepositAmount = $value;
        return $this;
    }

    /**
     * Kladná část zálohy bez daně v tuzemské měně
     * @see \Isdoc\Tests\Models\TaxedDeposit\GetSetTaxableDepositAmountTest
     */
    public function getTaxableDepositAmount(): float
    {
        if(!is_null($this->taxableDepositAmount)) {
            return $this->taxableDepositAmount;
        }

        $classifiedTaxCategory = $this->getClassifiedTaxCategory();
        if($classifiedTaxCategory->getVatCalculationMethod() == VatCalculationMethod::SHORA) {
            $price = new Price(
                $classifiedTaxCategory->getPercent(), 
                $classifiedTaxCategory->getVatCalculationMethod()
            );
            $price->setValue($this->getTaxInclusiveDepositAmount());
            return $price->getValueWithoutVat();
        }

        return (float) $this->taxableDepositAmount;
    }

    /**
     * Kladná část zálohy s daní v cizí měně
     * @see \Isdoc\Tests\Models\TaxedDeposit\GetSetTaxInclusiveDepositAmountCurrTest
     */
    public function getTaxInclusiveDepositAmountCurr(): float|null
    {
        return $this->getForeignAmountForMethod(__FUNCTION__);
    }

    /**
     * Kladná část zálohy s daní v cizí měně
     * @see \Isdoc\Tests\Models\TaxedDeposit\GetSetTaxInclusiveDepositAmountCurrTest
     */
    public function setTaxInclusiveDepositAmountCurr(float|null $value): static
    {
        $this->taxInclusiveDepositAmountCurr = $value;
        return $this;
    }

    /**
     * Kladná část zálohy s daní v tuzemské měně
     * @see \Isdoc\Tests\Models\TaxedDeposit\GetSetTaxInclusiveDepositAmountTest
     */
    public function setTaxInclusiveAmount(float|null $value): static
    {
        $this->taxInclusiveDepositAmount = $value;
        return $this;
    }

    /**
     * Kladná část zálohy s daní v tuzemské měně
     * @see \Isdoc\Tests\Models\TaxedDeposit\GetSetTaxInclusiveDepositAmountTest
     */
    public function getTaxInclusiveDepositAmount(): float
    {
        if(!is_null($this->taxInclusiveDepositAmount)) {
            return $this->taxInclusiveDepositAmount;
        }

        $classifiedTaxCategory = $this->getClassifiedTaxCategory();
        if($classifiedTaxCategory->getVatCalculationMethod() == VatCalculationMethod::ZDOLA) {
            $price = new Price(
                $classifiedTaxCategory->getPercent(), 
                $classifiedTaxCategory->getVatCalculationMethod()
            );
            $price->setValue($this->getTaxableDepositAmount());
            return $price->getValueWithVat();
        }

        return (float) $this->taxInclusiveDepositAmount;
    }

    /**
     * Složená položka DPH
     * @see \Isdoc\Tests\Models\TaxedDeposit\GetSetClassifiedTaxCategoryTest
     */
    public function setClassifiedTaxCategory(ClassifiedTaxCategory $classifiedTaxCategory): static
    {
        $this->classifiedTaxCategory = $classifiedTaxCategory;
        return $this;
    }

    /**
     * Složená položka DPH
     * @see \Isdoc\Tests\Models\TaxedDeposit\GetSetClassifiedTaxCategoryTest
     */
    public function getClassifiedTaxCategory(): ClassifiedTaxCategory
    {
        if(is_null($this->classifiedTaxCategory)) {
            throw new MissingClassifiedTaxCategory();
        }
        return $this->classifiedTaxCategory;
    }

    /**
     * @see \Isdoc\Tests\Models\TaxedDeposit\ToXmlElementTest
     */
    public function toXmlElement(): IsdocSimpleXMLElement
    {
        $xml = new IsdocSimpleXMLElement('<TaxedDeposit></TaxedDeposit>');
        $xml->addChild('ID', $this->getId());
        $xml->addChild('VariableSymbol', $this->getVariableSymbol());
        $xml->addChildOptional('TaxableDepositAmountCurr', $this->getTaxableDepositAmountCurr());
        $xml->addChild('TaxableDepositAmount', $this->getTaxableDepositAmount());
        $xml->addChildOptional('TaxInclusiveDepositAmountCurr', $this->getTaxInclusiveDepositAmountCurr());
        $xml->addChild('TaxInclusiveDepositAmount', $this->getTaxInclusiveDepositAmount());
        $xml->appendSimpleXMLElement($this->getClassifiedTaxCategory());
        return $xml;
    }
}