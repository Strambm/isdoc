<?php

namespace Isdoc\Models;

use Isdoc\Exceptions\MissingPartyIdentifivationId;
use Isdoc\Interfaces\RenderableInterface;

/**
 * Element identifikačních položek subjektu (firmy)
 */
class PartyIdentification implements RenderableInterface
{
    use \Isdoc\Traits\StringConversion;

    protected $userId = null;
    protected $catalogFirmIdentification = null;
    protected $id = null;

    /**
     * Uživatelské číslo firmy/provozovny
     * @see \Isdoc\Tests\Models\PartyIdentification\GetSetUserIdTest
     */
    public function setUserId(null|string $userId): static
    {
        $this->userId = $userId;
        return $this;
    }

    /**
     * Uživatelské číslo firmy/provozovny
     * @see \Isdoc\Tests\Models\PartyIdentification\GetSetUserIdTest
     */
    public function getUserId(): null|string
    {
        return $this->userId;
    }

    /**
     * Mezinárodní číslo firmy/provozovny, např. EAN
     * @see \Isdoc\Tests\Models\PartyIdentification\GetSetCatalogFirmIdentificationTest
     */
    public function setCatalogFirmIdentification(null|string $catalogFirmIdentification): static
    {
        $this->catalogFirmIdentification = $catalogFirmIdentification;
        return $this;
    }

    /**
     * Mezinárodní číslo firmy/provozovny, např. EAN
     * @see \Isdoc\Tests\Models\PartyIdentification\GetSetCatalogFirmIdentificationTest
     */
    public function getCatalogFirmIdentification(): null|string
    {
        return $this->catalogFirmIdentification;
    }

    /**
     * IČ
     * @see \Isdoc\Tests\Models\PartyIdentification\GetSetIdTest
     */
    public function setId(string $id): static
    {
        $this->id = $id;
        return $this;
    }

    /**
     * IČ
     * @see \Isdoc\Tests\Models\PartyIdentification\GetSetIdTest
     * @throws MissingPartyIdentifivationId
     */
    public function getId(): string
    {
        if(is_null($this->id)) {
            throw new MissingPartyIdentifivationId();
        }
        return $this->id;
    }

    /**
     * @see \Isdoc\Tests\Models\PartyIdentification\ToXmlElementTest
     */
    public function toXmlElement(): IsdocSimpleXMLElement
    {
        $partyIdentification = new IsdocSimpleXMLElement('<PartyIdentification></PartyIdentification>');
        $partyIdentification->addChildOptional('UserID', $this->getUserId());
        $partyIdentification->addChildOptional('CatalogFirmIdentification', $this->getCatalogFirmIdentification());
        $partyIdentification->addChild('ID', $this->getId());
        return $partyIdentification;
    }
}