<?php

namespace Isdoc\Models;

use Isdoc\Exceptions\MissingBic;
use Isdoc\Exceptions\MissingIban;
use Isdoc\Traits\NumberValidation;
use Isdoc\Exceptions\MissingBankId;
use Isdoc\Exceptions\MissingBankCode;
use Isdoc\Exceptions\InvalidNumericFormat;
use Isdoc\Traits\StringConversion;

/**
 * Bankovní účet
 */
class BankAccount
{ 
    use NumberValidation;
    use StringConversion;
    
    protected $id = null;
    protected $bankCode = null;
    protected $bankName = '';
    protected $iban = null;
    protected $bic = null;
    
    /** 
     * Číslo účtu lokální banky
     * @see \Isdoc\Tests\Models\BankAccount\GetSetIdTest
     */
    public function setId(string $id): static
    {
        $this->id = $id;
        return $this;
    }

    /** 
     * Číslo účtu lokální banky
     * @see \Isdoc\Tests\Models\BankAccount\GetSetIdTest
     * @throws MissingBankId
     */
    public function getId(): string
    {
        if(is_null($this->id)) {
            throw new MissingBankId();
        }
        return $this->id;
    }

    /** 
     * Kód lokální banky
     * @see \Isdoc\Tests\Models\BankAccount\GetSetBankCodeTest
     * @throws InvalidNumericFormat
     */
    public function setBankCode(string $bankCode): static
    {
        $this->validateNumeric($bankCode);
        $this->bankCode = $bankCode;
        return $this;
    }

    /** 
     * Kód lokální banky
     * @see \Isdoc\Tests\Models\BankAccount\GetSetBankCodeTest
     * @throws MissingBankCode
     */
    public function getBankCode(): string
    {
        if(is_null($this->bankCode)) {
            throw new MissingBankCode();
        }
        return $this->bankCode;
    }

    /** 
     * Název banky
     * @see \Isdoc\Tests\Models\BankAccount\GetSetNameTest
     */
    public function setName(string $name): static
    {
        $this->bankName = $name;
        return $this;
    }

    /** 
     * Název banky
     * @see \Isdoc\Tests\Models\BankAccount\GetSetNameTest
     */
    public function getName(): string
    {
        return $this->bankName;
    }

    /** 
     * Mezinárodní číslo účtu (IBAN)
     * @see \Isdoc\Tests\Models\BankAccount\GetSetIbanTest
     */
    public function setIban(string $iban): static
    {
        $this->iban = $iban;
        return $this;
    }

    /** 
     * Mezinárodní číslo účtu (IBAN)
     * @see \Isdoc\Tests\Models\BankAccount\GetSetIbanTest
     * @throws MissingIban
     */
    public function getIban(): string
    {
        if(is_null($this->iban)) {
            throw new MissingIban();
        }
        return $this->iban;
    }

    /** 
     * Kód banky podle ISO 9362, tzv. SWIFT kód
     * @see \Isdoc\Tests\Models\BankAccount\GetSetBicTest
     */
    public function setBic(string $bic): static
    {
        $this->bic = $bic;
        return $this;
    }

    /** 
     * Kód banky podle ISO 9362, tzv. SWIFT kód
     * @see \Isdoc\Tests\Models\BankAccount\GetSetBicTest
     * @throws MissingBic
     */
    public function getBic(): string
    {
        if(is_null($this->bic)) {
            throw new MissingBic();
        }
        return $this->bic;
    }

    /**
     * @see \Isdoc\Tests\Models\BankAccount\ToXmlElementTest
     */
    public function toXmlElement(): IsdocSimpleXMLElement
    {
        $bankAccountXml = new IsdocSimpleXMLElement('<AlternateBankAccount></AlternateBankAccount>');
        $bankAccountXml->addChild('ID', $this->getId());
        $bankAccountXml->addChild('BankCode', $this->getBankCode());
        $bankAccountXml->addChild('Name', $this->getName());
        $bankAccountXml->addChild('IBAN', $this->getIban());
        $bankAccountXml->addChild('BIC', $this->getBic());
        return $bankAccountXml;
    }
}