<?php

namespace Isdoc\Models;

use Isdoc\Traits\StringConversion;
use Isdoc\Interfaces\RenderableInterface;
use Isdoc\Traits\HasForeignCurrencyAttribute;

/**
 * Kolekce celkových částek na dokladu končící položkou částka k zaplacení
 */
class LegalMonetaryTotal implements RenderableInterface
{
    use StringConversion;
    use HasForeignCurrencyAttribute;

    protected $taxExclusiveAmount = 0;
    protected $taxExclusiveAmountCurr = null;
    protected $taxInclusiveAmount = 0;
    protected $taxInclusiveAmountCurr = null;
    protected $alreadyClaimedTaxExclusiveAmount = 0;
    protected $alreadyClaimedTaxExclusiveAmountCurr = null;
    protected $alreadyClaimedTaxInclusiveAmount = 0;
    protected $alreadyClaimedTaxInclusiveAmountCurr = null;
    protected $differenceTaxExclusiveAmount = null;
    protected $differenceTaxExclusiveAmountCurr = null;
    protected $differenceTaxInclusiveAmount = null;
    protected $differenceTaxInclusiveAmountCurr = null;
    protected $payableRoundingAmount = null;
    protected $payableRoundingAmountCurr = null;
    protected $paidDepositsAmount = 0;
    protected $paidDepositsAmountCurr = null;
    protected $payableAmount = null;
    protected $payableAmountCurr = null;

    public function __construct()
    {
        $this->throwManuallySettedForeignCurrencyValueWithoutExchangeRateEnable = false;
    }

    /** 
     * Celková částka bez daně v místní měně
     * @see \Isdoc\Tests\Models\LegalMonetaryTotal\GetSetTaxExclusiveAmountTest
     */
    public function setTaxExclusiveAmount(float $taxExclusiveAmount): static
    {
        $this->taxExclusiveAmount = $taxExclusiveAmount;
        return $this;
    }

    /** 
     * Celková částka bez daně v místní měně
     * @see \Isdoc\Tests\Models\LegalMonetaryTotal\GetSetTaxExclusiveAmountTest
     */
    public function getTaxExclusiveAmount(): float
    {
        return $this->taxExclusiveAmount;
    }

    /**
     * Celková částka bez daně v cizí měně
     * @see \Isdoc\Tests\Models\LegalMonetaryTotal\GetSetTaxExclusiveAmountCurrTest
     */
    public function setTaxExclusiveAmountCurr(null|float $taxExclusiveAmountCurr): static
    {
        $this->taxExclusiveAmountCurr = $taxExclusiveAmountCurr;
        return $this;
    }

    /**
     * Celková částka bez daně v cizí měně
     * @see \Isdoc\Tests\Models\LegalMonetaryTotal\GetSetTaxExclusiveAmountCurrTest
     */
    public function getTaxExclusiveAmountCurr(): null|float
    {
        return $this->getForeignAmountForMethod(__FUNCTION__);
    }

    /** 
     * Celková částka s daní daně v místní měně
     * @see \Isdoc\Tests\Models\LegalMonetaryTotal\GetSetTaxInclusiveAmountTest
     */
    public function setTaxInclusiveAmount(float $taxInclusiveAmount): static
    {
        $this->taxInclusiveAmount = $taxInclusiveAmount;
        return $this;
    }

    /** 
     * Celková částka s daní daně v místní měně
     * @see \Isdoc\Tests\Models\LegalMonetaryTotal\GetSetTaxInclusiveAmountTest
     */
    public function getTaxInclusiveAmount(): float
    {
        return $this->taxInclusiveAmount;
    }

    /** 
     * Celková částka s daní daně v cizí měně
     * @see \Isdoc\Tests\Models\LegalMonetaryTotal\GetSetTaxInclusiveAmountCurrTest
     */
    public function setTaxInclusiveAmountCurr(null|float $taxInclusiveAmountCurr): static
    {
        $this->taxInclusiveAmountCurr = $taxInclusiveAmountCurr;
        return $this;
    }

    /** 
     * Celková částka s daní daně v cizí měně
     * @see \Isdoc\Tests\Models\LegalMonetaryTotal\GetSetTaxInclusiveAmountCurrTest
     */
    public function getTaxInclusiveAmountCurr(): null|float
    {
        return $this->getForeignAmountForMethod(__FUNCTION__);
    }

    /** 
     * Celková částka bez daně v místní měně ze všech uplatněných daňových zálohových listů
     * @see \Isdoc\Tests\Models\LegalMonetaryTotal\GetSetAlreadyClaimedTaxExclusiveAmountTest
     */
    public function setAlreadyClaimedTaxExclusiveAmount(float $alreadyClaimedTaxExclusiveAmount): static
    {
        $this->alreadyClaimedTaxExclusiveAmount = $alreadyClaimedTaxExclusiveAmount;
        return $this;
    }

    /** 
     * Celková částka bez daně v místní měně ze všech uplatněných daňových zálohových listů
     * @see \Isdoc\Tests\Models\LegalMonetaryTotal\GetSetAlreadyClaimedTaxExclusiveAmountTest
     */
    public function getAlreadyClaimedTaxExclusiveAmount(): float
    {
        return $this->alreadyClaimedTaxExclusiveAmount;
    }

    /** 
     * Celková částka bez daně v cizí měně ze všech uplatněných daňových zálohových listů
     * @see \Isdoc\Tests\Models\LegalMonetaryTotal\GetSetAlreadyClaimedTaxExclusiveAmountCurrTest
     */
    public function setAlreadyClaimedTaxExclusiveAmountCurr(null|float $alreadyClaimedTaxExclusiveAmountCurr): static
    {
        $this->alreadyClaimedTaxExclusiveAmountCurr = $alreadyClaimedTaxExclusiveAmountCurr;
        return $this;
    }

    /** 
     * Celková částka bez daně v cizí měně ze všech uplatněných daňových zálohových listů
     * @see \Isdoc\Tests\Models\LegalMonetaryTotal\GetSetAlreadyClaimedTaxExclusiveAmountCurrTest
     */
    public function getAlreadyClaimedTaxExclusiveAmountCurr(): null|float
    {
        return $this->getForeignAmountForMethod(__FUNCTION__);
    }
    
    /** 
     * Celková částka s daní v místní měně ze všech uplatněných daňových zálohových listů3
     * @see \Isdoc\Tests\Models\LegalMonetaryTotal\GetSetAlreadyClaimedTaxInclusiveAmountTest
     */
    public function setAlreadyClaimedTaxInclusiveAmount(float $alreadyClaimedTaxInclusiveAmount): static
    {
        $this->alreadyClaimedTaxInclusiveAmount = $alreadyClaimedTaxInclusiveAmount;
        return $this;
    }

    /** 
     * Celková částka s daní v místní měně ze všech uplatněných daňových zálohových listů
     * @see \Isdoc\Tests\Models\LegalMonetaryTotal\GetSetAlreadyClaimedTaxInclusiveAmountTest
     */
    public function getAlreadyClaimedTaxInclusiveAmount(): float
    {
        return $this->alreadyClaimedTaxInclusiveAmount;
    }

    /** 
     * Celková částka s daní v cizí měně ze všech uplatněných daňových zálohových listů
     * @see \Isdoc\Tests\Models\LegalMonetaryTotal\GetSetAlreadyClaimedTaxInclusiveAmountCurrTest
     */
    public function setAlreadyClaimedTaxInclusiveAmountCurr(null|float $alreadyClaimedTaxInclusiveAmountCurr): static
    {
        $this->alreadyClaimedTaxInclusiveAmountCurr = $alreadyClaimedTaxInclusiveAmountCurr;
        return $this;
    }

    /** 
     * Celková částka s daní v cizí měně ze všech uplatněných daňových zálohových listů
     * @see \Isdoc\Tests\Models\LegalMonetaryTotal\GetSetAlreadyClaimedTaxInclusiveAmountCurrTest
     */
    public function getAlreadyClaimedTaxInclusiveAmountCurr(): null|float
    {
        return $this->getForeignAmountForMethod(__FUNCTION__);
    }

    /** 
     * Rozdíl mezi předpisem a "na záloze již uplatněno", bez daně v místní měně
     * @see \Isdoc\Tests\Models\LegalMonetaryTotal\GetSetDifferenceTaxExclusiveAmountTest
     */
    public function getDifferenceTaxExclusiveAmount(): float
    {
        if(!is_null($this->differenceTaxExclusiveAmount)) {
            return $this->differenceTaxExclusiveAmount;
        }

        return $this->getTaxExclusiveAmount() - $this->getAlreadyClaimedTaxExclusiveAmount();
    }

    /** 
     * Rozdíl mezi předpisem a "na záloze již uplatněno", bez daně v místní měně
     * @see \Isdoc\Tests\Models\LegalMonetaryTotal\GetSetDifferenceTaxExclusiveAmountTest
     */
    public function setDifferenceTaxExclusiveAmount(null|float $value): static
    {
        $this->differenceTaxExclusiveAmount = $value;
        return $this;
    }

    /** 
     * Rozdíl mezi předpisem a "na záloze již uplatněno", bez daně v cizí měně
     * @see \Isdoc\Tests\Models\LegalMonetaryTotal\GetSetDifferenceTaxExclusiveAmountCurrTest
     */
    public function setDifferenceTaxExclusiveAmountCurr(null|float $differenceTaxExclusiveAmountCurr): static
    {
        $this->differenceTaxExclusiveAmountCurr = $differenceTaxExclusiveAmountCurr;
        return $this;
    }

    /** 
     * Rozdíl mezi předpisem a "na záloze již uplatněno", bez daně v cizí měně
     * @see \Isdoc\Tests\Models\LegalMonetaryTotal\GetSetDifferenceTaxExclusiveAmountCurrTest
     */
    public function getDifferenceTaxExclusiveAmountCurr(): null|float
    {
        return $this->getForeignAmountForMethod(__FUNCTION__);
    }

    /** 
     * Rozdíl mezi předpisem a "na záloze již uplatněno", s daní v místní měně
     * @see \Isdoc\Tests\Models\LegalMonetaryTotal\GetSetDifferenceTaxInclusiveAmountTest
     */
    public function setDifferenceTaxInclusiveAmount(null|float $differenceTaxInclusiveAmount): static
    {
        $this->differenceTaxInclusiveAmount = $differenceTaxInclusiveAmount;
        return $this;
    }

    /** 
     * Rozdíl mezi předpisem a "na záloze již uplatněno", s daní v místní měně
     * @see \Isdoc\Tests\Models\LegalMonetaryTotal\GetSetDifferenceTaxInclusiveAmountTest
     */
    public function getDifferenceTaxInclusiveAmount(): float
    {
        if(!is_null($this->differenceTaxInclusiveAmount)) {
            return $this->differenceTaxInclusiveAmount;
        }

        return $this->getTaxInclusiveAmount() - $this->getAlreadyClaimedTaxInclusiveAmount();
    }

    /** 
     * Rozdíl mezi předpisem a "na záloze již uplatněno", s daní v cizí měně
     * @see \Isdoc\Tests\Models\LegalMonetaryTotal\GetSetDifferenceTaxInclusiveAmountCurrTest
     */
    public function setDifferenceTaxInclusiveAmountCurr(null|float $differenceTaxInclusiveAmountCurr): static
    {
        $this->differenceTaxInclusiveAmountCurr = $differenceTaxInclusiveAmountCurr;
        return $this;
    }

    /** 
     * Rozdíl mezi předpisem a "na záloze již uplatněno", s daní v cizí měně
     * @see \Isdoc\Tests\Models\LegalMonetaryTotal\GetSetDifferenceTaxInclusiveAmountCurrTest
     */
    public function getDifferenceTaxInclusiveAmountCurr(): null|float
    {
        return $this->getForeignAmountForMethod(__FUNCTION__);
    }

    /** 
     * Zaokrouhlení celé částky v tuzemské měně
     * @see \Isdoc\Tests\Models\LegalMonetaryTotal\GetSetPayableRoundingAmountTest
     */
    public function setPayableRoundingAmount(null|float $payableRoundingAmount): static
    {
        $this->payableRoundingAmount = $payableRoundingAmount;
        return $this;
    }

    /** 
     * Zaokrouhlení celé částky v tuzemské měně
     * @see \Isdoc\Tests\Models\LegalMonetaryTotal\GetSetPayableRoundingAmountTest
     */
    public function getPayableRoundingAmount(): null|float
    {
        return $this->payableRoundingAmount;
    }

    /** 
     * Zaokrouhlení celé částky v cizí měně
     * @see \Isdoc\Tests\Models\LegalMonetaryTotal\GetSetPayableRoundingAmountCurrTest
     */
    public function setPayableRoundingAmountCurr(null|float $payableRoundingAmountCurr): static
    {
        $this->payableRoundingAmountCurr = $payableRoundingAmountCurr;
        return $this;
    }

    /** 
     * Zaokrouhlení celé částky v cizí měně
     * @see \Isdoc\Tests\Models\LegalMonetaryTotal\GetSetPayableRoundingAmountCurrTest
     */
    public function getPayableRoundingAmountCurr(): null|float
    {
        return $this->payableRoundingAmountCurr;
    }

    /** 
     * Na nedaňové záloze zaplaceno v tuzemské měně
     * @see \Isdoc\Tests\Models\LegalMonetaryTotal\GetSetPaidDepositsAmountTest
     */
    public function setPaidDepositsAmount(float $paidDepositsAmount): static
    {
        $this->paidDepositsAmount = $paidDepositsAmount;
        return $this;
    }

    /** 
     * Na nedaňové záloze zaplaceno v tuzemské měně
     * @see \Isdoc\Tests\Models\LegalMonetaryTotal\GetSetPaidDepositsAmountTest
     */
    public function getPaidDepositsAmount(): float
    {
        return $this->paidDepositsAmount;
    }

    /** 
     * Na nedaňové záloze zaplaceno v cizí měně
     * @see \Isdoc\Tests\Models\LegalMonetaryTotal\GetSetPaidDepositsAmountCurrTest
     */
    public function setPaidDepositsAmountCurr(null|float $paidDepositsAmountCurr): static
    {
        $this->paidDepositsAmountCurr = $paidDepositsAmountCurr;
        return $this;
    }

    /** 
     * Na nedaňové záloze zaplaceno v cizí měně
     * @see \Isdoc\Tests\Models\LegalMonetaryTotal\GetSetPaidDepositsAmountCurrTest
     */
    public function getPaidDepositsAmountCurr(): null|float
    {
        return $this->getForeignAmountForMethod(__FUNCTION__);
    }

    /** 
     * K zaplacení v tuzemské měně
     * @see \Isdoc\Tests\Models\LegalMonetaryTotal\GetSetPayableAmountTest
     */
    public function setPayableAmount(null|float $payableAmount): static
    {
        $this->payableAmount = $payableAmount;
        return $this;
    }

    /** 
     * K zaplacení v tuzemské měně
     * @see \Isdoc\Tests\Models\LegalMonetaryTotal\GetSetPayableAmountTest
     */
    public function getPayableAmount(): float
    {
        if(!is_null($this->payableAmount)) {
            return $this->payableAmount;
        }

        return $this->getDifferenceTaxInclusiveAmount() + $this->getPayableRoundingAmount() - $this->getPaidDepositsAmount();
    }

    /** 
     * K zaplacení v cizí měně
     * @see \Isdoc\Tests\Models\LegalMonetaryTotal\GetSetPayableAmountCurrTest
     */
    public function setPayableAmountCurr(null|float $payableAmountCurr): static
    {
        $this->payableAmountCurr = $payableAmountCurr;
        return $this;
    }

    /** 
     * K zaplacení v cizí měně
     * @see \Isdoc\Tests\Models\LegalMonetaryTotal\GetSetPayableAmountCurrTest
     */
    public function getPayableAmountCurr(): null|float
    {
        return $this->getForeignAmountForMethod(__FUNCTION__);
    }

    /**
     * @see \Isdoc\Tests\Models\LegalMonetaryTotal\ToXmlElementTest
     */
    public function toXmlElement(): IsdocSimpleXMLElement
    {
        $legalMonetaryTotal = new IsdocSimpleXMLElement('<LegalMonetaryTotal></LegalMonetaryTotal>');
        $legalMonetaryTotal->addChild('TaxExclusiveAmount', $this->getTaxExclusiveAmount());
        $legalMonetaryTotal->addChildOptional('TaxExclusiveAmountCurr', $this->getTaxExclusiveAmountCurr());
        $legalMonetaryTotal->addChild('TaxInclusiveAmount', $this->getTaxInclusiveAmount());
        $legalMonetaryTotal->addChildOptional('TaxInclusiveAmountCurr', $this->getTaxInclusiveAmountCurr());
        $legalMonetaryTotal->addChild('AlreadyClaimedTaxExclusiveAmount', $this->getAlreadyClaimedTaxExclusiveAmount());
        $legalMonetaryTotal->addChildOptional('AlreadyClaimedTaxExclusiveAmountCurr', $this->getAlreadyClaimedTaxExclusiveAmountCurr());
        $legalMonetaryTotal->addChild('AlreadyClaimedTaxInclusiveAmount', $this->getAlreadyClaimedTaxInclusiveAmount());
        $legalMonetaryTotal->addChildOptional('AlreadyClaimedTaxInclusiveAmountCurr', $this->getAlreadyClaimedTaxInclusiveAmountCurr());
        $legalMonetaryTotal->addChild('DifferenceTaxExclusiveAmount', $this->getDifferenceTaxExclusiveAmount());
        $legalMonetaryTotal->addChildOptional('DifferenceTaxExclusiveAmountCurr', $this->getDifferenceTaxExclusiveAmountCurr());
        $legalMonetaryTotal->addChild('DifferenceTaxInclusiveAmount', $this->getDifferenceTaxInclusiveAmount());
        $legalMonetaryTotal->addChildOptional('DifferenceTaxInclusiveAmountCurr', $this->getDifferenceTaxInclusiveAmountCurr());
        $legalMonetaryTotal->addChildOptional('PayableRoundingAmount', $this->getPayableRoundingAmount());
        $legalMonetaryTotal->addChildOptional('PayableRoundingAmountCurr', $this->getPayableRoundingAmountCurr());
        $legalMonetaryTotal->addChild('PaidDepositsAmount', $this->getPaidDepositsAmount());
        $legalMonetaryTotal->addChildOptional('PaidDepositsAmountCurr', $this->getPaidDepositsAmountCurr());
        $legalMonetaryTotal->addChild('PayableAmount', $this->getPayableAmount());
        $legalMonetaryTotal->addChildOptional('PayableAmountCurr', $this->getPayableAmountCurr());

        return $legalMonetaryTotal;
    }
}
