<?php

namespace Isdoc\Models;

/**
 * Poznámka
 */
class Note extends NoteType
{
    protected function getTagName(): string
    {
        return 'Note';
    }
}