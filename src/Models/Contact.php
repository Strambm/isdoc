<?php

namespace Isdoc\Models;

use Isdoc\Traits\StringConversion;
use Isdoc\Interfaces\RenderableInterface;

/**
 * Kontaktní osoba
 */
class Contact implements RenderableInterface
{
    use StringConversion;
    
    protected $name = null;
    protected $telephone = null;
    protected $electronicMail = null;

    /**
     * Jméno kontaktu
     * @see \Isdoc\Tests\Models\Contact\GetSetNameTest
     */
    public function setName(null|string $name): static
    {
        $this->name = $name;
        return $this;
    }

    /**
     * Jméno kontaktu
     * @see \Isdoc\Tests\Models\Contact\GetSetNameTest
     */
    public function getName(): null|string
    {
        return $this->name;
    }

    /**
     * Telefonní číslo
     * @see \Isdoc\Tests\Models\Contact\GetSetTelephoneTest
     */
    public function setTelephone(null|string $telephone): static
    {
        $this->telephone = $telephone;
        return $this;
    }

    /**
     * Telefonní číslo
     * @see \Isdoc\Tests\Models\Contact\GetSetTelephoneTest
     */
    public function getTelephone(): null|string
    {
        return $this->telephone;
    }

    /**
     * E-mailová adresa
     * @see \Isdoc\Tests\Models\Contact\GetSetElectronicMailTest
     */
    public function setElectronicMail(null|string $electronicMail): static
    {
        $this->electronicMail = $electronicMail;
        return $this;
    }

    /**
     * E-mailová adresa
     * @see \Isdoc\Tests\Models\Contact\GetSetElectronicMailTest
     */
    public function getElectronicMail(): null|string
    {
        return $this->electronicMail;
    }

    /**
     * @see \Isdoc\Tests\Models\Contact\ToXmlElementTest
     */
    public function toXmlElement(): IsdocSimpleXMLElement
    {
        $contact = new IsdocSimpleXMLElement('<Contact></Contact>');
        $contact->addChildOptional('Name', $this->getName());
        $contact->addChildOptional('Telephone', $this->getTelephone());
        $contact->addChildOptional('ElectronicMail', $this->getElectronicMail());
        return $contact;
    }
}
