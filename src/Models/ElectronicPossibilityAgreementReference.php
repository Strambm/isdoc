<?php

namespace Isdoc\Models;

/**
 * Označení dokumentu, kterým dal příjemce vystaviteli souhlas s elektronickou formou faktury
 */
class ElectronicPossibilityAgreementReference extends NoteType
{
    protected function getTagName(): string
    {
        return 'ElectronicPossibilityAgreementReference';
    }
}
