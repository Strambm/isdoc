<?php

namespace Isdoc\Models;

/**
 * Citace paragrafu zákona, podle kterého je řádka osvobozena od DPH
 */
class VatNote extends NoteType
{
    protected function getTagName(): string
    {
        return 'VATNote';
    }
}