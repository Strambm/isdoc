<?php

namespace Isdoc\Models;

use Isdoc\Traits\StringConversion;
use Isdoc\Enums\VatCalculationMethod;
use Isdoc\Models\InvoiceLineReference;
use Isdoc\Exceptions\InvalidLineIdLength;
use Isdoc\Interfaces\RenderableInterface;
use Isdoc\Exceptions\UnimplementedException;
use Isdoc\Traits\HasForeignCurrencyAttribute;
use Isdoc\Exceptions\MissingClassifiedTaxCategory;
use Isdoc\Exceptions\ManuallySettedForeignCurrencyValueWithoutExchangeRate;

/**
 * Jednotlivý řádek faktury
 */
class InvoiceLine implements RenderableInterface
{
    use StringConversion;
    use HasForeignCurrencyAttribute;

    protected $id = '';
    protected $orderReference = null;
    protected $invoicedQuantity = null;
    protected $invoicedQuantityUnitCode = null;
    protected $lineExtensionAmount = null;
    protected $lineExtensionAmountCurr = null;
    protected $lineExtensionAmountBeforeDiscount = null;
    protected $lineExtensionAmountTaxInclusive = null;
    protected $lineExtensionAmountTaxInclusiveCurr = null;
    protected $lineExtensionAmountTaxInclusiveBeforeDiscount = null;
    protected $lineExtensionTaxAmount = null;
    protected $unitPrice = null;
    protected $unitPriceTaxInclusive = null;
    protected $classifiedTaxCategory = null;
    protected $note = null;
    protected $vatNote = null;
    protected $item = null;
    protected $deliveryNoteReference = null;
    protected $originalDocumentReference = null;

    /** 
     * Unikátní alfanumerický identifikátor řádku dokladu
     * @param string $id (s maximální délkou 36 znaků)
     * @see \Isdoc\Tests\Models\InvoiceLine\GetSetIdTest
     * @throws InvalidLineIdLength
     */
    public function setId(string $id): static
    {
        if (strlen($id) > 36) {
            throw new InvalidLineIdLength();
        }
        $this->id = $id;
        return $this;
    }

    /**
     * Unikátní alfanumerický identifikátor řádku dokladu
     * @see \Isdoc\Tests\Models\InvoiceLine\GetSetIdTest
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * Odkaz na řádku související objednávky
     * @see \Isdoc\Tests\Models\InvoiceLine\GetSetOrderReferenceTest
     */
    public function setOrderReference(InvoiceLineReference|null $orderReference): static
    {
        $this->orderReference = $orderReference;
        return $this;
    }

    /**
     * Odkaz na řádku související objednávky
     * @see \Isdoc\Tests\Models\InvoiceLine\GetSetOrderReferenceTest
     */
    public function getOrderReference(): InvoiceLineReference|null
    {
        return $this->orderReference;
    }

    /**
     * Odkaz na související dodací list
     * @see \Isdoc\Tests\Models\InvoiceLine\GetSetDeliveryNoteReferenceTest
     */
    public function setDeliveryNoteReference(InvoiceLineReference|null $deliveryNoteReference): static
    {
        $this->deliveryNoteReference = $deliveryNoteReference;
        return $this;
    }

    /**
     * Odkaz na související dodací list
     * @see \Isdoc\Tests\Models\InvoiceLine\GetSetDeliveryNoteReferenceTest
     */
    public function getDeliveryNoteReference(): InvoiceLineReference|null
    {
        return $this->deliveryNoteReference;
    }

    /**
     * Odkaz na původní doklad, který tento aktuální doklad opravuje (jen pro typy dokumentů 2, 3 a 6)
     * @see \Isdoc\Tests\Models\InvoiceLine\GetSetOriginalDocumentReferenceTest
     */
    public function setOriginalDocumentReference(InvoiceLineReference|null $originalDocumentReference): static
    {
        $this->originalDocumentReference = $originalDocumentReference;
        return $this;
    }

    /**
     * Odkaz na původní doklad, který tento aktuální doklad opravuje (jen pro typy dokumentů 2, 3 a 6)
     * @see \Isdoc\Tests\Models\InvoiceLine\GetSetOriginalDocumentReferenceTest
     */
    public function getOriginalDocumentReference(): InvoiceLineReference|null
    {
        return $this->originalDocumentReference;
    }

    /**
     * Účtované množství
     * @see \Isdoc\Tests\Models\InvoiceLine\GetSetInvoicedQuantityTest
     */
    public function setInvoicedQuantity(float|null $invoicedQuantity): static
    {
        $this->invoicedQuantity = $invoicedQuantity;
        return $this;
    }

    /**
     * Účtované množství
     * @see \Isdoc\Tests\Models\InvoiceLine\GetSetInvoicedQuantityTest
     */
    public function getInvoicedQuantity(): float|null
    {
        return $this->invoicedQuantity;
    }

    /**
     * Účtované množství - jednotka
     * @see \Isdoc\Tests\Models\InvoiceLine\GetSetInvoiceQuantityUnitCodeTest
     */
    public function setInvoiceQuantityUnitCode(string|null $unitCode): static
    {
        $this->invoicedQuantityUnitCode = $unitCode;
        return $this;
    }

    /**
     * Účtované množství - jednotka
     * @see \Isdoc\Tests\Models\InvoiceLine\GetSetInvoiceQuantityUnitCodeTest
     */
    public function getInvoiceQuantityUnitCode(): string|null
    {
        return $this->invoicedQuantityUnitCode;
    }

    /**
     * Celková cena včetně daně na řádku v cizí měně
     * @see \Isdoc\Tests\Models\InvoiceLine\GetSetLineExtensionAmountCurrTest
     */
    public function setLineExtensionAmountCurr(float|null $value): static
    {
        $this->lineExtensionAmountCurr = $value;
        return $this;
    }

    /**
     * Celková cena včetně daně na řádku v cizí měně
     * @see \Isdoc\Tests\Models\InvoiceLine\GetSetLineExtensionAmountCurrTest
     * @throws ManuallySettedForeignCurrencyValueWithoutExchangeRate
     */
    public function getLineExtensionAmountCurr(): float|null
    {
        return $this->getForeignAmountForMethod(__FUNCTION__);
    }

    /** 
     * Celková cena bez daně na řádku v tuzemské měně
     * @see \Isdoc\Tests\Models\InvoiceLine\GetSetLineExtensionAmountTest
     **/
    public function setLineExtensionAmount(float|null $value): static
    {
        $this->lineExtensionAmount = $value;
        return $this;
    }

    /** 
     * Celková cena bez daně na řádku v tuzemské měně
     * @see \Isdoc\Tests\Models\InvoiceLine\GetSetLineExtensionAmountTest
     **/
    public function getLineExtensionAmount(): float
    {
        if(!is_null($this->lineExtensionAmount)) {
            return $this->lineExtensionAmount;
        }

        $classifiedTaxCategory = $this->getClassifiedTaxCategory();
        if($classifiedTaxCategory->getVatCalculationMethod() == VatCalculationMethod::SHORA) {
            $price = new Price(
                $classifiedTaxCategory->getPercent(), 
                $classifiedTaxCategory->getVatCalculationMethod()
            );
            $price->setValue($this->getLineExtensionAmountTaxInclusive());
            return $price->getValueWithoutVat();
        }

        return 0;
    }

    /**
     * Celková cena bez daně na řádku v tuzemské měně před slevou
     * @see \Isdoc\Tests\Models\InvoiceLine\GetSetLineExtensionAmountBeforeDiscountTest
     */
    public function setLineExtensionAmountBeforeDiscount(float|null $value): static
    {
        $this->lineExtensionAmountBeforeDiscount = $value;
        return $this;
    }

    /**
     * Celková cena bez daně na řádku v tuzemské měně před slevou
     * @see \Isdoc\Tests\Models\InvoiceLine\GetSetLineExtensionAmountBeforeDiscountTest
     */
    public function getLineExtensionAmountBeforeDiscount(): float|null
    {
        if(!is_null($this->lineExtensionAmountBeforeDiscount)) {
            return $this->lineExtensionAmountBeforeDiscount;
        }

        $classifiedTaxCategory = $this->getClassifiedTaxCategory();
        if(
            $classifiedTaxCategory->getVatCalculationMethod() == VatCalculationMethod::SHORA
                && !is_null($this->getLineExtensionAmountTaxInclusiveBeforeDiscount())
        ) {
            $price = new Price(
                $classifiedTaxCategory->getPercent(), 
                $classifiedTaxCategory->getVatCalculationMethod()
            );
            $price->setValue($this->getLineExtensionAmountTaxInclusiveBeforeDiscount());
            return $price->getValueWithoutVat();
        }

        return null;
    }

    /** 
     * Celková cena včetně daně na řádku v cizí měně
     * @see \Isdoc\Tests\Models\InvoiceLine\GetSetLineExtensionAmountTaxInclusiveCurrTest
     */
    public function setLineExtensionAmountTaxInclusiveCurr(float|null $value): static
    {
        $this->lineExtensionAmountTaxInclusiveCurr = $value;
        return $this;
    }

    /** 
     * Celková cena včetně daně na řádku v cizí měně
     * @see \Isdoc\Tests\Models\InvoiceLine\GetSetLineExtensionAmountTaxInclusiveCurrTest
     */
    public function getLineExtensionAmountTaxInclusiveCurr(): float|null
    {
        return $this->getForeignAmountForMethod(__FUNCTION__);
    }

    /** 
     * Celková cena včetně daně na řádku v tuzemské měně
     * @see \Isdoc\Tests\Models\InvoiceLine\GetSetLineExtensionAmountTaxInclusiveTest
     **/
    public function setLineExtensionAmountTaxInclusive(float|null $value): static
    {
        $this->lineExtensionAmountTaxInclusive = $value;
        return $this;
    }

    /**
     * Celková cena včetně daně na řádku v tuzemské měně
     * @see \Isdoc\Tests\Models\InvoiceLine\GetSetLineExtensionAmountTaxInclusiveTest
     */
    public function getLineExtensionAmountTaxInclusive(): float
    {
        if(!is_null($this->lineExtensionAmountTaxInclusive)) {
            return $this->lineExtensionAmountTaxInclusive;
        }

        $classifiedTaxCategory = $this->getClassifiedTaxCategory();
        if($classifiedTaxCategory->getVatCalculationMethod() == VatCalculationMethod::ZDOLA) {
            $price = new Price(
                $classifiedTaxCategory->getPercent(), 
                $classifiedTaxCategory->getVatCalculationMethod()
            );
            $price->setValue($this->getLineExtensionAmount());
            return $price->getValueWithVat();
        }

        return 0;
    }

    /**
     * Celková cena včetně daně na řádku v tuzemské měně před slevou
     * @see \Isdoc\Tests\Models\InvoiceLine\GetSetLineExtensionAmountTaxInclusiveBeforeDiscountTest
     */
    public function setLineExtensionAmountTaxInclusiveBeforeDiscount(float|null $value): static
    {
        $this->lineExtensionAmountTaxInclusiveBeforeDiscount = $value;
        return $this;
    }

    /**
     * Celková cena včetně daně na řádku v tuzemské měně před slevou
     * @see \Isdoc\Tests\Models\InvoiceLine\GetSetLineExtensionAmountTaxInclusiveBeforeDiscountTest
     */
    public function getLineExtensionAmountTaxInclusiveBeforeDiscount(): float|null
    {
        if(!is_null($this->lineExtensionAmountTaxInclusiveBeforeDiscount)) {
            return $this->lineExtensionAmountTaxInclusiveBeforeDiscount;
        }

        $classifiedTaxCategory = $this->getClassifiedTaxCategory();
        if(
            $classifiedTaxCategory->getVatCalculationMethod() == VatCalculationMethod::ZDOLA
                && !is_null($this->getLineExtensionAmountBeforeDiscount())
        ) {
            $price = new Price(
                $classifiedTaxCategory->getPercent(), 
                $classifiedTaxCategory->getVatCalculationMethod()
            );
            $price->setValue($this->getLineExtensionAmountBeforeDiscount());
            return $price->getValueWithVat();
        }

        return null;
    }

    /** 
     * Částka daně na řádku v tuzemské měně
     * @see \Isdoc\Tests\Models\InvoiceLine\GetSetLineExtensionTaxAmountTest
     */
    public function setLineExtensionTaxAmount(float|null $value): static
    {
        $this->lineExtensionTaxAmount = $value;
        return $this;
    }

    /** 
     * Částka daně na řádku v tuzemské měně
     * @see \Isdoc\Tests\Models\InvoiceLine\GetSetLineExtensionTaxAmountTest
     */
    public function getLineExtensionTaxAmount(): float
    {
        if(!is_null($this->lineExtensionTaxAmount)) {
            return $this->lineExtensionTaxAmount;
        }

        $classifiedTaxCategory = $this->getClassifiedTaxCategory();
        switch($classifiedTaxCategory->getVatCalculationMethod()) {
            case VatCalculationMethod::SHORA:
                $priceValue = $this->getLineExtensionAmountTaxInclusive();
                break;
            case VatCalculationMethod::ZDOLA:
                $priceValue = $this->getLineExtensionAmount();
                break;
            default:
                throw new UnimplementedException('V \'' . static::class . '::' . __FUNCTION__ . '\' chybí implementace pro VatCalculationMethod = ' . $classifiedTaxCategory->getVatCalculationMethod());
        }
        $price = new Price(
            $classifiedTaxCategory->getPercent(), 
            $classifiedTaxCategory->getVatCalculationMethod()
        );
        $price->setValue($priceValue);
        return $price->getValueOfVat();
    }

    /** 
     * Jednotková cena bez daně na řádku v tuzemské měně
     * @see \Isdoc\Tests\Models\InvoiceLine\GetSetUnitPriceTest
     */
    public function setUnitPrice(float|null $value): static
    {
        $this->unitPrice = $value;
        return $this;
    }

    /** 
     * Jednotková cena bez daně na řádku v tuzemské měně
     * @see \Isdoc\Tests\Models\InvoiceLine\GetSetUnitPriceTest
     */
    public function getUnitPrice(): float
    {
        if(!is_null($this->unitPrice)) {
            return $this->unitPrice;
        } else {
            return $this->perInvoiceQuantity($this->getLineExtensionAmount());
        }
    }

    /**
     * Jednotková cena s daní na řádku v tuzemské měně
     * @see \Isdoc\Tests\Models\InvoiceLine\GetSetUnitPriceTaxInclusiveTest
     */
    public function setUnitPriceTaxInclusive(float|null $value): static
    {
        $this->unitPriceTaxInclusive = $value;
        return $this;
    }

    /**
     * Jednotková cena s daní na řádku v tuzemské měně
     * @see \Isdoc\Tests\Models\InvoiceLine\GetSetUnitPriceTaxInclusiveTest
     */
    public function getUnitPriceTaxInclusive(): float
    {
        if(!is_null($this->unitPriceTaxInclusive)) {
            return $this->unitPriceTaxInclusive;
        } else {
            return $this->perInvoiceQuantity($this->getLineExtensionAmountTaxInclusive());
        }
    }

    private function perInvoiceQuantity(float $value): float
    {
        if($this->getInvoicedQuantity() == 0) {
            return $value;
        } else {
            return $value / $this->getInvoicedQuantity();
        }
    }

    /**
     * Složená položka DPH
     * @see \Isdoc\Tests\Models\InvoiceLine\GetSetClassifiedTaxCategoryTest
     */
    public function setClassifiedTaxCategory(ClassifiedTaxCategory $classifiedTaxCategory): static
    {
        $this->classifiedTaxCategory = $classifiedTaxCategory;
        return $this;
    }

    /**
     * Složená položka DPH
     * @see \Isdoc\Tests\Models\InvoiceLine\GetSetClassifiedTaxCategoryTest
     * @throws MissingClassifiedTaxCategory
     */
    public function getClassifiedTaxCategory(): ClassifiedTaxCategory
    {
        if(is_null($this->classifiedTaxCategory)) {
            throw new MissingClassifiedTaxCategory();
        }
        return $this->classifiedTaxCategory;
    }

    /**
     * Poznámka
     * @see \Isdoc\Tests\Models\InvoiceLine\GetSetNoteTest
     */
    public function setNote(Note|null $note): static
    {
        $this->note = $note;
        return $this;
    }

    /**
     * Poznámka
     * @see \Isdoc\Tests\Models\InvoiceLine\GetSetNoteTest
     */
    public function getNote(): Note|null
    {
        return $this->note;
    }

    /**
     * Citace paragrafu zákona, podle kterého je řádka osvobozena od DPH
     * @see \Isdoc\Tests\Models\InvoiceLine\GetSetVatNoteTest
     */
    public function setVatNote(VatNote|null $note): static
    {
        $this->vatNote = $note;
        return $this;
    }

    /**
     * Citace paragrafu zákona, podle kterého je řádka osvobozena od DPH
     * @see \Isdoc\Tests\Models\InvoiceLine\GetSetVatNoteTest
     */
    public function getVatNote(): VatNote|null
    {
        return $this->vatNote;
    }

    /** 
     * Informace o položce faktury 
     * @see \Isdoc\Tests\Models\InvoiceLine\GetSetItemTest
     */
    public function setItem(Item|null $item): static
    {
        $this->item = $item;
        return $this;
    }

    /** 
     * Informace o položce faktury 
     * @see \Isdoc\Tests\Models\InvoiceLine\GetSetItemTest
     */
    public function getItem(): Item|null
    {
        return $this->item;
    }

    /**
     * @see \Isdoc\Tests\Models\InvoiceLine\ToXmlElementTest
     */
    public function toXmlElement(): IsdocSimpleXMLElement
    {
        $invoiceLine = new IsdocSimpleXMLElement('<InvoiceLine></InvoiceLine>');
        $invoiceLine->addChild('ID', $this->getId());
        $this->appendXmlReference($invoiceLine, 'OrderReference');
        $this->appendXmlReference($invoiceLine, 'DeliveryNoteReference');
        $this->appendXmlReference($invoiceLine, 'OriginalDocumentReference');
        if(!is_null($this->getInvoicedQuantity())) {
            $invoicedQuantity = $invoiceLine->addChild('InvoicedQuantity', $this->getInvoicedQuantity());
            if(!is_null($this->getInvoiceQuantityUnitCode())) {
                $invoicedQuantity->addAttribute('unitCode', $this->getInvoiceQuantityUnitCode());
            }
        }
        $invoiceLine->addChildOptional('LineExtensionAmountCurr', $this->getLineExtensionAmountCurr());
        $invoiceLine->addChild('LineExtensionAmount', $this->getLineExtensionAmount());
        $invoiceLine->addChildOptional('LineExtensionAmountBeforeDiscount', $this->getLineExtensionAmountBeforeDiscount());
        $invoiceLine->addChildOptional('LineExtensionAmountTaxInclusiveCurr', $this->getLineExtensionAmountTaxInclusiveCurr());
        $invoiceLine->addChild('LineExtensionAmountTaxInclusive', $this->getLineExtensionAmountTaxInclusive());
        $invoiceLine->addChildOptional('LineExtensionAmountTaxInclusiveBeforeDiscount', $this->getLineExtensionAmountTaxInclusiveBeforeDiscount());
        $invoiceLine->addChild('LineExtensionTaxAmount', $this->getLineExtensionTaxAmount());
        $invoiceLine->addChild('UnitPrice', $this->getUnitPrice());
        $invoiceLine->addChild('UnitPriceTaxInclusive', $this->getUnitPriceTaxInclusive());
        $invoiceLine->appendSimpleXMLElement($this->getClassifiedTaxCategory());
        $invoiceLine->appendSimpleXMLElementOptional($this->getNote());
        $invoiceLine->appendSimpleXMLElementOptional($this->getVatNote());
        $invoiceLine->appendSimpleXMLElementOptional($this->getItem());
        return $invoiceLine;
    }  

    private function appendXmlReference(IsdocSimpleXMLElement &$xml, string $name): void
    {
        $getMethod = 'get' . $name;
        /** @var null|InvoiceLineReference $referenceModel */
        $referenceModel = $this->$getMethod();
        if(!is_null($referenceModel)) {
            $reference = new IsdocSimpleXMLElement('<' . $name . '></' . $name . '>');
            $reference->addAttribute('ref', $referenceModel->getRef());
            $reference->addChild('LineID', $referenceModel->getLineId());
            $xml->appendSimpleXMLElement($reference);
        } 
    }
}
