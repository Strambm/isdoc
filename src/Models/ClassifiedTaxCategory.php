<?php

namespace Isdoc\Models;

use Isdoc\Traits\BoolValidation;
use Isdoc\Traits\StringConversion;
use Isdoc\Exceptions\MissingPercent;
use Isdoc\Enums\VatCalculationMethod;
use Isdoc\Interfaces\RenderableInterface;
use Isdoc\Exceptions\MissingVatCalculationMethod;
use Isdoc\Exceptions\UnvalidVatCalculationMethodFormat;

/**
 * Složená položka DPH
 */
class ClassifiedTaxCategory implements RenderableInterface
{
    use StringConversion;
    use BoolValidation;

    protected $percent = null;
    protected $vatCalculationMethod = null;
    protected $vatApplicable = null;
    protected $localReverseCharge = null;

    /** 
     * Procentní sazba DPH 
     * @see \Isdoc\Tests\Models\ClassifiedTaxCategory\GetSetPercentTest
     **/
    public function setPercent(float $percent): ClassifiedTaxCategory
    {
        $this->percent = $percent;
        return $this;
    }

    /** 
     * Procentní sazba DPH 
     * @see \Isdoc\Tests\Models\ClassifiedTaxCategory\GetSetPercentTest
     * @throws MissingPercent
     **/
    public function getPercent(): float
    {
        if(is_null($this->percent)) {
            throw new MissingPercent();
        }
        return $this->percent;
    }

    /** 
     * Způsob výpočtu DPH 
     * @param int $vatCalculationMethod (0 = zdola, 1 = shora)
     * @see \Isdoc\Tests\Models\ClassifiedTaxCategory\GetSetVatCalculationMethodTest
     * @throws UnvalidVatCalculationMethodFormat
     */
    public function setVatCalculationMethod(int $vatCalculationMethod): static
    {
        $this->vatCalculationMethod = VatCalculationMethod::validate($vatCalculationMethod);
        return $this;
    }

    /**
     * Způsob výpočtu DPH 
     * @return int (0 = zdola, 1 = shora)
     * @see \Isdoc\Tests\Models\ClassifiedTaxCategory\GetSetVatCalculationMethodTest
     * @throws MissingVatCalculationMethod
     */
    public function getVatCalculationMethod(): int
    {
        if(is_null($this->vatCalculationMethod)) {
            throw new MissingVatCalculationMethod();
        }
        return $this->vatCalculationMethod;
    }

    /** 
     * Je předmětem DPH 
     * @see \Isdoc\Tests\Models\ClassifiedTaxCategory\GetSetVatApplicableTest
     **/
    public function setVatApplicable(bool|null $vatApplicable): static
    {
        $this->vatApplicable = $vatApplicable;
        return $this;
    }

    /** 
     * Je předmětem DPH 
     * @see \Isdoc\Tests\Models\ClassifiedTaxCategory\GetSetVatApplicableTest
     **/
    public function getVatApplicable(): bool|null
    {
        return $this->vatApplicable;
    }

    /** 
     * Lokální režim přenesení daňové povinnosti
     * @see \Isdoc\Tests\Models\ClassifiedTaxCategory\GetSetLocalReverseChargeTest
     **/
    public function setLocalReverseCharge(bool|null $localReverseCharge): static
    {
        $this->localReverseCharge = $localReverseCharge;
        return $this;
    }

    /** 
     * Lokální režim přenesení daňové povinnosti
     * @see \Isdoc\Tests\Models\ClassifiedTaxCategory\GetSetLocalReverseChargeTest
     **/
    public function getLocalReverseCharge(): bool|null
    {
        return $this->localReverseCharge;
    }

    /**
     * @see \Isdoc\Tests\Models\ClassifiedTaxCategory\ToXmlElementTest
     */
    public function toXmlElement(): IsdocSimpleXMLElement
    {
        $classifiedTaxCategory = new IsdocSimpleXMLElement('<ClassifiedTaxCategory></ClassifiedTaxCategory>');
        $classifiedTaxCategory->addChild('Percent', $this->getPercent());
        $classifiedTaxCategory->addChild('VATCalculationMethod', $this->getVatCalculationMethod());
        $classifiedTaxCategory->addChildOptional('VATApplicable', $this->boolToStr($this->getVatApplicable()));
        $classifiedTaxCategory->addChildOptional('LocalReverseCharge', $this->boolToStr($this->getLocalReverseCharge()));
        return $classifiedTaxCategory;
    }
}
