<?php

namespace Isdoc\Models;

use Isdoc\Traits\DateValidation;
use Isdoc\Traits\UUIDValidation;
use Isdoc\Traits\StringConversion;
use Isdoc\Exceptions\InvalidDateFormat;
use Isdoc\Exceptions\InvalidUuidFormat;
use Isdoc\Exceptions\MissingSalesOrderId;
use Isdoc\Interfaces\RenderableInterface;

class OrderReference implements RenderableInterface
{
    use StringConversion;
    use DateValidation;
    use UUIDValidation;

    protected $salesOrderId = null;
    protected $externalOrderId = null;
    protected $issueDate = null;
    protected $issueDateExternal = null;
    protected $uuid = null;
    protected $idsId = null;
    protected $fileReference = null;
    protected $referenceNumber = null;

    /** 
     * Vlastní identifikátor objednávky přijaté u dodavatele
     * @see \Isdoc\Tests\Models\OrderReference\GetSetSalesOrderIdTest
     **/
    public function setSalesOrderId(string $id): static
    {
        $this->salesOrderId = $id;
        return $this;
    }

    /** 
     * Vlastní identifikátor objednávky přijaté u dodavatele
     * @throws MissingSalesOrderId
     * @see \Isdoc\Tests\Models\OrderReference\GetSetSalesOrderIdTest
     **/
    public function getSalesOrderId(): string
    {
        if(is_null($this->salesOrderId)) {
            throw new MissingSalesOrderId();
        }
        return $this->salesOrderId;
    }

    /** 
     * Externí číslo objednávky přijaté, typicky objednávka vydaná u odběratele 
     * @see \Isdoc\Tests\Models\OrderReference\GetSetExternalOrderIdTest
     **/
    public function setExternalOrderId(string|null $id): static
    {
        $this->externalOrderId = $id;
        return $this;
    }

    /** 
     * Externí číslo objednávky přijaté, typicky objednávka vydaná u odběratele 
     * @see \Isdoc\Tests\Models\OrderReference\GetSetExternalOrderIdTest
     **/
    public function getExternalOrderId(): null|string
    {
        return $this->externalOrderId;
    }

    /** 
     * Datum vystavení objednávky přijaté u dodavatele 
     * @see \Isdoc\Tests\Models\OrderReference\GetSetIssueDateTest
     * @throws InvalidDateFormat
     **/
    public function setIssueDate(string|null $date): static
    {
        $this->validateDateNullable($date);
        $this->issueDate = $date;
        return $this;
    }

    /** 
     * Datum vystavení objednávky přijaté u dodavatele 
     * @see \Isdoc\Tests\Models\OrderReference\GetSetIssueDateTest
     **/
    public function getIssueDate(): string|null
    {
        return $this->issueDate;
    }

    /** 
     * Datum vystavení objednávky vydané (u objednávající strany)
     * @see \Isdoc\Tests\Models\OrderReference\GetSetExternalOrderIdTest
     * @throws InvalidDateFormat
     **/
    public function setExternalIssueDate(string|null $issueDate): static
    {
        $this->validateDateNullable($issueDate);
        $this->issueDateExternal  = $issueDate;
        return $this;
    }

    /** 
     * Datum vystavení objednávky vydané (u objednávající strany)
     * @see \Isdoc\Tests\Models\OrderReference\GetSetExternalOrderIdTest
     **/
    public function getExternalIssueDate(): string|null
    {
        return $this->issueDateExternal;
    }

    /** 
     * Unikátní identifikátor GUID 
     * @throws InvalidUuidFormat
     * @see \Isdoc\Tests\Models\OrderReference\GetSetUuidTest
     **/
    public function setUuid(string|null $uuid): static
    {
        $this->validateUuidNullable($uuid);
        $this->uuid = $uuid;
        return $this;
    }

    /** 
     * Unikátní identifikátor GUID 
     * @see \Isdoc\Tests\Models\OrderReference\GetSetUuidTest
     **/
    public function getUuid(): string|null
    {
        return $this->uuid;
    }

    /** 
     * ID datové zprávy v ISDS, které vrátilo rozhraní ISDS při prvotním poslání této objednávky
     * @see \Isdoc\Tests\Models\OrderReference\GetSetIsdsIdTest
     **/
    public function setIsdsId(string|null $isdsId): static
    {
        $this->idsId = $isdsId;
        return $this;
    }

    /** 
     * ID datové zprávy v ISDS, které vrátilo rozhraní ISDS při prvotním poslání této objednávky
     * @see \Isdoc\Tests\Models\OrderReference\GetSetIsdsIdTest
     **/
    public function getIsdsId(): string|null
    {
        return $this->idsId;
    }

    /** 
     * Spisová značka, pod kterým byla vypravena písemnost Objednávka ve spisové službě objednatele 
     * @see \Isdoc\Tests\Models\OrderReference\GetSetFileReferenceTest
     **/
    public function setFileReference(string|null $fileReference): static
    {
        $this->fileReference = $fileReference;
        return $this;
    }

    /** 
     * Spisová značka, pod kterým byla vypravena písemnost Objednávka ve spisové službě objednatele 
     * @see \Isdoc\Tests\Models\OrderReference\GetSetFileReferenceTest
     **/
    public function getFileReference(): string|null
    {
        return $this->fileReference;
    }

    /** 
     * Číslo jednací, pod kterým byla vypravena písemnost Objednávka ve spisové službě objednatele
     * @see \Isdoc\Tests\Models\OrderReference\GetSetReferenceNumberTest
     **/
    public function setReferenceNumber(string|null $referenceNumber): static
    {
        $this->referenceNumber = $referenceNumber;
        return $this;
    }

    /** 
     * Číslo jednací, pod kterým byla vypravena písemnost Objednávka ve spisové službě objednatele
     * @see \Isdoc\Tests\Models\OrderReference\GetSetReferenceNumberTest
     **/
    public function getReferenceNumber(): string|null
    {
        return $this->referenceNumber;
    }

    /**
     * @see \Isdoc\Tests\Models\OrderReference\ToXmlElementTest
     */
    public function toXmlElement(): IsdocSimpleXMLElement
    {
        $reference = new IsdocSimpleXMLElement('<OrderReference></OrderReference>');
        $reference->addChild('SalesOrderID', $this->getSalesOrderId());
        $reference->addChildOptional('ExternalOrderID', $this->getExternalOrderId());
        $reference->addChildOptional('IssueDate', $this->getIssueDate());
        $reference->addChildOptional('ExternalOrderIssueDate', $this->getExternalIssueDate());
        $reference->addChildOptional('UUID', $this->getUuid());
        $reference->addChildOptional('ISDS_ID', $this->getIsdsId());
        $reference->addChildOptional('FileReference', $this->getFileReference());
        $reference->addChildOptional('ReferenceNumber', $this->getReferenceNumber());
        return $reference;
    }
}
