<?php

namespace Isdoc\Models;

class InvoiceLineReference
{
    protected $ref;
    protected $refLineId;

    /**
     * @see \Isdoc\Tests\Models\InvoiceLineReference\ComplexText
     */
    public function __construct(string $ref, string $refLineId)
    {
        $this->ref = $ref;
        $this->refLineId = $refLineId;
    }

    /**
     * @see \Isdoc\Tests\Models\InvoiceLineReference\ComplexText
     */
    public function getRef(): string
    {
        return $this->ref;
    }

    /**
     * @see \Isdoc\Tests\Models\InvoiceLineReference\ComplexText
     */
    public function getLineId(): string
    {
        return $this->refLineId;
    }
}
