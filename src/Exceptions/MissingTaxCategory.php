<?php

namespace Isdoc\Exceptions;

use Exception;
use Throwable;

class MissingTaxCategory extends Exception
{
    public function __construct(int $code = 0, Throwable|null $previous = null)
    {
        parent::__construct(
            'V objektu chybí attribut TaxCategory.',
            $code,
            $previous
        );
    }
}