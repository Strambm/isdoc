<?php

namespace Isdoc\Exceptions;

use Throwable;
use TypeError;

class InvalidLineIdLength extends TypeError
{
    public function __construct(int $code = 0, Throwable|null $previous = null)
    {
        parent::__construct(
            'Povolená délka řetězce je pouze 36 znaků',
            $code,
            $previous
        );
    }
}