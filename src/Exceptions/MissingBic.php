<?php

namespace Isdoc\Exceptions;

use Exception;
use Throwable;

class MissingBic extends Exception
{
    public function __construct(int $code = 0, Throwable|null $previous = null)
    {
        parent::__construct(
            'V bankovním účtu chybí hodnota pro BIC.',
            $code,
            $previous
        );
    }
}