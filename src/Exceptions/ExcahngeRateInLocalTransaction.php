<?php

namespace Isdoc\Exceptions;

use Exception;
use Throwable;

class ExcahngeRateInLocalTransaction extends Exception
{
    public function __construct(int $code = 0, Throwable|null $previous = null)
    {
        parent::__construct(
            'Je nastaven převodový kurz měn ačkoliv není nastavena cizí měna',
            $code,
            $previous
        );
    }
}