<?php

namespace Isdoc\Exceptions;

use Exception;
use Throwable;

class MissingDueDate extends Exception
{
    public function __construct(int $code = 0, Throwable|null $previous = null)
    {
        parent::__construct(
            'V modelu Chybí povinný atribut DueDate',
            $code,
            $previous
        );
    }
}