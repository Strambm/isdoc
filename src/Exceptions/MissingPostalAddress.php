<?php

namespace Isdoc\Exceptions;

use Exception;
use Throwable;

class MissingPostalAddress extends Exception
{
    public function __construct(int $code = 0, Throwable|null $previous = null)
    {
        parent::__construct(
            'V party contactu chybí povinný údaj postalAssress',
            $code,
            $previous
        );
    }
}