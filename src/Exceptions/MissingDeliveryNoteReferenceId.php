<?php

namespace Isdoc\Exceptions;

use Exception;
use Isdoc\Models\DeliveryNoteReference;
use Throwable;

class MissingDeliveryNoteReferenceId extends Exception
{
    public function __construct(int $code = 0, Throwable|null $previous = null)
    {
        parent::__construct(
            'V modelu \'' . DeliveryNoteReference::class . '\' chybí hodnota \'Id\'',
            $code,
            $previous
        );
    }
}