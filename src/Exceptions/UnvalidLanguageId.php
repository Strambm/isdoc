<?php

namespace Isdoc\Exceptions;

use Throwable;
use TypeError;

class UnvalidLanguageId extends TypeError
{
    public function __construct(int $code = 0, Throwable|null $previous = null)
    {
        parent::__construct(
            'LanguageId neodpovídá žádné z podporovaných konstant.',
            $code,
            $previous
        );
    }
}