<?php

namespace Isdoc\Exceptions;

use Throwable;
use TypeError;

class UnvalidPaymentMeansCode extends TypeError
{
    public function __construct(int $value, int $code = 0, Throwable|null $previous = null)
    {
        parent::__construct(
            'Kód způsobu platby <PaymentMeansCode>' . $value . '</PaymentMeansCode> neodpovídá žádné z podporovaných konstant kódů způsobů platby.',
            $code,
            $previous
        );
    }
}