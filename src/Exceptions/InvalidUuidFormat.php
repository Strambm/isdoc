<?php

namespace Isdoc\Exceptions;

use Isdoc\Traits\UUIDValidation;
use Throwable;
use TypeError;

class InvalidUuidFormat extends TypeError
{
    use UUIDValidation;
    public function __construct(int $code = 0, Throwable|null $previous = null)
    {
        parent::__construct(
            'Nevalidní UUID, pro podmínku podmínnku je potřeba splnit tento patern: \'' . $this->getPatern() . '\'',
            $code,
            $previous
        );
    }
}