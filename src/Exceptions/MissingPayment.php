<?php

namespace Isdoc\Exceptions;

use Exception;
use Throwable;

class MissingPayment extends Exception
{
    public function __construct(int $code = 0, Throwable|null $previous = null)
    {
        parent::__construct(
            'Chybí hodnota pro attribut Payment.',
            $code,
            $previous
        );
    }
}