<?php

namespace Isdoc\Exceptions;

use Exception;
use Throwable;

class MissingPartyIdentifivationId extends Exception
{
    public function __construct(int $code = 0, Throwable|null $previous = null)
    {
        parent::__construct(
            'Chybí identifikace firmy (IČ)',
            $code,
            $previous
        );
    }
}