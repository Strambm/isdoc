<?php

namespace Isdoc\Exceptions;

use Throwable;
use TypeError;

class UnvalidCountryCode extends TypeError
{
    public function __construct(int $code = 0, Throwable|null $previous = null)
    {
        parent::__construct(
            'Kód země <IdentificationCode>$countryCode</IdentificationCode> neodpovídá žádnému z podporovaných kódů zemí.',
            $code,
            $previous
        );
    }
}