<?php

namespace Isdoc\Exceptions;

use Throwable;
use TypeError;

class UnvalidDocumentType extends TypeError
{
    public function __construct(int $value, int $code = 0, Throwable|null $previous = null)
    {
        parent::__construct(
            'Typ dokumentu <DocumentType>' . $value . '</DocumentType> neodpovídá žádné z konstant typů dokumentu.',
            $code,
            $previous
        );
    }
}