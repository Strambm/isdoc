<?php

namespace Isdoc\Exceptions;

use Throwable;
use TypeError;

class UnvalidTaxScheme extends TypeError
{
    public function __construct(int $code = 0, Throwable|null $previous = null)
    {
        parent::__construct(
            'Daňové schéma: <TaxScheme>$taxScheme</TaxScheme> neodpovídá žádné z podporovaných konstant daňových schémat. Podporovaná schémata jsou VAT a TIN.',
            $code,
            $previous
        );
    }
}