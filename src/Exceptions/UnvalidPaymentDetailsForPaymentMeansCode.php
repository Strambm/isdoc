<?php

namespace Isdoc\Exceptions;

use Exception;
use Throwable;

class UnvalidPaymentDetailsForPaymentMeansCode extends Exception
{
    public function __construct(int $code = 0, Throwable|null $previous = null)
    {
        parent::__construct(
            'Pro Vámi zadaný PaymentMeansCode máte nesprávnou instanci PaymentDetails',
            $code,
            $previous
        );
    }
}