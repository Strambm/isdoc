<?php

namespace Isdoc\Exceptions;

use Throwable;
use TypeError;

class UnvalidVatCalculationMethodFormat extends TypeError
{
    public function __construct(int $value, int $code = 0, Throwable|null $previous = null)
    {
        parent::__construct(
            'Způsob výpočtu DPH: <VatCalculationMethod>' . $value . '</VatCalculationMethod> neodpovídá žádné z podporovaných konstant výpočtu DPH.',
            $code,
            $previous
        );
    }
}