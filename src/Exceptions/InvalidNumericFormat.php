<?php

namespace Isdoc\Exceptions;

use Throwable;
use TypeError;

class InvalidNumericFormat extends TypeError
{
    public function __construct(string $value, int $code = 0, Throwable|null $previous = null)
    {
        parent::__construct(
            '\'' . $value . '\' není numerická hodnota',
            $code,
            $previous
        );
    }
}