<?php

namespace Isdoc\Exceptions;

use Throwable;
use TypeError;

class InvalidDateFormat extends TypeError
{
    public function __construct(string $value, int $code = 0, Throwable|null $previous = null)
    {
        parent::__construct(
            'Datum \'' . $value . '\' je neplatné nebo není v podporovaném tvaru. Zadejte datum ve tvaru: YYYY-MM-DD.',
            $code,
            $previous
        );
    }
}