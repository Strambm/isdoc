<?php

namespace Isdoc\Exceptions;

use Throwable;
use TypeError;

class ManuallySettedForeignCurrencyValueWithoutExchangeRate extends TypeError
{
    public function __construct(int $code = 0, Throwable|null $previous = null)
    {
        parent::__construct(
            'Byla nastavena manuálně hodnota pro cizí měnu, ačkoli měna není nastavena.',
            $code,
            $previous
        );
    }
}