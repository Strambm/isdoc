<?php

namespace Isdoc\Exceptions;

use Exception;
use Isdoc\Models\OrderReference;
use Throwable;

class MissingSalesOrderId extends Exception
{
    public function __construct(int $code = 0, Throwable|null $previous = null)
    {
        parent::__construct(
            'V modelu \'' . OrderReference::class . '\' chybí hodnota \'salesOrderId\'',
            $code,
            $previous
        );
    }
}