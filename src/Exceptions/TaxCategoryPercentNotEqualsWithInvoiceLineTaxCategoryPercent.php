<?php

namespace Isdoc\Exceptions;

use Exception;
use Throwable;

class TaxCategoryPercentNotEqualsWithInvoiceLineTaxCategoryPercent extends Exception
{
    public function __construct(int $code = 0, Throwable|null $previous = null)
    {
        parent::__construct(
            'Snažíte se přičítat do TaxSubTotal InvoiceLine s rozdílnou daňovou zátěží.',
            $code,
            $previous
        );
    }
}