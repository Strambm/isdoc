<?php

namespace Isdoc\Exceptions;

use Exception;
use Throwable;

class TaxSubTotalWasCreatedBefore extends Exception
{
    public function __construct(int $code = 0, Throwable|null $previous = null)
    {
        parent::__construct(
            'Pokoušíte se přidat TaxSubTotal do TaxTotal s DPH sazbou, který již byl vytvořen dříve.',
            $code,
            $previous
        );
    }
}