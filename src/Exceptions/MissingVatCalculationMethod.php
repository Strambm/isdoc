<?php

namespace Isdoc\Exceptions;

use Exception;
use Isdoc\Models\ClassifiedTaxCategory;
use Throwable;

class MissingVatCalculationMethod extends Exception
{
    public function __construct(int $code = 0, Throwable|null $previous = null)
    {
        parent::__construct(
            'V modelu \'' . ClassifiedTaxCategory::class . '\' chybí nastavit parametr \'VatCalculationMethod\'',
            $code,
            $previous
        );
    }
}