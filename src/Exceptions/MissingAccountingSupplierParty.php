<?php

namespace Isdoc\Exceptions;

use Exception;
use Throwable;

class MissingAccountingSupplierParty extends Exception
{
    public function __construct(int $code = 0, Throwable|null $previous = null)
    {
        parent::__construct(
            'V modelu chybí attribut AccountingSupplierParty.',
            $code,
            $previous
        );
    }
}