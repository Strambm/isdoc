<?php

namespace Isdoc\Exceptions;

use Exception;
use Throwable;

class MissingIban extends Exception
{
    public function __construct(int $code = 0, Throwable|null $previous = null)
    {
        parent::__construct(
            'V bankovním účtu chybí hodnota pro IBAN.',
            $code,
            $previous
        );
    }
}