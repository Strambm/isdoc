<?php

namespace Isdoc;

use Isdoc\Models\Invoice;
use Isdoc\Parser\Invoice as InvoiceParser;
use Mpdf\Mpdf;
use Smalot\PdfParser\Parser;

class PdfConventor
{
    protected $invoiceParserClass = InvoiceParser::class;

    /**
     * @see \Isdoc\Tests\PdfConventor\EncodeDecodeTest
     */
    public function decode(string $fileContent): Invoice
    {
        $parser = new Parser();
        $pdf = $parser->parseContent($fileContent);
        $xml = current($pdf->getDictionary()['EmbeddedFile']['all'])->getContent();
        $invoiceParser = new ($this->invoiceParserClass)();
        return $invoiceParser->parseXml(simplexml_load_string($xml));
    }

    /**
     * @see \Isdoc\Tests\PdfConventor\EncodeDecodeTest
     */
    public function encode(Mpdf $pdf, Invoice $document): Mpdf
    {
        $pdf->SetAssociatedFiles([[
            'name' => 'document' . $document->getId(). '.isdoc',
            'mime' => 'text/xml',
            'description' => 'isdoc',
            'AFRelationship' => 'Alternative',
            'content' => $document->toString(true),
        ]]);
        return $pdf;
    } 
}
