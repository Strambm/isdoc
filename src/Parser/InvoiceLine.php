<?php

namespace Isdoc\Parser;

use SimpleXMLElement;
use Isdoc\Parser\Item;
use Isdoc\Parser\Note;
use Isdoc\Parser\Parser;
use Isdoc\Parser\VatNote;
use Isdoc\Models\InvoiceLineReference;
use Isdoc\Parser\ClassifiedTaxCategory;
use Isdoc\Models\InvoiceLine as ModelsInvoiceLine;

/**
 * @method ClassifiedTaxCategory getClassifiedTaxCategoryParser()
 * @method Note getNoteParser()
 * @method VatNote getVatNoteParser()
 * @method Item getItemParser()
 */
class InvoiceLine extends Parser
{
    protected $parsers = [
        'classifiedTaxCategory' => ClassifiedTaxCategory::class,
        'note' => Note::class,
        'vatNote' => VatNote::class,
        'item' => Item::class,
    ];

    /**
     * @see \Isdoc\Tests\Parser\InvoiceLine\ParseXmlTest
     */
    public function parseXml(SimpleXMLElement $xml): ModelsInvoiceLine
    {
        $result = new ModelsInvoiceLine();
        $result->setThrowManuallySettedForeignCurrencyValueWithoutExchangeRate(false);
        $result->setId((string) $xml->ID);
        $result->setClassifiedTaxCategory($this->getClassifiedTaxCategoryParser()->parseXml($xml->ClassifiedTaxCategory));
        if(isset($xml->OrderReference)) {
            $result->setOrderReference(new InvoiceLineReference($xml->OrderReference['ref'], $xml->OrderReference->LineID));
        }
        if(isset($xml->OriginalDocumentReference)) {
            $result->setOriginalDocumentReference(new InvoiceLineReference($xml->OriginalDocumentReference['ref'], $xml->OriginalDocumentReference->LineID));
        }
        if(isset($xml->DeliveryNoteReference)) {
            $result->setDeliveryNoteReference(new InvoiceLineReference($xml->DeliveryNoteReference['ref'], $xml->DeliveryNoteReference->LineID));
        }
        if(isset($xml->InvoicedQuantity)) {
            $result->setInvoicedQuantity($this->getAttributeOptional($xml->InvoicedQuantity));
            $result->setInvoiceQuantityUnitCode($this->getAttributeOptional($xml->InvoicedQuantity['unitCode']));
        }
        
        $result->setLineExtensionAmount($this->getAttributeOptional($xml->LineExtensionAmount));
        $result->setLineExtensionAmountBeforeDiscount($this->getAttributeOptional($xml->LineExtensionAmountBeforeDiscount));
        $result->setLineExtensionAmountCurr($this->getAttributeOptional($xml->LineExtensionAmountCurr));
        $result->setLineExtensionAmountTaxInclusive($this->getAttributeOptional($xml->LineExtensionAmountTaxInclusive));
        $result->setLineExtensionAmountTaxInclusiveBeforeDiscount($this->getAttributeOptional($xml->LineExtensionAmountTaxInclusiveBeforeDiscount));
        $result->setLineExtensionAmountTaxInclusiveCurr($this->getAttributeOptional($xml->LineExtensionAmountTaxInclusiveCurr));
        $result->setLineExtensionTaxAmount($this->getAttributeOptional($xml->LineExtensionTaxAmount));
        $result->setUnitPrice($this->getAttributeOptional($xml->UnitPrice));
        $result->setUnitPriceTaxInclusive($this->getAttributeOptional($xml->UnitPriceTaxInclusive));
        if(isset($xml->Note)) {
            $result->setNote($this->getNoteParser()->parseXml($xml->Note));
        }
        if(isset($xml->VATNote)) {
            $result->setVatNote($this->getVatNoteParser()->parseXml($xml->VATNote));
        }
        if(isset($xml->Item)) {
            $result->setItem($this->getItemParser()->parseXml($xml->Item));
        }
        return $result;
    }
}