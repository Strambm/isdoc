<?php

namespace Isdoc\Parser;

use SimpleXMLElement;
use Isdoc\Parser\Country;
use Isdoc\Models\PostalAddress as ModelsPostalAddress;

/**
 * @method Country getCountryParser()
 */
class PostalAddress extends Parser
{
    protected $parsers = [
        'country' => Country::class,
    ];

    /**
     * @see \Isdoc\Tests\Parser\PostalAddress\ParseXmlTest
     */
    public function parseXml(SimpleXMLElement $xml): ModelsPostalAddress
    {
        $result = new ModelsPostalAddress();
        $result->setStreetName((string) $xml->StreetName);
        $result->setBuildingNumber((string) $xml->BuildingNumber);
        $result->setCityName((string) $xml->CityName);
        $result->setPostalZone((string) $xml->PostalZone);
        if(isset($xml->Country)) {
            $result->setCountry(
                $this->getCountryParser()
                    ->parseXml($xml->Country)
            );
        }
        return $result;
    }
}