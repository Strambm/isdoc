<?php

namespace Isdoc\Parser;

use SimpleXMLElement;
use Isdoc\Parser\Note;
use Isdoc\Parser\TaxTotal;
use Isdoc\Parser\InvoiceLine;
use Isdoc\Models\ExchangeRate;
use Isdoc\Parser\PartyContact;
use Isdoc\Parser\PaymentMeans;
use Isdoc\Parser\TaxedDeposit;
use Isdoc\Parser\OrderReference;
use Isdoc\Traits\BoolValidation;
use Isdoc\Parser\NonTaxedDeposit;
use Isdoc\Parser\LegalMonetaryTotal;
use Isdoc\Parser\DeliveryNoteReference;
use Isdoc\Models\Invoice as ModelInvoice;
use Isdoc\Parser\OriginalDocumentReference;
use Isdoc\Parser\ElectronicPossibilityAgreementReference;

/**
 * @method PartyContact getAccountingSupplierPartyParser()
 * @method PartyContact getAccountingCustomerPartyParser()
 * @method InvoiceLine getInvoiceLineParser()
 * @method DeliveryNoteReference getDeliveryNoteReferenceParser()
 * @method OrderReference getOrderReferenceParser()
 * @method ElectronicPossibilityAgreementReference getElectronicPossibilityAgreementReferenceParser()
 * @method Note getNoteParser()
 * @method PaymentMeans getPaymentMeansParser()
 * @method OriginalDocumentReference getOriginalDocumentReferenceParser()
 * @method TaxTotal getTaxTotalParser()
 * @method LegalMonetaryTotal getLegalMonetaryTotalParser()
 * @method PartyContact getDeliveryAddressParser()
 * @method TaxedDeposit getTaxDepositParser()
 * @method NonTaxedDeposit getNonTaxDepositParser()
 */
class Invoice extends Parser
{
    use BoolValidation;

    protected $parsers = [
        'accountingSupplierParty' => PartyContact::class,
        'accountingCustomerParty' => PartyContact::class,
        'invoiceLine' => InvoiceLine::class,
        'deliveryNoteReference' => DeliveryNoteReference::class,
        'orderReference' => OrderReference::class,
        'electronicPossibilityAgreementReference' => ElectronicPossibilityAgreementReference::class,
        'note' => Note::class,
        'paymentMeans' => PaymentMeans::class,
        'originalDocumentReference' => OriginalDocumentReference::class,
        'taxTotal' => TaxTotal::class,
        'legalMonetaryTotal' => LegalMonetaryTotal::class,
        'deliveryAddress' => PartyContact::class,
        'taxDeposit' => TaxedDeposit::class,
        'nonTaxDeposit' => NonTaxedDeposit::class,
    ];

    /**
     * @see \Isdoc\Tests\Parser\Invoice\ParseXmlTest
     */
    public function parseXml(SimpleXMLElement $xml): ModelInvoice
    {
        $result = new ModelInvoice();
        $result->setDocumentType((int) $xml->DocumentType);
        $result->setId((string) $xml->ID);
        $result->setUuid((string) $xml->UUID);
        $result->setIssueDate((string) $xml->IssueDate);
        $result->setIssuingSystem($this->getAttributeOptional($xml->IssuingSystem));
        $result->setTaxPointDate($this->getAttributeOptional($xml->TaxPointDate));
        $result->setVATApplicable($this->strToBool($xml->VATApplicable));
        $result->setElectronicPossibilityAgreementReference($this->getElectronicPossibilityAgreementReferenceParser()->parseXml($xml->ElectronicPossibilityAgreementReference));
        if(isset($xml->Note)) {
            $result->setNote($this->getNoteParser()->parseXml($xml->Note));
        }
        $exchangeRate = new ExchangeRate(
            (string) $xml->LocalCurrencyCode,
            $this->getAttributeOptional($xml->ForeignCurrencyCode),
            (float) $xml->CurrRate,
            (float) $xml->RefCurrRate
        );
        $result->setExchangeRate($exchangeRate);
        $result->setAccountingCustomerParty($this->getAccountingCustomerPartyParser()->parseXml($xml->AccountingCustomerParty->Party));
        $result->setAccountingSupplierParty($this->getAccountingSupplierPartyParser()->parseXml($xml->AccountingSupplierParty->Party));
        if(isset($xml->PaymentMeans)) {
            $result->setPaymentMeans($this->getPaymentMeansParser()->parseXml($xml->PaymentMeans));
        }
        if(isset($xml->OriginalDocumentReferences)) {
            foreach($xml->OriginalDocumentReferences->OriginalDocumentReference as $originalDocumentReferenceXml) {
                $result->addOriginalDocumentReference($this->getOriginalDocumentReferenceParser()->parseXml($originalDocumentReferenceXml));
            }
        }
        $result->setTaxTotal($this->getTaxTotalParser()->parseXml($xml->TaxTotal));
        $result->setLegalMonetaryTotal($this->getLegalMonetaryTotalParser()->parseXml($xml->LegalMonetaryTotal));
        foreach($xml->InvoiceLines->InvoiceLine as $invoiceLineXml) {
            $result->addInvoiceLine($this->getInvoiceLineParser()->parseXml($invoiceLineXml));
        }
        if(isset($xml->DeliveryNoteReferences)) {
            foreach($xml->DeliveryNoteReferences->DeliveryNoteReference as $deliveryNoteReferenceXml) {
                $result->addDeliveryNoteReference($this->getDeliveryNoteReferenceParser()->parseXml($deliveryNoteReferenceXml));
            }
        }
        if(isset($xml->OrderReferences)) {
            foreach($xml->OrderReferences->OrderReference as $orderReferenceXml) {
                $result->addOrderReference($this->getOrderReferenceParser()->parseXml($orderReferenceXml));
            }
        }
        if(isset($xml->Delivery)) {
            $result->setDeliveryAddress($this->getDeliveryAddressParser()->parseXml($xml->Delivery->Party));
        }
        if(isset($xml->TaxedDeposits)) {
            foreach($xml->TaxedDeposits->TaxedDeposit as $taxDepositXml) {
                $result->addTaxedDeposit($this->getTaxDepositParser()->parseXml($taxDepositXml));
            }
        }
        if(isset($xml->TaxedDeposits)) {
            foreach($xml->NonTaxedDeposits->NonTaxedDeposit as $nonTaxedDepositXml) {
                $result->addNonTaxedDeposit($this->getNonTaxDepositParser()->parseXml($nonTaxedDepositXml));
            }
        }
        return $result;
    }
}