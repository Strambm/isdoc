<?php

namespace Isdoc\Parser;

use Isdoc\Models\DeliveryNoteReference as ModelsDeliveryNoteReference;

class DeliveryNoteReference extends ReferenceParser
{
    protected function getModelClass(): string
    {
        return ModelsDeliveryNoteReference::class;
    }
}