<?php

namespace Isdoc\Parser;

use Isdoc\Models\OrderReference as ModelsOrderReference;
use SimpleXMLElement;

class OrderReference extends Parser
{
    /**
     * @see \Isdoc\Tests\Parser\OrderReference\ParseXmlTest
     */
    public function parseXml(SimpleXMLElement $xml): ModelsOrderReference
    {
        $result = new ModelsOrderReference();
        $result->setSalesOrderId((string) $xml->SalesOrderID);
        $result->setExternalOrderId($this->getAttributeOptional($xml->ExternalOrderID));
        $result->setIssueDate($this->getAttributeOptional($xml->IssueDate));
        $result->setExternalIssueDate($this->getAttributeOptional($xml->ExternalOrderIssueDate));
        $result->setUuid($this->getAttributeOptional($xml->UUID));
        $result->setIsdsId($this->getAttributeOptional($xml->ISDS_ID));
        $result->setFileReference($this->getAttributeOptional($xml->FileReference));
        $result->setReferenceNumber($this->getAttributeOptional($xml->ReferenceNumber));
        return $result;
    }
}