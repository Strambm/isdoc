<?php

namespace Isdoc\Parser;

use Isdoc\Models\DocumentReference;
use SimpleXMLElement;

abstract class ReferenceParser extends Parser
{
    /**
     * @see \Isdoc\Tests\Parser\DocumentReference\ParseXmlTest
     */
    public function parseXml(SimpleXMLElement $xml): DocumentReference
    {
        $modelClass = $this->getModelClass();
        $result = new $modelClass();
        $result->setId((string) $xml->ID);
        $result->setIssueDate($this->getAttributeOptional($xml->IssueDate));
        $result->setUuid($this->getAttributeOptional($xml->UUID));
        return $result;
    }

    protected abstract function getModelClass(): string;
}