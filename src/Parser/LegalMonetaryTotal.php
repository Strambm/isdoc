<?php

namespace Isdoc\Parser;

use Isdoc\Models\LegalMonetaryTotal as IsdocLegalMonetaryTotal;
use SimpleXMLElement;

class LegalMonetaryTotal extends Parser
{
    /**
     * @see \Isdoc\Tests\Parser\LegalMonetaryTotal\ParseXmlTest
     */
    public function parseXml(SimpleXMLElement $xml): IsdocLegalMonetaryTotal
    {
        $result = new IsdocLegalMonetaryTotal();
        $result->setTaxExclusiveAmount((float) $xml->TaxExclusiveAmount);
        $result->setTaxExclusiveAmountCurr($this->getAttributeOptional($xml->TaxExclusiveAmountCurr));
        $result->setTaxInclusiveAmount((float) $xml->TaxInclusiveAmount);
        $result->setTaxInclusiveAmountCurr($this->getAttributeOptional($xml->TaxInclusiveAmountCurr));
        $result->setAlreadyClaimedTaxExclusiveAmount((float) $xml->AlreadyClaimedTaxExclusiveAmount);
        $result->setAlreadyClaimedTaxExclusiveAmountCurr($this->getAttributeOptional($xml->AlreadyClaimedTaxExclusiveAmountCurr));
        $result->setAlreadyClaimedTaxInclusiveAmount((float) $xml->AlreadyClaimedTaxInclusiveAmount);
        $result->setAlreadyClaimedTaxInclusiveAmountCurr($this->getAttributeOptional($xml->AlreadyClaimedTaxInclusiveAmountCurr));
        $result->setDifferenceTaxExclusiveAmount((float) $xml->DifferenceTaxExclusiveAmount);
        $result->setDifferenceTaxExclusiveAmountCurr($this->getAttributeOptional($xml->DifferenceTaxExclusiveAmountCurr));
        $result->setDifferenceTaxInclusiveAmount((float) $xml->DifferenceTaxInclusiveAmount);
        $result->setDifferenceTaxInclusiveAmountCurr($this->getAttributeOptional($xml->DifferenceTaxInclusiveAmountCurr));
        $result->setPayableRoundingAmount($this->getAttributeOptional($xml->PayableRoundingAmount));
        $result->setPayableRoundingAmountCurr($this->getAttributeOptional($xml->PayableRoundingAmountCurr));
        $result->setPaidDepositsAmount((float) $xml->PaidDepositsAmount);
        $result->setPaidDepositsAmountCurr($this->getAttributeOptional($xml->PaidDepositsAmountCurr));
        $result->setPayableAmount((float) $xml->PayableAmount);
        $result->setPayableAmountCurr($this->getAttributeOptional($xml->PayableAmountCurr));
        return $result;
    }
}