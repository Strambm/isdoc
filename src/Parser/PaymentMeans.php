<?php

namespace Isdoc\Parser;

use SimpleXMLElement;
use Isdoc\Parser\Payment;
use Isdoc\Parser\BankAccount;
use Isdoc\Models\PaymentMeans as ModelsPaymentMeans;

/**
 * @method Payment getPaymentParser()
 * @method BankAccount getBankAccountParser()
 */
class PaymentMeans extends Parser
{
    protected $parsers = [
        'payment' => Payment::class,
        'bankAccount' => BankAccount::class,
    ];

    /**
     * @see \Isdoc\Tests\Parser\PaymentMeans\ParseXmlTest
     */
    public function parseXml(SimpleXMLElement $xml): ModelsPaymentMeans
    {
        $result = new ModelsPaymentMeans();
        foreach($xml->Payment as $paymentXml) {
            $result->addPayment($this->getPaymentParser()->parseXml($paymentXml));
        }
        if(isset($xml->AlternateBankAccounts)) {
            foreach($xml->AlternateBankAccounts->AlternateBankAccount as $bankAccountXml) {
                $result->addAlternativeBankAccount($this->getBankAccountParser()->parseXml($bankAccountXml));
            }
        }
        return $result;
    }
}