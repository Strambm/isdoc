<?php

namespace Isdoc\Parser;

use Isdoc\Models\TaxCategory as IsdocTaxCategory;
use Isdoc\Traits\BoolValidation;
use SimpleXMLElement;

class TaxCategory extends Parser
{
    use BoolValidation;

    /**
     * @see \Isdoc\Tests\Parser\TaxCategory\ParseXmlTest
     */
    public function parseXml(SimpleXMLElement $xml): IsdocTaxCategory
    {
        $result = new IsdocTaxCategory();
        $result->setPercent((float )$xml->Percent);
        $result->setTaxScheme($this->getAttributeOptional($xml->TaxScheme));
        $result->setVATAplicable($this->strToBool($this->getAttributeOptional($xml->VATApplicable)));
        $result->setLocalReverseChargeFlag($this->strToBool($this->getAttributeOptional($xml->LocalReverseChargeFlag)));
        return $result;
    }
}