<?php

namespace Isdoc\Parser;

use Isdoc\Models\NoteType;
use SimpleXMLElement;

abstract class NoteParser extends Parser
{
    /**
     * @see \Isdoc\Tests\Parser\NoteParser\ParseXmlTest
     */
    public function parseXml(SimpleXMLElement $xml): NoteType|null
    {
        $modelClass = $this->getModelClass();
        $result = new $modelClass();
        if(isset($xml['languageID'])) {
            $result->setLanguageId((string) $xml['languageID']);
        }
        $result->setValue((string) $xml);
        return $result;
    }

    protected abstract function getModelClass(): string;
}