<?php

namespace Isdoc\Parser;

use Isdoc\Models\Item as ModelsItem;
use SimpleXMLElement;

class Item extends Parser
{
    /**
     * @see \Isdoc\Tests\Parser\Item\ParseXmlTest
     */
    public function parseXml(SimpleXMLElement $xml): ModelsItem
    {
        $result = new ModelsItem();
        $result->setDescription($this->getAttributeOptional($xml->Description));
        $result->setBuyersItemIdentification($this->getAttributeIdOptional($xml->BuyersItemIdentification));
        $result->setCatalogueItemIdentification($this->getAttributeIdOptional($xml->CatalogueItemIdentification));
        $result->setSecondarySellersItemIdentification($this->getAttributeIdOptional($xml->SecondarySellersItemIdentification));
        $result->setTertiarySellersItemIdentification($this->getAttributeIdOptional($xml->TertiarySellersItemIdentification));
        $result->setSellersItemIdentification($this->getAttributeIdOptional($xml->SellersItemIdentification));
        return $result;
    }

    private function getAttributeIdOptional(SimpleXMLElement $xml): string|null
    {
        if(isset($xml->ID)) {
            return $this->getAttributeOptional($xml->ID);
        }
        return null;
    }
}