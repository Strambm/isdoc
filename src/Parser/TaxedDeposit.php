<?php

namespace Isdoc\Parser;

use SimpleXMLElement;
use Isdoc\Models\TaxedDeposit as Model;
use Isdoc\Parser\ClassifiedTaxCategory;

/**
 * @method ClassifiedTaxCategory getClassifiedTaxCategoryParser()
 */
class TaxedDeposit extends Parser
{
    protected $parsers = [
        'classifiedTaxCategory' => ClassifiedTaxCategory::class,
    ];

    public function parseXml(SimpleXMLElement $xml): Model
    {
        $result = new Model();
        $result->setThrowManuallySettedForeignCurrencyValueWithoutExchangeRate(false);
        $result->setClassifiedTaxCategory($this->getClassifiedTaxCategoryParser()->parseXml($xml->ClassifiedTaxCategory));
        $result->setId((string) $xml->ID);
        $result->setVariableSymbol((string) $xml->VariableSymbol);
        $result->setTaxableDepositAmount((float) $xml->TaxableDepositAmount);
        $result->setTaxableDepositAmountCurr($this->getAttributeOptional($xml->TaxableDepositAmountCurr));
        $result->setTaxInclusiveAmount((float) $xml->TaxInclusiveDepositAmount);
        $result->setTaxInclusiveDepositAmountCurr($this->getAttributeOptional($xml->TaxInclusiveDepositAmountCurr));
        return $result;
    }
}