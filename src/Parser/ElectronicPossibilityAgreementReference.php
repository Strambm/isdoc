<?php

namespace Isdoc\Parser;

use Isdoc\Models\ElectronicPossibilityAgreementReference as ModelsElectronicPossibilityAgreementReference;

class ElectronicPossibilityAgreementReference extends NoteParser
{
    protected function getModelClass(): string
    {
        return ModelsElectronicPossibilityAgreementReference::class;
    }
}