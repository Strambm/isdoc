<?php

namespace Isdoc\Parser;

use SimpleXMLElement;
use Isdoc\Parser\TaxCategory;
use Isdoc\Models\TaxSubTotal as IsdocTaxSubTotal;

/**
 * @method TaxCategory getTaxCategoryParser()
 */
class TaxSubTotal extends Parser
{
    protected $parsers = [
        'taxCategory' => TaxCategory::class,
    ];

    /**
     * @see \Isdoc\Tests\Parser\TaxSubTotal\ParseXmlTest
     */
    public function parseXml(SimpleXMLElement $xml): IsdocTaxSubTotal
    {
        $result = new IsdocTaxSubTotal();
        $result->setTaxCategory($this->getTaxCategoryParser()->parseXml($xml->TaxCategory));

        $result->setAlreadyClaimedTaxableAmount((float) $xml->AlreadyClaimedTaxableAmount);
        $result->setAlreadyClaimedTaxableAmountCurr($this->getAttributeOptional($xml->AlreadyClaimedTaxableAmountCurr));
        $result->setAlreadyClaimedTaxAmount((float) $xml->AlreadyClaimedTaxAmount);
        $result->setAlreadyClaimedTaxAmountCurr($this->getAttributeOptional($xml->AlreadyClaimedTaxAmountCurr));
        $result->setAlreadyClaimedTaxInclusiveAmount((float) $xml->AlreadyClaimedTaxInclusiveAmount);
        $result->setAlreadyClaimedTaxInclusiveAmountCurr($this->getAttributeOptional($xml->AlreadyClaimedTaxInclusiveAmountCurr));

        $result->setDifferenceTaxableAmount((float) $xml->DifferenceTaxableAmount);
        $result->setDifferenceTaxableAmountCurr($this->getAttributeOptional($xml->DifferenceTaxableAmountCurr));
        $result->setDifferenceTaxAmount((float) $xml->DifferenceTaxAmount);
        $result->setDifferenceTaxAmountCurr($this->getAttributeOptional($xml->DifferenceTaxAmountCurr));
        $result->setDifferenceTaxInclusiveAmount((float) $xml->DifferenceTaxInclusiveAmount);
        $result->setDifferenceTaxInclusiveAmountCurr($this->getAttributeOptional($xml->DifferenceTaxInclusiveAmountCurr));

        $result->setTaxableAmount((float) $xml->TaxableAmount);
        $result->setTaxableAmountCurr($this->getAttributeOptional($xml->TaxableAmountCurr));
        $result->setTaxAmount((float) $xml->TaxAmount);
        $result->setTaxAmountCurr($this->getAttributeOptional($xml->TaxAmountCurr));
        $result->setTaxInclusiveAmount((float) $xml->TaxInclusiveAmount);
        $result->setTaxInclusiveAmountCurr($this->getAttributeOptional($xml->TaxInclusiveAmountCurr));

        return $result;
    }
}