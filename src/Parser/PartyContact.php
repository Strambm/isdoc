<?php

namespace Isdoc\Parser;

use SimpleXMLElement;
use Isdoc\Parser\Contact;
use Isdoc\Parser\PostalAddress;
use Isdoc\Parser\PartyTaxScheme;
use Isdoc\Parser\PartyIdentification;
use Isdoc\Models\PartyContact as ModelsPartyContact;

/**
 * @method PostalAddress getPostalAddressParser()
 * @method PartyTaxScheme getPartyTaxSchemeParser()
 * @method PartyIdentification getPartyIdentificationParser()
 * @method Contact getContactParser()
 */
class PartyContact extends Parser
{
    protected $parsers = [
        'postalAddress' => PostalAddress::class,
        'partyTaxScheme' => PartyTaxScheme::class,
        'partyIdentification' => PartyIdentification::class,
        'contact' => Contact::class,
    ];

    /**
     * @see \Isdoc\Tests\Parser\PartyContact\ParseXmlTest
     */
    public function parseXml(SimpleXMLElement $xml): ModelsPartyContact
    {
        $result = new ModelsPartyContact();
        $result->setPartyIdentification($this->getPartyIdentificationParser()->parseXml($xml->PartyIdentification));
        $result->setName((string) $xml->PartyName->Name);
        $result->setPostalAddress($this->getPostalAddressParser()->parseXml($xml->PostalAddress));
        foreach($xml->PartyTaxScheme as $partyTaxScheme) {
            $result->addPartyTaxScheme($this->getPartyTaxSchemeParser()->parseXml($partyTaxScheme));
        }
        if(isset($xml->Contact)) {
            $result->setContact($this->getContactParser()->parseXml($xml->Contact));
        }
        return $result;
    }
}