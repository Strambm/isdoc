<?php

namespace Isdoc\Parser;

use Isdoc\Models\Country as ModelsCountry;
use SimpleXMLElement;

class Country extends Parser
{
    /**
     * @see \Isdoc\Tests\Parser\Country
     */
    public function parseXml(SimpleXMLElement $xml): ModelsCountry
    {
        $result = new ModelsCountry();
        $result->setName($this->getAttributeOptional($xml->Name));
        $result->setIdentificationCode($this->getAttributeOptional($xml->IdentificationCode));
        return $result;
    }
}