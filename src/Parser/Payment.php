<?php

namespace Isdoc\Parser;

use SimpleXMLElement;
use Isdoc\Parser\PaymentDetail;
use Isdoc\Traits\BoolValidation;
use Isdoc\Models\Payment as ModelsPayment;

/**
 * @method PaymentDetail getDetailsParser()
 */
class Payment extends Parser
{
    use BoolValidation;

    protected $parsers = [
        'details' => PaymentDetail::class,
    ];

    /**
     * @see \Isdoc\Tests\Parser\Payment\ParseXmlTest
     */
    public function parseXml(SimpleXMLElement $xml): ModelsPayment
    {
        $result = new ModelsPayment();
        $result->setPaidAmount((float) $xml->PaidAmount);
        $result->setPaymentMeansCode((int) $xml->PaymentMeansCode);
        if(isset($xml->Details)) {
            $result->setDetails($this->getDetailsParser()->parseXml($xml->Details));
        }
        if(isset($xml['partialPayment'])) {
            $result->setHasPartialPayment($this->strToBool((string) $xml['partialPayment']));
        } else {
            $result->setHasPartialPayment(null);
        }
        return $result;
    }
}