<?php

namespace Isdoc\Parser;

use Isdoc\Models\NonTaxedDeposit as Model;
use SimpleXMLElement;

class NonTaxedDeposit extends Parser
{
    /**
     * @see \Isdoc\Tests\Parser\NonTaxedDeposit\ParseXmlTest
     */
    public function parseXml(SimpleXMLElement $xml): Model
    {
        $result = new Model();
        $result->setThrowManuallySettedForeignCurrencyValueWithoutExchangeRate(false);
        $result->setId((string) $xml->ID);
        $result->setVariableSymbol((string) $xml->VariableSymbol);
        $result->setDepositAmount((float) $xml->DepositAmount);
        $result->setDepositAmountCurr($this->getAttributeOptional($xml->DepositAmountCurr));
        return $result;
    }
}