<?php

namespace Isdoc\Parser;

use Isdoc\Models\ClassifiedTaxCategory as ModelsClassifiedTaxCategory;
use Isdoc\Traits\BoolValidation;
use SimpleXMLElement;

class ClassifiedTaxCategory extends Parser
{
    use BoolValidation;

    /**
     * @see \Isdoc\Tests\Parser\ClassifiedTaxCategory\ParseXmlTest
     */
    public function parseXml(SimpleXMLElement $xml): ModelsClassifiedTaxCategory
    {
        $result = new ModelsClassifiedTaxCategory();
        $result->setPercent((float) $xml->Percent);
        $result->setVatCalculationMethod((int) $xml->VATCalculationMethod);
        $result->setVatApplicable($this->strToBool((string) $xml->VATApplicable));
        $result->setLocalReverseCharge($this->strToBool((string) $xml->LocalReverseCharge));
        return $result;
    }
}