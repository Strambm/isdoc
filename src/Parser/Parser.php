<?php

namespace Isdoc\Parser;

use Isdoc\Exceptions\UnimplementedException;
use SimpleXMLElement;

abstract class Parser
{
    protected $parsers = [];

    /**
     * @see \Isdoc\Tests\Parser\Parser\GetAttributeOptionalTest
     */
    public function getAttributeOptional(SimpleXMLElement|null $xml): string|null
    {
        $value = (string) $xml;
        if(trim($value) === '') {
            return null;
        }
        return $value;
    }

    abstract public function parseXml(SimpleXMLElement $xml);

    /**
     * @throws UnimplementedException
     * @see \Isdoc\Tests\Parser\Parser\CallTest
     */
    public function __call(string $methodName, array $arguments)
    {
        if($this->isGetterParserMethodName($methodName)) {
            $class = $this->parsers[$this->getParserKeyForMethodName($methodName)];
            return new $class();
        }
        throw new UnimplementedException('Method name \'' . $methodName . '\'');
    }

    private function isGetterParserMethodName(string $methodName): bool
    {
        return substr($methodName, 0, 3) == 'get'
            && substr($methodName, -6) == 'Parser'
            && in_array($this->getParserKeyForMethodName($methodName), array_keys($this->parsers));
    }

    private function getParserKeyForMethodName(string $methodName): string
    {
        return strtolower($methodName[3]) . substr($methodName, 4, -6);
    }
}