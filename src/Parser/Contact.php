<?php

namespace Isdoc\Parser;

use Isdoc\Models\Contact as ModelsContact;
use SimpleXMLElement;

class Contact extends Parser
{
    /**
     * @see \Isdoc\Tests\Parser\Contact\ParseXmlTest
     */
    public function parseXml(SimpleXMLElement $xml): ModelsContact
    {
        $result = new ModelsContact();
        $result->setName($this->getAttributeOptional($xml->Name));
        $result->setElectronicMail($this->getAttributeOptional($xml->ElectronicMail));
        $result->setTelephone($this->getAttributeOptional($xml->Telephone));
        return $result;
    }
}