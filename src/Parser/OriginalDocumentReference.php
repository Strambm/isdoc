<?php

namespace Isdoc\Parser;

use Isdoc\Models\OriginalDocumentReference as ModelsOriginalDocumentReference;

class OriginalDocumentReference extends ReferenceParser
{
    protected function getModelClass(): string
    {
        return ModelsOriginalDocumentReference::class;
    }
}