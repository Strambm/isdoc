<?php

namespace Isdoc\Parser;

use SimpleXMLElement;
use Isdoc\Parser\BankAccount;
use Isdoc\Models\PaymentDetails;
use Isdoc\Models\PaymentDetailCash;
use Isdoc\Models\PaymentDetailBankTransaction;

/**
 * @method BankAccount getBankAccountParser()
 */
class PaymentDetail extends Parser
{
    protected $parsers = [
        'bankAccount' => BankAccount::class,
    ];

    /**
     * @see \Isdoc\Tests\Parser\PaymentDetail\ParseXmlTest
     */
    public function parseXml(SimpleXMLElement $xml): PaymentDetails
    {
        if($this->isCashDetail($xml)) {
            $result = new PaymentDetailCash();
            $result->setDocumentId((string) $xml->DocumentID);
            $result->setIssueDate((string) $xml->IssueDate);
        } elseif($this->isBankAccountDetail($xml)) {
            $result = new PaymentDetailBankTransaction();
            $result->setBankAccount($this->getBankAccountParser()->parseXml($xml));
            $result->setPaymentDueDate((string) $xml->PaymentDueDate);
            $result->setVs($this->getAttributeOptional($xml->VariableSymbol));
            $result->setConstantSymbol($this->getAttributeOptional($xml->ConstantSymbol));
            $result->setSpecificSymbol($this->getAttributeOptional($xml->SpecificSymbol));
        }
        return $result;
    }

    private function isCashDetail(SimpleXMLElement $xml): bool
    {
        return isset($xml->DocumentID) && isset($xml->IssueDate);
    }

    private function isBankAccountDetail(SimpleXMLElement $xml): bool
    {
        return isset($xml->PaymentDueDate)
            && isset($xml->ID)
            && isset($xml->BankCode)
            && isset($xml->Name)
            && isset($xml->IBAN)
            && isset($xml->BIC);
    }
}