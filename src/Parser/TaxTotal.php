<?php

namespace Isdoc\Parser;

use SimpleXMLElement;
use Isdoc\Parser\TaxSubTotal;
use Isdoc\Models\TaxTotal as IsdocTaxTotal;

/**
 * @method TaxSubTotal getTaxSubTotalParser()
 */
class TaxTotal extends Parser
{
    protected $parsers = [
        'taxSubTotal' => TaxSubTotal::class,
    ];

    /**
     * @see \Isdoc\Tests\Parser\TaxTotal\ParseXmlTest
     */
    public function parseXml(SimpleXMLElement $xml): IsdocTaxTotal
    {
        $result = new IsdocTaxTotal();
        foreach($xml->TaxSubTotal as $sub) {
            $result->addTaxSubTotal($this->getTaxSubTotalParser()->parseXml($sub));
        }
        $result->setTaxAmount((float) $xml->TaxAmount);
        $result->setTaxAmountCurr($this->getAttributeOptional($xml->TaxAmountCurr));

        return $result;
    }
}