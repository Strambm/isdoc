<?php

namespace Isdoc\Parser;

use Isdoc\Models\VatNote as ModelsVatNote;

class VatNote extends NoteParser
{
    protected function getModelClass(): string
    {
        return ModelsVatNote::class;
    }
}