<?php

namespace Isdoc\Parser;

use Isdoc\Models\Note as ModelsNote;

class Note extends NoteParser
{
    protected function getModelClass(): string
    {
        return ModelsNote::class;
    }
}