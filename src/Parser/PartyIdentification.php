<?php

namespace Isdoc\Parser;

use Isdoc\Models\PartyIdentification as ModelsPartyIdentification;
use SimpleXMLElement;

class PartyIdentification extends Parser
{
    /**
     * @see \Isdoc\Tests\Parser\PartyIdentification\ParseXmlTest
     */
    public function parseXml(SimpleXMLElement $xml): ModelsPartyIdentification
    {
        $result = new ModelsPartyIdentification();
        $result->setId((string) $xml->ID);
        $result->setUserId($this->getAttributeOptional($xml->UserID));
        $result->setCatalogFirmIdentification($this->getAttributeOptional($xml->CatalogFirmIdentification));
        return $result;
    }  
}