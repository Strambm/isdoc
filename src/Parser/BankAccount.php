<?php

namespace Isdoc\Parser;

use Isdoc\Models\BankAccount as ModelsBankAccount;
use SimpleXMLElement;

class BankAccount extends Parser
{
    /**
     * @see \Isdoc\Tests\Parser\BankAccount\ParseXmlTest
     */
    public function parseXml(SimpleXMLElement $xml): ModelsBankAccount
    {
        $result = new ModelsBankAccount();
        $result->setBankCode((string) $xml->BankCode);
        $result->setBic((string) $xml->BIC);
        $result->setIban((string) $xml->IBAN);
        $result->setId((string) $xml->ID);
        $result->setName((string) $xml->Name);
        return $result;
    }
}