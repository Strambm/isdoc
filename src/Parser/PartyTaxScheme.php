<?php

namespace Isdoc\Parser;

use Isdoc\Models\PartyTaxScheme as ModelsPartyTaxScheme;
use SimpleXMLElement;

class PartyTaxScheme extends Parser
{
    /**
     * @see \Isdoc\Tests\Parser\PartyTaxScheme\ParseXmlTest
     */
    public function parseXml(SimpleXMLElement $xml): ModelsPartyTaxScheme
    {
        $result = new ModelsPartyTaxScheme((string) $xml->CompanyID, (string) $xml->TaxScheme);
        return $result;
    }
}