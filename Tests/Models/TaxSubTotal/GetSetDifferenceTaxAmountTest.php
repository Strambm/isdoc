<?php

namespace Isdoc\Tests\Models\TaxSubTotal;

use Isdoc\Models\TaxSubTotal;
use PHPUnit\Framework\TestCase;
use Cetria\Helpers\Reflection\Reflection;

class GetSetDifferenceTaxAmountTest extends TestCase
{
    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function get(float|null $newValue): void
    {
        $model = new TaxSubTotal();
        Reflection::setHiddenProperty($model, 'differenceTaxAmount', $newValue);
        $this->assertEquals($newValue, $model->getDifferenceTaxAmount());
    }

    public static function getSetDataProvider(): array
    {
        return [
            [
                0
            ], [
                15
            ], [
                -18.5
            ],
        ];
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function set(float|null $newValue): void
    {
        $model = new TaxSubTotal();
        $model->setDifferenceTaxAmount($newValue);
        $this->assertEquals($newValue, Reflection::getHiddenProperty($model, 'differenceTaxAmount'));
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function getSet(float|null $newValue): void
    {
        $model = new TaxSubTotal();
        $this->assertEquals($newValue, $model->setDifferenceTaxAmount($newValue)->getDifferenceTaxAmount());
    }

    /**
     * @test
     */
    public function automaticsCalc(): void
    {
        $model = new TaxSubTotal();
        $model->setTaxAmount(100);
        $model->setAlreadyClaimedTaxAmount(20);
        $this->assertEquals(80, $model->getDifferenceTaxAmount());
    }
}
