<?php

namespace Isdoc\Tests\Models\TaxSubTotal;

use Isdoc\Enums\CurrencyCode;
use Isdoc\Models\TaxCategory;
use Isdoc\Models\TaxedDeposit;
use Isdoc\Models\TaxSubTotal;
use Isdoc\Models\ExchangeRate;
use PHPUnit\Framework\TestCase;
use Isdoc\Enums\VatCalculationMethod;
use Isdoc\Models\ClassifiedTaxCategory;
use Isdoc\Exceptions\TaxCategoryPercentNotEqualsWithInvoiceLineTaxCategoryPercent;

class AddTaxedDepositTest extends TestCase
{
    /**
     * @test
     */
    public function throwIfTaxPercentNotEquals(): void
    {
        $this->expectException(TaxCategoryPercentNotEqualsWithInvoiceLineTaxCategoryPercent::class);
        $taxedDeposit = (new TaxedDeposit)
            ->setClassifiedTaxCategory((new ClassifiedTaxCategory)->setVatCalculationMethod(VatCalculationMethod::SHORA)->setPercent(10));
        $model = (new TaxSubTotal())
            ->setTaxCategory((new TaxCategory)->setPercent(10.1));
        $model->addTaxedDeposit($taxedDeposit);
    }

    /**
     * @test
     */
    public function inLocalCurrency(): void
    {
        $taxedDeposit = (new TaxedDeposit)
            ->setClassifiedTaxCategory((new ClassifiedTaxCategory)->setVatCalculationMethod(VatCalculationMethod::SHORA)->setPercent(10))
            ->setTaxInclusiveAmount(110);
        $model = (new TaxSubTotal())
            ->setTaxCategory((new TaxCategory)->setPercent(10));
        $model->addTaxedDeposit($taxedDeposit);
        $model->addTaxedDeposit($taxedDeposit);
        $this->assertEqualsWithDelta(20, $model->getAlreadyClaimedTaxAmount(), 0.001);
        $this->assertEqualsWithDelta(200, $model->getAlreadyClaimedTaxableAmount(), 0.001);
        $this->assertEqualsWithDelta(220, $model->getAlreadyClaimedTaxInclusiveAmount(), 0.001);
        $this->assertNull($model->getAlreadyClaimedTaxableAmountCurr());
        $this->assertNull($model->getAlreadyClaimedTaxAmountCurr());
        $this->assertNull($model->getAlreadyClaimedTaxInclusiveAmountCurr());
    }

    /**
     * @test
     */
    public function inForeignCurrency(): void
    {
        $taxedDeposit = (new TaxedDeposit)
            ->setClassifiedTaxCategory((new ClassifiedTaxCategory)->setVatCalculationMethod(VatCalculationMethod::SHORA)->setPercent(10))
            ->setTaxInclusiveAmount(110)
            ->setExchangeRate(new ExchangeRate(CurrencyCode::CZK, CurrencyCode::EUR, 23.55, 1));
        $model = (new TaxSubTotal())
            ->setTaxCategory((new TaxCategory)->setPercent(10));
        $model->addTaxedDeposit($taxedDeposit);
        $model->addTaxedDeposit($taxedDeposit);
        $this->assertEqualsWithDelta(20, $model->getAlreadyClaimedTaxAmount(), 0.001);
        $this->assertEqualsWithDelta(200, $model->getAlreadyClaimedTaxableAmount(), 0.001);
        $this->assertEqualsWithDelta(220, $model->getAlreadyClaimedTaxInclusiveAmount(), 0.001);
        $this->assertEqualsWithDelta(20 / 23.55, $model->getAlreadyClaimedTaxAmountCurr(), 0.001);
        $this->assertEqualsWithDelta(200 / 23.55, $model->getAlreadyClaimedTaxableAmountCurr(), 0.001);
        $this->assertEqualsWithDelta(220 / 23.55, $model->getAlreadyClaimedTaxInclusiveAmountCurr(), 0.001);
    }
}
