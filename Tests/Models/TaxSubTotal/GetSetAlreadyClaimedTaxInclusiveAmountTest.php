<?php

namespace Isdoc\Tests\Models\TaxSubTotal;

use Isdoc\Models\TaxSubTotal;
use PHPUnit\Framework\TestCase;
use Cetria\Helpers\Reflection\Reflection;

class GetSetAlreadyClaimedTaxInclusiveAmountTest extends TestCase
{
    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function get(float|null $newValue): void
    {
        $model = new TaxSubTotal();
        Reflection::setHiddenProperty($model, 'alreadyClaimedTaxInclusive', $newValue);
        $this->assertEquals($newValue, $model->getAlreadyClaimedTaxInclusiveAmount());
    }

    public static function getSetDataProvider(): array
    {
        return [
            [
                0
            ], [
                15
            ], [
                -18.5
            ]
        ];
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function set(float|null $newValue): void
    {
        $model = new TaxSubTotal();
        $model->setAlreadyClaimedTaxInclusiveAmount($newValue);
        $this->assertEquals($newValue, Reflection::getHiddenProperty($model, 'alreadyClaimedTaxInclusive'));
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function getSet(float|null $newValue): void
    {
        $model = new TaxSubTotal();
        $this->assertEquals($newValue, $model->setAlreadyClaimedTaxInclusiveAmount($newValue)->getAlreadyClaimedTaxInclusiveAmount());
    }
}
