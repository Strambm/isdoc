<?php

namespace Isdoc\Tests\Models\TaxSubTotal;

use Isdoc\Models\TaxSubTotal;
use PHPUnit\Framework\TestCase;
use Cetria\Helpers\Reflection\Reflection;

class GetSetDifferenceTaxableAmountTest extends TestCase
{
    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function get(float|null $newValue): void
    {
        $model = new TaxSubTotal();
        Reflection::setHiddenProperty($model, 'differenceTaxableAmount', $newValue);
        $this->assertEquals($newValue, $model->getDifferenceTaxableAmount());
    }

    public static function getSetDataProvider(): array
    {
        return [
            [
                0
            ], [
                15
            ], [
                -18.5
            ],
        ];
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function set(float|null $newValue): void
    {
        $model = new TaxSubTotal();
        $model->setDifferenceTaxableAmount($newValue);
        $this->assertEquals($newValue, Reflection::getHiddenProperty($model, 'differenceTaxableAmount'));
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function getSet(float|null $newValue): void
    {
        $model = new TaxSubTotal();
        $this->assertEquals($newValue, $model->setDifferenceTaxableAmount($newValue)->getDifferenceTaxableAmount());
    }

    /**
     * @test
     */
    public function automaticsCalc(): void
    {
        $model = new TaxSubTotal();
        $model->setTaxableAmount(100);
        $model->setAlreadyClaimedTaxableAmount(20);
        $this->assertEquals(80, $model->getDifferenceTaxableAmount());
    }
}
