<?php

namespace Isdoc\Tests\Models\TaxSubTotal;

use Isdoc\Models\Price;
use Isdoc\Models\TaxCategory;
use Isdoc\Models\TaxSubTotal;
use PHPUnit\Framework\TestCase;
use Isdoc\Enums\VatCalculationMethod;
use Isdoc\Exceptions\MissingTaxCategory;

class ToXmlElementTest extends TestCase
{
    /**
     * @test
     */
    public function withoutTaxCategory(): void
    {
        $this->expectException(MissingTaxCategory::class);
        $model = new TaxSubTotal();
        $model->toString();
    }

    /**
     * @test
     */
    public function onlyRequiredAttributes(): void
    {
        $taxCategory = (new TaxCategory)->setPercent(10);
        $price = (new Price($taxCategory->getPercent(), VatCalculationMethod::SHORA))->setValue(100); 
        $model = new TaxSubTotal();
        $model->setTaxCategory($taxCategory)
            ->setTaxableAmount($price->getValueWithoutVat())
            ->setTaxAmount($price->getValueOfVat())
            ->setTaxInclusiveAmount($price->getValueWithVat());
        $result = $model->toString();
        $structure = json_decode(json_encode(simplexml_load_string($result)), true);
        $this->assertCount(10, $structure);
        $this->assertEqualsWithDelta($price->getValueWithoutVat(), $structure['TaxableAmount'], 0.0001); 
        $this->assertEqualsWithDelta($price->getValueOfVat(), $structure['TaxAmount'], 0.0001); 
        $this->assertEqualsWithDelta($price->getValueWithVat(), $structure['TaxInclusiveAmount'], 0.0001); 
        $this->assertEqualsWithDelta(0, $structure['AlreadyClaimedTaxableAmount'], 0.0001); 
        $this->assertEqualsWithDelta(0, $structure['AlreadyClaimedTaxAmount'], 0.0001); 
        $this->assertEqualsWithDelta(0, $structure['AlreadyClaimedTaxInclusiveAmount'], 0.0001); 
        $this->assertEqualsWithDelta($price->getValueWithoutVat(), $structure['DifferenceTaxableAmount'], 0.0001); 
        $this->assertEqualsWithDelta($price->getValueOfVat(), $structure['DifferenceTaxAmount'], 0.0001); 
        $this->assertEqualsWithDelta($price->getValueWithVat(), $structure['DifferenceTaxInclusiveAmount'], 0.0001); 
        $this->assertEquals($taxCategory->getPercent(), $structure['TaxCategory']['Percent']);  
    }

    /**
     * @test
     */
    public function withNecessaryParameters(): void
    {
        $taxCategory = (new TaxCategory)->setPercent(20);
        $priceCZK = (new Price($taxCategory->getPercent(), VatCalculationMethod::SHORA))->setValue(100); 
        $priceUSD = (new Price($taxCategory->getPercent(), VatCalculationMethod::SHORA))->setValue(25); 
        $pricePaidBeforeCZK = (new Price($taxCategory->getPercent(), VatCalculationMethod::SHORA))->setValue(50); 
        $pricePaidBeforeUSD = (new Price($taxCategory->getPercent(), VatCalculationMethod::SHORA))->setValue(50); 
        $model = new TaxSubTotal();
        $model->setTaxCategory($taxCategory)
            ->setTaxableAmount($priceCZK->getValueWithoutVat())
            ->setTaxAmount($priceCZK->getValueOfVat())
            ->setTaxInclusiveAmount($priceCZK->getValueWithVat())
            ->setTaxableAmountCurr($priceUSD->getValueWithoutVat())
            ->setTaxAmountCurr($priceUSD->getValueOfVat())
            ->setTaxInclusiveAmountCurr($priceUSD->getValueWithVat())
            ->setAlreadyClaimedTaxableAmount($pricePaidBeforeCZK->getValueWithoutVat())
            ->setAlreadyClaimedTaxAmount($pricePaidBeforeCZK->getValueOfVat())
            ->setAlreadyClaimedTaxInclusiveAmount($pricePaidBeforeCZK->getValueWithVat())
            ->setAlreadyClaimedTaxableAmountCurr($pricePaidBeforeUSD->getValueWithoutVat())
            ->setAlreadyClaimedTaxAmountCurr($pricePaidBeforeUSD->getValueOfVat())
            ->setAlreadyClaimedTaxInclusiveAmountCurr($pricePaidBeforeUSD->getValueWithVat());
        $result = $model->toString();
        $structure = json_decode(json_encode(simplexml_load_string($result)), true);
        $this->assertCount(19, $structure);
        $this->assertEqualsWithDelta($priceCZK->getValueWithoutVat(), $structure['TaxableAmount'], 0.0001); 
        $this->assertEqualsWithDelta($priceCZK->getValueOfVat(), $structure['TaxAmount'], 0.0001); 
        $this->assertEqualsWithDelta($priceCZK->getValueWithVat(), $structure['TaxInclusiveAmount'], 0.0001); 
        $this->assertEqualsWithDelta($priceUSD->getValueWithoutVat(), $structure['TaxableAmountCurr'], 0.0001); 
        $this->assertEqualsWithDelta($priceUSD->getValueOfVat(), $structure['TaxAmountCurr'], 0.0001); 
        $this->assertEqualsWithDelta($priceUSD->getValueWithVat(), $structure['TaxInclusiveAmountCurr'], 0.0001); 
        $this->assertEqualsWithDelta($pricePaidBeforeCZK->getValueWithoutVat(), $structure['AlreadyClaimedTaxableAmount'], 0.0001); 
        $this->assertEqualsWithDelta($pricePaidBeforeCZK->getValueOfVat(), $structure['AlreadyClaimedTaxAmount'], 0.0001); 
        $this->assertEqualsWithDelta($pricePaidBeforeCZK->getValueWithVat(), $structure['AlreadyClaimedTaxInclusiveAmount'], 0.0001); 
        $this->assertEqualsWithDelta($pricePaidBeforeUSD->getValueWithoutVat(), $structure['AlreadyClaimedTaxableAmountCurr'], 0.0001); 
        $this->assertEqualsWithDelta($pricePaidBeforeUSD->getValueOfVat(), $structure['AlreadyClaimedTaxAmountCurr'], 0.0001); 
        $this->assertEqualsWithDelta($pricePaidBeforeUSD->getValueWithVat(), $structure['AlreadyClaimedTaxInclusiveAmountCurr'], 0.0001); 
        $this->assertEqualsWithDelta($priceCZK->getValueWithoutVat() - $pricePaidBeforeCZK->getValueWithoutVat(), $structure['DifferenceTaxableAmount'], 0.0001); 
        $this->assertEqualsWithDelta($priceCZK->getValueOfVat() - $pricePaidBeforeCZK->getValueOfVat(), $structure['DifferenceTaxAmount'], 0.0001); 
        $this->assertEqualsWithDelta($priceCZK->getValueWithVat() - $pricePaidBeforeCZK->getValueWithVat(), $structure['DifferenceTaxInclusiveAmount'], 0.0001); 
        $this->assertEqualsWithDelta($priceUSD->getValueWithoutVat() - $pricePaidBeforeUSD->getValueWithoutVat(), $structure['DifferenceTaxableAmountCurr'], 0.0001); 
        $this->assertEqualsWithDelta($priceUSD->getValueOfVat() - $pricePaidBeforeUSD->getValueOfVat(), $structure['DifferenceTaxAmountCurr'], 0.0001); 
        $this->assertEqualsWithDelta($priceUSD->getValueWithVat() - $pricePaidBeforeUSD->getValueWithVat(), $structure['DifferenceTaxInclusiveAmountCurr'], 0.0001);
        $this->assertEquals($taxCategory->getPercent(), $structure['TaxCategory']['Percent']); 
    }
}
