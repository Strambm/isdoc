<?php

namespace Isdoc\Tests\Models\TaxSubTotal;

use Isdoc\Models\TaxSubTotal;
use PHPUnit\Framework\TestCase;
use Cetria\Helpers\Reflection\Reflection;

class GetSetDifferenceTaxInclusiveAmountTest extends TestCase
{
    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function get(float|null $newValue): void
    {
        $model = new TaxSubTotal();
        Reflection::setHiddenProperty($model, 'differenceTaxInclusiveAmount', $newValue);
        $this->assertEquals($newValue, $model->getDifferenceTaxInclusiveAmount());
    }

    public static function getSetDataProvider(): array
    {
        return [
            [
                0
            ], [
                15
            ], [
                -18.5
            ],
        ];
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function set(float|null $newValue): void
    {
        $model = new TaxSubTotal();
        $model->setDifferenceTaxInclusiveAmount($newValue);
        $this->assertEquals($newValue, Reflection::getHiddenProperty($model, 'differenceTaxInclusiveAmount'));
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function getSet(float|null $newValue): void
    {
        $model = new TaxSubTotal();
        $this->assertEquals($newValue, $model->setDifferenceTaxInclusiveAmount($newValue)->getDifferenceTaxInclusiveAmount());
    }

    /**
     * @test
     */
    public function automaticsCalc(): void
    {
        $model = new TaxSubTotal();
        $model->setTaxInclusiveAmount(100);
        $model->setAlreadyClaimedTaxInclusiveAmount(20);
        $this->assertEquals(80, $model->getDifferenceTaxInclusiveAmount());
    }
}
