<?php

namespace Isdoc\Tests\Models\TaxSubTotal;

use Isdoc\Enums\CurrencyCode;
use Isdoc\Models\InvoiceLine;
use Isdoc\Models\TaxCategory;
use Isdoc\Models\TaxSubTotal;
use Isdoc\Models\ExchangeRate;
use PHPUnit\Framework\TestCase;
use Isdoc\Enums\VatCalculationMethod;
use Isdoc\Models\ClassifiedTaxCategory;
use Isdoc\Exceptions\TaxCategoryPercentNotEqualsWithInvoiceLineTaxCategoryPercent;

class AddInvoiceLineTest extends TestCase
{
    /**
     * @test
     */
    public function throwIfTaxPercentNotEquals(): void
    {
        $this->expectException(TaxCategoryPercentNotEqualsWithInvoiceLineTaxCategoryPercent::class);
        $invoiceLine = (new InvoiceLine)
            ->setClassifiedTaxCategory((new ClassifiedTaxCategory)->setVatCalculationMethod(VatCalculationMethod::SHORA)->setPercent(10));
        $model = (new TaxSubTotal())
            ->setTaxCategory((new TaxCategory)->setPercent(10.1));
        $model->addInvoiceLine($invoiceLine);
    }

    /**
     * @test
     */
    public function inLocalCurrency(): void
    {
        $invoiceLine = (new InvoiceLine)
            ->setClassifiedTaxCategory((new ClassifiedTaxCategory)->setVatCalculationMethod(VatCalculationMethod::SHORA)->setPercent(10))
            ->setLineExtensionAmountTaxInclusive(110);
        $model = (new TaxSubTotal())
            ->setTaxCategory((new TaxCategory)->setPercent(10));
        $model->addInvoiceLine($invoiceLine);
        $model->addInvoiceLine($invoiceLine);
        $this->assertEqualsWithDelta(20, $model->getTaxAmount(), 0.001);
        $this->assertEqualsWithDelta(200, $model->getTaxableAmount(), 0.001);
        $this->assertEqualsWithDelta(220, $model->getTaxInclusiveAmount(), 0.001);
        $this->assertNull($model->getTaxableAmountCurr());
        $this->assertNull($model->getTaxAmountCurr());
        $this->assertNull($model->getTaxInclusiveAmountCurr());
    }

    /**
     * @test
     */
    public function inForeignCurrency(): void
    {
        $invoiceLine = (new InvoiceLine)
            ->setClassifiedTaxCategory((new ClassifiedTaxCategory)->setVatCalculationMethod(VatCalculationMethod::SHORA)->setPercent(10))
            ->setLineExtensionAmountTaxInclusive(110)
            ->setExchangeRate(new ExchangeRate(CurrencyCode::CZK, CurrencyCode::EUR, 23.55, 1));
        $model = (new TaxSubTotal())
            ->setTaxCategory((new TaxCategory)->setPercent(10));
        $model->addInvoiceLine($invoiceLine);
        $model->addInvoiceLine($invoiceLine);
        $this->assertEqualsWithDelta(20, $model->getTaxAmount(), 0.001);
        $this->assertEqualsWithDelta(200, $model->getTaxableAmount(), 0.001);
        $this->assertEqualsWithDelta(220, $model->getTaxInclusiveAmount(), 0.001);
        $this->assertEqualsWithDelta(20 / 23.55, $model->getTaxAmountCurr(), 0.001);
        $this->assertEqualsWithDelta(200 / 23.55, $model->getTaxableAmountCurr(), 0.001);
        $this->assertEqualsWithDelta(220 / 23.55, $model->getTaxInclusiveAmountCurr(), 0.001);
    }
}
