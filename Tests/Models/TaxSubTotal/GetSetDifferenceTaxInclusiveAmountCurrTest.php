<?php

namespace Isdoc\Tests\Models\TaxSubTotal;

use Isdoc\Models\TaxSubTotal;
use PHPUnit\Framework\TestCase;
use Cetria\Helpers\Reflection\Reflection;

class GetSetDifferenceTaxInclusiveAmountCurrTest extends TestCase
{
    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function get(float|null $newValue): void
    {
        $model = new TaxSubTotal();
        Reflection::setHiddenProperty($model, 'differenceTaxInclusiveAmountCurr', $newValue);
        $this->assertEquals($newValue, $model->getDifferenceTaxInclusiveAmountCurr());
    }

    public static function getSetDataProvider(): array
    {
        return [
            [
                0
            ], [
                15
            ], [
                -18.5
            ],
        ];
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function set(float|null $newValue): void
    {
        $model = new TaxSubTotal();
        $model->setDifferenceTaxInclusiveAmountCurr($newValue);
        $this->assertEquals($newValue, Reflection::getHiddenProperty($model, 'differenceTaxInclusiveAmountCurr'));
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function getSet(float|null $newValue): void
    {
        $model = new TaxSubTotal();
        $this->assertEquals($newValue, $model->setDifferenceTaxInclusiveAmountCurr($newValue)->getDifferenceTaxInclusiveAmountCurr());
    }

    /**
     * @test
     */
    public function automaticsCalc(): void
    {
        $model = new TaxSubTotal();
        $model->setTaxInclusiveAmountCurr(100);
        $model->setAlreadyClaimedTaxInclusiveAmountCurr(20);
        $this->assertEquals(80, $model->getDifferenceTaxInclusiveAmountCurr());
    }
}
