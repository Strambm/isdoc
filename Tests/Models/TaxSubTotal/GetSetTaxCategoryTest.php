<?php

namespace Isdoc\Tests\Models\TaxSubTotal;

use Isdoc\Enums\TaxScheme;
use Isdoc\Models\TaxCategory;
use Isdoc\Models\TaxSubTotal;
use PHPUnit\Framework\TestCase;
use Isdoc\Exceptions\MissingTaxCategory;
use Cetria\Helpers\Reflection\Reflection;

class GetSetTaxCategoryTest extends TestCase
{
    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function get(TaxCategory $newValue): void
    {
        $model = new TaxSubTotal();
        Reflection::setHiddenProperty($model, 'taxCategory', $newValue);
        $result = $model->getTaxCategory();
        $this->assertEquals($newValue, $result);
    }

    public static function getSetDataProvider(): array
    {
        return [
            [
                (new TaxCategory)
                    ->setPercent(0)
                    ->setTaxScheme(TaxScheme::VAT)
                    ->setVATAplicable(true)
                    ->setLocalReverseChargeFlag(false)
            ], [
                (new TaxCategory)
                    ->setPercent(12.5)
            ]
        ];
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function set(TaxCategory $newValue): void
    {
        $model = new TaxSubTotal();
        $model->setTaxCategory($newValue);
        $result = Reflection::getHiddenProperty($model, 'taxCategory');
        $this->assertEquals($newValue, $result);
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function getSet(TaxCategory $newValue): void
    {
        $model = new TaxSubTotal();
        $result = $model->setTaxCategory($newValue)->getTaxCategory();
        $this->assertEquals($newValue, $result);
    }

    /**
     * @test
     */
    public function withoutValue(): void
    {
        $this->expectException(MissingTaxCategory::class);
        $model = new TaxSubTotal();
        $model->getTaxCategory();
    }
}
