<?php

namespace Isdoc\Tests\Models\TaxSubTotal;

use Isdoc\Models\TaxSubTotal;
use PHPUnit\Framework\TestCase;
use Cetria\Helpers\Reflection\Reflection;

class GetSetDifferenceTaxAmountCurrTest extends TestCase
{
    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function get(float|null $newValue): void
    {
        $model = new TaxSubTotal();
        Reflection::setHiddenProperty($model, 'differenceTaxAmountCurr', $newValue);
        $this->assertEquals($newValue, $model->getDifferenceTaxAmountCurr());
    }

    public static function getSetDataProvider(): array
    {
        return [
            [
                0
            ], [
                15
            ], [
                -18.5
            ],
        ];
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function set(float|null $newValue): void
    {
        $model = new TaxSubTotal();
        $model->setDifferenceTaxAmountCurr($newValue);
        $this->assertEquals($newValue, Reflection::getHiddenProperty($model, 'differenceTaxAmountCurr'));
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function getSet(float|null $newValue): void
    {
        $model = new TaxSubTotal();
        $this->assertEquals($newValue, $model->setDifferenceTaxAmountCurr($newValue)->getDifferenceTaxAmountCurr());
    }

    /**
     * @test
     */
    public function automaticsCalc(): void
    {
        $model = new TaxSubTotal();
        $model->setTaxAmountCurr(100);
        $model->setAlreadyClaimedTaxAmountCurr(20);
        $this->assertEquals(80, $model->getDifferenceTaxAmountCurr());
    }
}
