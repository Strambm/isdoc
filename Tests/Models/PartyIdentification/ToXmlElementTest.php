<?php

namespace Isdoc\Tests\Models\PartyIdentification;

use Isdoc\Exceptions\MissingPartyIdentifivationId;
use Isdoc\Models\PartyIdentification;
use PHPUnit\Framework\TestCase;

class ToXmlElementTest extends TestCase
{
    /**
     * @test
     */
    public function missingIdAttribute(): void
    {
        $this->expectException(MissingPartyIdentifivationId::class);
        $model = new PartyIdentification();
        $result = $model->toXmlElement();
    }

    /**
     * @test
     */
    public function onlyRequiredAttributes(): void
    {
        $ico = '27120384';
        $model = new PartyIdentification();
        $model->setId($ico);
        $result = $model->toString();
        $structure = json_decode(json_encode(simplexml_load_string($result)), true);
        $this->assertCount(1, $structure);
        $this->assertEquals($ico, $structure['ID']);
    }

    /**
     * @test
     */
    public function withNecessaryParameters(): void
    {
        $ico = '27120385';
        $userId = '123';
        $ean = '123456789x';
        $model = new PartyIdentification();
        $model->setCatalogFirmIdentification($ean)
            ->setId($ico)
            ->setUserId($userId);
        $result = $model->toString();
        $structure = json_decode(json_encode(simplexml_load_string($result)), true);
        $this->assertCount(3, $structure);
        $this->assertEquals($userId, $structure['UserID']);
        $this->assertEquals($ean, $structure['CatalogFirmIdentification']);
        $this->assertEquals($ico, $structure['ID']);
    }
}
