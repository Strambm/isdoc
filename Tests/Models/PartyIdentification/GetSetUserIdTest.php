<?php

namespace Isdoc\Tests\Models\PartyIdentification;

use Isdoc\Models\PartyIdentification;
use PHPUnit\Framework\TestCase;
use Cetria\Helpers\Reflection\Reflection;

class GetSetUserIdTest extends TestCase
{
    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function get(string|null $newValue): void
    {
        $model = new PartyIdentification();
        Reflection::setHiddenProperty($model, 'userId', $newValue);
        $this->assertEquals($newValue, $model->getUserId());
    }

    public static function getSetDataProvider(): array
    {
        return [
            [
                '12'
            ], [
                'XD-12'
            ], [
                null
            ]
        ];
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function set(string|null $newValue): void
    {
        $model = new PartyIdentification();
        $model->setUserId($newValue);
        $this->assertEquals($newValue, Reflection::getHiddenProperty($model, 'userId'));
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function getSet(string|null $newValue): void
    {
        $model = new PartyIdentification();
        $this->assertEquals($newValue, $model->setUserId($newValue)->getUserId());
    }
}
