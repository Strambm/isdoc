<?php

namespace Isdoc\Tests\Models\PartyIdentification;

use Isdoc\Exceptions\MissingPartyIdentifivationId;
use Isdoc\Models\PartyIdentification;
use PHPUnit\Framework\TestCase;
use Cetria\Helpers\Reflection\Reflection;

class GetSetIdTest extends TestCase
{
    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function get(string $newValue): void
    {
        $model = new PartyIdentification();
        Reflection::setHiddenProperty($model, 'id', $newValue);
        $this->assertEquals($newValue, $model->getId());
    }

    public static function getSetDataProvider(): array
    {
        return [
            [
                '25111299'
            ], [
                '44012373 '
            ]
        ];
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function set(string $newValue): void
    {
        $model = new PartyIdentification();
        $model->setId($newValue);
        $this->assertEquals($newValue, Reflection::getHiddenProperty($model, 'id'));
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function getSet(string $newValue): void
    {
        $model = new PartyIdentification();
        $this->assertEquals($newValue, $model->setId($newValue)->getId());
    }

    /**
     * @test
     */
    public function getWithoutValue(): void
    {
        $this->expectException(MissingPartyIdentifivationId::class);
        $model = new PartyIdentification();
        $model->getId();
    }
}
