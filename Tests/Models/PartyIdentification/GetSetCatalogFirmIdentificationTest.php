<?php

namespace Isdoc\Tests\Models\PartyIdentification;

use Isdoc\Models\PartyIdentification;
use PHPUnit\Framework\TestCase;
use Cetria\Helpers\Reflection\Reflection;

class GetSetCatalogFirmIdentificationTest extends TestCase
{
    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function get(string|null $newValue): void
    {
        $model = new PartyIdentification();
        Reflection::setHiddenProperty($model, 'catalogFirmIdentification', $newValue);
        $this->assertEquals($newValue, $model->getCatalogFirmIdentification());
    }

    public static function getSetDataProvider(): array
    {
        return [
            [
                '0000 0004 0504 2170'
            ], [
                '0000 0001 2127 202X'
            ], [
                null
            ]
        ];
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function set(string|null $newValue): void
    {
        $model = new PartyIdentification();
        $model->setCatalogFirmIdentification($newValue);
        $this->assertEquals($newValue, Reflection::getHiddenProperty($model, 'catalogFirmIdentification'));
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function getSet(string|null $newValue): void
    {
        $model = new PartyIdentification();
        $this->assertEquals($newValue, $model->setCatalogFirmIdentification($newValue)->getCatalogFirmIdentification());
    }
}
