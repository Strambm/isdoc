<?php

namespace Isdoc\Tests\Models\Contact;

use Isdoc\Models\Contact;
use PHPUnit\Framework\TestCase;
use Cetria\Helpers\Reflection\Reflection;

class GetSetElectronicMailTest extends TestCase
{
    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function get(string|null $newValue): void
    {
        $model = new Contact();
        Reflection::setHiddenProperty($model, 'electronicMail', $newValue);
        $this->assertEquals($newValue, $model->getElectronicMail());
    }

    public static function getSetDataProvider(): array
    {
        return [
            [
                'test@test.com'
            ], [
                'a@a.cz'
            ], [
                null
            ]
        ];
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function set(string|null $newValue): void
    {
        $model = new Contact();
        $model->setElectronicMail($newValue);
        $this->assertEquals($newValue, Reflection::getHiddenProperty($model, 'electronicMail'));
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function getSet(string|null $newValue): void
    {
        $model = new Contact();
        $this->assertEquals($newValue, $model->setElectronicMail($newValue)->getElectronicMail());
    }
}
