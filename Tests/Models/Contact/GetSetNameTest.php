<?php

namespace Isdoc\Tests\Models\Contact;

use Isdoc\Models\Contact;
use PHPUnit\Framework\TestCase;
use Cetria\Helpers\Reflection\Reflection;

class GetSetNameTest extends TestCase
{
    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function get(string|null $newValue): void
    {
        $model = new Contact();
        Reflection::setHiddenProperty($model, 'name', $newValue);
        $this->assertEquals($newValue, $model->getName());
    }

    public static function getSetDataProvider(): array
    {
        return [
            [
                'Jmeno'
            ], [
                'Jméno a Příjmení'
            ], [
                null
            ]
        ];
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function set(string|null $newValue): void
    {
        $model = new Contact();
        $model->setName($newValue);
        $this->assertEquals($newValue, Reflection::getHiddenProperty($model, 'name'));
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function getSet(string|null $newValue): void
    {
        $model = new Contact();
        $this->assertEquals($newValue, $model->setName($newValue)->getName());
    }
}
