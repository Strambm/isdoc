<?php

namespace Isdoc\Tests\Models\Contact;

use Isdoc\Models\Contact;
use PHPUnit\Framework\TestCase;
use Cetria\Helpers\Reflection\Reflection;

class GetSetTelephoneTest extends TestCase
{
    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function get(string|null $newValue): void
    {
        $model = new Contact();
        Reflection::setHiddenProperty($model, 'telephone', $newValue);
        $this->assertEquals($newValue, $model->getTelephone());
    }

    public static function getSetDataProvider(): array
    {
        return [
            [
                '+420 800 500 800'
            ], [
                '+420000000'
            ], [
                null
            ], [
                '00500500500'
            ]
        ];
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function set(string|null $newValue): void
    {
        $model = new Contact();
        $model->setTelephone($newValue);
        $this->assertEquals($newValue, Reflection::getHiddenProperty($model, 'telephone'));
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function getSet(string|null $newValue): void
    {
        $model = new Contact();
        $this->assertEquals($newValue, $model->setTelephone($newValue)->getTelephone());
    }
}
