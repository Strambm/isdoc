<?php

namespace Isdoc\Tests\Models\Contact;

use Isdoc\Models\Contact;
use PHPUnit\Framework\TestCase;

class ToXmlElementTest extends TestCase
{
    /**
     * @test
     */
    public function empty(): void
    {
        $model = new Contact();
        $result = $model->toString();
        $structure = json_decode(json_encode(simplexml_load_string($result)), true);
        $this->assertCount(0, $structure);
    }

    /**
     * @test
     */
    public function withNecessaryParameters(): void
    {
        $name = 'Test Testovič';
        $phone = '123456789';
        $mail = 'a@seznam.cz';
        $model = new Contact();
        $model->setElectronicMail($mail)
            ->setName($name)
            ->setTelephone($phone);
        $result = $model->toString();
        $structure = json_decode(json_encode(simplexml_load_string($result)), true);
        $this->assertCount(3, $structure);
        $this->assertEquals($name, $structure['Name']);
        $this->assertEquals($phone, $structure['Telephone']);  
        $this->assertEquals($mail, $structure['ElectronicMail']); 
    }
}
