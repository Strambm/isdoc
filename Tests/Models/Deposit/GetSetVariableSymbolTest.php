<?php

namespace Isdoc\Tests\Models\Deposit;

use Isdoc\Models\Deposit;
use PHPUnit\Framework\TestCase;
use Cetria\Helpers\Reflection\Reflection;

class GetSetVariableSymbolTest extends TestCase
{
    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function get(string $newValue): void
    {
        $model = $this->getObject();
        Reflection::setHiddenProperty($model, 'vs', $newValue);
        $result = $model->getVariableSymbol();
        $this->assertEquals($newValue, $result);
    }

    protected function getObject(): Deposit
    {
        $model = $this->getMockForAbstractClass(Deposit::class);
        return $model;
    }

    public static function getSetDataProvider(): array
    {
        return [
            [
                ''
            ], [
                '1234'
            ], [
                '123456'
            ]
        ];
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function set(string $newValue): void
    {
        $model = $this->getObject();
        $model->setVariableSymbol($newValue);
        $result = Reflection::getHiddenProperty($model, 'vs');
        $this->assertEquals($newValue, $result);
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function getSet(string $newValue): void
    {
        $model = $this->getObject();
        $result = $model->setVariableSymbol($newValue)->getVariableSymbol();
        $this->assertEquals($newValue, $result);
    }
}
