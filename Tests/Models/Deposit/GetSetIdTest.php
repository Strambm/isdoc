<?php

namespace Isdoc\Tests\Models\TaxedDeposit;

use Isdoc\Models\Deposit;
use PHPUnit\Framework\TestCase;
use Cetria\Helpers\Reflection\Reflection;

class GetSetIdTest extends TestCase
{
    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function get(string $newValue): void
    {
        $model = $this->getObject();
        Reflection::setHiddenProperty($model, 'id', $newValue);
        $result = $model->getId();
        $this->assertEquals($newValue, $result);
    }

    protected function getObject(): Deposit
    {
        $model = $this->getMockForAbstractClass(Deposit::class);
        return $model;
    }

    public static function getSetDataProvider(): array
    {
        return [
            [
                ''
            ], [
                '1234w'
            ], [
                'AB-1515-HI-48'
            ]
        ];
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function set(string $newValue): void
    {
        $model = $this->getObject();
        $model->setId($newValue);
        $result = Reflection::getHiddenProperty($model, 'id');
        $this->assertEquals($newValue, $result);
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function getSet(string $newValue): void
    {
        $model = $this->getObject();
        $result = $model->setId($newValue)->getId();
        $this->assertEquals($newValue, $result);
    }
}
