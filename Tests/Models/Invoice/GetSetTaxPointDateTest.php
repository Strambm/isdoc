<?php

namespace Isdoc\Tests\Models\Invoice;

use Isdoc\Models\Invoice;
use PHPUnit\Framework\TestCase;
use Isdoc\Exceptions\InvalidDateFormat;
use Cetria\Helpers\Reflection\Reflection;

class GetSetTaxPointDateTest extends TestCase
{
    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function get(string|null $newValue): void
    {
        $model = new Invoice();
        Reflection::setHiddenProperty($model, 'taxPointDate', $newValue);
        $this->assertEquals($newValue, $model->getTaxPointDate());
    }

    public static function getSetDataProvider(): array
    {
        return [
            [
                '2021-01-01'
            ], [
                '2023-01-01'
            ], [
                null
            ]
        ];
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function set(string|null $newValue): void
    {
        $model = new Invoice();
        $model->setTaxPointDate($newValue);
        $this->assertEquals($newValue, Reflection::getHiddenProperty($model, 'taxPointDate'));
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function getSet(string|null $newValue): void
    {
        $model = new Invoice();
        $this->assertEquals($newValue, $model->setTaxPointDate($newValue)->getTaxPointDate());
    }

    /**
     * @test
     * @dataProvider invalidDateFormatDataProvider
     */
    public function throwInvalidDateFormat(string $value): void
    {
        $this->expectException(InvalidDateFormat::class);
        $model = new Invoice();
        $model->setTaxPointDate($value);
    }

    public static function invalidDateFormatDataProvider(): array
    {
        return [
            [
                '1.2.2022',
            ], [
                ''
            ], [
                '2022-01-01 15:00:00'
            ]
        ];
    }
}
