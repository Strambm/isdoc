<?php

namespace Isdoc\Tests\Models\Invoice;

use Isdoc\Models\Invoice;
use PHPUnit\Framework\TestCase;
use Cetria\Helpers\Reflection\Reflection;
use Isdoc\Exceptions\MissingVATApplicable;

class GetSetVATAplicableTest extends TestCase
{
    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function get(bool|null $newValue): void
    {
        $model = new Invoice();
        Reflection::setHiddenProperty($model, 'vatApplicable', $newValue);
        $result = $model->getVATApplicable();
        $this->assertEquals($newValue, $result);
    }

    public static function getSetDataProvider(): array
    {
        return [
            [
                true
            ], [
                false
            ]
        ];
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function set(bool|null $newValue): void
    {
        $model = new Invoice();
        $model->setVATApplicable($newValue);
        $result = Reflection::getHiddenProperty($model, 'vatApplicable');
        $this->assertEquals($newValue, $result);
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function getSet(bool|null $newValue): void
    {
        $model = new Invoice();
        $result = $model->setVATApplicable($newValue)->getVATApplicable();
        $this->assertEquals($newValue, $result);
    }

    /**
     * @test
     */
    public function withoutValue(): void
    {
        $this->expectException(MissingVATApplicable::class);
        $model = new Invoice();
        $model->getVATApplicable();
    }
}
