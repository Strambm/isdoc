<?php

namespace Isdoc\Tests\Models\Invoice;

use Isdoc\Enums\DocumentType;
use Isdoc\Exceptions\UnvalidDocumentType;
use Isdoc\Models\Invoice;
use PHPUnit\Framework\TestCase;
use Cetria\Helpers\Reflection\Reflection;

class GetSetDocumentTypeTest extends TestCase
{
    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function get(int $newValue): void
    {
        $model = new Invoice();
        Reflection::setHiddenProperty($model, 'documentType', $newValue);
        $this->assertEquals($newValue, $model->getDocumentType());
    }

    public static function getSetDataProvider(): array
    {
        return [
            [
                DocumentType::BASIC_INVOICE
            ], [
                DocumentType::CORRECTING_INVOICE
            ], [
                DocumentType::PROFORMA
            ],
        ];
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function set(int $newValue): void
    {
        $model = $model = new Invoice();
        $model->setDocumentType($newValue);
        $this->assertEquals($newValue, Reflection::getHiddenProperty($model, 'documentType'));
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function getSet(int $newValue): void
    {
        $model = $model = new Invoice();
        $this->assertEquals($newValue, $model->setDocumentType($newValue)->getDocumentType());
    }

    /**
     * @test
     */
    public function throwUnvalidocumentType(): void
    {
        $this->expectException(UnvalidDocumentType::class);
        $model = $model = new Invoice();
        $model->setDocumentType(-55);
    }
}
