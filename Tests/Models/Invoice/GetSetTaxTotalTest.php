<?php

namespace Isdoc\Tests\Models\Invoice;

use Isdoc\Enums\VatCalculationMethod;
use Isdoc\Models\ClassifiedTaxCategory;
use Isdoc\Models\Invoice;
use Isdoc\Models\InvoiceLine;
use Isdoc\Models\TaxCategory;
use Isdoc\Models\TaxedDeposit;
use Isdoc\Models\TaxSubTotal;
use Isdoc\Models\TaxTotal;
use PHPUnit\Framework\TestCase;
use Cetria\Helpers\Reflection\Reflection;

class GetSetTaxTotalTest extends TestCase
{
    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function get(TaxTotal $newValue): void
    {
        $model = new Invoice();
        Reflection::setHiddenProperty($model, 'taxTotal', $newValue);
        $this->assertEquals($newValue, $model->getTaxTotal());
    }

    public static function getSetDataProvider(): array
    {
        return [
            [
                (new TaxTotal())
                    ->addTaxSubTotal(
                        (new TaxSubTotal())
                            ->setTaxCategory(
                                (new TaxCategory())
                                    ->setPercent(10)
                            )->setTaxableAmount(500)
                            ->setTaxAmount(50)
                            ->setTaxInclusiveAmount(550)
                    )
            ], 
        ];
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function set(TaxTotal $newValue): void
    {
        $model = new Invoice();
        $model->setTaxTotal($newValue);
        $this->assertEquals($newValue, Reflection::getHiddenProperty($model, 'taxTotal'));
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function getSet(TaxTotal $newValue): void
    {
        $model = new Invoice();
        $this->assertEquals($newValue, $model->setTaxTotal($newValue)->getTaxTotal());
    }

    /**
     * @test
     */
    public function automaticsCalc(): void
    {
        $invoiceLine = (new InvoiceLine())
            ->setLineExtensionAmount(100)
            ->setClassifiedTaxCategory(
                (new ClassifiedTaxCategory())
                    ->setPercent(10)
                    ->setVatCalculationMethod(VatCalculationMethod::ZDOLA)
            );
        $taxedDeposit = (new TaxedDeposit())
            ->setClassifiedTaxCategory(
                (new ClassifiedTaxCategory())
                    ->setPercent(10)
                    ->setVatCalculationMethod(VatCalculationMethod::ZDOLA)
            )->setTaxableDepositAmount(100);
        $model = new Invoice();
        $model->addInvoiceLine($invoiceLine);
        $model->addTaxedDeposit($taxedDeposit);
        $taxTotal = $model->getTaxTotal();
        $this->assertEqualsWithDelta(100, $taxTotal->getTaxSubTotals()[0]->getTaxableAmount(), 0.001);
        $this->assertEqualsWithDelta(100, $taxTotal->getTaxSubTotals()[0]->getAlreadyClaimedTaxableAmount(), 0.001);
        $this->assertEqualsWithDelta(10, $taxTotal->getTaxAmount(), 0.001);
    }
}
