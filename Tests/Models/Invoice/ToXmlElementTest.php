<?php

namespace Isdoc\Tests\Models\Invoice;

use Isdoc\Models\Note;
use Isdoc\Models\Contact;
use Isdoc\Models\Invoice;
use Isdoc\Models\Payment;
use Isdoc\Enums\TaxScheme;
use Isdoc\Enums\CurrencyCode;
use Isdoc\Enums\DocumentType;
use Isdoc\Models\InvoiceLine;
use Isdoc\Models\ExchangeRate;
use Isdoc\Models\PartyContact;
use Isdoc\Models\PaymentMeans;
use Isdoc\Models\TaxedDeposit;
use Isdoc\Models\PostalAddress;
use PHPUnit\Framework\TestCase;
use Isdoc\Models\OrderReference;
use Isdoc\Models\PartyTaxScheme;
use Isdoc\Traits\BoolValidation;
use Isdoc\Traits\UUIDValidation;
use Isdoc\Enums\PaymentMeansCode;
use Isdoc\Models\NonTaxedDeposit;
use Isdoc\Enums\VatCalculationMethod;
use Isdoc\Models\PartyIdentification;
use Isdoc\Models\ClassifiedTaxCategory;
use Isdoc\Models\DeliveryNoteReference;
use Isdoc\Models\OriginalDocumentReference;
use Isdoc\Models\ElectronicPossibilityAgreementReference;

class ToXmlElementTest extends TestCase
{
    use UUIDValidation;
    use BoolValidation;
    /**
     * @test
     */
    public function onlyRequiredAttributes(): void
    {
        $invoice = new Invoice();
        $invoice->setId('TEST-1')
            ->setIssueDate('2024-01-01')
            ->setVATApplicable(true)
            ->setExchangeRate(new ExchangeRate(CurrencyCode::CZK))
            ->setAccountingSupplierParty(
                (new PartyContact())
                    ->setPartyIdentification(
                        (new PartyIdentification())
                            ->setId('123456')
                    )->setName('Společnost XYZ')
                    ->setPostalAddress(
                        (new PostalAddress())
                            ->setStreetName('UliceX')
                            ->setBuildingNumber('12/X')
                            ->setCityName('MěstoX')
                            ->setPostalZone('12345')
                    )->addPartyTaxScheme(new PartyTaxScheme('123456', TaxScheme::VAT))
            )->setAccountingCustomerParty(
                (new PartyContact())
                ->setPartyIdentification(
                    (new PartyIdentification())
                        ->setId('')
                )->setName('')
                ->setPostalAddress(
                    (new PostalAddress())
                        ->setStreetName('')
                        ->setBuildingNumber('')
                        ->setCityName('')
                        ->setPostalZone('')
                )
            )->addInvoiceLine(
                (new InvoiceLine())
                    ->setId('IL-0001')
                    ->setLineExtensionAmount(5000)
                    ->setUnitPrice(500)
                    ->setClassifiedTaxCategory(
                        (new ClassifiedTaxCategory())
                            ->setPercent(10)
                            ->setVatCalculationMethod(VatCalculationMethod::ZDOLA)
                    )
            );
            $result = $invoice->toString();
            $structure = json_decode(json_encode(simplexml_load_string($result)), true);
            $this->assertCount(16, $structure);
            $this->assertTrue(array_key_exists('@attributes', $structure));
            $this->assertEquals(DocumentType::INVOICE, $structure['DocumentType']);
            $this->assertEquals('TEST-1', $structure['ID']);
            $this->assertTrue(array_key_exists('UUID', $structure));
            $this->validateUuid($structure['UUID']);
            $this->assertTrue(array_key_exists('IssuingSystem', $structure));
            $this->assertEquals('2024-01-01', $structure['IssueDate']);
            $this->assertEquals('CZK', $structure['LocalCurrencyCode']);
            $this->assertEquals($this->boolToStr(true), $structure['VATApplicable']);
            $this->assertTrue(array_key_exists('ElectronicPossibilityAgreementReference', $structure));
            $this->assertEquals(1, $structure['CurrRate']);
            $this->assertEquals(1, $structure['RefCurrRate']);
            $this->assertEquals('123456', $structure['AccountingSupplierParty']['Party']['PartyIdentification']['ID']);
            $this->assertTrue(array_key_exists('AccountingCustomerParty', $structure));
            $this->assertEquals('IL-0001', $structure['InvoiceLines']['InvoiceLine']['ID']);
            $this->assertEquals('5000', $structure['TaxTotal']['TaxSubTotal']['TaxableAmount']);
            $this->assertEquals('5000', $structure['LegalMonetaryTotal']['TaxExclusiveAmount']);
    }

    /**
     * @test
     */
    public function withNecessaryParameters(): void
    {
        $invoice = new Invoice();
        $invoice->setId('TEST-1')
            ->setDocumentType(DocumentType::CORRECTING_INVOICE)
            ->setElectronicPossibilityAgreementReference(
                (new ElectronicPossibilityAgreementReference())
                    ->setValue('ZPRAVA 1')
            )
            ->setNote(
                (new Note())
                    ->setValue('ZPRAVA 2')
            )
            ->setIssueDate('2024-01-01')
            ->setTaxPointDate('2024-02-01')
            ->setVATApplicable(true)
            ->setExchangeRate(new ExchangeRate(CurrencyCode::CZK, CurrencyCode::EUR, 2, 4))
            ->setAccountingSupplierParty(
                (new PartyContact())
                    ->setPartyIdentification(
                        (new PartyIdentification())
                            ->setId('123456')
                    )->setName('Společnost XYZ')
                    ->setPostalAddress(
                        (new PostalAddress())
                            ->setStreetName('UliceX')
                            ->setBuildingNumber('12/X')
                            ->setCityName('MěstoX')
                            ->setPostalZone('12345')
                    )->addPartyTaxScheme(new PartyTaxScheme('123456', TaxScheme::VAT))
            )->setAccountingCustomerParty(
                (new PartyContact())
                ->setPartyIdentification(
                    (new PartyIdentification())
                        ->setId('')
                )->setName('')
                ->setPostalAddress(
                    (new PostalAddress())
                        ->setStreetName('')
                        ->setBuildingNumber('')
                        ->setCityName('')
                        ->setPostalZone('')
                )
            )->addInvoiceLine(
                (new InvoiceLine())
                    ->setId('IL-0001')
                    ->setLineExtensionAmount(5000)
                    ->setUnitPrice(500)
                    ->setClassifiedTaxCategory(
                        (new ClassifiedTaxCategory())
                            ->setPercent(10)
                            ->setVatCalculationMethod(VatCalculationMethod::ZDOLA)
                    )
            )->addInvoiceLine(
                (new InvoiceLine())
                    ->setId('IL-0002')
                    ->setLineExtensionAmount(5000)
                    ->setUnitPrice(500)
                    ->setClassifiedTaxCategory(
                        (new ClassifiedTaxCategory())
                            ->setPercent(10)
                            ->setVatCalculationMethod(VatCalculationMethod::ZDOLA)
                    )
            )->addOriginalDocumentReference(
                (new OriginalDocumentReference())
                    ->setId('TEST-2')
            )->setPaymentMeans(
                (new PaymentMeans())
                    ->addPayment(
                        (new Payment())
                            ->setPaidAmount(10000)
                            ->setPaymentMeansCode(PaymentMeansCode::CASH_ON_DELIVERY)
                    )
            )->setIssuingSystem('SYSTEM123')
            ->addDeliveryNoteReference(
                (new DeliveryNoteReference())
                    ->setId('TEST-3')
            )->addOrderReference(
                (new OrderReference())
                    ->setSalesOrderId('ORDER-1')
            )->setDeliveryAddress(
                (new PartyContact())
                    ->setName('Test Testovič')
                    ->setContact(
                        (new Contact())
                            ->setName('Test Testovič')
                            ->setElectronicMail('test@isdoc.cz')
                            ->setTelephone('123 456 789')
                    )->setPostalAddress(
                        (new PostalAddress())
                            ->setBuildingNumber('1')
                            ->setCityName('Testovací město')
                            ->setPostalZone('78945')
                            ->setStreetName('Testovací ulice')
                    )->setPartyIdentification(
                        (new PartyIdentification())
                            ->setId('')
                    )
            )->addTaxedDeposit(
                (new TaxedDeposit())
                    ->setId('TEST-4')
                    ->setVariableSymbol('123')
                    ->setTaxableDepositAmount(2000)
                    ->setClassifiedTaxCategory(
                        (new ClassifiedTaxCategory())
                            ->setPercent(10)
                            ->setVatCalculationMethod(VatCalculationMethod::ZDOLA)
                    )
            )->addNonTaxedDeposit(
                (new NonTaxedDeposit())
                    ->setId('TEST-5')
                    ->setVariableSymbol('1234')
                    ->setDepositAmount(2000)
            );
            $result = $invoice->toString();
            $structure = json_decode(json_encode(simplexml_load_string($result)), true);
            $this->assertCount(26, $structure);
            $this->assertTrue(array_key_exists('@attributes', $structure));
            $this->assertEquals(DocumentType::CORRECTING_INVOICE, $structure['DocumentType']);
            $this->assertEquals('TEST-1', $structure['ID']);
            $this->assertTrue(array_key_exists('UUID', $structure));
            $this->validateUuid($structure['UUID']);
            $this->assertTrue(array_key_exists('IssuingSystem', $structure));
            $this->assertEquals('2024-01-01', $structure['IssueDate']);
            $this->assertEquals('2024-02-01', $structure['TaxPointDate']);
            $this->assertEquals('CZK', $structure['LocalCurrencyCode']);
            $this->assertEquals('EUR', $structure['ForeignCurrencyCode']);
            $this->assertEquals($this->boolToStr(true), $structure['VATApplicable']);
            $this->assertEquals('ZPRAVA 1', $structure['ElectronicPossibilityAgreementReference']);
            $this->assertEquals('ZPRAVA 2', $structure['Note']);
            $this->assertEquals(2, $structure['CurrRate']);
            $this->assertEquals(4, $structure['RefCurrRate']);
            $this->assertEquals('123456', $structure['AccountingSupplierParty']['Party']['PartyIdentification']['ID']);
            $this->assertTrue(array_key_exists('AccountingCustomerParty', $structure));
            $this->assertEquals('IL-0001', $structure['InvoiceLines']['InvoiceLine'][0]['ID']);
            $this->assertEquals('2500', $structure['InvoiceLines']['InvoiceLine'][0]['LineExtensionAmountCurr']);
            $this->assertEquals('10000', $structure['TaxTotal']['TaxSubTotal']['TaxableAmount']);
            $this->assertEquals('10000', $structure['LegalMonetaryTotal']['TaxExclusiveAmount']);
            $this->assertEquals('TEST-2', $structure['OriginalDocumentReferences']['OriginalDocumentReference']['ID']);
            $this->assertEquals('10000', $structure['PaymentMeans']['Payment']['PaidAmount']);
            $this->assertEquals('SYSTEM123', $structure['IssuingSystem']);
            $this->assertEquals('TEST-3', $structure['DeliveryNoteReferences']['DeliveryNoteReference']['ID']);
            $this->assertEquals('ORDER-1', $structure['OrderReferences']['OrderReference']['SalesOrderID']);
            $this->assertEquals('123 456 789', $structure['Delivery']['Party']['Contact']['Telephone']);
            $this->assertEquals('TEST-4', $structure['TaxedDeposits']['TaxedDeposit']['ID']);
            $this->assertEquals('TEST-5', $structure['NonTaxedDeposits']['NonTaxedDeposit']['ID']);
    }
}
