<?php

namespace Isdoc\Tests\Models\Invoice;

use Isdoc\Models\Invoice;
use Isdoc\Models\OriginalDocumentReference;
use PHPUnit\Framework\TestCase;
use Cetria\Helpers\Reflection\Reflection;

class GetAddOriginalDocumentReferenceTest extends TestCase
{
    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function get(array $newValue): void
    {
        $model = new Invoice();
        Reflection::setHiddenProperty($model, 'originalDocumentReferences', $newValue);
        $result = $model->getOriginalDocumentReferences();
        $this->assertEquals($newValue, $result);
    }

    public static function getSetDataProvider(): array
    {
        return [
            [
                [
                    (new OriginalDocumentReference())
                        ->setId('123'),
                    (new OriginalDocumentReference())
                        ->setId('124')
                ]
            ]
        ];
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function set(array $newValue): void
    {
        $model = new Invoice();
        foreach($newValue as $value) {
            $model = $model->addOriginalDocumentReference($value);
        }
        $result = Reflection::getHiddenProperty($model, 'originalDocumentReferences');
        $this->assertEquals($newValue, $result);
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function getSet(array $newValue): void
    {
        $model = new Invoice();
        foreach($newValue as $value) {
            $model = $model->addOriginalDocumentReference($value);
        }
        $result = $model->getOriginalDocumentReferences();
        $this->assertEquals($newValue, $result);
    }

    /**
     * @test
     */
    public function withoutValue(): void
    {
        $model = new Invoice();
        $this->assertNull($model->getOrderReferences());
    }
}
