<?php

namespace Isdoc\Tests\Models\Invoice;

use Isdoc\Models\DeliveryNoteReference;
use Isdoc\Models\Invoice;
use PHPUnit\Framework\TestCase;
use Cetria\Helpers\Reflection\Reflection;

class GetAddDeliveryNoteReferenceTest extends TestCase
{
    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function get(array $newValue): void
    {
        $model = new Invoice();
        Reflection::setHiddenProperty($model, 'deliveryNoteReferences', $newValue);
        $result = $model->getDeliveryNoteReferences();
        $this->assertEquals($newValue, $result);
    }

    public static function getSetDataProvider(): array
    {
        return [
            [
                [
                    (new DeliveryNoteReference())
                        ->setId('123'),
                    (new DeliveryNoteReference())
                        ->setId('124')
                ]
            ]
        ];
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function set(array $newValue): void
    {
        $model = new Invoice();
        foreach($newValue as $value) {
            $model = $model->addDeliveryNoteReference($value);
        }
        $result = Reflection::getHiddenProperty($model, 'deliveryNoteReferences');
        $this->assertEquals($newValue, $result);
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function getSet(array $newValue): void
    {
        $model = new Invoice();
        foreach($newValue as $value) {
            $model = $model->addDeliveryNoteReference($value);
        }
        $result = $model->getDeliveryNoteReferences();
        $this->assertEquals($newValue, $result);
    }

    /**
     * @test
     */
    public function withoutValue(): void
    {
        $model = new Invoice();
        $this->assertNull($model->getOrderReferences());
    }
}
