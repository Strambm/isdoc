<?php

namespace Isdoc\Tests\Models\Invoice;

use Isdoc\Enums\VatCalculationMethod;
use Isdoc\Models\ClassifiedTaxCategory;
use Isdoc\Models\Invoice;
use Isdoc\Models\TaxedDeposit;
use PHPUnit\Framework\TestCase;
use Cetria\Helpers\Reflection\Reflection;

class GetAddTaxedDepositTest extends TestCase
{
    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function get(array $newValue): void
    {
        $model = new Invoice();
        Reflection::setHiddenProperty($model, 'taxedDeposits', $newValue);
        $result = $model->getTaxedDeposits();
        $this->assertEquals($newValue, $result);
    }

    public static function getSetDataProvider(): array
    {
        return [
            [
                [
                    (new TaxedDeposit)
                        ->setClassifiedTaxCategory(
                            (new ClassifiedTaxCategory())
                                ->setPercent(0)
                                ->setVatCalculationMethod(VatCalculationMethod::ZDOLA)
                                ->setVatApplicable(true)
                        )->setVariableSymbol('852')
                        ->setTaxableDepositAmount(200),
                    (new TaxedDeposit)
                        ->setClassifiedTaxCategory(
                            (new ClassifiedTaxCategory())
                                ->setPercent(15)
                                ->setVatCalculationMethod(VatCalculationMethod::ZDOLA)
                                ->setVatApplicable(true)
                        )->setVariableSymbol('852')
                        ->setTaxableDepositAmount(400),
                ]
            ], 
        ];
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function set(array $newValue): void
    {
        $model = new Invoice();
        foreach($newValue as $value) {
            $model = $model->addTaxedDeposit($value);
        }
        $result = Reflection::getHiddenProperty($model, 'taxedDeposits');
        $this->assertEquals($newValue, $result);
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function getSet(array $newValue): void
    {
        $model = new Invoice();
        foreach($newValue as $value) {
            $model = $model->addTaxedDeposit($value);
        }
        $result = $model->getTaxedDeposits();
        $this->assertEquals($newValue, $result);
    }

    /**
     * @test
     */
    public function empty(): void
    {
        $model = new Invoice();
        $this->assertNull($model->getTaxedDeposits());
    }
}
