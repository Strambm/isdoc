<?php

namespace Isdoc\Tests\Models\Invoice;

use Isdoc\Enums\VatCalculationMethod;
use Isdoc\Models\ClassifiedTaxCategory;
use Isdoc\Models\Invoice;
use Isdoc\Models\InvoiceLine;
use PHPUnit\Framework\TestCase;
use Cetria\Helpers\Reflection\Reflection;

class GetAddInvoiceLineTest extends TestCase
{
    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function get(array $newValue): void
    {
        $model = new Invoice();
        Reflection::setHiddenProperty($model, 'invoiceLines', $newValue);
        $result = $model->getInvoiceLines();
        $this->assertEquals($newValue, $result);
    }

    public static function getSetDataProvider(): array
    {
        return [
            [
                [
                    (new InvoiceLine())
                        ->setClassifiedTaxCategory(
                            (new ClassifiedTaxCategory())
                                ->setPercent(21)
                                ->setVatApplicable(true)
                                ->setVatCalculationMethod(VatCalculationMethod::SHORA)
                        )->setId('123')
                        ->setInvoicedQuantity(1)
                        ->setInvoiceQuantityUnitCode('ks')
                        ->setLineExtensionAmountTaxInclusive(250),
                        
                    (new InvoiceLine())
                        ->setClassifiedTaxCategory(
                            (new ClassifiedTaxCategory())
                                ->setPercent(21)
                                ->setVatApplicable(true)
                                ->setVatCalculationMethod(VatCalculationMethod::ZDOLA)
                        )->setId('124')
                        ->setInvoicedQuantity(2)
                        ->setInvoiceQuantityUnitCode('ks')
                        ->setLineExtensionAmount(500)
                ]
            ], [
                [

                ]
            ] 
        ];
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function set(array $newValue): void
    {
        $model = new Invoice();
        foreach($newValue as $value) {
            $model = $model->addInvoiceLine($value);
        }
        $result = Reflection::getHiddenProperty($model, 'invoiceLines');
        $this->assertEquals($newValue, $result);
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function getSet(array $newValue): void
    {
        $model = new Invoice();
        foreach($newValue as $value) {
            $model = $model->addInvoiceLine($value);
        }
        $result = $model->getInvoiceLines();
        $this->assertEquals($newValue, $result);
    }
}
