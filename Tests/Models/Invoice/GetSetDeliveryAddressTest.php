<?php

namespace Isdoc\Tests\Models\Invoice;

use Isdoc\Models\Country;
use Isdoc\Models\Invoice;
use Isdoc\Enums\CountryCode;
use Isdoc\Models\PartyContact;
use Isdoc\Models\PostalAddress;
use PHPUnit\Framework\TestCase;
use Isdoc\Models\PartyIdentification;
use Cetria\Helpers\Reflection\Reflection;

class GetSetDeliveryAddressTest extends TestCase
{
    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function get(PartyContact|null $newValue): void
    {
        $model = new Invoice();
        Reflection::setHiddenProperty($model, 'deliveryAddress', $newValue);
        $result = $model->getDeliveryAddress();
        $this->assertEquals($newValue, $result);
    }

    public static function getSetDataProvider(): array
    {
        return [
            [
                (new PartyContact())
                    ->setName('firma XYZ')
                    ->setPostalAddress(
                        (new PostalAddress)
                            ->setBuildingNumber('1')
                            ->setCityName('Praha')
                            ->setStreetName('ulice')
                            ->setPostalZone('12345')
                            ->setCountry((new Country())->setIdentificationCode(CountryCode::CZ))
                    )
                    ->setPartyIdentification(
                        (new PartyIdentification())
                            ->setId('123456789')
                    )
            ], [
                null
            ]
        ];
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function set(PartyContact|null $newValue): void
    {
        $model = new Invoice();
        $model->setDeliveryAddress($newValue);
        $result = Reflection::getHiddenProperty($model, 'deliveryAddress');
        $this->assertEquals($newValue, $result);
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function getSet(PartyContact|null $newValue): void
    {
        $model = new Invoice();
        $result = $model->setDeliveryAddress($newValue)->getDeliveryAddress();
        $this->assertEquals($newValue, $result);
    }
}
