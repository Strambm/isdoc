<?php

namespace Isdoc\Tests\Models\Invoice;

use Isdoc\Models\Invoice;
use Isdoc\Models\Payment;
use Isdoc\Models\BankAccount;
use Isdoc\Models\PaymentMeans;
use PHPUnit\Framework\TestCase;
use Isdoc\Enums\PaymentMeansCode;
use Cetria\Helpers\Reflection\Reflection;
use Isdoc\Models\PaymentDetailBankTransaction;

class GetSetPaymentMeansTest extends TestCase
{
    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function get(PaymentMeans|null $newValue): void
    {
        $model = new Invoice();
        Reflection::setHiddenProperty($model, 'paymentMeans', $newValue);
        $this->assertEquals($newValue, $model->getPaymentMeans());
    }

    public static function getSetDataProvider(): array
    {
        return [
            [
                (new PaymentMeans())
                    ->addPayment(
                        (new Payment())
                            ->setPaidAmount(100)
                            ->setPaymentMeansCode(PaymentMeansCode::CARD)
                    )
            ], [
                (new PaymentMeans())
                    ->addPayment(
                        (new Payment())
                            ->setPaidAmount(100)
                            ->setPaymentMeansCode(PaymentMeansCode::CARD)
                    )->addPayment(
                        (new Payment())
                            ->setPaidAmount(100)
                            ->setPaymentMeansCode(PaymentMeansCode::BANK_TRANSFER)
                            ->setDetails(
                                (new PaymentDetailBankTransaction())
                                    ->setBankAccount(
                                        (new BankAccount())
                                            ->setBankCode('100')
                                            ->setBic('BIC0123')
                                            ->setIban('IBAN1234')
                                            ->setId('186463')
                                            ->setName('Testovací banka')
                                    )
                                    ->setPaymentDueDate('2024-09-11')
                                    ->setVs('123456')
                            )
                    )->addAlternativeBankAccount(
                        (new BankAccount())
                            ->setBankCode('200')
                            ->setBic('BIC2')
                            ->setIban('IBAN2')
                            ->setId('4846')
                            ->setName('Testovací banka 2')
                    )->addAlternativeBankAccount(
                        (new BankAccount())
                            ->setBankCode('200')
                            ->setBic('BIC2')
                            ->setIban('IBAN2')
                            ->setId('48465238')
                            ->setName('Testovací banka 3')
                    )
            ], [
                null
            ],
        ];
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function set(PaymentMeans|null $newValue): void
    {
        $model = $model = new Invoice();
        $model->setPaymentMeans($newValue);
        $this->assertEquals($newValue, Reflection::getHiddenProperty($model, 'paymentMeans'));
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function getSet(PaymentMeans|null $newValue): void
    {
        $model = $model = new Invoice();
        $this->assertEquals($newValue, $model->setPaymentMeans($newValue)->getPaymentMeans());
    }
}
