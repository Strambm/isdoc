<?php

namespace Isdoc\Tests\Models\Invoice;

use Isdoc\Enums\VatCalculationMethod;
use Isdoc\Models\ClassifiedTaxCategory;
use Isdoc\Models\Invoice;
use Isdoc\Models\InvoiceLine;
use Isdoc\Models\LegalMonetaryTotal;
use Isdoc\Models\NonTaxedDeposit;
use Isdoc\Models\TaxedDeposit;
use PHPUnit\Framework\TestCase;
use Cetria\Helpers\Reflection\Reflection;

class GetSetLegalMonetaryTotalTest extends TestCase
{
    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function get(LegalMonetaryTotal $newValue): void
    {
        $model = new Invoice();
        Reflection::setHiddenProperty($model, 'legalMonetaryTotal', $newValue);
        $this->assertEquals($newValue, $model->getLegalMonetaryTotal());
    }

    public static function getSetDataProvider(): array
    {
        return [
            [
                (new LegalMonetaryTotal())
                    ->setTaxExclusiveAmount(100)
                    ->setTaxInclusiveAmount(100)
                    ->setAlreadyClaimedTaxExclusiveAmount(0)
                    ->setAlreadyClaimedTaxInclusiveAmount(0)
            ], 
        ];
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function set(LegalMonetaryTotal $newValue): void
    {
        $model = new Invoice();
        $model->setLegalMonetaryTotal($newValue);
        $this->assertEquals($newValue, Reflection::getHiddenProperty($model, 'legalMonetaryTotal'));
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function getSet(LegalMonetaryTotal $newValue): void
    {
        $model = new Invoice();
        $this->assertEquals($newValue, $model->setLegalMonetaryTotal($newValue)->getLegalMonetaryTotal());
    }

    /**
     * @test
     */
    public function automaticsCalc(): void
    {
        $classifiedTaxCategory = (new ClassifiedTaxCategory())
            ->setPercent(10)
            ->setVatCalculationMethod(VatCalculationMethod::SHORA);
        $invoiceLine = (new InvoiceLine())
            ->setClassifiedTaxCategory($classifiedTaxCategory)
            ->setLineExtensionAmountTaxInclusive(500);
        $nonTaxedDeposit = (new NonTaxedDeposit())
            ->setDepositAmount(300);
        $taxedDeposit = (new TaxedDeposit())
            ->setClassifiedTaxCategory($classifiedTaxCategory)
            ->setTaxInclusiveAmount(100);
        $model = new Invoice();
        $model->addInvoiceLine($invoiceLine);
        $model->addNonTaxedDeposit($nonTaxedDeposit);
        $model->addTaxedDeposit($taxedDeposit);
        $result = $model->getLegalMonetaryTotal();
        $this->assertEqualsWithDelta(500, $result->getTaxInclusiveAmount(), 0.001);
        $this->assertEqualsWithDelta(500 / 1.1, $result->getTaxExclusiveAmount(), 0.001);
        $this->assertEqualsWithDelta(100 / 1.1, $result->getAlreadyClaimedTaxExclusiveAmount(), 0.001);
        $this->assertEqualsWithDelta(100, $result->getAlreadyClaimedTaxInclusiveAmount(), 0.001);
        $this->assertEqualsWithDelta((500 - 100) / 1.1, $result->getDifferenceTaxExclusiveAmount(), 0.001);
        $this->assertEqualsWithDelta(500 - 100, $result->getDifferenceTaxInclusiveAmount(), 0.001);
        $this->assertEqualsWithDelta(300, $result->getPaidDepositsAmount(), 0.001);
        $this->assertEqualsWithDelta(500 - 100 - 300, $result->getPayableAmount(), 0.001);
    }
}
