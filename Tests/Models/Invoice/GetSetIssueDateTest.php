<?php

namespace Isdoc\Tests\Models\Invoice;

use Isdoc\Models\Invoice;
use PHPUnit\Framework\TestCase;
use Isdoc\Exceptions\MissingIssueDate;
use Isdoc\Exceptions\InvalidDateFormat;
use Cetria\Helpers\Reflection\Reflection;

class GetSetIssueDateTest extends TestCase
{
    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function get(string $newValue): void
    {
        $model = new Invoice();
        Reflection::setHiddenProperty($model, 'issueDate', $newValue);
        $this->assertEquals($newValue, $model->getIssueDate());
    }

    public static function getSetDataProvider(): array
    {
        return [
            [
                '2021-01-01'
            ], [
                '2023-01-01'
            ]
        ];
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function set(string $newValue): void
    {
        $model = new Invoice();
        $model->setIssueDate($newValue);
        $this->assertEquals($newValue, Reflection::getHiddenProperty($model, 'issueDate'));
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function getSet(string $newValue): void
    {
        $model = new Invoice();
        $this->assertEquals($newValue, $model->setIssueDate($newValue)->getIssueDate());
    }

    /**
     * @test
     * @dataProvider invalidDateFormatDataProvider
     */
    public function throwInvalidDateFormat(string $value): void
    {
        $this->expectException(InvalidDateFormat::class);
        $model = new Invoice();
        $model->setIssueDate($value);
    }

    public static function invalidDateFormatDataProvider(): array
    {
        return [
            [
                '1.2.2022',
            ], [
                ''
            ], [
                '2022-01-01 15:00:00'
            ]
        ];
    }

    /**
     * @test
     */
    public function throwsMissingIssueDate(): void
    {
        $this->expectException(MissingIssueDate::class);
        $model = new Invoice();
        $model->getIssueDate();
    }
}
