<?php

namespace Isdoc\Tests\Models\Invoice;

use Isdoc\Models\Invoice;
use PHPUnit\Framework\TestCase;
use Isdoc\Traits\UUIDValidation;
use Isdoc\Exceptions\InvalidUuidFormat;
use Cetria\Helpers\Reflection\Reflection;

class GetSetUuidTest extends TestCase
{
    use UUIDValidation;

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function get(string $newValue): void
    {
        $model = new Invoice();
        Reflection::setHiddenProperty($model, 'uuid', $newValue);
        $this->assertEquals($newValue, $model->getUuid());
    }

    public static function getSetDataProvider(): array
    {
        $invoice = new Invoice();
        $methodName = 'generateUuid';
        $method = Reflection::getHiddenMethod($invoice, $methodName);
        return [
            [
                $method->invoke($invoice),
            ],
        ];
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function set(string $newValue): void
    {
        $model = new Invoice();
        $model->setUuid($newValue);
        $this->assertEquals($newValue, Reflection::getHiddenProperty($model, 'uuid'));
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function getSet(string $newValue): void
    {
        $model = new Invoice();
        $this->assertEquals($newValue, $model->setUuid($newValue)->getUuid());
    }

    /**
     * @test
     * @doesNotPerformAssertions
     */
    public function setNullReturnGenerated(): void
    {
        $model = new Invoice();
        $uuid = $model->setUuid(null)->getUuid();
        $this->validateUuid($uuid);
    }

    /**
     * @test
     */
    public function invalidValue(): void
    {
        $this->expectException(InvalidUuidFormat::class);
        $model = new Invoice();
        $model->setUuid('123456');
    }
}
