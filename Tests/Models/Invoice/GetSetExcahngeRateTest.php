<?php

namespace Isdoc\Tests\Models\Invoice;

use Isdoc\Models\ExchangeRate;
use Isdoc\Models\Invoice;
use Isdoc\Enums\CurrencyCode;
use PHPUnit\Framework\TestCase;

class GetSetExcahngeRateTest extends TestCase
{
    /**
     * @test
     */
    public function default(): void
    {
        $model = new Invoice();
        $this->assertEquals(CurrencyCode::CZK, $model->getLocalCurrencyCode());
        $this->assertEquals(1, $model->getCurrRate());
        $this->assertEquals(1, $model->getRefCurrRate());
        $this->assertNull($model->getForeignCurrencyCode());
    }

    /**
     * @test
     */
    public function withForeignCurrency(): void
    {
        $exchangeRate = new ExchangeRate(CurrencyCode::EUR, CurrencyCode::USD, 25.38, 23.39);
        $model = new Invoice();
        $model->setExchangeRate($exchangeRate);
        $this->assertEquals($exchangeRate->getLocalCurrencyCode(), $model->getLocalCurrencyCode());
        $this->assertEquals($exchangeRate->getForeignCurrencyCode(), $model->getForeignCurrencyCode());
        $this->assertEquals($exchangeRate->getCurrRate(), $model->getCurrRate());
        $this->assertEquals($exchangeRate->getRefCurrRate(), $model->getRefCurrRate());
    }
}
