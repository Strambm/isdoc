<?php

namespace Isdoc\Tests\Models\Invoice;

use Isdoc\Models\Country;
use Isdoc\Models\Invoice;
use Isdoc\Enums\CountryCode;
use Isdoc\Models\PartyContact;
use Isdoc\Models\PostalAddress;
use PHPUnit\Framework\TestCase;
use Isdoc\Models\PartyIdentification;
use Cetria\Helpers\Reflection\Reflection;
use Isdoc\Exceptions\MissingAccountingSupplierParty;

class GetSetAccountingSupplierPartyTest extends TestCase
{
    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function get(PartyContact $newValue): void
    {
        $model = new Invoice();
        Reflection::setHiddenProperty($model, 'accountingSupplierParty', $newValue);
        $result = $model->getAccountingSupplierParty();
        $this->assertEquals($newValue, $result);
    }

    public static function getSetDataProvider(): array
    {
        return [
            [
                (new PartyContact())
                    ->setName('firma XYZ')
                    ->setPostalAddress(
                        (new PostalAddress)
                            ->setBuildingNumber('1')
                            ->setCityName('Praha')
                            ->setStreetName('ulice')
                            ->setPostalZone('12345')
                            ->setCountry((new Country())->setIdentificationCode(CountryCode::CZ))
                    )
                    ->setPartyIdentification(
                        (new PartyIdentification())
                            ->setId('123456789')
                    )
            ]
        ];
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function set(PartyContact $newValue): void
    {
        $model = new Invoice();
        $model->setAccountingSupplierParty($newValue);
        $result = Reflection::getHiddenProperty($model, 'accountingSupplierParty');
        $this->assertEquals($newValue, $result);
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function getSet(PartyContact $newValue): void
    {
        $model = new Invoice();
        $result = $model->setAccountingSupplierParty($newValue)->getAccountingSupplierParty();
        $this->assertEquals($newValue, $result);
    }

    /**
     * @test
     */
    public function withoutValue(): void
    {
        $this->expectException(MissingAccountingSupplierParty::class);
        $model = new Invoice();
        $model->getAccountingSupplierParty();
    }
}
