<?php

namespace Isdoc\Tests\Models\Invoice;

use Isdoc\Models\Invoice;
use PHPUnit\Framework\TestCase;
use Isdoc\Traits\UUIDValidation;
use Isdoc\Exceptions\InvalidUuidFormat;
use Cetria\Helpers\Reflection\Reflection;

class GetSetIssuingSystemTest extends TestCase
{
    use UUIDValidation;

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function get(string $newValue): void
    {
        $model = new Invoice();
        Reflection::setHiddenProperty($model, 'issuingSystem', $newValue);
        $this->assertEquals($newValue, $model->getIssuingSystem());
    }

    public static function getSetDataProvider(): array
    {
        return [
            [
                'testName',
            ],
        ];
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function set(string $newValue): void
    {
        $model = new Invoice();
        $model->setIssuingSystem($newValue);
        $this->assertEquals($newValue, Reflection::getHiddenProperty($model, 'issuingSystem'));
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function getSet(string $newValue): void
    {
        $model = new Invoice();
        $this->assertEquals($newValue, $model->setIssuingSystem($newValue)->getIssuingSystem());
    }

    /**
     * @test
     */
    public function setNullReturnGenerated(): void
    {
        
        $model = new Invoice();
        $methodName = 'getDefaultIssuringSystem';
        $method = Reflection::getHiddenMethod($model, $methodName);
        $this->assertEquals($method->invoke($model), $model->setIssuingSystem(null)->getIssuingSystem());
    }
}
