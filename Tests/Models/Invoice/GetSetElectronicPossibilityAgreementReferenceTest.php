<?php

namespace Isdoc\Tests\Models\Invoice;

use Isdoc\Models\Invoice;
use Isdoc\Enums\LanguageCode;
use PHPUnit\Framework\TestCase;
use Cetria\Helpers\Reflection\Reflection;
use Isdoc\Models\ElectronicPossibilityAgreementReference;

class GetSetElectronicPossibilityAgreementReferenceTest extends TestCase
{
    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function get(ElectronicPossibilityAgreementReference|null $newValue): void
    {
        $model = new Invoice();
        Reflection::setHiddenProperty($model, 'electronicPossibilityAgreementReference', $newValue);
        $result = $model->getElectronicPossibilityAgreementReference();
        $this->assertEquals($newValue, $result);
    }

    public static function getSetDataProvider(): array
    {
        return [
            [
                (new ElectronicPossibilityAgreementReference())
                    ->setLanguageId(LanguageCode::BG)
                    ->setValue('nfuiefgpw')
            ]
        ];
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function set(ElectronicPossibilityAgreementReference|null $newValue): void
    {
        $model = new Invoice();
        $model->setElectronicPossibilityAgreementReference($newValue);
        $result = Reflection::getHiddenProperty($model, 'electronicPossibilityAgreementReference');
        $this->assertEquals($newValue, $result);
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function getSet(ElectronicPossibilityAgreementReference|null $newValue): void
    {
        $model = new Invoice();
        $result = $model->setElectronicPossibilityAgreementReference($newValue)->getElectronicPossibilityAgreementReference();
        $this->assertEquals($newValue, $result);
    }

    /**
     * @test
     */
    public function withoutValue(): void
    {
        $model = new Invoice();
        $model->setElectronicPossibilityAgreementReference(null);
        $this->assertEquals(new ElectronicPossibilityAgreementReference(), $model->getElectronicPossibilityAgreementReference());
    }
}
