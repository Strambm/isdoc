<?php

namespace Isdoc\Tests\Models\Invoice;

use Isdoc\Models\Invoice;
use Isdoc\Models\NonTaxedDeposit;
use PHPUnit\Framework\TestCase;
use Cetria\Helpers\Reflection\Reflection;

class GetAddNonTaxedDepositTest extends TestCase
{
    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function get(array $newValue): void
    {
        $model = new Invoice();
        Reflection::setHiddenProperty($model, 'nonTaxedDeposits', $newValue);
        $result = $model->getNonTaxedDeposits();
        $this->assertEquals($newValue, $result);
    }

    public static function getSetDataProvider(): array
    {
        return [
            [
                [
                    (new NonTaxedDeposit)
                        ->setVariableSymbol('852')
                        ->setDepositAmount(200),
                    (new NonTaxedDeposit)
                        ->setVariableSymbol('852')
                        ->setDepositAmount(400),
                ]
            ], 
        ];
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function set(array $newValue): void
    {
        $model = new Invoice();
        foreach($newValue as $value) {
            $model = $model->addNonTaxedDeposit($value);
        }
        $result = Reflection::getHiddenProperty($model, 'nonTaxedDeposits');
        $this->assertEquals($newValue, $result);
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function getSet(array $newValue): void
    {
        $model = new Invoice();
        foreach($newValue as $value) {
            $model = $model->addNonTaxedDeposit($value);
        }
        $result = $model->getNonTaxedDeposits();
        $this->assertEquals($newValue, $result);
    }

    /**
     * @test
     */
    public function empty(): void
    {
        $model = new Invoice();
        $this->assertNull($model->getNonTaxedDeposits());
    }
}
