<?php

namespace Isdoc\Tests\Models\Invoice;

use Isdoc\Models\Invoice;
use PHPUnit\Framework\TestCase;
use Cetria\Helpers\Reflection\Reflection;

class GetSetIdTest extends TestCase
{
    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function get(string $newValue): void
    {
        $model = new Invoice();
        Reflection::setHiddenProperty($model, 'id', $newValue);
        $this->assertEquals($newValue, $model->getId());
    }

    public static function getSetDataProvider(): array
    {
        return [
            [
                'X15613'
            ], [
                '123456'
            ], [
                '2024-0002568'
            ],
        ];
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function set(string $newValue): void
    {
        $model = new Invoice();
        $model->setId($newValue);
        $this->assertEquals($newValue, Reflection::getHiddenProperty($model, 'id'));
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function getSet(string $newValue): void
    {
        $model = new Invoice();
        $this->assertEquals($newValue, $model->setId($newValue)->getId());
    }
}
