<?php

namespace Isdoc\Tests\Models\Invoice;

use Isdoc\Models\Invoice;
use Isdoc\Traits\UUIDValidation;
use PHPUnit\Framework\TestCase;
use Cetria\Helpers\Reflection\Reflection;

class GenerateUuidTest extends TestCase
{
    use UUIDValidation;

    /**
     * @test
     * @doesNotPerformAssertions
     */
    public function generaetAndValidate(): void
    {
        $model = new Invoice();
        $methodName = 'generateUuid';
        $method = Reflection::getHiddenMethod($model, $methodName);
        for($a = 0; $a < 50; $a ++) {
            $uuid = $method->invoke($model);
            $this->validateUuid($uuid);
        }
    }
}
