<?php

namespace Isdoc\Tests\Models\Invoice;

use Isdoc\Models\Invoice;
use Isdoc\Enums\LanguageCode;
use Isdoc\Models\Note;
use PHPUnit\Framework\TestCase;
use Cetria\Helpers\Reflection\Reflection;

class GetSetNoteTest extends TestCase
{
    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function get(Note|null $newValue): void
    {
        $model = new Invoice();
        Reflection::setHiddenProperty($model, 'note', $newValue);
        $result = $model->getNote();
        $this->assertEquals($newValue, $result);
    }

    public static function getSetDataProvider(): array
    {
        return [
            [
                null
            ], [
                (new Note())
                    ->setLanguageId(LanguageCode::BG)
                    ->setValue('nfuiefgpw')
            ]
        ];
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function set(Note|null $newValue): void
    {
        $model = new Invoice();
        $model->setNote($newValue);
        $result = Reflection::getHiddenProperty($model, 'note');
        $this->assertEquals($newValue, $result);
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function getSet(Note|null $newValue): void
    {
        $model = new Invoice();
        $result = $model->setNote($newValue)->getNote();
        $this->assertEquals($newValue, $result);
    }
}
