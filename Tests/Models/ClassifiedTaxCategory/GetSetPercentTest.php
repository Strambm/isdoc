<?php

namespace Isdoc\Tests\Models\ClassifiedTaxCategory;

use Isdoc\Exceptions\MissingPercent;
use PHPUnit\Framework\TestCase;
use Isdoc\Models\ClassifiedTaxCategory;
use Cetria\Helpers\Reflection\Reflection;

class GetSetPercentTest extends TestCase
{
    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function get(float $newValue): void
    {
        $model = new ClassifiedTaxCategory();
        Reflection::setHiddenProperty($model, 'percent', $newValue);
        $this->assertEquals($newValue, $model->getPercent());
    }

    public static function getSetDataProvider(): array
    {
        return [
            [
                10
            ], [
                12.5
            ], [
                0
            ]
        ];
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function set(float $newValue): void
    {
        $model = new ClassifiedTaxCategory();
        $model->setPercent($newValue);
        $this->assertEquals($newValue, Reflection::getHiddenProperty($model, 'percent'));
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function getSet(float $newValue): void
    {
        $model = new ClassifiedTaxCategory();
        $this->assertEquals($newValue, $model->setPercent($newValue)->getPercent());
    }

    /**
     * @test
     */
    public function getWithoutValue(): void
    {
        $this->expectException(MissingPercent::class);
        $model = new ClassifiedTaxCategory();
        $model->getPercent();
    }
}
