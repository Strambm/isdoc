<?php

namespace Isdoc\Tests\Models\ClassifiedTaxCategory;

use PHPUnit\Framework\TestCase;
use Isdoc\Models\ClassifiedTaxCategory;
use Cetria\Helpers\Reflection\Reflection;

class GetSetLocalReverseChargeTest extends TestCase
{
    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function get(null|bool $newValue): void
    {
        $model = new ClassifiedTaxCategory();
        Reflection::setHiddenProperty($model, 'localReverseCharge', $newValue);
        $this->assertEquals($newValue, $model->getLocalReverseCharge());
    }

    public static function getSetDataProvider(): array
    {
        return [
            [
                true
            ], [
                null
            ], [
                false
            ]
        ];
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function set(null|bool $newValue): void
    {
        $model = new ClassifiedTaxCategory();
        $model->setLocalReverseCharge($newValue);
        $this->assertEquals($newValue, Reflection::getHiddenProperty($model, 'localReverseCharge'));
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function getSet(null|bool $newValue): void
    {
        $model = new ClassifiedTaxCategory();
        $this->assertEquals($newValue, $model->setLocalReverseCharge($newValue)->getLocalReverseCharge());
    }
}
