<?php

namespace Isdoc\Tests\Models\ClassifiedTaxCategory;

use Isdoc\Exceptions\MissingPercent;
use Isdoc\Exceptions\MissingVatCalculationMethod;
use Isdoc\Models\ClassifiedTaxCategory;
use PHPUnit\Framework\TestCase;

class ToXmlElementTest extends TestCase
{
    /**
     * @test
     */
    public function missingPercentAttribute(): void
    {
        $this->expectException(MissingPercent::class);
        $model = new ClassifiedTaxCategory();
        $model->setVatCalculationMethod(1);
        $result = $model->toXmlElement();
    }

    /**
     * @test
     */
    public function missingVatCalculationMethod(): void
    {
        $this->expectException(MissingVatCalculationMethod::class);
        $model = new ClassifiedTaxCategory();
        $model->setPercent(15);
        $result = $model->toXmlElement();
    }

    /**
     * @test
     */
    public function onlyRequiredAttributes(): void
    {
        $percent = 15;
        $vatCalculationMethod = 1;
        $model = new ClassifiedTaxCategory();
        $model->setPercent($percent)
            ->setVatCalculationMethod($vatCalculationMethod);
        $result = $model->toString();
        $structure = json_decode(json_encode(simplexml_load_string($result)), true);
        $this->assertCount(2, $structure);
        $this->assertEquals($percent, $structure['Percent']);
        $this->assertEquals($vatCalculationMethod, $structure['VATCalculationMethod']);
    }

    /**
     * @test
     */
    public function withNecessaryParameters(): void
    {
        $percent = 18;
        $vatCalculationMethod = 1;
        $vatApplicable = true;
        $localReverseCharge = false;
        $model = new ClassifiedTaxCategory();
        $model->setLocalReverseCharge($localReverseCharge)
            ->setPercent($percent)
            ->setVatApplicable($vatApplicable)
            ->setVatCalculationMethod($vatCalculationMethod);
        $result = $model->toString();
        $structure = json_decode(json_encode(simplexml_load_string($result)), true);
        $this->assertCount(4, $structure);
        $this->assertEquals($percent, $structure['Percent']);
        $this->assertEquals($vatCalculationMethod, $structure['VATCalculationMethod']);
        $this->assertEquals('true', $structure['VATApplicable']);
        $this->assertEquals('false', $structure['LocalReverseCharge']);
    }
}
