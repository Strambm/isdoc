<?php

namespace Isdoc\Tests\Models\ClassifiedTaxCategory;

use Isdoc\Exceptions\MissingVatCalculationMethod;
use Isdoc\Exceptions\UnvalidVatCalculationMethodFormat;
use PHPUnit\Framework\TestCase;
use Isdoc\Models\ClassifiedTaxCategory;
use Cetria\Helpers\Reflection\Reflection;

class GetSetVatCalculationMethodTest extends TestCase
{
    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function get(float $newValue): void
    {
        $model = new ClassifiedTaxCategory();
        Reflection::setHiddenProperty($model, 'vatCalculationMethod', $newValue);
        $this->assertEquals($newValue, $model->getVatCalculationMethod());
    }

    public static function getSetDataProvider(): array
    {
        return [
            [
                0
            ], [
                1
            ],
        ];
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function set(float $newValue): void
    {
        $model = new ClassifiedTaxCategory();
        $model->setVatCalculationMethod($newValue);
        $this->assertEquals($newValue, Reflection::getHiddenProperty($model, 'vatCalculationMethod'));
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function getSet(float $newValue): void
    {
        $model = new ClassifiedTaxCategory();
        $this->assertEquals($newValue, $model->setVatCalculationMethod($newValue)->getVatCalculationMethod());
    }

    /**
     * @test
     */
    public function getWithoutValue(): void
    {
        $this->expectException(MissingVatCalculationMethod::class);
        $model = new ClassifiedTaxCategory();
        $model->getVatCalculationMethod();
    }

    /**
     * @test
     * @dataProvider setOverRangeDataProvider
     */
    public function setOverRange(int $newValue): void
    {
        $this->expectException(UnvalidVatCalculationMethodFormat::class);
        $model = new ClassifiedTaxCategory();
        $model->setVatCalculationMethod($newValue);
    }

    public static function setOverRangeDataProvider(): array
    {
        return [
            [
                -1,
            ], [
                3,
            ],
        ];
    }
}
