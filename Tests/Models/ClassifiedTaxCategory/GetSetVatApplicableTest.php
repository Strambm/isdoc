<?php

namespace Isdoc\Tests\Models\ClassifiedTaxCategory;

use PHPUnit\Framework\TestCase;
use Isdoc\Models\ClassifiedTaxCategory;
use Cetria\Helpers\Reflection\Reflection;

class GetSetVatApplicableTest extends TestCase
{
    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function get(null|bool $newValue): void
    {
        $model = new ClassifiedTaxCategory();
        Reflection::setHiddenProperty($model, 'vatApplicable', $newValue);
        $this->assertEquals($newValue, $model->getVatApplicable());
    }

    public static function getSetDataProvider(): array
    {
        return [
            [
                true
            ], [
                null
            ], [
                false
            ]
        ];
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function set(null|bool $newValue): void
    {
        $model = new ClassifiedTaxCategory();
        $model->setVatApplicable($newValue);
        $this->assertEquals($newValue, Reflection::getHiddenProperty($model, 'vatApplicable'));
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function getSet(null|bool $newValue): void
    {
        $model = new ClassifiedTaxCategory();
        $this->assertEquals($newValue, $model->setVatApplicable($newValue)->getVatApplicable());
    }
}
