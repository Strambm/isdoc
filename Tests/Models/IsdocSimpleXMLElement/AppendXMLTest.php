<?php

namespace Isdoc\Tests\Models\IsdocSimpleXMLElement;

use PHPUnit\Framework\TestCase;
use Isdoc\Models\IsdocSimpleXMLElement;

class AppendXMLTest extends TestCase
{
    /**
     * @test
     */
    public function emptyTag(): void
    {
        $parent = new IsdocSimpleXMLElement('<parent></parent>');
        $parent->appendXML('<child></child>');
        $childValue = $parent->xpath('/parent/child');
        $this->assertEquals('<child/>', $childValue[0]->asXml());
    }

    /**
     * @test
     */
    public function tagWithValue(): void
    {
        $parent = new IsdocSimpleXMLElement('<parent></parent>');
        $parent->appendXML('<child>test</child>');
        $childValue = $parent->xpath('/parent/child');
        $this->assertEquals('<child>test</child>', $childValue[0]->asXml());
    }
}
