<?php

namespace Isdoc\Tests\Models\IsdocSimpleXMLElement;

use PHPUnit\Framework\TestCase;
use Isdoc\Models\IsdocSimpleXMLElement;

class AddChildOptionalTest extends TestCase
{
    /**
     * @test
     */
    public function childAdded(): void
    {
        $parent = new IsdocSimpleXMLElement('<parent></parent>');
        $parent->addChildOptional('childOptional', 'isSet');
        $child = $parent->xpath('/parent/childOptional');
        $this->assertEquals('isSet', $child[0]);
    }

    /**
     * @test
     */
    public function childNotAdded(): void
    {
        $parent = new IsdocSimpleXMLElement('<parent></parent>');
        $parent->addChildOptional('childOptional', null);
        $child = $parent->xpath('/parent/childOptional');
        $this->assertEquals([], $child);
    }
}
