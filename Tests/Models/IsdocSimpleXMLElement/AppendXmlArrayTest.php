<?php

namespace Isdoc\Tests\Models\IsdocSimpleXMLElement;

use SimpleXMLElement;
use PHPUnit\Framework\TestCase;
use Isdoc\Models\IsdocSimpleXMLElement;

class AppendXmlArrayTest extends TestCase
{
    /**
     * @test
     */
    public function emptyArray(): void
    {
        $parent = new IsdocSimpleXMLElement('<parent></parent>');
        $parent->appendXmlArray('child', []);
        $childValue = $parent->xpath('/parent/child');
        $this->assertEquals([], $childValue);
    }

    /**
     * @test
     */
    public function nullValue(): void
    {
        $parent = new IsdocSimpleXMLElement('<parent></parent>');
        $parent->appendXmlArray('child', null);
        $childValue = $parent->xpath('/parent/child');
        $this->assertEquals([], $childValue);
    }

    /**
     * @test
     */
    public function singleValue(): void
    {
        $value1 = new SimpleXMLElement('<children>1</children>');
        $parent = new IsdocSimpleXMLElement('<parent></parent>');
        $parent->appendXmlArray('child', [$value1]);
        $childValue = $parent->xpath('/parent/child/children');
        $this->assertEquals(trim(preg_replace('/<\?xml.*?\?>/', '', $value1->asXml())), trim($childValue[0]->asXml()));
    }

    /**
     * @test
     */
    public function multipleValue(): void
    {
        $value1 = new SimpleXMLElement('<children>1</children>');
        $parent = new IsdocSimpleXMLElement('<parent></parent>');
        $parent->appendXmlArray('child', [$value1, $value1]);
        $childValue = $parent->xpath('/parent/child/children');
        $this->assertEquals(trim(preg_replace('/<\?xml.*?\?>/', '', $value1->asXml())), trim($childValue[0]->asXml()));
        $this->assertEquals(trim(preg_replace('/<\?xml.*?\?>/', '', $value1->asXml())), trim($childValue[1]->asXml()));
    }
}
