<?php

namespace Isdoc\Tests\Models\IsdocSimpleXMLElement;

use Isdoc\Traits\StringConversion;
use PHPUnit\Framework\TestCase;
use Isdoc\Models\IsdocSimpleXMLElement;
use Isdoc\Interfaces\RenderableInterface;

class AppendSimpleXMLElementOptionalTest extends TestCase
{
    /**
     * @test
     */
    public function simpleXMLElement(): void
    {
        $parent = new IsdocSimpleXMLElement('<parent></parent>');
        $child = new IsdocSimpleXMLElement('<child></child>');
        $result = $parent->appendSimpleXMLElementOptional($child);
        $childValue = $parent->xpath('/parent/child');
        $this->assertEquals($child, $childValue[0]);
        $this->assertTrue($result);
    }

    /**
     * @test
     */
    public function renderable(): void
    {
        $parent = new IsdocSimpleXMLElement('<parent></parent>');
        $child = $this->getRenderable();
        $result = $parent->appendSimpleXMLElementOptional($child);
        $childValue = $parent->xpath('/parent/child');
        $this->assertEquals($child->toXmlElement(), $childValue[0]);
        $this->assertTrue($result);
    }

    private function getRenderable(): RenderableInterface
    {
        $model = new class() implements RenderableInterface
        {
            use StringConversion;

            public function toXmlElement(): IsdocSimpleXMLElement
            {
                return new IsdocSimpleXMLElement('<child>value</child>');
            }
        };
        return $model;
    }

    /**
     * @test
     */
    public function nullValue(): void
    {
        $parent = new IsdocSimpleXMLElement('<parent></parent>');
        $result = $parent->appendSimpleXMLElementOptional(null);
        $childValue = $parent->xpath('/parent/child');
        $this->assertEquals([], $childValue);
        $this->assertFalse($result);
    }
}
