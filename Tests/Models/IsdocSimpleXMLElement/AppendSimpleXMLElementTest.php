<?php

namespace Isdoc\Tests\Models\IsdocSimpleXMLElement;

use Isdoc\Traits\StringConversion;
use PHPUnit\Framework\TestCase;
use Isdoc\Models\IsdocSimpleXMLElement;
use Isdoc\Interfaces\RenderableInterface;

class AppendSimpleXMLElementTest extends TestCase
{
    /**
     * @test
     */
    public function simpleXMLElement(): void
    {
        $parent = new IsdocSimpleXMLElement('<parent></parent>');
        $child = new IsdocSimpleXMLElement('<child></child>');
        $parent->appendSimpleXMLElement($child);
        $childValue = $parent->xpath('/parent/child');
        $this->assertEquals($child, $childValue[0]);
    }

    /**
     * @test
     */
    public function renderable(): void
    {
        $parent = new IsdocSimpleXMLElement('<parent></parent>');
        $child = $this->getRenderable();
        $parent->appendSimpleXMLElement($child);
        $childValue = $parent->xpath('/parent/child');
        $this->assertEquals($child->toXmlElement(), $childValue[0]);
    }

    private function getRenderable(): RenderableInterface
    {
        $model = new class() implements RenderableInterface
        {
            use StringConversion;

            public function toXmlElement(): IsdocSimpleXMLElement
            {
                return new IsdocSimpleXMLElement('<child>value</child>');
            }
        };
        return $model;
    }
}
