<?php

namespace Isdoc\Tests\Models\TaxCategory;

use Isdoc\Exceptions\MissingPercent;
use Isdoc\Models\TaxCategory;
use PHPUnit\Framework\TestCase;
use Cetria\Helpers\Reflection\Reflection;

class GetSetPercentTest extends TestCase
{
    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function get(float $newValue): void
    {
        $model = new TaxCategory();
        Reflection::setHiddenProperty($model, 'percent', $newValue);
        $result = $model->getPercent();
        $this->assertEquals($newValue, $result);
    }

    public static function getSetDataProvider(): array
    {
        return [
            [
                0
            ], [
                0.15
            ], [
                12.5
            ]
        ];
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function set(float $newValue): void
    {
        $model = new TaxCategory();
        $model->setPercent($newValue);
        $result = Reflection::getHiddenProperty($model, 'percent');
        $this->assertEquals($newValue, $result);
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function getSet(float $newValue): void
    {
        $model = new TaxCategory();
        $result = $model->setPercent($newValue)->getPercent();
        $this->assertEquals($newValue, $result);
    }

    /**
     * @test
     */
    public function getWithoutValue(): void
    {
        $this->expectException(MissingPercent::class);
        $model = new TaxCategory();
        $model->getPercent();
    }
}
