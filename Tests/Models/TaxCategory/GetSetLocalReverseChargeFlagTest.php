<?php

namespace Isdoc\Tests\Models\TaxCategory;

use Isdoc\Models\TaxCategory;
use PHPUnit\Framework\TestCase;
use Cetria\Helpers\Reflection\Reflection;

class GetSetLocalReverseChargeFlagTest extends TestCase
{
    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function get(bool|null $newValue): void
    {
        $model = new TaxCategory();
        Reflection::setHiddenProperty($model, 'localReverseChargeFlag', $newValue);
        $result = $model->getLocalReverseChargeFlag();
        $this->assertEquals($newValue, $result);
    }

    public static function getSetDataProvider(): array
    {
        return [
            [
                null
            ], [
                true
            ], [
                false
            ]
        ];
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function set(bool|null $newValue): void
    {
        $model = new TaxCategory();
        $model->setLocalReverseChargeFlag($newValue);
        $result = Reflection::getHiddenProperty($model, 'localReverseChargeFlag');
        $this->assertEquals($newValue, $result);
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function getSet(bool|null $newValue): void
    {
        $model = new TaxCategory();
        $result = $model->setLocalReverseChargeFlag($newValue)->getLocalReverseChargeFlag();
        $this->assertEquals($newValue, $result);
    }
}
