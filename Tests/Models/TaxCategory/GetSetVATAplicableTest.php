<?php

namespace Isdoc\Tests\Models\TaxCategory;

use Isdoc\Models\TaxCategory;
use PHPUnit\Framework\TestCase;
use Cetria\Helpers\Reflection\Reflection;

class GetSetVATAplicableTest extends TestCase
{
    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function get(bool|null $newValue): void
    {
        $model = new TaxCategory();
        Reflection::setHiddenProperty($model, 'vatApplicable', $newValue);
        $result = $model->getVATAplicable();
        $this->assertEquals($newValue, $result);
    }

    public static function getSetDataProvider(): array
    {
        return [
            [
                null
            ], [
                true
            ], [
                false
            ]
        ];
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function set(bool|null $newValue): void
    {
        $model = new TaxCategory();
        $model->setVATAplicable($newValue);
        $result = Reflection::getHiddenProperty($model, 'vatApplicable');
        $this->assertEquals($newValue, $result);
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function getSet(bool|null $newValue): void
    {
        $model = new TaxCategory();
        $result = $model->setVATAplicable($newValue)->getVATAplicable();
        $this->assertEquals($newValue, $result);
    }
}
