<?php

namespace Isdoc\Tests\Models\TaxCategory;

use Isdoc\Enums\TaxScheme;
use Isdoc\Models\TaxCategory;
use PHPUnit\Framework\TestCase;
use Isdoc\Exceptions\MissingPercent;

class ToXmlElementTest extends TestCase
{
    /**
     * @test
     */
    public function withoutPercent(): void
    {
        $this->expectException(MissingPercent::class);
        $model = new TaxCategory();
        $model->toString();
    }

    /**
     * @test
     */
    public function onlyRequiredAttributes(): void
    {
        $percent = 8;
        $model = new TaxCategory();
        $model->setPercent($percent);
        $result = $model->toString();
        $structure = json_decode(json_encode(simplexml_load_string($result)), true);
        $this->assertCount(1, $structure);
        $this->assertEquals($percent, $structure['Percent']);  
    }

    /**
     * @test
     */
    public function withNecessaryParameters(): void
    {
        $percent = 8.26;
        $taxScheme = TaxScheme::TIN;
        $vatApplicable = true;
        $localReverseChargeFlag = false;
        $model = new TaxCategory();
        $model->setPercent($percent)
            ->setLocalReverseChargeFlag($localReverseChargeFlag)
            ->setTaxScheme($taxScheme)
            ->setVATAplicable($vatApplicable);
        $result = $model->toString();
        $structure = json_decode(json_encode(simplexml_load_string($result)), true);
        $this->assertCount(4, $structure);
        $this->assertEquals($percent, $structure['Percent']);  
        $this->assertEquals($taxScheme, $structure['TaxScheme']);  
        $this->assertEquals('false', $structure['LocalReverseChargeFlag']);  
        $this->assertEquals('true', $structure['VATApplicable']);  
    }
}
