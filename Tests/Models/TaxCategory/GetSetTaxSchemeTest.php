<?php

namespace Isdoc\Tests\Models\TaxCategory;

use Isdoc\Enums\TaxScheme;
use Isdoc\Exceptions\UnvalidTaxScheme;
use Isdoc\Models\TaxCategory;
use PHPUnit\Framework\TestCase;
use Cetria\Helpers\Reflection\Reflection;

class GetSetTaxSchemeTest extends TestCase
{
    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function get(string|null $newValue): void
    {
        $model = new TaxCategory();
        Reflection::setHiddenProperty($model, 'taxScheme', $newValue);
        $result = $model->getTaxScheme();
        $this->assertEquals($newValue, $result);
    }

    public static function getSetDataProvider(): array
    {
        return [
            [
                null
            ], [
                TaxScheme::TIN
            ], [
                TaxScheme::VAT
            ]
        ];
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function set(string|null $newValue): void
    {
        $model = new TaxCategory();
        $model->setTaxScheme($newValue);
        $result = Reflection::getHiddenProperty($model, 'taxScheme');
        $this->assertEquals($newValue, $result);
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function getSet(string|null $newValue): void
    {
        $model = new TaxCategory();
        $result = $model->setTaxScheme($newValue)->getTaxScheme();
        $this->assertEquals($newValue, $result);
    }

    /**
     * @test
     */
    public function unvalidValue(): void
    {
        $this->expectException(UnvalidTaxScheme::class);
        $model = new TaxCategory();
        $model->setTaxScheme('123');
    }
}
