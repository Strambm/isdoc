<?php

namespace Isdoc\Tests\Models\PaymentDetailBankTransaction;

use Isdoc\Exceptions\InvalidNumericFormat;
use Isdoc\Models\PaymentDetailBankTransaction;
use PHPUnit\Framework\TestCase;
use Cetria\Helpers\Reflection\Reflection;

class GetSetSpecificSymbolTest extends TestCase
{
    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function get(string|null $newValue): void
    {
        $model = new PaymentDetailBankTransaction();
        Reflection::setHiddenProperty($model, 'ss', $newValue);
        $this->assertEquals($newValue, $model->getSpecificSymbol());
    }

    public static function getSetDataProvider(): array
    {
        return [
            [
                null
            ], [
                '12345'
            ]
        ];
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function set(string|null $newValue): void
    {
        $model = new PaymentDetailBankTransaction();
        $model->setSpecificSymbol($newValue);
        $this->assertEquals($newValue, Reflection::getHiddenProperty($model, 'ss'));
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function getSet(string|null $newValue): void
    {
        $model = new PaymentDetailBankTransaction();
        $this->assertEquals($newValue, $model->setSpecificSymbol($newValue)->getSpecificSymbol());
    }

    /**
     * @test
     */
    public function throwInvalidFormat(): void
    {
        $this->expectException(InvalidNumericFormat::class);
        $model = new PaymentDetailBankTransaction();
        $model->setSpecificSymbol('a123');
    }
}
