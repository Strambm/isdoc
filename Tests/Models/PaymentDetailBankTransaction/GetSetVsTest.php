<?php

namespace Isdoc\Tests\Models\PaymentDetailBankTransaction;

use Isdoc\Exceptions\InvalidNumericFormat;
use Isdoc\Models\PaymentDetailBankTransaction;
use PHPUnit\Framework\TestCase;
use Cetria\Helpers\Reflection\Reflection;

class GetSetVsTest extends TestCase
{
    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function get(string|null $newValue): void
    {
        $model = new PaymentDetailBankTransaction();
        Reflection::setHiddenProperty($model, 'vs', $newValue);
        $this->assertEquals($newValue, $model->getVs());
    }

    public static function getSetDataProvider(): array
    {
        return [
            [
                null
            ], [
                '12345'
            ]
        ];
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function set(string|null $newValue): void
    {
        $model = new PaymentDetailBankTransaction();
        $model->setVs($newValue);
        $this->assertEquals($newValue, Reflection::getHiddenProperty($model, 'vs'));
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function getSet(string|null $newValue): void
    {
        $model = new PaymentDetailBankTransaction();
        $this->assertEquals($newValue, $model->setVs($newValue)->getVs());
    }

    /**
     * @test
     */
    public function throwInvalidFormat(): void
    {
        $this->expectException(InvalidNumericFormat::class);
        $model = new PaymentDetailBankTransaction();
        $model->setVs('a123');
    }
}
