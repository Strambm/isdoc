<?php

namespace Isdoc\Tests\Models\PaymentDetailBankTransaction;

use Isdoc\Exceptions\MissingDueDate;
use Isdoc\Models\PaymentDetailBankTransaction;
use PHPUnit\Framework\TestCase;
use Isdoc\Exceptions\InvalidDateFormat;
use Cetria\Helpers\Reflection\Reflection;

class GetSetPaymentDueDateTest extends TestCase
{
    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function get(string $newValue): void
    {
        $model = new PaymentDetailBankTransaction();
        Reflection::setHiddenProperty($model, 'paymentDueDate', $newValue);
        $this->assertEquals($newValue, $model->getPaymentDueDate());
    }

    public static function getSetDataProvider(): array
    {
        return [
            [
                '2022-02-02'
            ], 
        ];
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function set(string $newValue): void
    {
        $model = new PaymentDetailBankTransaction();
        $model->setPaymentDueDate($newValue);
        $this->assertEquals($newValue, Reflection::getHiddenProperty($model, 'paymentDueDate'));
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function getSet(string $newValue): void
    {
        $model = new PaymentDetailBankTransaction();
        $this->assertEquals($newValue, $model->setPaymentDueDate($newValue)->getPaymentDueDate());
    }

    /**
     * @test
     * @dataProvider invalidDateFormatDataProvider
     */
    public function throwInvalidDateFormat(string $value): void
    {
        $this->expectException(InvalidDateFormat::class);
        $model = new PaymentDetailBankTransaction();
        $model->setPaymentDueDate($value);
    }

    public static function invalidDateFormatDataProvider(): array
    {
        return [
            [
                '1.2.2022',
            ], [
                ''
            ], [
                '2022-01-01 15:00:00'
            ]
        ];
    }

    /**
     * @test
     */
    public function throwsMissingIssueDate(): void
    {
        $this->expectException(MissingDueDate::class);
        $model = new PaymentDetailBankTransaction();
        $model->getPaymentDueDate();
    }
}
