<?php

namespace Isdoc\Tests\Models\PaymentDetailBankTransaction;

use Isdoc\Exceptions\MissingBankAccount;
use Isdoc\Models\BankAccount;
use Isdoc\Models\PaymentDetailBankTransaction;
use PHPUnit\Framework\TestCase;
use Cetria\Helpers\Reflection\Reflection;

class GetSetBankAccountTest extends TestCase
{
    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function get(BankAccount $newValue): void
    {
        $model = new PaymentDetailBankTransaction();
        Reflection::setHiddenProperty($model, 'bankAccount', $newValue);
        $this->assertEquals($newValue, $model->getBankAccount());
    }

    public static function getSetDataProvider(): array
    {
        return [
            [
                (new BankAccount())
                    ->setId('661946821')
                    ->setBic('KOMBCZPPXXX')
                    ->setIban('CZ2001000000000661946821')
                    ->setName('Komerční banka, a.s.')
                    ->setBankCode('0100')
            ], 
        ];
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function set(BankAccount $newValue): void
    {
        $model = new PaymentDetailBankTransaction();
        $model->setBankAccount($newValue);
        $this->assertEquals($newValue, Reflection::getHiddenProperty($model, 'bankAccount'));
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function getSet(BankAccount $newValue): void
    {
        $model = new PaymentDetailBankTransaction();
        $this->assertEquals($newValue, $model->setBankAccount($newValue)->getBankAccount());
    }

    /**
     * @test
     */
    public function throwsMissingIssueDate(): void
    {
        $this->expectException(MissingBankAccount::class);
        $model = new PaymentDetailBankTransaction();
        $model->getBankAccount();
    }
}
