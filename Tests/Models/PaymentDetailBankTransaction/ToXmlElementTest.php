<?php

namespace Isdoc\Tests\Models\PaymentDetailBankTransaction;

use Isdoc\Models\BankAccount;
use Isdoc\Models\PaymentDetailBankTransaction;
use PHPUnit\Framework\TestCase;

class ToXmlElementTest extends TestCase
{
    /**
     * @test
     */
    public function onlyRequiredAttributes(): void
    {
        $model = new PaymentDetailBankTransaction();
        $model->setBankAccount(
            (new BankAccount)
                ->setBankCode('0100')
                ->setId('123456789')
                ->setIban('159783')
                ->setBic('BIC')
            )->setPaymentDueDate('2025-01-01');
        $result = $model->toString();
        $structure = json_decode(json_encode(simplexml_load_string($result)), true);
        $this->assertCount(6, $structure);
        $this->assertEquals('2025-01-01', $structure['PaymentDueDate']);
        $this->assertEquals('123456789', $structure['ID']);
        $this->assertEquals('0100', $structure['BankCode']);
        $this->assertEquals([], $structure['Name']);
        $this->assertEquals('159783', $structure['IBAN']);
        $this->assertEquals('BIC', $structure['BIC']);
    }

    /**
     * @test
     */
    public function withNecessaryParameters(): void
    {
        $model = new PaymentDetailBankTransaction();
        $model->setBankAccount(
            (new BankAccount)
                ->setBankCode('0100')
                ->setId('123456789')
                ->setIban('159783')
                ->setBic('BIC')
                ->setName('Name')
            )->setPaymentDueDate('2025-01-01')
            ->setConstantSymbol('1')
            ->setVs('2')
            ->setSpecificSymbol('3');
        $result = $model->toString();
        $structure = json_decode(json_encode(simplexml_load_string($result)), true);
        $this->assertCount(9, $structure);
        $this->assertEquals('2025-01-01', $structure['PaymentDueDate']);
        $this->assertEquals('123456789', $structure['ID']);
        $this->assertEquals('0100', $structure['BankCode']);
        $this->assertEquals('Name', $structure['Name']);
        $this->assertEquals('159783', $structure['IBAN']);
        $this->assertEquals('BIC', $structure['BIC']);
        $this->assertEquals('2', $structure['VariableSymbol']);
        $this->assertEquals('1', $structure['ConstantSymbol']);
        $this->assertEquals('3', $structure['SpecificSymbol']);
    }
}
