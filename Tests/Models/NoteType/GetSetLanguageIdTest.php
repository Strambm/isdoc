<?php

namespace Isdoc\Tests\Models\NoteType;

use Isdoc\Enums\LanguageCode;
use Cetria\Helpers\Reflection\Reflection;
use Isdoc\Exceptions\UnvalidLanguageId;

class GetSetLanguageIdTest extends TestCase
{
    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function get(string|null $newValue): void
    {
        $model = $this->getModel('test');
        Reflection::setHiddenProperty($model, 'languageId', $newValue);
        $this->assertEquals($newValue, $model->getLanguageId());
    }

    public static function getSetDataProvider(): array
    {
        return [
            [
                LanguageCode::CS
            ], [
                LanguageCode::DE
            ], [
                LanguageCode::EN
            ], [
                null
            ]
        ];
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function set(string|null $newValue): void
    {
        $model = $model = $this->getModel('test');
        $model->setLanguageId($newValue);
        $this->assertEquals($newValue, Reflection::getHiddenProperty($model, 'languageId'));
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function getSet(string|null $newValue): void
    {
        $model = $model = $this->getModel('test');
        $this->assertEquals($newValue, $model->setLanguageId($newValue)->getLanguageId());
    }

    /**
     * @test
     */
    public function setUnvalidLanguage(): void
    {
        $this->expectException(UnvalidLanguageId::class);
        $model = $model = $this->getModel('test');
        $model->setLanguageId('výmysl');
    }
}
