<?php

namespace Isdoc\Tests\Models\NoteType;

use Isdoc\Enums\LanguageCode;
use Isdoc\Exceptions\MissingNoteValue;

class ToXmlElementTest extends TestCase
{
    /**
     * @test
     */
    public function missingValueAttribute(): void
    {
        $this->expectException(MissingNoteValue::class);
        $model = $this->getModel('test');
        $model->setLanguageId(LanguageCode::BG);
        $result = $model->toXmlElement();
    }

    /**
     * @test
     */
    public function onlyRequiredAttributes(): void
    {
        $value = '1234567dewe15';
        $languageId = LanguageCode::DA;
        $model = $this->getModel('test');
        $model->setLanguageId($languageId);
        $model->setValue($value);
        $result = $model->toString();
        $structure = json_decode(json_encode(simplexml_load_string($result)), true);
        $this->assertCount(2, $structure);
        $this->assertEquals($value, $structure[0]);
        $this->assertEquals($languageId, $structure['@attributes']['languageID']);
    }
}
