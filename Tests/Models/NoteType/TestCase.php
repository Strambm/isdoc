<?php

namespace Isdoc\Tests\Models\NoteType;

use Isdoc\Models\NoteType;
use PHPUnit\Framework\TestCase as parentTestCase;
use PHPUnit\Framework\MockObject\MockObject;

abstract class TestCase extends parentTestCase
{
    protected function getModel(string $tagNameResult): NoteType|MockObject
    {
        $model = $this->getMockForAbstractClass(
            NoteType::class,
            [],
            '',
            true,
            true,
            true,
            [
                'getTagName'
            ]
        );
        $model->expects($this->any())
            ->method('getTagName')
            ->willReturn($tagNameResult);
        return $model;
    }
}
