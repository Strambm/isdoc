<?php

namespace Isdoc\Tests\Models\NoteType;

use Isdoc\Exceptions\MissingNoteValue;
use Cetria\Helpers\Reflection\Reflection;

class GetSetValueTest extends TestCase
{
    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function get(string $newValue): void
    {
        $model = $this->getModel('test');
        Reflection::setHiddenProperty($model, 'value', $newValue);
        $this->assertEquals($newValue, $model->getValue());
    }

    public static function getSetDataProvider(): array
    {
        return [
            [
                ''
            ], [
                'Poznámka o .....'
            ],
        ];
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function set(string $newValue): void
    {
        $model = $model = $this->getModel('test');
        $model->setValue($newValue);
        $this->assertEquals($newValue, Reflection::getHiddenProperty($model, 'value'));
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function getSet(string $newValue): void
    {
        $model = $model = $this->getModel('test');
        $this->assertEquals($newValue, $model->setValue($newValue)->getValue());
    }

    /**
     * @test
     */
    public function getWithoutValue(): void
    {
        $this->expectException(MissingNoteValue::class);
        $model = $model = $this->getModel('test');
        $model->getValue();
    }
}
