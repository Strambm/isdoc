<?php

namespace Isdoc\Tests\Models\TaxTotal;

use Isdoc\Models\TaxTotal;
use Isdoc\Models\TaxCategory;
use Isdoc\Models\TaxSubTotal;
use PHPUnit\Framework\TestCase;
use Isdoc\Exceptions\TaxSubTotalWasCreatedBefore;
use Cetria\Helpers\Reflection\Reflection;

class AddGetTaxTotalTest extends TestCase
{
    /**
     * @test
     */
    public function ThrowIfTaxSubTotalWithSamePercentExists(): void
    {
        $this->expectException(TaxSubTotalWasCreatedBefore::class);
        $taxSubTotal = (new TaxSubTotal)->setTaxCategory((new TaxCategory)->setPercent(10));
        (new TaxTotal)->addTaxSubTotal($taxSubTotal)->addTaxSubTotal($taxSubTotal);
    }

    /**
     * @test
     * @dataProvider addGetDataProvider
     */
    public function addGet(array $taxSubTotals): void
    {
        $model = new TaxTotal;
        foreach($taxSubTotals as $taxSubTotal) {
            $model = $model->addTaxSubTotal($taxSubTotal);
        }
        $this->assertEquals($taxSubTotals, Reflection::getHiddenProperty($model, 'taxSubTotals'));
        $this->assertEquals($taxSubTotals, $model->getTaxSubTotals());
    }

    public static function addGetDataProvider(): array
    {
        return [
            [
                [
                    (new TaxSubTotal)->setTaxCategory((new TaxCategory)->setPercent(10)),
                    (new TaxSubTotal)->setTaxCategory((new TaxCategory)->setPercent(20))
                ]
            ], [
                [

                ]
            ] 
        ];
    }

    /**
     * @test
     * @dataProvider addGetDataProvider
     */
    public function get(array $taxSubTotals): void
    {
        $taxSubTotal = new TaxTotal();
        Reflection::setHiddenProperty($taxSubTotal, 'taxSubTotals', $taxSubTotals);
        $this->assertEquals($taxSubTotals, $taxSubTotal->getTaxSubTotals());
    }
}
