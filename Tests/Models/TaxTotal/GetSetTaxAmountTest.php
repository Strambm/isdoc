<?php

namespace Isdoc\Tests\Models\TaxTotal;

use Isdoc\Models\TaxCategory;
use Isdoc\Models\TaxSubTotal;
use Isdoc\Models\TaxTotal;
use PHPUnit\Framework\TestCase;
use Cetria\Helpers\Reflection\Reflection;

class GetSetTaxAmountTest extends TestCase
{
    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function get(float|null $newValue): void
    {
        $model = new TaxTotal();
        Reflection::setHiddenProperty($model, 'taxAmount', $newValue);
        $this->assertEquals($newValue, $model->getTaxAmount());
    }

    public static function getSetDataProvider(): array
    {
        return [
            [
                0
            ], [
                15
            ], [
                -18.5
            ],
        ];
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function set(float|null $newValue): void
    {
        $model = new TaxTotal();
        $model->setTaxAmount($newValue);
        $this->assertEquals($newValue, Reflection::getHiddenProperty($model, 'taxAmount'));
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function getSet(float|null $newValue): void
    {
        $model = new TaxTotal();
        $this->assertEquals($newValue, $model->setTaxAmount($newValue)->getTaxAmount());
    }

    /**
     * @test
     */
    public function automaticsCalc(): void
    {
        $taxSubTotal1 = new TaxSubTotal();
        $taxSubTotal1->setTaxAmount(20)->setTaxCategory((new TaxCategory)->setPercent(10));
        $taxSubTotal2 = new TaxSubTotal();
        $taxSubTotal2->setTaxAmount(20)->setTaxCategory((new TaxCategory)->setPercent(20));
        $taxTotal = (new TaxTotal())->addTaxSubTotal($taxSubTotal1)->addTaxSubTotal($taxSubTotal2);
        $this->assertEquals(40, $taxTotal->getTaxAmount());
    }

    /**
     * @test
     */
    public function automaticCalcRetunZero(): void
    {
        $taxSubTotal1 = new TaxSubTotal();
        $taxSubTotal1->setTaxCategory((new TaxCategory)->setPercent(10));
        $taxTotal = (new TaxTotal())->addTaxSubTotal($taxSubTotal1);
        $this->assertEquals(0, $taxTotal->getTaxAmount());
    }
}
