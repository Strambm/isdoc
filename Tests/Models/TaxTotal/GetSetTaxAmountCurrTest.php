<?php

namespace Isdoc\Tests\Models\TaxTotal;

use Isdoc\Models\TaxCategory;
use Isdoc\Models\TaxSubTotal;
use Isdoc\Models\TaxTotal;
use PHPUnit\Framework\TestCase;
use Cetria\Helpers\Reflection\Reflection;

class GetSetTaxAmountCurrTest extends TestCase
{
    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function get(float|null $newValue): void
    {
        $model = new TaxTotal();
        Reflection::setHiddenProperty($model, 'taxAmountCurr', $newValue);
        $this->assertEquals($newValue, $model->getTaxAmountCurr());
    }

    public static function getSetDataProvider(): array
    {
        return [
            [
                0
            ], [
                15
            ], [
                -18.5
            ],
        ];
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function set(float|null $newValue): void
    {
        $model = new TaxTotal();
        $model->setTaxAmountCurr($newValue);
        $this->assertEquals($newValue, Reflection::getHiddenProperty($model, 'taxAmountCurr'));
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function getSet(float|null $newValue): void
    {
        $model = new TaxTotal();
        $this->assertEquals($newValue, $model->setTaxAmountCurr($newValue)->getTaxAmountCurr());
    }

    /**
     * @test
     */
    public function automaticsCalc(): void
    {
        $taxSubTotal1 = new TaxSubTotal();
        $taxSubTotal1->setTaxAmountCurr(20)->setTaxCategory((new TaxCategory)->setPercent(10));
        $taxSubTotal2 = new TaxSubTotal();
        $taxSubTotal2->setTaxAmountCurr(20)->setTaxCategory((new TaxCategory)->setPercent(20));
        $taxTotal = (new TaxTotal())->addTaxSubTotal($taxSubTotal1)->addTaxSubTotal($taxSubTotal2);
        $this->assertEquals(40, $taxTotal->getTaxAmountCurr());
    }

    /**
     * @test
     */
    public function automaticCalcRetunNull(): void
    {
        $taxSubTotal1 = new TaxSubTotal();
        $taxSubTotal1->setTaxCategory((new TaxCategory)->setPercent(10));
        $taxTotal = (new TaxTotal())->addTaxSubTotal($taxSubTotal1);
        $this->assertNull($taxTotal->getTaxAmountCurr());
    }
}
