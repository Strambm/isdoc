<?php

namespace Isdoc\Tests\Models\TaxTotal;

use Isdoc\Enums\VatCalculationMethod;
use Isdoc\Models\ClassifiedTaxCategory;
use Isdoc\Models\TaxedDeposit;
use Isdoc\Models\TaxTotal;
use PHPUnit\Framework\TestCase;

class AddTaxedDepositTest extends TestCase
{
    /**
     * @test
     */
    public function emptyTaxTotal(): void
    {
        $taxedDeposit = (new TaxedDeposit)
            ->setClassifiedTaxCategory((new ClassifiedTaxCategory)
                ->setPercent(10)
                ->setVatCalculationMethod(VatCalculationMethod::SHORA)
            )->setTaxInclusiveAmount(82);
        $model = new TaxTotal();
        $model->addTaxedDeposit($taxedDeposit);
        $result = $model->getTaxSubTotals();
        $this->assertCount(1, $result);
        $this->assertEquals(10, $result[0]->getTaxCategory()->getPercent());
        $this->assertEquals(82, $result[0]->getAlreadyClaimedTaxInclusiveAmount());
    }

    /**
     * @test
     */
    public function taxSubTotalExists(): void
    {
        $taxedDeposit = (new TaxedDeposit)
            ->setClassifiedTaxCategory((new ClassifiedTaxCategory)
                ->setPercent(10)
                ->setVatCalculationMethod(VatCalculationMethod::SHORA)
            )->setTaxInclusiveAmount(82);
        $model = new TaxTotal();
        $model->addTaxedDeposit($taxedDeposit);
        $model->addTaxedDeposit($taxedDeposit);
        $result = $model->getTaxSubTotals();
        $this->assertCount(1, $result);
        $this->assertEquals(10, $result[0]->getTaxCategory()->getPercent());
        $this->assertEquals(82 * 2, $result[0]->getAlreadyClaimedTaxInclusiveAmount());
    }
}
