<?php

namespace Isdoc\Tests\Models\TaxTotal;

use Isdoc\Models\TaxCategory;
use Isdoc\Models\TaxSubTotal;
use Isdoc\Models\TaxTotal;
use PHPUnit\Framework\TestCase;

class ToXmlElementTest extends TestCase
{
    /**
     * @test
     */
    public function empty(): void
    {
        $model = new TaxTotal();
        $result = $model->toString();
        $structure = json_decode(json_encode(simplexml_load_string($result)), true);
        $this->assertCount(1, $structure);
        $this->assertEquals(0, $structure['TaxAmount']); 
    }

    /**
     * @test
     */
    public function withNecessaryParameters(): void
    {
        $taxSubTotal0 = (new TaxSubTotal)->setTaxCategory((new TaxCategory)->setPercent(0)->setVATAplicable(true))->setTaxAmount(0)->setTaxAmountCurr(0);
        $taxSubTotal125 = (new TaxSubTotal)->setTaxCategory((new TaxCategory)->setPercent(12.5)->setVATAplicable(true))->setTaxAmount(12.5)->setTaxAmountCurr(0.125);
        $taxSubTotal21 = (new TaxSubTotal)->setTaxCategory((new TaxCategory)->setPercent(21)->setVATAplicable(true))->setTaxAmount(21)->setTaxAmountCurr(0.21);
        $model = new TaxTotal();
        $model->addTaxSubTotal($taxSubTotal0)->addTaxSubTotal($taxSubTotal125)->addTaxSubTotal($taxSubTotal21);
        $result = $model->toString();
        $structure = json_decode(json_encode(simplexml_load_string($result)), true);
        $this->assertCount(3, $structure);
        $this->assertEquals(33.5, $structure['TaxAmount']);
        $this->assertEquals(0.335, $structure['TaxAmountCurr']);
        $this->assertCount(3, $structure['TaxSubTotal']);
        $this->assertEquals(12.5, $structure['TaxSubTotal'][1]['TaxAmount']);
    }
}
