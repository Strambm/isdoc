<?php

namespace Isdoc\Tests\Models\TaxTotal;

use Isdoc\Enums\VatCalculationMethod;
use Isdoc\Models\ClassifiedTaxCategory;
use Isdoc\Models\InvoiceLine;
use Isdoc\Models\TaxTotal;
use PHPUnit\Framework\TestCase;

class AddInvoiceLineTest extends TestCase
{
    /**
     * @test
     */
    public function emptyTaxTotal(): void
    {
        $invoiceLine = (new InvoiceLine)
            ->setClassifiedTaxCategory((new ClassifiedTaxCategory)
                ->setPercent(10)
                ->setVatCalculationMethod(VatCalculationMethod::SHORA)
            )->setLineExtensionAmountTaxInclusive(82);
        $model = new TaxTotal();
        $model->addInvoiceLine($invoiceLine);
        $result = $model->getTaxSubTotals();
        $this->assertCount(1, $result);
        $this->assertEquals(10, $result[0]->getTaxCategory()->getPercent());
        $this->assertEquals(82, $result[0]->getTaxInclusiveAmount());
    }

    /**
     * @test
     */
    public function taxSubTotalExists(): void
    {
        $invoiceLine = (new InvoiceLine)
            ->setClassifiedTaxCategory((new ClassifiedTaxCategory)
                ->setPercent(10)
                ->setVatCalculationMethod(VatCalculationMethod::SHORA)
            )->setLineExtensionAmountTaxInclusive(82);
        $model = new TaxTotal();
        $model->addInvoiceLine($invoiceLine);
        $model->addInvoiceLine($invoiceLine);
        $result = $model->getTaxSubTotals();
        $this->assertCount(1, $result);
        $this->assertEquals(10, $result[0]->getTaxCategory()->getPercent());
        $this->assertEquals(82 * 2, $result[0]->getTaxInclusiveAmount());
    }
}
