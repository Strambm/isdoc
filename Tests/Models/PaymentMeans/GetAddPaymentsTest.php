<?php

namespace Isdoc\Tests\Models\PaymentMeans;

use Isdoc\Enums\PaymentMeansCode;
use Isdoc\Exceptions\MissingPayment;
use Isdoc\Models\Payment;
use Isdoc\Models\PaymentMeans;
use PHPUnit\Framework\TestCase;
use Cetria\Helpers\Reflection\Reflection;

class GetAddPaymentsTest extends TestCase
{
    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function get(array $newValue): void
    {
        $model = new PaymentMeans();
        Reflection::setHiddenProperty($model, 'payments', $newValue);
        $this->assertEquals($newValue, $model->getPayments());
    }

    public static function getSetDataProvider(): array
    {
        return [
            [
                [
                    (new Payment)
                        ->setPaidAmount(10)
                        ->setPaymentMeansCode(PaymentMeansCode::CASH_ON_DELIVERY)
                ]
            ], [
                [
                    (new Payment)
                        ->setPaidAmount(10)
                        ->setPaymentMeansCode(PaymentMeansCode::CASH_ON_DELIVERY),
                    (new Payment)
                        ->setPaidAmount(10)
                        ->setPaymentMeansCode(PaymentMeansCode::BILL_BETWEEN_PARTNERS),
                ]
            ]
        ];
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function set(array $newValue): void
    {
        $model = new PaymentMeans();
        foreach($newValue as $value) {
            $model = $model->addPayment($value);
        }
        $this->assertEquals($newValue, Reflection::getHiddenProperty($model, 'payments'));
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function getSet(array $newValue): void
    {
        $model = new PaymentMeans();
        foreach($newValue as $value) {
            $model = $model->addPayment($value);
        }
        $this->assertEquals($newValue, $model->getPayments());
    }

    /**
     * @test
     */
    public function getWithoutValue(): void
    {
        $this->expectException(MissingPayment::class);
        $model = new PaymentMeans();
        $model->getPayments();
    }
}
