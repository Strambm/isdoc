<?php

namespace Isdoc\Tests\Models\PaymentMeans;

use Isdoc\Models\Payment;
use Isdoc\Models\BankAccount;
use Isdoc\Models\PaymentMeans;
use PHPUnit\Framework\TestCase;
use Isdoc\Enums\PaymentMeansCode;

class ToXmlElementTest extends TestCase
{
    /**
     * @test
     */
    public function onlyRequiredAttributes(): void
    {
        $model = new PaymentMeans();
        $model->addPayment((new Payment())->setPaidAmount(122)->setPaymentMeansCode(PaymentMeansCode::CASH));
        $result = $model->toString();
        $structure = json_decode(json_encode(simplexml_load_string($result)), true);
        $this->assertCount(1, $structure);
        $this->assertEquals(122, $structure['Payment']['PaidAmount']);
    }

    /**
     * @test
     */
    public function withNecessaryParameters(): void
    {
        $model = new PaymentMeans();
        $model
            ->addPayment((new Payment())->setPaidAmount(122)->setPaymentMeansCode(PaymentMeansCode::CASH))
            ->addPayment((new Payment())->setPaidAmount(123)->setPaymentMeansCode(PaymentMeansCode::BANK_TRANSFER))
            ->addAlternativeBankAccount((new BankAccount())->setBankCode('123')->setBic('BIC1')->setIban('IBAN1')->setId('1-123456')->setName('NAME1'))
            ->addAlternativeBankAccount((new BankAccount())->setBankCode('123')->setBic('BIC2')->setIban('IBAN2')->setId('2-123456')->setName('NAME2'));
        $result = $model->toString();
        $structure = json_decode(json_encode(simplexml_load_string($result)), true);
        $this->assertCount(2, $structure);
        $this->assertEquals(123, $structure['Payment'][1]['PaidAmount']);
        $this->assertEquals('BIC2', $structure['AlternateBankAccounts']['AlternateBankAccount'][1]['BIC']);
    }
}
