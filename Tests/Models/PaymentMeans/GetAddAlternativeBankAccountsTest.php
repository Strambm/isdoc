<?php

namespace Isdoc\Tests\Models\PaymentMeans;

use Isdoc\Models\BankAccount;
use Isdoc\Models\PaymentMeans;
use PHPUnit\Framework\TestCase;
use Cetria\Helpers\Reflection\Reflection;

class GetAddAlternativeBankAccountsTest extends TestCase
{
    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function get(array $newValue): void
    {
        $model = new PaymentMeans();
        Reflection::setHiddenProperty($model, 'alternativeBankAccounts', $newValue);
        $this->assertEquals($newValue, $model->getAlternativeBankAccounts());
    }

    public static function getSetDataProvider(): array
    {
        return [
            [
                [
                    (new BankAccount)
                        ->setBankCode('11')
                        ->setBic('BIC1')
                        ->setId('123456789')
                        ->setIban('IBAN123456')
                        ->setName('BANKA1')
                ]
            ], [
                [
                    (new BankAccount)
                        ->setBankCode('11')
                        ->setBic('BIC1')
                        ->setId('123456789')
                        ->setIban('IBAN123456')
                        ->setName('BANKA1'),
                    (new BankAccount)
                        ->setBankCode('12')
                        ->setBic('BIC2')
                        ->setId('1234567890')
                        ->setIban('IBAN1234560')
                        ->setName('BANKA2')
                ]
            ], [
                [
                    
                ]
            ]
        ];
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function set(array $newValue): void
    {
        $model = new PaymentMeans();
        foreach($newValue as $value) {
            $model = $model->addAlternativeBankAccount($value);
        }
        $this->assertEquals($newValue, Reflection::getHiddenProperty($model, 'alternativeBankAccounts'));
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function getSet(array $newValue): void
    {
        $model = new PaymentMeans();
        foreach($newValue as $value) {
            $model = $model->addAlternativeBankAccount($value);
        }
        $this->assertEquals($newValue, $model->getAlternativeBankAccounts());
    }
}
