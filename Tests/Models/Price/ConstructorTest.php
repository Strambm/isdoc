<?php

namespace Isdoc\Tests\Models\Price;

use Isdoc\Models\Price;
use PHPUnit\Framework\TestCase;
use Isdoc\Enums\VatCalculationMethod;
use Cetria\Helpers\Reflection\Reflection;

class ConstructorTest extends TestCase
{
    /**
     * @test
     */
    public function basic(): void
    {
        $model = new Price(10, VatCalculationMethod::SHORA);
        $this->assertEquals(10, Reflection::getHiddenProperty($model, 'vatPercent'));
        $this->assertEquals(VatCalculationMethod::SHORA, Reflection::getHiddenProperty($model, 'vatCalculationMethod'));
    }
}
