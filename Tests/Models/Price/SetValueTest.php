<?php

namespace Isdoc\Tests\Models\Price;

use Isdoc\Models\Price;
use PHPUnit\Framework\TestCase;
use Cetria\Helpers\Reflection\Reflection;

class SetValueTest extends TestCase
{
    /**
     * @test
     */
    public function basic(): void
    {
        $model = $this->getModel();
        $model->setValue(120);
        $this->assertEquals(120, Reflection::getHiddenProperty($model, 'value'));
    }

    private function getModel(): Price
    {
        $model = $this->getMockBuilder(Price::class)
            ->onlyMethods([])
            ->disableOriginalConstructor()
            ->getMock();
        return $model;
    }
}
