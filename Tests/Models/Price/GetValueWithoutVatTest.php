<?php

namespace Isdoc\Tests\Models\Price;

use Isdoc\Enums\VatCalculationMethod;
use Isdoc\Models\Price;
use PHPUnit\Framework\TestCase;

class GetValueWithoutVatTest extends TestCase
{
    /**
     * @test
     */
    public function FromUp(): void
    {
        $model = new Price(10, VatCalculationMethod::SHORA);
        $model->setValue(110);
        $this->assertEqualsWithDelta(100, $model->getValueWithoutVat(), 0.001);
    }

    /**
     * @test
     */
    public function FromBottom(): void
    {
        $model = new Price(10, VatCalculationMethod::ZDOLA);
        $model->setValue(100);
        $this->assertEqualsWithDelta(100, $model->getValueWithoutVat(), 0.001);
    }
}
