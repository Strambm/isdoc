<?php

namespace Isdoc\Tests\Models\Payment;

use Isdoc\Exceptions\UnvalidPaymentDetailsForPaymentMeansCode;
use Isdoc\Models\Payment;
use PHPUnit\Framework\TestCase;
use Isdoc\Models\PaymentDetails;
use Isdoc\Enums\PaymentMeansCode;
use Isdoc\Models\PaymentDetailCash;
use Cetria\Helpers\Reflection\Reflection;
use Isdoc\Models\PaymentDetailBankTransaction;

class GetSetDetailsTest extends TestCase
{
    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function get(int $paymentMeansCode, null|PaymentDetails $details): void
    {
        $model = new Payment();
        $model->setPaymentMeansCode($paymentMeansCode);
        Reflection::setHiddenProperty($model, 'details', $details);
        $this->assertEquals($details, $model->getDetails());
    }

    public static function getSetDataProvider(): array
    {
        return [
            [
                PaymentMeansCode::BANK_TRANSFER,
                (new PaymentDetailBankTransaction)
            ], [
                PaymentMeansCode::CASH,
                (new PaymentDetailCash)
            ], [
                PaymentMeansCode::CASH_ON_DELIVERY,
                null
            ]
        ];
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function set(int $paymentMeansCode, null|PaymentDetails $details): void
    {
        $model = new Payment();
        $model->setPaymentMeansCode($paymentMeansCode);
        $model->setDetails($details);
        $this->assertEquals($details, Reflection::getHiddenProperty($model, 'details'));
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function getSet(int $paymentMeansCode, null|PaymentDetails $details): void
    {
        $model = new Payment();
        $model->setPaymentMeansCode($paymentMeansCode);
        $this->assertEquals($details, $model->setDetails($details)->getDetails());
    }

    /**
     * @test
     */
    public function throwUnvalidPaymentDetailsForCode(): void
    {
        $this->expectException(UnvalidPaymentDetailsForPaymentMeansCode::class);
        $model = new Payment();
        $model->setPaymentMeansCode(PaymentMeansCode::CARD);
        $model->setDetails((new PaymentDetailBankTransaction))->getDetails();
    }
}
