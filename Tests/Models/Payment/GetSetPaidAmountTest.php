<?php

namespace Isdoc\Tests\Models\Payment;

use Isdoc\Exceptions\MissingPaidAmount;
use Isdoc\Models\Payment;
use PHPUnit\Framework\TestCase;
use Cetria\Helpers\Reflection\Reflection;

class GetSetPaidAmountTest extends TestCase
{
    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function get(float $newValue): void
    {
        $model = new Payment();
        Reflection::setHiddenProperty($model, 'paidAmount', $newValue);
        $this->assertEquals($newValue, $model->getPaidAmount());
    }

    public static function getSetDataProvider(): array
    {
        return [
            [
                12356
            ], [
                12.256
            ], [
                -1
            ]
        ];
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function set(float $newValue): void
    {
        $model = new Payment();
        $model->setPaidAmount($newValue);
        $this->assertEquals($newValue, Reflection::getHiddenProperty($model, 'paidAmount'));
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function getSet(float $newValue): void
    {
        $model = new Payment();
        $this->assertEquals($newValue, $model->setPaidAmount($newValue)->getPaidAmount());
    }

    /**
     * @test
     */
    public function getWithoutValue(): void
    {
        $this->expectException(MissingPaidAmount::class);
        $model = new Payment();
        $model->getPaidAmount();
    }
}
