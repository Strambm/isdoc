<?php

namespace Isdoc\Tests\Models\Payment;

use Isdoc\Models\Payment;
use PHPUnit\Framework\TestCase;
use Isdoc\Enums\PaymentMeansCode;
use Cetria\Helpers\Reflection\Reflection;
use Isdoc\Exceptions\MissingPaymentMeansCode;
use Isdoc\Exceptions\UnvalidPaymentMeansCode;

class GetSetPaymentMeansCodeTest extends TestCase
{
    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function get(int $newValue): void
    {
        $model = new Payment();
        Reflection::setHiddenProperty($model, 'paymentMeansCode', $newValue);
        $this->assertEquals($newValue, $model->getPaymentMeansCode());
    }

    public static function getSetDataProvider(): array
    {
        return [
            [
                PaymentMeansCode::BANK_TRANSFER
            ], [
                PaymentMeansCode::CASH
            ], [
                PaymentMeansCode::CASH_ON_DELIVERY
            ]
        ];
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function set(int $newValue): void
    {
        $model = new Payment();
        $model->setPaymentMeansCode($newValue);
        $this->assertEquals($newValue, Reflection::getHiddenProperty($model, 'paymentMeansCode'));
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function getSet(int $newValue): void
    {
        $model = new Payment();
        $this->assertEquals($newValue, $model->setPaymentMeansCode($newValue)->getPaymentMeansCode());
    }

    /**
     * @test
     */
    public function getWithoutValue(): void
    {
        $this->expectException(MissingPaymentMeansCode::class);
        $model = new Payment();
        $model->getPaymentMeansCode();
    }

    /**
     * @test
     */
    public function unvalidCode(): void
    {
        $this->expectException(UnvalidPaymentMeansCode::class);
        $model = new Payment();
        $model->setPaymentMeansCode(123);
    }
}
