<?php

namespace Isdoc\Tests\Models\Payment;

use Isdoc\Models\Payment;
use PHPUnit\Framework\TestCase;
use Isdoc\Enums\PaymentMeansCode;
use Isdoc\Models\PaymentDetailCash;

class ToXmlElementTest extends TestCase
{
    /**
     * @test
     */
    public function onlyRequiredAttributes(): void
    {
        $model = new Payment();
        $model->setPaidAmount(100);
        $model->setPaymentMeansCode(PaymentMeansCode::CHECK);
        $result = $model->toString();
        $structure = json_decode(json_encode(simplexml_load_string($result)), true);
        $this->assertCount(3, $structure);
        $this->assertEquals('false', $structure['@attributes']['partialPayment']);
        $this->assertEquals(100, $structure['PaidAmount']);
        $this->assertEquals(PaymentMeansCode::CHECK, $structure['PaymentMeansCode']);
    }

    /**
     * @test
     */
    public function withNecessaryParameters(): void
    {
        $detail = (new PaymentDetailCash)
            ->setDocumentId('123456')
            ->setIssueDate('2023-11-11');
        $model = new Payment();
        $model->setDetails($detail)
            ->setHasPartialPayment(true)
            ->setPaidAmount(123)
            ->setPaymentMeansCode(PaymentMeansCode::CASH);
        $result = $model->toString();
        $structure = json_decode(json_encode(simplexml_load_string($result)), true);
        $this->assertCount(4, $structure);
        $this->assertEquals('true', $structure['@attributes']['partialPayment']);
        $this->assertEquals(123, $structure['PaidAmount']);
        $this->assertEquals(PaymentMeansCode::CASH, $structure['PaymentMeansCode']);
        $this->assertEquals('123456', $structure['Details']['DocumentID']);
    }
}
