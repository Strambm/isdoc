<?php

namespace Isdoc\Tests\Models\Payment;

use Isdoc\Models\Payment;
use PHPUnit\Framework\TestCase;
use Cetria\Helpers\Reflection\Reflection;

class GetSetHasPartialPaymentTest extends TestCase
{
    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function get(bool $newValue): void
    {
        $model = new Payment();
        Reflection::setHiddenProperty($model, 'partialPayment', $newValue);
        $this->assertEquals($newValue, $model->getHasPartialPayment());
    }

    public static function getSetDataProvider(): array
    {
        return [
            [
                true
            ], [
                false
            ]
        ];
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function set(bool $newValue): void
    {
        $model = new Payment();
        $model->setHasPartialPayment($newValue);
        $this->assertEquals($newValue, Reflection::getHiddenProperty($model, 'partialPayment'));
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function getSet(bool $newValue): void
    {
        $model = new Payment();
        $this->assertEquals($newValue, $model->setHasPartialPayment($newValue)->getHasPartialPayment());
    }
}
