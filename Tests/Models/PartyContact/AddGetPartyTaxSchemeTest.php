<?php

namespace Isdoc\Tests\Models\PartyContact;

use Isdoc\Enums\TaxScheme;
use Isdoc\Models\PartyContact;
use Isdoc\Models\PartyTaxScheme;
use PHPUnit\Framework\TestCase;
use Cetria\Helpers\Reflection\Reflection;

class AddGetPartyTaxSchemeTest extends TestCase
{
    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function get(array $newValue): void
    {
        $model = new PartyContact();
        Reflection::setHiddenProperty($model, 'partyTaxSchemes', $newValue);
        $this->assertEquals($newValue, $model->getPartyTaxScheme());
    }

    public static function getSetDataProvider(): array
    {
        return [
            [
                [
                    
                ]
            ], [
                [
                    (new PartyTaxScheme('123456', TaxScheme::TIN))
                ], [
                    (new PartyTaxScheme('123456', TaxScheme::VAT)),
                    (new PartyTaxScheme('123456', TaxScheme::VAT))
                ]
            ]
        ];
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function add(array $newValue): void
    {
        $model = new PartyContact();
        foreach($newValue as $value) {
            $model = $model->addPartyTaxScheme($value);
        }
        $this->assertEquals($newValue, Reflection::getHiddenProperty($model, 'partyTaxSchemes'));
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function addGet(array $newValue): void
    {
        $model = new PartyContact();
        foreach($newValue as $value) {
            $model->addPartyTaxScheme($value);
        }
        $this->assertEquals($newValue, $model->getPartyTaxScheme());
    }
}
