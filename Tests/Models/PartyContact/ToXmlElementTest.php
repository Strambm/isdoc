<?php

namespace Isdoc\Tests\Models\PartyContact;

use Isdoc\Models\Contact;
use Isdoc\Enums\TaxScheme;
use Isdoc\Models\PartyContact;
use Isdoc\Models\PostalAddress;
use PHPUnit\Framework\TestCase;
use Isdoc\Models\PartyTaxScheme;
use Isdoc\Models\PartyIdentification;
use Isdoc\Exceptions\MissingPartyName;
use Isdoc\Exceptions\MissingPostalAddress;
use Isdoc\Exceptions\MissingPartyIdentifivation;

class ToXmlElementTest extends TestCase
{
    /**
     * @test
     */
    public function missingPartyIdentificationAttribute(): void
    {
        $this->expectException(MissingPartyIdentifivation::class);
        $model = new PartyContact();
        $model->setName('Name')
            ->setPostalAddress((new PostalAddress));
        $model->toXmlElement();
    }

    /**
     * @test
     */
    public function missingPartyNameAttribute(): void
    {
        $this->expectException(MissingPartyName::class);
        $model = new PartyContact();
        $model->setPartyIdentification((new PartyIdentification)->setId('123'))
            ->setPostalAddress((new PostalAddress));
        $model->toXmlElement();
    }

    /**
     * @test
     */
    public function missingPostalAddressAttribute(): void
    {
        $this->expectException(MissingPostalAddress::class);
        $model = new PartyContact();
        $model->setPartyIdentification((new PartyIdentification)->setId('123'))
            ->setName('Name');
        $model->toXmlElement();
    }

    /**
     * @test
     */
    public function onlyRequiredAttributes(): void
    {
        $name = 'Name1248';
        $id = '12';
        $city = 'City123';
        $model = new PartyContact();
        $model->setName($name)
            ->setPartyIdentification((new PartyIdentification)->setId($id))
            ->setPostalAddress((new PostalAddress)->setCityName($city));
        $result = $model->toString();
        $structure = json_decode(json_encode(simplexml_load_string($result)), true);
        $this->assertCount(3, $structure);
        $this->assertEquals($name, $structure['PartyName']['Name']);
        $this->assertEquals($city, $structure['PostalAddress']['CityName']);
        $this->assertEquals($id, $structure['PartyIdentification']['ID']);
    }

    /**
     * @test
     */
    public function withNecessaryParameters(): void
    {
        $name = 'Name';
        $id = 'id';
        $city = 'city';
        $email = 'a@a.com';
        $taxScheme1 = new PartyTaxScheme('123', TaxScheme::TIN);
        $taxScheme2 = new PartyTaxScheme('1234', TaxScheme::VAT);
        $model = new PartyContact();
        $model->setContact((new Contact)->setElectronicMail($email))
            ->setName($name)
            ->setPartyIdentification((new PartyIdentification)->setId($id))
            ->setPostalAddress((new PostalAddress)->setCityName($city))
            ->addPartyTaxScheme($taxScheme1)
            ->addPartyTaxScheme($taxScheme2);
        $result = $model->toString();
        $structure = json_decode(json_encode(simplexml_load_string($result)), true);
        $this->assertCount(5, $structure);
        $this->assertEquals($name, $structure['PartyName']['Name']);
        $this->assertEquals($city, $structure['PostalAddress']['CityName']);
        $this->assertEquals($id, $structure['PartyIdentification']['ID']);
        $this->assertEquals($email, $structure['Contact']['ElectronicMail']);
        $this->assertEquals($taxScheme1->getTaxScheme(), $structure['PartyTaxScheme'][0]['TaxScheme']);
        $this->assertEquals($taxScheme2->getTaxScheme(), $structure['PartyTaxScheme'][1]['TaxScheme']);
    }
}
