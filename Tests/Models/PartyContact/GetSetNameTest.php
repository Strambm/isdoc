<?php

namespace Isdoc\Tests\Models\PartyContact;

use Isdoc\Exceptions\MissingPartyName;
use Isdoc\Models\PartyContact;
use PHPUnit\Framework\TestCase;
use Cetria\Helpers\Reflection\Reflection;

class GetSetNameTest extends TestCase
{
    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function get(string $newValue): void
    {
        $model = new PartyContact();
        Reflection::setHiddenProperty($model, 'name', $newValue);
        $this->assertEquals($newValue, $model->getName());
    }

    public static function getSetDataProvider(): array
    {
        return [
            [
                'ABBA s.r.o.'
            ], [
                'Skanska a.s.'
            ]
        ];
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function set(string $newValue): void
    {
        $model = new PartyContact();
        $model->setName($newValue);
        $this->assertEquals($newValue, Reflection::getHiddenProperty($model, 'name'));
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function getSet(string $newValue): void
    {
        $model = new PartyContact();
        $this->assertEquals($newValue, $model->setName($newValue)->getName());
    }

    /**
     * @test
     */
    public function getWithoutValue(): void
    {
        $this->expectException(MissingPartyName::class);
        $model = new PartyContact();
        $model->getName();
    }
}
