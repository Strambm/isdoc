<?php

namespace Isdoc\Tests\Models\PartyContact;

use Isdoc\Models\Country;
use Isdoc\Enums\CountryCode;
use Isdoc\Models\PartyContact;
use Isdoc\Models\PostalAddress;
use PHPUnit\Framework\TestCase;
use Cetria\Helpers\Reflection\Reflection;
use Isdoc\Exceptions\MissingPostalAddress;

class GetSetPostalAddressTest extends TestCase
{
    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function get(PostalAddress $newValue): void
    {
        $model = new PartyContact();
        Reflection::setHiddenProperty($model, 'postalAddress', $newValue);
        $this->assertEquals($newValue, $model->getPostalAddress());
    }

    public static function getSetDataProvider(): array
    {
        return [
            [
                (new PostalAddress)
            ], [
                (new PostalAddress)
                    ->setBuildingNumber('12')
                    ->setCityName('Město')
                    ->setCountry((new Country)->setIdentificationCode(CountryCode::CZ))
                    ->setPostalZone('45689')
                    ->setStreetName('ulice')
            ]
        ];
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function set(PostalAddress $newValue): void
    {
        $model = new PartyContact();
        $model->setPostalAddress($newValue);
        $this->assertEquals($newValue, Reflection::getHiddenProperty($model, 'postalAddress'));
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function getSet(PostalAddress $newValue): void
    {
        $model = new PartyContact();
        $this->assertEquals($newValue, $model->setPostalAddress($newValue)->getPostalAddress());
    }

    /**
     * @test
     */
    public function getWithoutValue(): void
    {
        $this->expectException(MissingPostalAddress::class);
        $model = new PartyContact();
        $model->getPostalAddress();
    }
}
