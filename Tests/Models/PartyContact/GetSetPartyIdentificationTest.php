<?php

namespace Isdoc\Tests\Models\PartyContact;

use Isdoc\Exceptions\MissingPartyIdentifivation;
use Isdoc\Models\PartyContact;
use Isdoc\Models\PartyIdentification;
use PHPUnit\Framework\TestCase;
use Cetria\Helpers\Reflection\Reflection;

class GetSetPartyIdentificationTest extends TestCase
{
    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function get(PartyIdentification $newValue): void
    {
        $model = new PartyContact();
        Reflection::setHiddenProperty($model, 'partyIdentification', $newValue);
        $this->assertEquals($newValue, $model->getPartyIdentification());
    }

    public static function getSetDataProvider(): array
    {
        return [
            [
                (new PartyIdentification)
                    ->setId('123456')
            ],
        ];
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function set(PartyIdentification $newValue): void
    {
        $model = new PartyContact();
        $model->setPartyIdentification($newValue);
        $this->assertEquals($newValue, Reflection::getHiddenProperty($model, 'partyIdentification'));
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function getSet(PartyIdentification $newValue): void
    {
        $model = new PartyContact();
        $this->assertEquals($newValue, $model->setPartyIdentification($newValue)->getPartyIdentification());
    }

    /**
     * @test
     */
    public function getWithoutValue(): void
    {
        $this->expectException(MissingPartyIdentifivation::class);
        $model = new PartyContact();
        $model->getPartyIdentification();
    }
}
