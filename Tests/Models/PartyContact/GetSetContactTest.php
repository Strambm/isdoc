<?php

namespace Isdoc\Tests\Models\PartyContact;

use Isdoc\Models\Contact;
use Isdoc\Models\PartyContact;
use PHPUnit\Framework\TestCase;
use Cetria\Helpers\Reflection\Reflection;

class GetSetContactTest extends TestCase
{
    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function get(Contact|null $newValue): void
    {
        $model = new PartyContact();
        Reflection::setHiddenProperty($model, 'contact', $newValue);
        $this->assertEquals($newValue, $model->getContact());
    }

    public static function getSetDataProvider(): array
    {
        return [
            [
                null
            ], [
                (new Contact)
                    ->setElectronicMail('test@gigaprint.cz')
                    ->setName('Jméno Příjmení')
                    ->setTelephone('+420604555555')
            ], [
                (new Contact)
                    ->setElectronicMail('email@seznam.cz')
                    ->setTelephone('603444888')
            ]
        ];
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function set(Contact|null $newValue): void
    {
        $model = new PartyContact();
        $model->setContact($newValue);
        $this->assertEquals($newValue, Reflection::getHiddenProperty($model, 'contact'));
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function getSet(Contact|null $newValue): void
    {
        $model = new PartyContact();
        $this->assertEquals($newValue, $model->setContact($newValue)->getContact());
    }
}
