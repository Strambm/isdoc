<?php

namespace Isdoc\Tests\Models\NonTaxedDeposit;

use Isdoc\Enums\CurrencyCode;
use Isdoc\Models\ExchangeRate;
use Isdoc\Models\NonTaxedDeposit;
use PHPUnit\Framework\TestCase;

class ToXmlElementTest extends TestCase
{
    /**
     * @test
     */
    public function onlyRequiredAttributes(): void
    {
        $model = new NonTaxedDeposit();
        $model->setId('OD-123')
            ->setVariableSymbol('123');
        $result = $model->toString();
        $structure = json_decode(json_encode(simplexml_load_string($result)), true);
        $this->assertCount(3, $structure);
        $this->assertEquals('OD-123', $structure['ID']);  
        $this->assertEquals('123', $structure['VariableSymbol']);  
        $this->assertEqualsWithDelta(0, $structure['DepositAmount'], 0.001); 

    }

    /**
     * @test
     */
    public function withNecessaryParameters(): void
    {
        $model = new NonTaxedDeposit();
        $model->setDepositAmount(100)
            ->setId('1234567')
            ->setVariableSymbol('1234567')
            ->setExchangeRate(new ExchangeRate(CurrencyCode::CZK, CurrencyCode::CNY, 1, 1));
        $result = $model->toString();
        $structure = json_decode(json_encode(simplexml_load_string($result)), true);
        $this->assertCount(4, $structure);
        $this->assertEquals($model->getVariableSymbol(), $structure['VariableSymbol']);  
        $this->assertEqualsWithDelta(100, $structure['DepositAmount'], 0.001); 
        $this->assertEqualsWithDelta(100, $structure['DepositAmountCurr'], 0.001); 
        $this->assertEquals($model->getId(), $structure['ID']);  
    }
}
