<?php

namespace Isdoc\Tests\Models\NonTaxedDeposit;

use Isdoc\Models\NonTaxedDeposit;
use PHPUnit\Framework\TestCase;
use Cetria\Helpers\Reflection\Reflection;

class GetSetDepositAmountTest extends TestCase
{
    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function get(float|null $newValue): void
    {
        $model = new NonTaxedDeposit();
        Reflection::setHiddenProperty($model, 'depositAmount', $newValue);
        $this->assertEquals($newValue, $model->getDepositAmount());
    }

    public static function getSetDataProvider(): array
    {
        return [
            [
                12356
            ], [
                12.256
            ]
        ];
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function set(float|null $newValue): void
    {
        $model = new NonTaxedDeposit();
        $model->setDepositAmount($newValue);
        $this->assertEquals($newValue, Reflection::getHiddenProperty($model, 'depositAmount'));
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function getSet(float|null $newValue): void
    {
        $model = new NonTaxedDeposit();
        $this->assertEquals($newValue, $model->setDepositAmount($newValue)->getDepositAmount());
    }
}
