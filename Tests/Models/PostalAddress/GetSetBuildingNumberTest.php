<?php

namespace Isdoc\Tests\Models\PostalAddress;

use Isdoc\Models\PostalAddress;
use PHPUnit\Framework\TestCase;
use Cetria\Helpers\Reflection\Reflection;

class GetSetBuildingNumberTest extends TestCase
{
    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function get(string $newValue): void
    {
        $model = new PostalAddress();
        Reflection::setHiddenProperty($model, 'buildingNumber', $newValue);
        $result = $model->getBuildingNumber();
        $this->assertEquals($newValue, $result);
    }

    public static function getSetDataProvider(): array
    {
        return [
            [
                '15'
            ], [
                ''
            ], [
                '85/4B'
            ]
        ];
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function set(string $newValue): void
    {
        $model = new PostalAddress();
        $model->setBuildingNumber($newValue);
        $result = Reflection::getHiddenProperty($model, 'buildingNumber');
        $this->assertEquals($newValue, $result);
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function getSet(string $newValue): void
    {
        $model = new PostalAddress();
        $result = $model->setBuildingNumber($newValue)->getBuildingNumber();
        $this->assertEquals($newValue, $result);
    }
}
