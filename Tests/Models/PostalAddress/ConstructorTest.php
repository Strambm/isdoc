<?php

namespace Isdoc\Tests\Models\PostalAddress;

use Isdoc\Models\PostalAddress;
use PHPUnit\Framework\TestCase;

class ConstructorTest extends TestCase
{
    /**
     * @test
     */
    public function basic(): void
    {
        $model = new PostalAddress();
        $this->assertEquals('CZ', $model->getCountry()->getIdentificationCode());
    }
}
