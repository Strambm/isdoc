<?php

namespace Isdoc\Tests\Models\PostalAddress;

use Isdoc\Models\PostalAddress;
use PHPUnit\Framework\TestCase;
use Cetria\Helpers\Reflection\Reflection;

class GetSetStreetNameTest extends TestCase
{
    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function get(string $newValue): void
    {
        $model = new PostalAddress();
        Reflection::setHiddenProperty($model, 'streetName', $newValue);
        $result = $model->getStreetName();
        $this->assertEquals($newValue, $result);
    }

    public static function getSetDataProvider(): array
    {
        return [
            [
                'Benešovo Nábřeží'
            ], [
                ''
            ], [
                'Liptál'
            ]
        ];
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function set(string $newValue): void
    {
        $model = new PostalAddress();
        $model->setStreetName($newValue);
        $result = Reflection::getHiddenProperty($model, 'streetName');
        $this->assertEquals($newValue, $result);
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function getSet(string $newValue): void
    {
        $model = new PostalAddress();
        $result = $model->setStreetName($newValue)->getStreetName();
        $this->assertEquals($newValue, $result);
    }
}
