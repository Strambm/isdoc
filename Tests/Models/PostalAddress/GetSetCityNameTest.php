<?php

namespace Isdoc\Tests\Models\PostalAddress;

use Isdoc\Models\PostalAddress;
use PHPUnit\Framework\TestCase;
use Cetria\Helpers\Reflection\Reflection;

class GetSetCityNameTest extends TestCase
{
    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function get(string $newValue): void
    {
        $model = new PostalAddress();
        Reflection::setHiddenProperty($model, 'cityName', $newValue);
        $result = $model->getCityName();
        $this->assertEquals($newValue, $result);
    }

    public static function getSetDataProvider(): array
    {
        return [
            [
                '15'
            ], [
                ''
            ], [
                '85/4B'
            ]
        ];
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function set(string $newValue): void
    {
        $model = new PostalAddress();
        $model->setCityName($newValue);
        $result = Reflection::getHiddenProperty($model, 'cityName');
        $this->assertEquals($newValue, $result);
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function getSet(string $newValue): void
    {
        $model = new PostalAddress();
        $result = $model->setCityName($newValue)->getCityName();
        $this->assertEquals($newValue, $result);
    }
}
