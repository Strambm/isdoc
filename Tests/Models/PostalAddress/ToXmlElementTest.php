<?php

namespace Isdoc\Tests\Models\PostalAddress;

use Isdoc\Enums\CountryCode;
use Isdoc\Exceptions\MissingSalesOrderId;
use Isdoc\Models\Country;
use Isdoc\Models\OrderReference;
use Isdoc\Models\PostalAddress;
use PHPUnit\Framework\TestCase;

class ToXmlElementTest extends TestCase
{
    /**
     * @test
     */
    public function empty(): void
    {
        $model = new PostalAddress();
        $result = $model->toString();
        $structure = json_decode(json_encode(simplexml_load_string($result)), true);
        $this->assertCount(5, $structure);
        $this->assertEquals([], $structure['StreetName']);
        $this->assertEquals([], $structure['BuildingNumber']);
        $this->assertEquals([], $structure['CityName']);
        $this->assertEquals([], $structure['PostalZone']);
        $this->assertEquals(CountryCode::CZ, $structure['Country']['IdentificationCode']);
    }

    /**
     * @test
     */
    public function withValues(): void
    {
        $city = 'Praha';
        $country = (new Country)->setIdentificationCode(CountryCode::CZ);
        $number = '256';
        $psc = '15644';
        $street = 'Testovací';
        $model = new PostalAddress();
        $model->setBuildingNumber($number)
            ->setCityName($city)
            ->setCountry($country)
            ->setPostalZone($psc)
            ->setStreetName($street);
        $result = $model->toString();
        $structure = json_decode(json_encode(simplexml_load_string($result)), true);
        $this->assertCount(5, $structure);
        $this->assertEquals($street, $structure['StreetName']);
        $this->assertEquals($number, $structure['BuildingNumber']);
        $this->assertEquals($city, $structure['CityName']);
        $this->assertEquals($psc, $structure['PostalZone']);
        $this->assertEquals(CountryCode::CZ, $structure['Country']['IdentificationCode']);
    }
}
