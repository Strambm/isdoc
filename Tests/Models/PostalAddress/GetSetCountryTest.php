<?php

namespace Isdoc\Tests\Models\PostalAddress;

use Isdoc\Enums\CountryCode;
use Isdoc\Models\Country;
use Isdoc\Models\PostalAddress;
use PHPUnit\Framework\TestCase;
use Cetria\Helpers\Reflection\Reflection;

class GetSetCountryTest extends TestCase
{
    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function get(Country $newValue): void
    {
        $model = new PostalAddress();
        Reflection::setHiddenProperty($model, 'country', $newValue);
        $result = $model->getCountry();
        $this->assertEquals($newValue, $result);
    }

    public static function getSetDataProvider(): array
    {
        return [
            [
                (new Country)
                    ->setIdentificationCode(CountryCode::CZ)
            ], [
                (new Country)
                    ->setIdentificationCode(CountryCode::SK)
            ],
        ];
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function set(Country $newValue): void
    {
        $model = new PostalAddress();
        $model->setCountry($newValue);
        $result = Reflection::getHiddenProperty($model, 'country');
        $this->assertEquals($newValue, $result);
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function getSet(Country $newValue): void
    {
        $model = new PostalAddress();
        $result = $model->setCountry($newValue)->getCountry();
        $this->assertEquals($newValue, $result);
    }
}
