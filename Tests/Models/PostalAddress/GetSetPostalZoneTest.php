<?php

namespace Isdoc\Tests\Models\PostalAddress;

use Isdoc\Models\PostalAddress;
use PHPUnit\Framework\TestCase;
use Cetria\Helpers\Reflection\Reflection;

class GetSetPostalZoneTest extends TestCase
{
    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function get(string $newValue): void
    {
        $model = new PostalAddress();
        Reflection::setHiddenProperty($model, 'postalZone', $newValue);
        $result = $model->getPostalZone();
        $this->assertEquals($newValue, $result);
    }

    public static function getSetDataProvider(): array
    {
        return [
            [
                '756 66'
            ], [
                '012345'
            ], [
                '12345'
            ]
        ];
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function set(string $newValue): void
    {
        $model = new PostalAddress();
        $model->setPostalZone($newValue);
        $result = Reflection::getHiddenProperty($model, 'postalZone');
        $this->assertEquals($newValue, $result);
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function getSet(string $newValue): void
    {
        $model = new PostalAddress();
        $result = $model->setPostalZone($newValue)->getPostalZone();
        $this->assertEquals($newValue, $result);
    }
}
