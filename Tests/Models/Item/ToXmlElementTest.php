<?php

namespace Isdoc\Tests\Models\Item;

use Isdoc\Models\Item;
use PHPUnit\Framework\TestCase;

class ToXmlElementTest extends TestCase
{
    /**
     * @test
     */
    public function onlyRequiredAttributes(): void
    {
        $model = new Item();
        $result = $model->toString();
        $structure = json_decode(json_encode(simplexml_load_string($result)), true);
        $this->assertCount(0, $structure);
    }

    /**
     * @test
     */
    public function withNecessaryParameters(): void
    {
        $description = 'description_';
        $id1 = '123456';
        $id2 = 'a123456';
        $id3 = '987654';
        $ean = '088698524824';
        $buyers = '123';
        $model = new Item();
        $model->setDescription($description)
            ->setSellersItemIdentification($id1)
            ->setSecondarySellersItemIdentification($id2)
            ->setTertiarySellersItemIdentification($id3)
            ->setCatalogueItemIdentification($ean)
            ->setBuyersItemIdentification($buyers);
        $result = $model->toString();
        $structure = json_decode(json_encode(simplexml_load_string($result)), true);
        $this->assertCount(6, $structure);
        $this->assertEquals($description, $structure['Description']);
        $this->assertEquals($id1, $structure['SellersItemIdentification']['ID']);  
        $this->assertEquals($id2, $structure['SecondarySellersItemIdentification']['ID']); 
        $this->assertEquals($id3, $structure['TertiarySellersItemIdentification']['ID']); 
        $this->assertEquals($buyers, $structure['BuyersItemIdentification']['ID']); 
        $this->assertEquals($ean, $structure['CatalogueItemIdentification']['ID']);   
    }
}
