<?php

namespace Isdoc\Tests\Models\Item;

use Isdoc\Models\Item;
use PHPUnit\Framework\TestCase;
use Cetria\Helpers\Reflection\Reflection;

class GetSetSecondarySellersItemIdentificationTest extends TestCase
{
    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function get(string|null $newValue): void
    {
        $model = new Item();
        Reflection::setHiddenProperty($model, 'secondarySellersItemIdentification', $newValue);
        $this->assertEquals($newValue, $model->getSecondarySellersItemIdentification());
    }

    public static function getSetDataProvider(): array
    {
        return [
            [
                'DG014HPNG0'
            ], [
                'DO052HPNG0'
            ], [
                null
            ]
        ];
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function set(string|null $newValue): void
    {
        $model = $model = new Item();
        $model->setSecondarySellersItemIdentification($newValue);
        $this->assertEquals($newValue, Reflection::getHiddenProperty($model, 'secondarySellersItemIdentification'));
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function getSet(string|null $newValue): void
    {
        $model = $model = new Item();
        $this->assertEquals($newValue, $model->setSecondarySellersItemIdentification($newValue)->getSecondarySellersItemIdentification());
    }
}
