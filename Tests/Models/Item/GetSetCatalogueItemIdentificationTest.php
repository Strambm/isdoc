<?php

namespace Isdoc\Tests\Models\Item;

use Isdoc\Models\Item;
use PHPUnit\Framework\TestCase;
use Cetria\Helpers\Reflection\Reflection;

class GetSetCatalogueItemIdentificationTest extends TestCase
{
    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function get(string|null $newValue): void
    {
        $model = new Item();
        Reflection::setHiddenProperty($model, 'catalogueItemIdentification', $newValue);
        $this->assertEquals($newValue, $model->getCatalogueItemIdentification());
    }

    public static function getSetDataProvider(): array
    {
        return [
            [
                '6938623920587'
            ], [
                '194850740626'
            ], [
                '9991811131520'
            ], [
                null
            ]
        ];
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function set(string|null $newValue): void
    {
        $model = $model = new Item();
        $model->setCatalogueItemIdentification($newValue);
        $this->assertEquals($newValue, Reflection::getHiddenProperty($model, 'catalogueItemIdentification'));
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function getSet(string|null $newValue): void
    {
        $model = $model = new Item();
        $this->assertEquals($newValue, $model->setCatalogueItemIdentification($newValue)->getCatalogueItemIdentification());
    }
}
