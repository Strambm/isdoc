<?php

namespace Isdoc\Tests\Models\Item;

use Isdoc\Models\Item;
use PHPUnit\Framework\TestCase;
use Cetria\Helpers\Reflection\Reflection;

class GetSetDescriptionTest extends TestCase
{
    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function get(string|null $newValue): void
    {
        $model = new Item();
        Reflection::setHiddenProperty($model, 'description', $newValue);
        $this->assertEquals($newValue, $model->getDescription());
    }

    public static function getSetDataProvider(): array
    {
        return [
            [
                'Židle kancelářská FollowMe 451-SYQ-N6, područky BR-795-N6, kříž hliník leštěný, kolečka na tvrdé podlahy, 64092 červená'
            ], [
                'Koš odpadkový Leitz WOW, plastový, 15 l, červený'
            ], [
                'Taška papírová Natur, dárková vánoční, 31x12x41,5 cm, design 0073'
            ], [
                null
            ]
        ];
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function set(string|null $newValue): void
    {
        $model = $model = new Item();
        $model->setDescription($newValue);
        $this->assertEquals($newValue, Reflection::getHiddenProperty($model, 'description'));
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function getSet(string|null $newValue): void
    {
        $model = $model = new Item();
        $this->assertEquals($newValue, $model->setDescription($newValue)->getDescription());
    }
}
