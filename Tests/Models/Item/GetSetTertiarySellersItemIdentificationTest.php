<?php

namespace Isdoc\Tests\Models\Item;

use Isdoc\Models\Item;
use PHPUnit\Framework\TestCase;
use Cetria\Helpers\Reflection\Reflection;

class GetSetTertiarySellersItemIdentificationTest extends TestCase
{
    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function get(string|null $newValue): void
    {
        $model = new Item();
        Reflection::setHiddenProperty($model, 'tertiarySellersItemIdentification', $newValue);
        $this->assertEquals($newValue, $model->getTertiarySellersItemIdentification());
    }

    public static function getSetDataProvider(): array
    {
        return [
            [
                'DG014HPNG0'
            ], [
                'DO052HPNG0'
            ], [
                null
            ]
        ];
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function set(string|null $newValue): void
    {
        $model = $model = new Item();
        $model->setTertiarySellersItemIdentification($newValue);
        $this->assertEquals($newValue, Reflection::getHiddenProperty($model, 'tertiarySellersItemIdentification'));
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function getSet(string|null $newValue): void
    {
        $model = $model = new Item();
        $this->assertEquals($newValue, $model->setTertiarySellersItemIdentification($newValue)->getTertiarySellersItemIdentification());
    }
}
