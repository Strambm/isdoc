<?php

namespace Isdoc\Tests\Models\InvoiceLine;

class GetSetOrderReferenceTest extends GetSetReferenceTestCase
{
    protected function getAttributeName(): string
    {
        return 'orderReference';
    }
}
