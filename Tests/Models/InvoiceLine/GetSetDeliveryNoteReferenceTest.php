<?php

namespace Isdoc\Tests\Models\InvoiceLine;

class GetSetDeliveryNoteReferenceTest extends GetSetReferenceTestCase
{
    protected function getAttributeName(): string
    {
        return 'deliveryNoteReference';
    }
}
