<?php

namespace Isdoc\Tests\Models\InvoiceLine;

use Isdoc\Models\Item;
use Isdoc\Models\InvoiceLine;
use PHPUnit\Framework\TestCase;
use Cetria\Helpers\Reflection\Reflection;

class GetSetItemTest extends TestCase
{
    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function get(Item|null $newValue): void
    {
        $model = new InvoiceLine();
        Reflection::setHiddenProperty($model, 'item', $newValue);
        $result = $model->getItem();
        $this->compareResults($newValue, $result);
    }

    public static function getSetDataProvider(): array
    {
        return [
            [
                (new Item)
            ], [
                (new Item)
                    ->setSellersItemIdentification('XD156')
            ], [
                null
            ]
        ];
    }

    private function compareResults(Item|null $expected, Item|null $result): void
    {
        if(is_null($expected)) {
            $this->assertEquals($expected, $result);
        } else {
            $this->assertEquals($expected->toString(), $result->toString());
        }
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function set(Item|null $newValue): void
    {
        $model = new InvoiceLine();
        $model->setItem($newValue);
        $result = Reflection::getHiddenProperty($model, 'item');
        $this->compareResults($newValue, $result);
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function getSet(Item|null $newValue): void
    {
        $model = new InvoiceLine();
        $result = $model->setItem($newValue)->getItem();
        $this->compareResults($newValue, $result);
    }
}
