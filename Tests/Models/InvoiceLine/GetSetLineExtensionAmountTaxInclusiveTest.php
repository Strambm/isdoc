<?php

namespace Isdoc\Tests\Models\InvoiceLine;

use Isdoc\Models\InvoiceLine;
use PHPUnit\Framework\TestCase;
use Isdoc\Enums\VatCalculationMethod;
use Isdoc\Models\ClassifiedTaxCategory;
use Cetria\Helpers\Reflection\Reflection;
use PHPUnit\Framework\MockObject\MockObject;

class GetSetLineExtensionAmountTaxInclusiveTest extends TestCase
{
    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function get(float|null $newValue): void
    {
        $model = new InvoiceLine();
        Reflection::setHiddenProperty($model, 'lineExtensionAmountTaxInclusive', $newValue);
        $this->assertEquals($newValue, $model->getLineExtensionAmountTaxInclusive());
    }

    private function getMockedModel(float $vatPercent, int $vatCalculationMethod, float|null $lineExtensionAmount): InvoiceLine|MockObject
    {
        $classifiedTaxCategory = (new ClassifiedTaxCategory())
            ->setPercent($vatPercent)
            ->setVatCalculationMethod($vatCalculationMethod);

        $mock = $this->getMockBuilder(InvoiceLine::class)
            ->onlyMethods([
                'getClassifiedTaxCategory',
                'getLineExtensionAmount',
            ])
            ->getMock();
        $mock->expects($this->any())
            ->method('getClassifiedTaxCategory')
            ->willReturn($classifiedTaxCategory);

        $mock->expects($this->any())
            ->method('getLineExtensionAmount')
            ->willReturn($lineExtensionAmount);

        return $mock;
    }

    public static function getSetDataProvider(): array
    {
        return [
            [
                12356
            ], [
                12.256
            ]
        ];
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function set(float|null $newValue): void
    {
        $model = new InvoiceLine();
        $model->setLineExtensionAmountTaxInclusive($newValue);
        $this->assertEquals($newValue, Reflection::getHiddenProperty($model, 'lineExtensionAmountTaxInclusive'));
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function getSet(float|null $newValue): void
    {
        $model = new InvoiceLine();
        $this->assertEquals($newValue, $model->setLineExtensionAmountTaxInclusive($newValue)->getLineExtensionAmountTaxInclusive());
    }

    /**
     * @test
     */
    public function nullFromDown(): void
    {
        $model = $this->getMockedModel(10, VatCalculationMethod::ZDOLA, 50);
        $this->assertEqualsWithDelta(50 * 1.1 , $model->getLineExtensionAmountTaxInclusive(), 0.001);
    }

    /**
     * @test
     */
    public function nullFromUp(): void
    {
        $model = $this->getMockedModel(10, VatCalculationMethod::SHORA, 50);
        $this->assertEquals(0, $model->getLineExtensionAmountTaxInclusive());
    }

    /**
     * @test
     */
    public function settedValueFromDown(): void
    {
        $model = $this->getMockedModel(10, VatCalculationMethod::ZDOLA, 50);
        Reflection::setHiddenProperty($model, 'lineExtensionAmountTaxInclusive', 11);
        $this->assertEquals(11, $model->getLineExtensionAmountTaxInclusive());
    }
}
