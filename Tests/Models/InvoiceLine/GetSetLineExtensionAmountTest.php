<?php

namespace Isdoc\Tests\Models\InvoiceLine;

use Isdoc\Models\InvoiceLine;
use PHPUnit\Framework\TestCase;
use Isdoc\Enums\VatCalculationMethod;
use Isdoc\Models\ClassifiedTaxCategory;
use Cetria\Helpers\Reflection\Reflection;
use PHPUnit\Framework\MockObject\MockObject;

class GetSetLineExtensionAmountTest extends TestCase
{
    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function get(float|null $newValue): void
    {
        $model = new InvoiceLine();
        Reflection::setHiddenProperty($model, 'lineExtensionAmount', $newValue);
        $this->assertEquals($newValue, $model->getLineExtensionAmount());
    }

    private function getMockedModel(float $vatPercent, int $vatCalculationMethod, float|null $lineExtensionAmountTaxInclusive): InvoiceLine|MockObject
    {
        $classifiedTaxCategory = (new ClassifiedTaxCategory())
            ->setPercent($vatPercent)
            ->setVatCalculationMethod($vatCalculationMethod);

        $mock = $this->getMockBuilder(InvoiceLine::class)
            ->onlyMethods([
                'getClassifiedTaxCategory',
                'getLineExtensionAmountTaxInclusive',
            ])
            ->getMock();
        $mock->expects($this->any())
            ->method('getClassifiedTaxCategory')
            ->willReturn($classifiedTaxCategory);

        $mock->expects($this->any())
            ->method('getLineExtensionAmountTaxInclusive')
            ->willReturn($lineExtensionAmountTaxInclusive);

        return $mock;
    }

    public static function getSetDataProvider(): array
    {
        return [
            [
                12356
            ], [
                12.256
            ]
        ];
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function set(float|null $newValue): void
    {
        $model = new InvoiceLine();
        $model->setLineExtensionAmount($newValue);
        $this->assertEquals($newValue, Reflection::getHiddenProperty($model, 'lineExtensionAmount'));
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function getSet(float|null $newValue): void
    {
        $model = new InvoiceLine();
        $this->assertEquals($newValue, $model->setLineExtensionAmount($newValue)->getLineExtensionAmount());
    }

    /**
     * @test
     */
    public function nullFromDown(): void
    {
        $model = $this->getMockedModel(10, VatCalculationMethod::ZDOLA, 50);
        $this->assertEquals(0, $model->getLineExtensionAmount());
    }

    /**
     * @test
     */
    public function nullFromUp(): void
    {
        $model = $this->getMockedModel(10, VatCalculationMethod::SHORA, 50);
        $this->assertEquals(50/1.1, $model->getLineExtensionAmount());
    }

    /**
     * @test
     */
    public function settedValueFromUp(): void
    {
        $model = $this->getMockedModel(10, VatCalculationMethod::SHORA, 50);
        Reflection::setHiddenProperty($model, 'lineExtensionAmount', 11);
        $this->assertEquals(11, $model->getLineExtensionAmount());
    }
}
