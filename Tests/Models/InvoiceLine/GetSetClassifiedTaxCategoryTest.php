<?php

namespace Isdoc\Tests\Models\InvoiceLine;

use Isdoc\Enums\VatCalculationMethod;
use Isdoc\Exceptions\MissingClassifiedTaxCategory;
use Isdoc\Models\ClassifiedTaxCategory;
use Isdoc\Models\InvoiceLine;
use PHPUnit\Framework\TestCase;
use Cetria\Helpers\Reflection\Reflection;

class GetSetClassifiedTaxCategoryTest extends TestCase
{
    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function get(ClassifiedTaxCategory $newValue): void
    {
        $model = new InvoiceLine();
        Reflection::setHiddenProperty($model, 'classifiedTaxCategory', $newValue);
        $result = $model->getClassifiedTaxCategory();
        $this->assertEquals($newValue, $result);
    }

    public static function getSetDataProvider(): array
    {
        return [
            [
                (new ClassifiedTaxCategory)
                    ->setPercent(0)
                    ->setVatCalculationMethod(VatCalculationMethod::SHORA)
            ], [
                (new ClassifiedTaxCategory)
                    ->setPercent(12.5)
                    ->setVatCalculationMethod(VatCalculationMethod::ZDOLA)
                    ->setVatApplicable(true)
                    ->setLocalReverseCharge(false)
            ]
        ];
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function set(ClassifiedTaxCategory $newValue): void
    {
        $model = new InvoiceLine();
        $model->setClassifiedTaxCategory($newValue);
        $result = Reflection::getHiddenProperty($model, 'classifiedTaxCategory');
        $this->assertEquals($newValue, $result);
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function getSet(ClassifiedTaxCategory $newValue): void
    {
        $model = new InvoiceLine();
        $result = $model->setClassifiedTaxCategory($newValue)->getClassifiedTaxCategory();
        $this->assertEquals($newValue, $result);
    }

    /**
     * @test
     */
    public function withoutValue(): void
    {
        $this->expectException(MissingClassifiedTaxCategory::class);
        $model = new InvoiceLine();
        $model->getClassifiedTaxCategory();
    }
}
