<?php

namespace Isdoc\Tests\Models\InvoiceLine;

use Isdoc\Models\InvoiceLine;
use PHPUnit\Framework\TestCase;
use Isdoc\Enums\VatCalculationMethod;
use Isdoc\Models\ClassifiedTaxCategory;
use Cetria\Helpers\Reflection\Reflection;

class GetSetLineExtensionAmountTaxInclusiveBeforeDiscountTest extends TestCase
{
    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function get(float|null $newValue): void
    {
        $model = new InvoiceLine();
        Reflection::setHiddenProperty($model, 'lineExtensionAmountTaxInclusiveBeforeDiscount', $newValue);
        $this->assertEquals($newValue, $model->getLineExtensionAmountTaxInclusiveBeforeDiscount());
    }

    public static function getSetDataProvider(): array
    {
        return [
            [
                12356
            ], [
                12.256
            ]
        ];
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function set(float|null $newValue): void
    {
        $model = new InvoiceLine();
        $model->setLineExtensionAmountTaxInclusiveBeforeDiscount($newValue);
        $this->assertEquals($newValue, Reflection::getHiddenProperty($model, 'lineExtensionAmountTaxInclusiveBeforeDiscount'));
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function getSet(float|null $newValue): void
    {
        $model = new InvoiceLine();
        $this->assertEquals($newValue, $model->setLineExtensionAmountTaxInclusiveBeforeDiscount($newValue)->getLineExtensionAmountTaxInclusiveBeforeDiscount());
    }

    /**
     * @test
     */
    public function nullFromDown(): void
    {
        $model = $this->getMockedModel(10, VatCalculationMethod::ZDOLA, 50);
        $this->assertEqualsWithDelta(50 * 1.1, $model->getLineExtensionAmountTaxInclusiveBeforeDiscount(), 0.001);
    }

    private function getMockedModel(float $vatPercent, int $vatCalculationMethod, float|null $lineExtensionAmount): InvoiceLine|MockObject
    {
        $classifiedTaxCategory = (new ClassifiedTaxCategory())
            ->setPercent($vatPercent)
            ->setVatCalculationMethod($vatCalculationMethod);

        $mock = $this->getMockBuilder(InvoiceLine::class)
            ->onlyMethods([
                'getClassifiedTaxCategory',
                'getLineExtensionAmountBeforeDiscount',
            ])
            ->getMock();
        $mock->expects($this->any())
            ->method('getClassifiedTaxCategory')
            ->willReturn($classifiedTaxCategory);

        $mock->expects($this->any())
            ->method('getLineExtensionAmountBeforeDiscount')
            ->willReturn($lineExtensionAmount);

        return $mock;
    }

    /**
     * @test
     */
    public function nullFromUp(): void
    {
        $model = $this->getMockedModel(10, VatCalculationMethod::SHORA, 50);
        $this->assertNull($model->getLineExtensionAmountTaxInclusiveBeforeDiscount());
    }
}
