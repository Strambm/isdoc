<?php

namespace Isdoc\Tests\Models\InvoiceLine;

use Isdoc\Models\InvoiceLine;
use Isdoc\Models\InvoiceLineReference;
use PHPUnit\Framework\TestCase;
use Cetria\Helpers\Reflection\Reflection;

abstract class GetSetReferenceTestCase extends TestCase
{
    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function get(InvoiceLineReference|null $newValue): void
    {
        $model = new InvoiceLine();
        Reflection::setHiddenProperty($model, $this->getAttributeName(), $newValue);
        $getMethodName = $this->getMethodName('get');
        $result = $model->$getMethodName();
        $this->compareResults($newValue, $result);
    }

    protected abstract function getAttributeName(): string;

    protected function getMethodName(string $prefix): string
    {
        $attributeName = $this->getAttributeName();
        $methodName = $prefix . strtoupper($this->getAttributeName()[0]) . substr($attributeName, 1);
        return $methodName;
    }

    public static function getSetDataProvider(): array
    {
        return [
            [
                new InvoiceLineReference('TD-123', '123-123')
            ], [
                null
            ]
        ];
    }

    private function compareResults(InvoiceLineReference|null $expected, InvoiceLineReference|null $result): void
    {
        if(is_null($expected)) {
            $this->assertEquals($expected, $result);
        } else {
            $this->assertEquals($expected->getLineId(), $result->getLineId());
            $this->assertEquals($expected->getRef(), $result->getRef());
        }
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function set(InvoiceLineReference|null $newValue): void
    {
        $model = new InvoiceLine();
        $setMethodName = $this->getMethodName('set');
        $model->$setMethodName($newValue);
        $result = Reflection::getHiddenProperty($model, $this->getAttributeName());
        $this->compareResults($newValue, $result);
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function getSet(InvoiceLineReference|null $newValue): void
    {
        $model = new InvoiceLine();
        $setMethodName = $this->getMethodName('set');
        $getMethodName = $this->getMethodName('get');
        $result = $model->$setMethodName($newValue)->$getMethodName();
        $this->compareResults($newValue, $result);
    }
}
