<?php

namespace Isdoc\Tests\Models\InvoiceLine;

use Isdoc\Models\InvoiceLineReference;
use Isdoc\Models\Item;
use Isdoc\Models\Note;
use Isdoc\Models\VatNote;
use Isdoc\Enums\CurrencyCode;
use Isdoc\Enums\LanguageCode;
use Isdoc\Models\InvoiceLine;
use Isdoc\Models\ExchangeRate;
use PHPUnit\Framework\TestCase;
use Isdoc\Enums\VatCalculationMethod;
use Isdoc\Models\ClassifiedTaxCategory;

class ToXmlElementTest extends TestCase
{
    /**
     * @test
     */
    public function onlyRequiredAttributes(): void
    {
        $id = '123';
        $classificationVatCategory = (new ClassifiedTaxCategory())
            ->setPercent(0)
            ->setVatCalculationMethod(VatCalculationMethod::SHORA);
        $price = 100.55;

        $model = new InvoiceLine();
        $model->setId($id)
            ->setClassifiedTaxCategory($classificationVatCategory)
            ->setLineExtensionAmountTaxInclusive($price);
        $result = $model->toString();
        $structure = json_decode(json_encode(simplexml_load_string($result)), true);
        $this->assertCount(7, $structure);
        $this->assertEquals($id, $structure['ID']);
        $this->assertEquals($price, $structure['LineExtensionAmount']);
        $this->assertEquals($price, $structure['LineExtensionAmountTaxInclusive']);
        $this->assertEquals(0, $structure['LineExtensionTaxAmount']);
        $this->assertEquals($price, $structure['UnitPrice']);
        $this->assertEquals($price, $structure['UnitPriceTaxInclusive']);
        $this->assertEquals($classificationVatCategory->getPercent(), $structure['ClassifiedTaxCategory']['Percent']);
        $this->assertEquals($classificationVatCategory->getVatCalculationMethod(), $structure['ClassifiedTaxCategory']['VATCalculationMethod']);
    }

    /**
     * @test
     */
    public function withNecessaryParameters(): void
    {
        $id = '1234';
        $classificationVatCategory = (new ClassifiedTaxCategory())
            ->setPercent(21)
            ->setVatCalculationMethod(VatCalculationMethod::SHORA);
        $price = 100.55;
        $exchangeRate = new ExchangeRate(CurrencyCode::CZK, CurrencyCode::EUR, 24, 2);
        $amount = 10;
        $priceBeforeDiscount = 150;
        $orderReferenceNote = new InvoiceLineReference('ORN-123', '303030');
        $deliveryReferenceNote = new InvoiceLineReference('DRN-123', '101010');
        $originalDocumentReference = new InvoiceLineReference('ODR-123', '202020');
        $unit = 'ks';
        $note = (new Note())
            ->setLanguageId(LanguageCode::CS)
            ->setValue('Červané proedení');
        $vatNote = (new VatNote())
            ->setLanguageId(LanguageCode::CS)
            ->setValue('Není důvod k této zprávě');
        $item = (new Item())
            ->setDescription('Název produktu')
            ->setCatalogueItemIdentification('123456')
            ->setSellersItemIdentification('ID123456');
        $model = (new InvoiceLine())
            ->setClassifiedTaxCategory($classificationVatCategory)
            ->setDeliveryNoteReference($deliveryReferenceNote)
            ->setId($id)
            ->setInvoicedQuantity($amount)
            ->setInvoiceQuantityUnitCode($unit)
            ->setItem($item)
            ->setLineExtensionAmountTaxInclusive($price)
            ->setLineExtensionAmountTaxInclusiveBeforeDiscount($priceBeforeDiscount)
            ->setNote($note)
            ->setOrderReference($orderReferenceNote)
            ->setOriginalDocumentReference($originalDocumentReference)
            ->setVatNote($vatNote)
            ->setExchangeRate($exchangeRate);
        $result = $model->toString();
        $structure = json_decode(json_encode(simplexml_load_string($result)), true);
        $this->assertCount(18, $structure);
        $this->assertEquals($id, $structure['ID']);
        $this->assertEquals($orderReferenceNote->getLineId(), $structure['OrderReference']['LineID']);
        $this->assertEquals($orderReferenceNote->getRef(), $structure['OrderReference']['@attributes']['ref']);
        $this->assertEquals($deliveryReferenceNote->getLineId(), $structure['DeliveryNoteReference']['LineID']);
        $this->assertEquals($deliveryReferenceNote->getRef(), $structure['DeliveryNoteReference']['@attributes']['ref']);
        $this->assertEquals($originalDocumentReference->getLineId(), $structure['OriginalDocumentReference']['LineID']);
        $this->assertEquals($originalDocumentReference->getRef(), $structure['OriginalDocumentReference']['@attributes']['ref']);
        $this->assertEquals($amount, $structure['InvoicedQuantity']);
        $this->assertEqualsWithDelta($price / 1.21 / 24, $structure['LineExtensionAmountCurr'], 0.001);
        $this->assertEqualsWithDelta($price / 1.21, $structure['LineExtensionAmount'], 0.001);
        $this->assertEqualsWithDelta($priceBeforeDiscount / 1.21, $structure['LineExtensionAmountBeforeDiscount'], 0.001);
        $this->assertEqualsWithDelta($price / 24, $structure['LineExtensionAmountTaxInclusiveCurr'], 0.001);
        $this->assertEquals($price, $structure['LineExtensionAmountTaxInclusive']);
        $this->assertEquals($priceBeforeDiscount, $structure['LineExtensionAmountTaxInclusiveBeforeDiscount']);
        $this->assertEqualsWithDelta($price - ($price / 1.21), $structure['LineExtensionTaxAmount'], 0.001);
        $this->assertEqualsWithDelta($price / 1.21 / $amount, $structure['UnitPrice'], 0.001);
        $this->assertEqualsWithDelta($price / $amount, $structure['UnitPriceTaxInclusive'], 0.001);
        $this->assertEquals($classificationVatCategory->getPercent(), $structure['ClassifiedTaxCategory']['Percent']);
        $this->assertEquals($classificationVatCategory->getVatCalculationMethod(), $structure['ClassifiedTaxCategory']['VATCalculationMethod']);
        $this->assertEquals($note->getValue(), $structure['Note']);
        $this->assertEquals($vatNote->getValue(), $structure['VATNote']);
        $this->assertEquals($item->getDescription(), $structure['Item']['Description']);
        $this->assertEquals($item->getCatalogueItemIdentification(), $structure['Item']['CatalogueItemIdentification']['ID']);
        $this->assertEquals($item->getSellersItemIdentification(), $structure['Item']['SellersItemIdentification']['ID']);
    }
}
