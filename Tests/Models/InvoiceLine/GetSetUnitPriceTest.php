<?php

namespace Isdoc\Tests\Models\InvoiceLine;

use Isdoc\Models\InvoiceLine;
use PHPUnit\Framework\TestCase;
use Cetria\Helpers\Reflection\Reflection;
use PHPUnit\Framework\MockObject\MockObject;

class GetSetUnitPriceTest extends TestCase
{
    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function get(float $newValue): void
    {
        $model = new InvoiceLine();
        Reflection::setHiddenProperty($model, 'unitPrice', $newValue);
        $result = $model->getUnitPrice();
        $this->assertEquals($newValue, $result);
    }

    public static function getSetDataProvider(): array
    {
        return [
            [
                12
            ], [
                1.234
            ]
        ];
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function set(float $newValue): void
    {
        $model = new InvoiceLine();
        $model->setUnitPrice($newValue);
        $result = Reflection::getHiddenProperty($model, 'unitPrice');
        $this->assertEquals($newValue, $result);
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function getSet(float $newValue): void
    {
        $model = new InvoiceLine();
        $result = $model->setUnitPrice($newValue)->getUnitPrice();
        $this->assertEquals($newValue, $result);
    }

    /**
     * @test
     * @dataProvider automaticsDataProvider
     */
    public function automatics(float $total, float $pcs): void
    {
        $object = $this->getMockObject($total, $pcs);
        $this->assertEqualsWithDelta($total / $pcs, $object->getUnitPrice(), 0.001);
    }

    public static function automaticsDataProvider(): array
    {
        return [
            [
                1000,
                10
            ], [
                15,
                1.5
            ], [
                0.15,
                0.15
            ]
        ];
    }

    private function getMockObject(float $total, float|null $pcs): InvoiceLine|MockObject
    {
        $object = $this->getMockBuilder(InvoiceLine::class)
            ->onlyMethods([
                'getInvoicedQuantity',
                'getLineExtensionAmount',
            ])
            ->getMock();
        
        $object->expects($this->any())
            ->method('getInvoicedQuantity')
            ->willReturn($pcs);

        $object->expects($this->any())
            ->method('getLineExtensionAmount')
            ->willReturn($total);

        return $object;
    }

    /**
     * @test
     */
    public function withZeroPcs(): void
    {
        $object = $this->getMockObject(11, null);
        $this->assertEquals(11, $object->getUnitPrice());
    }
}
