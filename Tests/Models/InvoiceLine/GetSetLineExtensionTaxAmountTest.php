<?php

namespace Isdoc\Tests\Models\InvoiceLine;

use Isdoc\Models\InvoiceLine;
use PHPUnit\Framework\TestCase;
use Isdoc\Enums\VatCalculationMethod;
use Isdoc\Models\ClassifiedTaxCategory;
use Cetria\Helpers\Reflection\Reflection;
use PHPUnit\Framework\MockObject\MockObject;

class GetSetLineExtensionTaxAmountTest extends TestCase
{
    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function get(float|null $newValue): void
    {
        $model = new InvoiceLine();
        Reflection::setHiddenProperty($model, 'lineExtensionTaxAmount', $newValue);
        $this->assertEquals($newValue, $model->getLineExtensionTaxAmount());
    }

    private function getMockedModel(float $vatPercent, int $vatCalculationMethod, float|null $lineExtensionAmountTaxInclusive): InvoiceLine|MockObject
    {
        $classifiedTaxCategory = (new ClassifiedTaxCategory())
            ->setPercent($vatPercent)
            ->setVatCalculationMethod($vatCalculationMethod);

        $mock = $this->getMockBuilder(InvoiceLine::class)
            ->onlyMethods([
                'getClassifiedTaxCategory',
                'getLineExtensionAmountTaxInclusive',
                'getLineExtensionAmount',
            ])
            ->getMock();
        $mock->expects($this->any())
            ->method('getClassifiedTaxCategory')
            ->willReturn($classifiedTaxCategory);

        $mock->expects($this->any())
            ->method('getLineExtensionAmountTaxInclusive')
            ->willReturn($lineExtensionAmountTaxInclusive);

        $mock->expects($this->any())
            ->method('getLineExtensionAmount')
            ->willReturn($lineExtensionAmountTaxInclusive);

        return $mock;
    }

    public static function getSetDataProvider(): array
    {
        return [
            [
                12356
            ], [
                12.256
            ]
        ];
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function set(float|null $newValue): void
    {
        $model = new InvoiceLine();
        $model->setLineExtensionTaxAmount($newValue);
        $this->assertEquals($newValue, Reflection::getHiddenProperty($model, 'lineExtensionTaxAmount'));
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function getSet(float|null $newValue): void
    {
        $model = new InvoiceLine();
        $this->assertEquals($newValue, $model->setLineExtensionTaxAmount($newValue)->getLineExtensionTaxAmount());
    }

    /**
     * @test
     */
    public function nullFromDown(): void
    {
        $model = $this->getMockedModel(10, VatCalculationMethod::ZDOLA, 50);
        $this->assertEqualsWithDelta(50 * 0.1, $model->getLineExtensionTaxAmount(), 0.001);
    }

    /**
     * @test
     */
    public function nullFromUp(): void
    {
        $model = $this->getMockedModel(10, VatCalculationMethod::SHORA, 50);
        $this->assertEqualsWithDelta(50 - (50 / 1.1), $model->getLineExtensionTaxAmount(), 0.001);
    }

    /**
     * @test
     */
    public function settedValueFromUp(): void
    {
        $model = $this->getMockedModel(10, VatCalculationMethod::SHORA, 50);
        Reflection::setHiddenProperty($model, 'lineExtensionTaxAmount', 11);
        $this->assertEqualsWithDelta(11, $model->getLineExtensionTaxAmount(), 0.001);
    }
}
