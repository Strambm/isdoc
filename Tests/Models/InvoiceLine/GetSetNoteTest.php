<?php

namespace Isdoc\Tests\Models\InvoiceLine;

use Isdoc\Enums\LanguageCode;
use Isdoc\Models\InvoiceLine;
use Isdoc\Models\Note;
use PHPUnit\Framework\TestCase;
use Cetria\Helpers\Reflection\Reflection;

class GetSetNoteTest extends TestCase
{
    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function get(Note|null $newValue): void
    {
        
        $model = new InvoiceLine();
        Reflection::setHiddenProperty($model, 'note', $newValue);
        $this->assertEquals($newValue, $model->getNote());
    }

    public static function getSetDataProvider(): array
    {
        return [
            [
                null
            ], [
                (new Note())
                    ->setLanguageId(LanguageCode::CS)
                    ->setValue('Poznámka v českém jazyce')
            ],
        ];
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function set(Note|null $newValue): void
    {
        $model = new InvoiceLine();
        $model->setNote($newValue);
        $this->assertEquals($newValue, Reflection::getHiddenProperty($model, 'note'));
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function getSet(Note|null $newValue): void
    {
        $model = new InvoiceLine();
        $this->assertEquals($newValue, $model->setNote($newValue)->getNote());
    }
}
