<?php

namespace Isdoc\Tests\Models\InvoiceLine;

use Isdoc\Exceptions\InvalidLineIdLength;
use Isdoc\Models\InvoiceLine;
use PHPUnit\Framework\TestCase;
use Cetria\Helpers\Reflection\Reflection;

class GetSetIdTest extends TestCase
{
    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function get(string $newValue): void
    {
        $model = new InvoiceLine();
        Reflection::setHiddenProperty($model, 'id', $newValue);
        $this->assertEquals($newValue, $model->getId());
    }

    public static function getSetDataProvider(): array
    {
        return [
            [
                ''
            ], [
                '123456'
            ], [
                'X123456'
            ]
        ];
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function set(string $newValue): void
    {
        $model = new InvoiceLine();
        $model->setId($newValue);
        $this->assertEquals($newValue, Reflection::getHiddenProperty($model, 'id'));
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function getSet(string $newValue): void
    {
        $model = new InvoiceLine();
        $this->assertEquals($newValue, $model->setId($newValue)->getId());
    }

    /**
     * @test
     */
    public function outOfRange(): void
    {
        $this->expectException(InvalidLineIdLength::class);
        $model = new InvoiceLine();
        $model->setId('1234567890123456789012345678901234567');
    }
}
