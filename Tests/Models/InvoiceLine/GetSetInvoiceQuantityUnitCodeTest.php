<?php

namespace Isdoc\Tests\Models\InvoiceLine;

use Isdoc\Models\InvoiceLine;
use PHPUnit\Framework\TestCase;
use Cetria\Helpers\Reflection\Reflection;

class GetSetInvoiceQuantityUnitCodeTest extends TestCase
{
    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function get(string|null $newValue): void
    {
        $model = new InvoiceLine();
        Reflection::setHiddenProperty($model, 'invoicedQuantityUnitCode', $newValue);
        $this->assertEquals($newValue, $model->getInvoiceQuantityUnitCode());
    }

    public static function getSetDataProvider(): array
    {
        return [
            [
                null
            ], [
                'ks'
            ], [
                ' '
            ], [
                'l'
            ], [
                '1'
            ]
        ];
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function set(string|null $newValue): void
    {
        $model = new InvoiceLine();
        $model->setInvoiceQuantityUnitCode($newValue);
        $this->assertEquals($newValue, Reflection::getHiddenProperty($model, 'invoicedQuantityUnitCode'));
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function getSet(string|null $newValue): void
    {
        $model = new InvoiceLine();
        $this->assertEquals($newValue, $model->setInvoiceQuantityUnitCode($newValue)->getInvoiceQuantityUnitCode());
    }
}
