<?php

namespace Isdoc\Tests\Models\InvoiceLine;

use Isdoc\Enums\LanguageCode;
use Isdoc\Models\InvoiceLine;
use Isdoc\Models\VatNote;
use PHPUnit\Framework\TestCase;
use Cetria\Helpers\Reflection\Reflection;

class GetSetVatNoteTest extends TestCase
{
    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function get(VatNote|null $newValue): void
    {
        $model = new InvoiceLine();
        Reflection::setHiddenProperty($model, 'vatNote', $newValue);
        $this->assertEquals($newValue, $model->getVatNote());
    }

    public static function getSetDataProvider(): array
    {
        return [
            [
                null
            ], [
                (new VatNote())
                    ->setLanguageId(LanguageCode::CS)
                    ->setValue('Poznámka v českém jazyce')
            ],
        ];
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function set(VatNote|null $newValue): void
    {
        $model = new InvoiceLine();
        $model->setVatNote($newValue);
        $this->assertEquals($newValue, Reflection::getHiddenProperty($model, 'vatNote'));
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function getSet(VatNote|null $newValue): void
    {
        $model = new InvoiceLine();
        $this->assertEquals($newValue, $model->setVatNote($newValue)->getVatNote());
    }
}
