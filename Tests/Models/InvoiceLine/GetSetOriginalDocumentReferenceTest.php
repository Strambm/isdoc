<?php

namespace Isdoc\Tests\Models\InvoiceLine;

class GetSetOriginalDocumentReferenceTest extends GetSetReferenceTestCase
{
    protected function getAttributeName(): string
    {
        return 'originalDocumentReference';
    }
}
