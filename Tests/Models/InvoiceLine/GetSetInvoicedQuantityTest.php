<?php

namespace Isdoc\Tests\Models\InvoiceLine;

use Isdoc\Models\InvoiceLine;
use PHPUnit\Framework\TestCase;
use Cetria\Helpers\Reflection\Reflection;

class GetSetInvoicedQuantityTest extends TestCase
{
    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function get(float|null $newValue): void
    {
        $model = new InvoiceLine();
        Reflection::setHiddenProperty($model, 'invoicedQuantity', $newValue);
        $this->assertEquals($newValue, $model->getInvoicedQuantity());
    }

    public static function getSetDataProvider(): array
    {
        return [
            [
                null
            ], [
                123
            ], [
                0.54
            ]
        ];
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function set(float|null $newValue): void
    {
        $model = new InvoiceLine();
        $model->setInvoicedQuantity($newValue);
        $this->assertEquals($newValue, Reflection::getHiddenProperty($model, 'invoicedQuantity'));
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function getSet(float|null $newValue): void
    {
        $model = new InvoiceLine();
        $this->assertEquals($newValue, $model->setInvoicedQuantity($newValue)->getInvoicedQuantity());
    }
}
