<?php

namespace Isdoc\Tests\Models\PartyTaxScheme;

use Isdoc\Enums\TaxScheme;
use Isdoc\Models\PartyTaxScheme;
use PHPUnit\Framework\TestCase;
use Cetria\Helpers\Reflection\Reflection;

class GetCompanyIdTest extends TestCase
{
    /**
     * @test
     */
    public function basic(): void
    {
        $value = '123456';
        $scheme = TaxScheme::TIN;
        $model = new PartyTaxScheme($value, $scheme);
        $this->assertEquals($value, Reflection::getHiddenProperty($model, 'companyId'));
        $this->assertEquals($value, $model->getCompanyId());
    }
}
