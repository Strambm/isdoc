<?php

namespace Isdoc\Tests\Models\PartyTaxScheme;

use Isdoc\Enums\TaxScheme;
use Isdoc\Models\PartyTaxScheme;
use PHPUnit\Framework\TestCase;

class ToXmlElementTest extends TestCase
{
    /**
     * @test
     */
    public function onlyRequiredAttributes(): void
    {
        $dic = 'CZ27120384';
        $type = TaxScheme::VAT;
        $model = new PartyTaxScheme($dic, $type);
        $result = $model->toString();
        $structure = json_decode(json_encode(simplexml_load_string($result)), true);
        $this->assertCount(2, $structure);
        $this->assertEquals($dic, $structure['CompanyID']);
        $this->assertEquals($type, $structure['TaxScheme']);
    }
}
