<?php

namespace Isdoc\Tests\Models\PartyTaxScheme;

use Isdoc\Enums\TaxScheme;
use Isdoc\Exceptions\UnvalidTaxScheme;
use Isdoc\Models\PartyTaxScheme;
use PHPUnit\Framework\TestCase;
use Cetria\Helpers\Reflection\Reflection;

class ConstructorTest extends TestCase
{
    /**
     * @test
     */
    public function basic(): void
    {
        $value = '123456';
        $scheme = TaxScheme::TIN;
        $model = new PartyTaxScheme($value, $scheme);
        $this->assertEquals($value, Reflection::getHiddenProperty($model, 'companyId'));
        $this->assertEquals($scheme, Reflection::getHiddenProperty($model, 'taxScheme'));
    }

    /**
     * @test
     */
    public function unvalidSchemeValue(): void
    {
        $this->expectException(UnvalidTaxScheme::class);
        new PartyTaxScheme('123456', 'unvalid');
    }
}
