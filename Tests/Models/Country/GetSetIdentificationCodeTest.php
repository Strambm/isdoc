<?php

namespace Isdoc\Tests\Models\Country;

use Isdoc\Models\Country;
use Isdoc\Enums\CountryCode;
use PHPUnit\Framework\TestCase;
use Isdoc\Exceptions\UnvalidCountryCode;
use Cetria\Helpers\Reflection\Reflection;

class GetSetIdentificationCodeTest extends TestCase
{
    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function get(string|null $newValue): void
    {
        $model = new Country();
        Reflection::setHiddenProperty($model, 'identificationCode', $newValue);
        $this->assertEquals($newValue, $model->getIdentificationCode());
    }

    public static function getSetDataProvider(): array
    {
        return [
            [
                CountryCode::AT
            ], [
                CountryCode::CZ
            ], [
                null
            ]
        ];
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function set(string|null $newValue): void
    {
        $model = new Country();
        $model->setIdentificationCode($newValue);
        $this->assertEquals($newValue, Reflection::getHiddenProperty($model, 'identificationCode'));
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function getSet(string|null $newValue): void
    {
        $model = new Country();
        $this->assertEquals($newValue, $model->setIdentificationCode($newValue)->getIdentificationCode());
    }

    /**
     * @test
     */
    public function setUnvalidValue(): void
    {
        $this->expectException(UnvalidCountryCode::class);
        $model = new Country();
        $model->setIdentificationCode('invalid');
    }
}
