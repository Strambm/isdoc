<?php

namespace Isdoc\Tests\Models\Country;

use Isdoc\Models\Country;
use Isdoc\Enums\CountryCode;
use PHPUnit\Framework\TestCase;
use Cetria\Helpers\Reflection\Reflection;

class GetSetNameTest extends TestCase
{
    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function get(string|null $newValue): void
    {
        $model = new Country();
        Reflection::setHiddenProperty($model, 'name', $newValue);
        $this->assertEquals($newValue, $model->getName());
    }

    public static function getSetDataProvider(): array
    {
        return [
            [
                'Česká republika'
            ], [
                'Czech Republic'
            ], [
                'Název'
            ]
        ];
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function set(string|null $newValue): void
    {
        $model = $model = new Country();
        $model->setName($newValue);
        $this->assertEquals($newValue, Reflection::getHiddenProperty($model, 'name'));
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function getSet(string|null $newValue): void
    {
        $model = $model = new Country();
        $this->assertEquals($newValue, $model->setName($newValue)->getName());
    }

    /**
     * @test
     */
    public function extendNameFromCountryCode(): void
    {
        $model = new Country();
        Reflection::setHiddenProperty($model, 'identificationCode', CountryCode::DE);
        $this->assertEquals(CountryCode::getNameFromCode(CountryCode::DE), $model->getName());
    }
}
