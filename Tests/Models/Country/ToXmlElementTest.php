<?php

namespace Isdoc\Tests\Models\Country;

use Isdoc\Enums\CountryCode;
use Isdoc\Exceptions\MissingIdentificationCode;
use Isdoc\Models\Country;
use PHPUnit\Framework\TestCase;

class ToXmlElementTest extends TestCase
{
    /**
     * @test
     */
    public function onlyRequiredAttributes(): void
    {
        $countryCode = CountryCode::BE;

        $model = new Country();
        $model->setIdentificationCode($countryCode);
        $result = $model->toString();
        $structure = json_decode(json_encode(simplexml_load_string($result)), true);
        $this->assertCount(2, $structure);
        $this->assertEquals($countryCode, $structure['IdentificationCode']);
        $this->assertEquals(CountryCode::getNameFromCode($countryCode), $structure['Name']);
    }
}
