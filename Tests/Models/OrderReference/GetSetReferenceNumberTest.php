<?php

namespace Isdoc\Tests\Models\OrderReference;

use Isdoc\Models\OrderReference;
use PHPUnit\Framework\TestCase;
use Cetria\Helpers\Reflection\Reflection;

class GetSetReferenceNumberTest extends TestCase
{
    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function get(string|null $newValue): void
    {
        $model = new OrderReference();
        Reflection::setHiddenProperty($model, 'referenceNumber', $newValue);
        $this->assertEquals($newValue, $model->getReferenceNumber());
    }

    public static function getSetDataProvider(): array
    {
        return [
            [
                '123456'
            ], [
                'ABC-123456-CD'
            ], [
                null
            ]
        ];
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function set(string|null $newValue): void
    {
        $model = new OrderReference();
        $model->setReferenceNumber($newValue);
        $this->assertEquals($newValue, Reflection::getHiddenProperty($model, 'referenceNumber'));
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function getSet(string|null $newValue): void
    {
        $model = new OrderReference();
        $this->assertEquals($newValue, $model->setReferenceNumber($newValue)->getReferenceNumber());
    }
}
