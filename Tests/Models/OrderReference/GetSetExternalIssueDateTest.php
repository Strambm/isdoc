<?php

namespace Isdoc\Tests\Models\OrderReference;

use Isdoc\Exceptions\InvalidDateFormat;
use Isdoc\Models\OrderReference;
use PHPUnit\Framework\TestCase;
use Cetria\Helpers\Reflection\Reflection;

class GetSetExternalIssueDateTest extends TestCase
{
    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function get(string|null $newValue): void
    {
        $model = new OrderReference();
        Reflection::setHiddenProperty($model, 'issueDateExternal', $newValue);
        $this->assertEquals($newValue, $model->getExternalIssueDate());
    }

    public static function getSetDataProvider(): array
    {
        return [
            [
                null
            ], [
                '2020-02-02'
            ], [
                '2024-10-15'
            ]
        ];
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function set(string|null $newValue): void
    {
        $model = new OrderReference();
        $model->setExternalIssueDate($newValue);
        $this->assertEquals($newValue, Reflection::getHiddenProperty($model, 'issueDateExternal'));
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function getSet(string|null $newValue): void
    {
        $model = new OrderReference();
        $this->assertEquals($newValue, $model->setExternalIssueDate($newValue)->getExternalIssueDate());
    }

    /**
     * @test
     * @dataProvider invalidDateDataProvider
     */
    public function invalidDate(string $date): void
    {
        $this->expectException(InvalidDateFormat::class);
        $model = new OrderReference();
        $model->setExternalIssueDate($date);
    }

    public static function invalidDateDataProvider(): array
    {
        return [
            [
                '2020-02-30'
            ], [
                '2011-13-01'
            ]
        ];
    }
}
