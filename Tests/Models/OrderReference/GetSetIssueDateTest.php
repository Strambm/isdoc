<?php

namespace Isdoc\Tests\Models\OrderReference;

use Isdoc\Exceptions\InvalidDateFormat;
use Isdoc\Models\OrderReference;
use PHPUnit\Framework\TestCase;
use Cetria\Helpers\Reflection\Reflection;

class GetSetIssueDateTest extends TestCase
{
    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function get(string|null $newValue): void
    {
        $model = new OrderReference();
        Reflection::setHiddenProperty($model, 'issueDate', $newValue);
        $this->assertEquals($newValue, $model->getIssueDate());
    }

    public static function getSetDataProvider(): array
    {
        return [
            [
                null
            ], [
                '2020-02-02'
            ], [
                '2024-10-15'
            ]
        ];
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function set(string|null $newValue): void
    {
        $model = new OrderReference();
        $model->setIssueDate($newValue);
        $this->assertEquals($newValue, Reflection::getHiddenProperty($model, 'issueDate'));
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function getSet(string|null $newValue): void
    {
        $model = new OrderReference();
        $this->assertEquals($newValue, $model->setIssueDate($newValue)->getIssueDate());
    }

    /**
     * @test
     * @dataProvider invalidDateDataProvider
     */
    public function invalidDate(string $date): void
    {
        $this->expectException(InvalidDateFormat::class);
        $model = new OrderReference();
        $model->setIssueDate($date);
    }

    public static function invalidDateDataProvider(): array
    {
        return [
            [
                '2020-02-30'
            ], [
                '2011-13-01'
            ]
        ];
    }
}
