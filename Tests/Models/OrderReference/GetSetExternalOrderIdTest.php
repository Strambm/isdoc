<?php

namespace Isdoc\Tests\Models\OrderReference;

use Isdoc\Models\OrderReference;
use PHPUnit\Framework\TestCase;
use Cetria\Helpers\Reflection\Reflection;

class GetSetExternalOrderIdTest extends TestCase
{
    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function get(string|null $newValue): void
    {
        $model = new OrderReference();
        Reflection::setHiddenProperty($model, 'externalOrderId', $newValue);
        $this->assertEquals($newValue, $model->getExternalOrderId());
    }

    public static function getSetDataProvider(): array
    {
        return [
            [
                ''
            ], [
                '123456'
            ], [
                'X123456'
            ], [
                null
            ]
        ];
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function set(string|null $newValue): void
    {
        $model = new OrderReference();
        $model->setExternalOrderId($newValue);
        $this->assertEquals($newValue, Reflection::getHiddenProperty($model, 'externalOrderId'));
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function getSet(string|null $newValue): void
    {
        $model = new OrderReference();
        $this->assertEquals($newValue, $model->setExternalOrderId($newValue)->getExternalOrderId());
    }
}
