<?php

namespace Isdoc\Tests\Models\OrderReference;

use Isdoc\Models\OrderReference;
use PHPUnit\Framework\TestCase;
use Cetria\Helpers\Reflection\Reflection;

class GetSetFileReferenceTest extends TestCase
{
    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function get(string|null $newValue): void
    {
        $model = new OrderReference();
        Reflection::setHiddenProperty($model, 'fileReference', $newValue);
        $this->assertEquals($newValue, $model->getFileReference());
    }

    public static function getSetDataProvider(): array
    {
        return [
            [
                '123456'
            ], [
                'AB-123456'
            ], [
                null
            ]
        ];
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function set(string|null $newValue): void
    {
        $model = new OrderReference();
        $model->setFileReference($newValue);
        $this->assertEquals($newValue, Reflection::getHiddenProperty($model, 'fileReference'));
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function getSet(string|null $newValue): void
    {
        $model = new OrderReference();
        $this->assertEquals($newValue, $model->setFileReference($newValue)->getFileReference());
    }
}
