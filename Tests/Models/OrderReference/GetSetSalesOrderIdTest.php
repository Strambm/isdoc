<?php

namespace Isdoc\Tests\Models\OrderReference;

use Isdoc\Exceptions\MissingSalesOrderId;
use Isdoc\Models\OrderReference;
use PHPUnit\Framework\TestCase;
use Cetria\Helpers\Reflection\Reflection;

class GetSetSalesOrderIdTest extends TestCase
{
    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function get(string $newValue): void
    {
        $model = new OrderReference();
        Reflection::setHiddenProperty($model, 'salesOrderId', $newValue);
        $this->assertEquals($newValue, $model->getSalesOrderId());
    }

    public static function getSetDataProvider(): array
    {
        return [
            [
                ''
            ], [
                '123456'
            ], [
                'X123456'
            ]
        ];
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function set(string $newValue): void
    {
        $model = new OrderReference();
        $model->setSalesOrderId($newValue);
        $this->assertEquals($newValue, Reflection::getHiddenProperty($model, 'salesOrderId'));
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function getSet(string $newValue): void
    {
        $model = new OrderReference();
        $this->assertEquals($newValue, $model->setSalesOrderId($newValue)->getSalesOrderId());
    }

    /**
     * @test
     */
    public function getWithoutValue(): void
    {
        $this->expectException(MissingSalesOrderId::class);
        $model = new OrderReference();
        $model->getSalesOrderId();
    }
}
