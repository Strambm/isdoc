<?php

namespace Isdoc\Tests\Models\OrderReference;

use Isdoc\Models\OrderReference;
use PHPUnit\Framework\TestCase;
use Cetria\Helpers\Reflection\Reflection;

class GetSetIsdsIdTest extends TestCase
{
    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function get(string|null $newValue): void
    {
        $model = new OrderReference();
        Reflection::setHiddenProperty($model, 'idsId', $newValue);
        $this->assertEquals($newValue, $model->getIsdsId());
    }

    public static function getSetDataProvider(): array
    {
        return [
            [
                '123456'
            ], [
                '123456ar123'
            ], [
                null
            ]
        ];
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function set(string|null $newValue): void
    {
        $model = new OrderReference();
        $model->setIsdsId($newValue);
        $this->assertEquals($newValue, Reflection::getHiddenProperty($model, 'idsId'));
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function getSet(string|null $newValue): void
    {
        $model = new OrderReference();
        $this->assertEquals($newValue, $model->setIsdsId($newValue)->getIsdsId());
    }
}
