<?php

namespace Isdoc\Tests\Models\OrderReference;

use Isdoc\Exceptions\InvalidUuidFormat;
use Isdoc\Models\OrderReference;
use PHPUnit\Framework\TestCase;
use Cetria\Helpers\Reflection\Reflection;

class GetSetUuidTest extends TestCase
{
    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function get(string|null $newValue): void
    {
        $model = new OrderReference();
        Reflection::setHiddenProperty($model, 'uuid', $newValue);
        $this->assertEquals($newValue, $model->getUuid());
    }

    public static function getSetDataProvider(): array
    {
        return [
            [
                'ABCDEF12-3456-7890-ABCD-EF0123456789'
            ], [
                '0FEDCBA9-8765-4321-FEDC-BA0987654321'
            ], [
                'A1B2C3D4-E5F6-789A-BCDE-F0123456789A'
            ]
        ];
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function set(string|null $newValue): void
    {
        $model = new OrderReference();
        $model->setUuid($newValue);
        $this->assertEquals($newValue, Reflection::getHiddenProperty($model, 'uuid'));
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function getSet(string|null $newValue): void
    {
        $model = new OrderReference();
        $this->assertEquals($newValue, $model->setUuid($newValue)->getUuid());
    }

    /**
     * @test
     * @dataProvider invalidUuidDataProvider
     */
    public function invalidUuid(string $date): void
    {
        $this->expectException(InvalidUuidFormat::class);
        $model = new OrderReference();
        $model->setUuid($date);
    }

    public static function invalidUuidDataProvider(): array
    {
        return [
            [
                '165wfwf'
            ], [
                '5004-818'
            ]
        ];
    }
}
