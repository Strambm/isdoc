<?php

namespace Isdoc\Tests\Models\OrderReference;

use Isdoc\Exceptions\MissingSalesOrderId;
use Isdoc\Models\OrderReference;
use PHPUnit\Framework\TestCase;

class ToXmlElementTest extends TestCase
{
    /**
     * @test
     */
    public function missingSalesIdAttributeAttribute(): void
    {
        $this->expectException(MissingSalesOrderId::class);
        $model = new OrderReference();
        $result = $model->toXmlElement();
    }

    /**
     * @test
     */
    public function onlyRequiredAttributes(): void
    {
        $salesOrderId = 'IdonlyRequiredAttributes';
        $model = new OrderReference();
        $model->setSalesOrderId($salesOrderId);
        $result = $model->toString();
        $structure = json_decode(json_encode(simplexml_load_string($result)), true);
        $this->assertCount(1, $structure);
        $this->assertEquals($salesOrderId, $structure['SalesOrderID']);
    }

    /**
     * @test
     */
    public function withNecessaryParameters(): void
    {
        $salesOrderId = '123456';
        $externalOrderId = '123';
        $issueDate = '2024-10-15';
        $externalIssueDate = '2024-10-16';
        $uuid = 'ABCDEF12-3456-7890-ABCD-EF0123456789';
        $isdsId = '12345678';
        $fileReference = 'fileReference';
        $referenceNumber = '987654';
        $model = new OrderReference();
        $model->setSalesOrderId($salesOrderId)
            ->setExternalIssueDate($externalIssueDate)
            ->setExternalOrderId($externalOrderId)
            ->setFileReference($fileReference)
            ->setIsdsId($isdsId)
            ->setIssueDate($issueDate)
            ->setReferenceNumber($referenceNumber)
            ->setUuid($uuid);
        $result = $model->toString();
        $structure = json_decode(json_encode(simplexml_load_string($result)), true);
        $this->assertCount(8, $structure);
        $this->assertEquals($salesOrderId, $structure['SalesOrderID']);
        $this->assertEquals($externalOrderId, $structure['ExternalOrderID']);
        $this->assertEquals($issueDate, $structure['IssueDate']);
        $this->assertEquals($externalIssueDate, $structure['ExternalOrderIssueDate']);
        $this->assertEquals($uuid, $structure['UUID']);
        $this->assertEquals($isdsId, $structure['ISDS_ID']);
        $this->assertEquals($fileReference, $structure['FileReference']);
        $this->assertEquals($referenceNumber, $structure['ReferenceNumber']);
    }
}
