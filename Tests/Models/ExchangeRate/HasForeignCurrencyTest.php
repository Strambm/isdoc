<?php

namespace Isdoc\Tests\Models\ExchangeRate;

use Isdoc\Models\ExchangeRate;
use PHPUnit\Framework\TestCase;
use Cetria\Helpers\Reflection\Reflection;
use PHPUnit\Framework\MockObject\MockObject;

class HasForeignCurrencyTest extends TestCase
{
    /**
     * @test
     */
    public function withForeign(): void
    {
        $model = $this->getModel();
        Reflection::setHiddenProperty($model, 'foreignCurrencyCode', 'CZK');
        $this->assertTrue($model->hasForeignCurrency());
    }

    private function getModel(): ExchangeRate|MockObject
    {
        $model = $this->getMockBuilder(ExchangeRate::class)
            ->disableOriginalConstructor()
            ->onlyMethods([])
            ->getMock();
        return $model;
    }

    /**
     * @test
     */
    public function withoutForeign(): void
    {
        $model = $this->getModel();
        Reflection::setHiddenProperty($model, 'foreignCurrencyCode', null);
        $this->assertFalse($model->hasForeignCurrency());
    }
}
