<?php

namespace Isdoc\Tests\Models\ExchangeRate;

use Isdoc\Models\ExchangeRate;
use PHPUnit\Framework\TestCase;
use PHPUnit\Framework\MockObject\MockObject;

class GetExchangeAmountTest extends TestCase
{
    /**
     * @test
     */
    public function withoutForeignCurrency(): void
    {
        $model = $this->getModel(false, 2);
        $this->assertNull($model->getExchangeAmount(8));
    }

    private function getModel(bool $hasForeignCurrency, float $exchangeRate): ExchangeRate|MockObject
    {
        $model = $this->getMockBuilder(ExchangeRate::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['hasForeignCurrency', 'getExchangeRate'])
            ->getMock();
        $model->expects($this->any())
            ->method('hasForeignCurrency')
            ->willReturn($hasForeignCurrency);
        $model->expects($this->any())
            ->method('getExchangeRate')
            ->willReturn($exchangeRate);
        return $model;
    }

    /**
     * @test
     */
    public function hasForeignCurrency(): void
    {
        $model = $this->getModel(true, 10);
        $this->assertEqualsWithDelta(10, $model->getExchangeAmount(100), 0.001);
    }
}
