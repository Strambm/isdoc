<?php

namespace Isdoc\Tests\Models\ExchangeRate;

use Isdoc\Enums\CurrencyCode;
use Isdoc\Models\ExchangeRate;
use PHPUnit\Framework\TestCase;
use Cetria\Helpers\Reflection\Reflection;
use Isdoc\Exceptions\UnvalidCurrencyCode;
use PHPUnit\Framework\MockObject\MockObject;

class GetSetLocalCurrencyCodeTest extends TestCase
{
    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function get(string $newValue): void
    {
        $model = $this->getModel();
        Reflection::setHiddenProperty($model, 'localCurrencyCode', $newValue);
        $this->assertEquals($newValue, $model->getLocalCurrencyCode());
    }

    private function getModel(): ExchangeRate|MockObject
    {
        $model = $this->getMockBuilder(ExchangeRate::class)
            ->disableOriginalConstructor()
            ->onlyMethods([])
            ->getMock();
        return $model;
    }

    public static function getSetDataProvider(): array
    {
        return [
            [
                CurrencyCode::CNY
            ], [
                CurrencyCode::CZK
            ]
        ];
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function set(string $newValue): void
    {
        $model = $this->getModel();
        $model->setLocalCurrencyCode($newValue);
        $this->assertEquals($newValue, Reflection::getHiddenProperty($model, 'localCurrencyCode'));
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function getSet(string $newValue): void
    {
        $model = $this->getModel();
        $this->assertEquals($newValue, $model->setLocalCurrencyCode($newValue)->getLocalCurrencyCode());
    }

    /**
     * @test
     */
    public function unvalid(): void
    {
        $model = $this->getModel();
        $this->expectException(UnvalidCurrencyCode::class);
        $model->setLocalCurrencyCode('Špatná hodnota');
    }
}
