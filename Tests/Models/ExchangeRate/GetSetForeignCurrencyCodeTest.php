<?php

namespace Isdoc\Tests\Models\ExchangeRate;

use Isdoc\Enums\CurrencyCode;
use Isdoc\Models\ExchangeRate;
use PHPUnit\Framework\TestCase;
use Cetria\Helpers\Reflection\Reflection;
use Isdoc\Exceptions\UnvalidCurrencyCode;
use PHPUnit\Framework\MockObject\MockObject;

class GetSetForeignCurrencyCodeTest extends TestCase
{
    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function get(string|null $newValue): void
    {
        $model = $this->getModel();
        Reflection::setHiddenProperty($model, 'foreignCurrencyCode', $newValue);
        $this->assertEquals($newValue, $model->getForeignCurrencyCode());
    }

    private function getModel(): ExchangeRate|MockObject
    {
        $model = $this->getMockBuilder(ExchangeRate::class)
            ->disableOriginalConstructor()
            ->onlyMethods([])
            ->getMock();
        return $model;
    }

    public static function getSetDataProvider(): array
    {
        return [
            [
                CurrencyCode::CNY
            ], [
                CurrencyCode::CZK
            ], [
                null
            ]
        ];
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function set(string|null $newValue): void
    {
        $model = $this->getModel();
        $model->setForeignCurrencyCode($newValue);
        $this->assertEquals($newValue, Reflection::getHiddenProperty($model, 'foreignCurrencyCode'));
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function getSet(string|null $newValue): void
    {
        $model = $this->getModel();
        $this->assertEquals($newValue, $model->setForeignCurrencyCode($newValue)->getForeignCurrencyCode());
    }

    /**
     * @test
     */
    public function unvalid(): void
    {
        $model = $this->getModel();
        $this->expectException(UnvalidCurrencyCode::class);
        $model->setForeignCurrencyCode('Špatná hodnota');
    }

    /**
     * @test
     */
    public function setEmptyString(): void
    {
        $model = $this->getModel();
        $model->setForeignCurrencyCode('');
        $this->assertNull(Reflection::getHiddenProperty($model, 'foreignCurrencyCode'));
    }
}
