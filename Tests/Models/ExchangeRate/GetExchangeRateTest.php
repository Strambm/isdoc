<?php

namespace Isdoc\Tests\Models\ExchangeRate;

use Isdoc\Enums\CurrencyCode;
use Isdoc\Models\ExchangeRate;
use PHPUnit\Framework\TestCase;
use Cetria\Helpers\Reflection\Reflection;
use PHPUnit\Framework\MockObject\MockObject;

class GetExchangeRateTest extends TestCase
{
    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function basic(float $newValue): void
    {
        $model = $this->getModel();
        Reflection::setHiddenProperty($model, 'currRate', $newValue);
        $this->assertEquals($newValue, $model->getExchangeRate());
    }

    private function getModel(): ExchangeRate|MockObject
    {
        $model = $this->getMockBuilder(ExchangeRate::class)
            ->disableOriginalConstructor()
            ->onlyMethods([])
            ->getMock();
        $model->setForeignCurrencyCode(CurrencyCode::EUR);
        return $model;
    }

    public static function getSetDataProvider(): array
    {
        return [
            [
                0.256
            ], [
                22.1
            ]
        ];
    }
}
