<?php

namespace Isdoc\Tests\Models\ExchangeRate;

use Isdoc\Enums\CurrencyCode;
use Isdoc\Models\ExchangeRate;
use PHPUnit\Framework\TestCase;
use Cetria\Helpers\Reflection\Reflection;
use PHPUnit\Framework\MockObject\MockObject;
use Isdoc\Exceptions\ExcahngeRateInLocalTransaction;

class GetSetRefCurrRateTest extends TestCase
{
    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function get(float $newValue): void
    {
        $model = $this->getModel(CurrencyCode::EUR);
        Reflection::setHiddenProperty($model, 'refCurrRate', $newValue);
        $this->assertEquals($newValue, $model->getRefCurrRate());
    }

    private function getModel(string|null $referenceCurrencyCode): ExchangeRate|MockObject
    {
        $model = $this->getMockBuilder(ExchangeRate::class)
            ->disableOriginalConstructor()
            ->onlyMethods([])
            ->getMock();
        $model->setForeignCurrencyCode($referenceCurrencyCode);
        return $model;
    }

    public static function getSetDataProvider(): array
    {
        return [
            [
                1
            ], [
                0.25
            ]
        ];
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function set(float $newValue): void
    {
        $model = $this->getModel(CurrencyCode::EUR);
        $model->setRefCurrRate($newValue);
        $this->assertEquals($newValue, Reflection::getHiddenProperty($model, 'refCurrRate'));
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function getSet(float $newValue): void
    {
        $model = $this->getModel(CurrencyCode::EUR);
        $this->assertEquals($newValue, $model->setRefCurrRate($newValue)->getRefCurrRate());
    }

    /**
     * @test
     */
    public function withoutForeignCurrency(): void
    {
        $model = $this->getModel(null);
        $this->expectException(ExcahngeRateInLocalTransaction::class);
        $model->setRefCurrRate(5)->getRefCurrRate();
    }
}
