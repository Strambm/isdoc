<?php

namespace Isdoc\Tests\Models\ExchangeRate;

use Isdoc\Enums\CurrencyCode;
use Isdoc\Models\ExchangeRate;
use PHPUnit\Framework\TestCase;

class ConstructorTest extends TestCase
{
    /**
     * @test
     */
    public function withForeign(): void
    {
        $model = new ExchangeRate(CurrencyCode::USD, CurrencyCode::CZK, 5, 9);
        $this->assertEquals(CurrencyCode::USD, $model->getLocalCurrencyCode());
        $this->assertEquals(CurrencyCode::CZK, $model->getForeignCurrencyCode());
        $this->assertEquals(5, $model->getCurrRate());
        $this->assertEquals(9, $model->getRefCurrRate());
    }

    /**
     * @test
     */
    public function defaultValues(): void
    {
        $model = new ExchangeRate(CurrencyCode::EUR);
        $this->assertNull($model->getForeignCurrencyCode());
        $this->assertEquals(1, $model->getCurrRate());
        $this->assertEquals(1, $model->getRefCurrRate());
    }
}
