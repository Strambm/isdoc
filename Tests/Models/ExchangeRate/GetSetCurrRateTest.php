<?php

namespace Isdoc\Tests\Models\ExchangeRate;

use Isdoc\Enums\CurrencyCode;
use Isdoc\Models\ExchangeRate;
use PHPUnit\Framework\TestCase;
use Cetria\Helpers\Reflection\Reflection;
use PHPUnit\Framework\MockObject\MockObject;
use Isdoc\Exceptions\ExcahngeRateInLocalTransaction;

class GetSetCurrRateTest extends TestCase
{
    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function get(float $newValue): void
    {
        $model = $this->getModel(CurrencyCode::EUR);
        Reflection::setHiddenProperty($model, 'currRate', $newValue);
        $this->assertEquals($newValue, $model->getCurrRate());
    }

    private function getModel(string|null $referenceCurrencyCode): ExchangeRate|MockObject
    {
        $model = $this->getMockBuilder(ExchangeRate::class)
            ->disableOriginalConstructor()
            ->onlyMethods([])
            ->getMock();
        $model->setForeignCurrencyCode($referenceCurrencyCode);
        return $model;
    }

    public static function getSetDataProvider(): array
    {
        return [
            [
                1
            ], [
                0.25
            ]
        ];
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function set(float $newValue): void
    {
        $model = $this->getModel(CurrencyCode::EUR);
        $model->setCurrRate($newValue);
        $this->assertEquals($newValue, Reflection::getHiddenProperty($model, 'currRate'));
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function getSet(float $newValue): void
    {
        $model = $this->getModel(CurrencyCode::EUR);
        $this->assertEquals($newValue, $model->setCurrRate($newValue)->getCurrRate());
    }

    /**
     * @test
     */
    public function withoutForeignCurrency(): void
    {
        $model = $this->getModel(null);
        $this->expectException(ExcahngeRateInLocalTransaction::class);
        $model->setCurrRate(5)->getCurrRate();
    }
}
