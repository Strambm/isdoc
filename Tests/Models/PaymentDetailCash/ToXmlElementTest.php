<?php

namespace Isdoc\Tests\Models\PaymentDetailCash;

use Isdoc\Models\PaymentDetailCash;
use PHPUnit\Framework\TestCase;

class ToXmlElementTest extends TestCase
{
    /**
     * @test
     */
    public function onlyRequiredAttributes(): void
    {
        $issueDate = '2024-01-01';
        $model = new PaymentDetailCash();
        $model->setIssueDate($issueDate);
        $result = $model->toString();
        $structure = json_decode(json_encode(simplexml_load_string($result)), true);
        $this->assertCount(2, $structure);
        $this->assertEquals([], $structure['DocumentID']);
        $this->assertEquals($issueDate, $structure['IssueDate']);
    }

    /**
     * @test
     */
    public function withNecessaryParameters(): void
    {
        $issueDate = '2024-01-01';
        $model = new PaymentDetailCash();
        $model->setIssueDate($issueDate)
            ->setDocumentId('123');
        $result = $model->toString();
        $structure = json_decode(json_encode(simplexml_load_string($result)), true);
        $this->assertCount(2, $structure);
        $this->assertEquals('123', $structure['DocumentID']);
        $this->assertEquals($issueDate, $structure['IssueDate']);
    }
}
