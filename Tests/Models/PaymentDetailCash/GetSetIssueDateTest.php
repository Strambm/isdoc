<?php

namespace Isdoc\Tests\Models\PaymentDetailCash;

use Isdoc\Exceptions\MissingIssueDate;
use PHPUnit\Framework\TestCase;
use Isdoc\Models\PaymentDetailCash;
use Isdoc\Exceptions\InvalidDateFormat;
use Cetria\Helpers\Reflection\Reflection;

class GetSetIssueDateTest extends TestCase
{
    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function get(string $newValue): void
    {
        $model = new PaymentDetailCash();
        Reflection::setHiddenProperty($model, 'issueDate', $newValue);
        $this->assertEquals($newValue, $model->getIssueDate());
    }

    public static function getSetDataProvider(): array
    {
        return [
            [
                '2022-02-02'
            ], 
        ];
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function set(string $newValue): void
    {
        $model = new PaymentDetailCash();
        $model->setIssueDate($newValue);
        $this->assertEquals($newValue, Reflection::getHiddenProperty($model, 'issueDate'));
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function getSet(string $newValue): void
    {
        $model = new PaymentDetailCash();
        $this->assertEquals($newValue, $model->setIssueDate($newValue)->getIssueDate());
    }

    /**
     * @test
     * @dataProvider invalidDateFormatDataProvider
     */
    public function throwInvalidDateFormat(string $value): void
    {
        $this->expectException(InvalidDateFormat::class);
        $model = new PaymentDetailCash();
        $model->setIssueDate($value);
    }

    public static function invalidDateFormatDataProvider(): array
    {
        return [
            [
                '1.2.2022',
            ], [
                ''
            ], [
                '2022-01-01 15:00:00'
            ]
        ];
    }

    /**
     * @test
     */
    public function throwsMissingIssueDate(): void
    {
        $this->expectException(MissingIssueDate::class);
        $model = new PaymentDetailCash();
        $model->getIssueDate();
    }
}
