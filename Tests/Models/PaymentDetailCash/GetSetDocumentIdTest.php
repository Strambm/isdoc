<?php

namespace Isdoc\Tests\Models\PaymentDetailCash;

use PHPUnit\Framework\TestCase;
use Isdoc\Models\PaymentDetailCash;
use Cetria\Helpers\Reflection\Reflection;

class GetSetDocumentIdTest extends TestCase
{
    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function get(string $newValue): void
    {
        $model = new PaymentDetailCash();
        Reflection::setHiddenProperty($model, 'documentId', $newValue);
        $this->assertEquals($newValue, $model->getDocumentId());
    }

    public static function getSetDataProvider(): array
    {
        return [
            [
                '1285288'
            ], [
                '11515'
            ], [
                ''
            ]
        ];
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function set(string $newValue): void
    {
        $model = $model = new PaymentDetailCash();
        $model->setDocumentId($newValue);
        $this->assertEquals($newValue, Reflection::getHiddenProperty($model, 'documentId'));
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function getSet(string $newValue): void
    {
        $model = $model = new PaymentDetailCash();
        $this->assertEquals($newValue, $model->setDocumentId($newValue)->getDocumentId());
    }
}
