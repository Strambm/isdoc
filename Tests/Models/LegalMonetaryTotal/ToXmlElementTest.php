<?php

namespace Isdoc\Tests\Models\LegalMonetaryTotal;

use Isdoc\Enums\CurrencyCode;
use Isdoc\Models\ExchangeRate;
use Isdoc\Models\LegalMonetaryTotal;
use PHPUnit\Framework\TestCase;

class ToXmlElementTest extends TestCase
{
    /**
     * @test
     */
    public function onlyRequiredAttributes(): void
    {
        $model = new LegalMonetaryTotal();
        $result = $model->toString();
        $structure = json_decode(json_encode(simplexml_load_string($result)), true);
        $this->assertCount(8, $structure);
        $this->assertEquals(0, $structure['TaxExclusiveAmount']);
        $this->assertEquals(0, $structure['TaxInclusiveAmount']);
        $this->assertEquals(0, $structure['AlreadyClaimedTaxExclusiveAmount']);
        $this->assertEquals(0, $structure['AlreadyClaimedTaxInclusiveAmount']);
        $this->assertEquals(0, $structure['DifferenceTaxExclusiveAmount']);
        $this->assertEquals(0, $structure['DifferenceTaxInclusiveAmount']);
        $this->assertEquals(0, $structure['PaidDepositsAmount']);
        $this->assertEquals(0, $structure['PayableAmount']);
    }

    /**
     * @test
     */
    public function withNecessaryParameters(): void
    {
        $model = (new LegalMonetaryTotal())
            ->setExchangeRate(new ExchangeRate(CurrencyCode::CZK, CurrencyCode::EUR, 2, 2))
            ->setTaxExclusiveAmount(10)
            ->setTaxInclusiveAmount(20)
            ->setAlreadyClaimedTaxExclusiveAmount(1)
            ->setAlreadyClaimedTaxInclusiveAmount(2)
            ->setPaidDepositsAmount(1)
            ->setPayableRoundingAmount(0)
            ->setPayableRoundingAmountCurr(0);
        $result = $model->toString();
        $structure = json_decode(json_encode(simplexml_load_string($result)), true);
        $this->assertCount(18, $structure);
        $this->assertEquals(10, $structure['TaxExclusiveAmount']);
        $this->assertEquals(5, $structure['TaxExclusiveAmountCurr']);
        $this->assertEquals(20, $structure['TaxInclusiveAmount']);
        $this->assertEquals(10, $structure['TaxInclusiveAmountCurr']);
        $this->assertEquals(1, $structure['AlreadyClaimedTaxExclusiveAmount']);
        $this->assertEquals(0.5, $structure['AlreadyClaimedTaxExclusiveAmountCurr']);
        $this->assertEquals(2, $structure['AlreadyClaimedTaxInclusiveAmount']);
        $this->assertEquals(1, $structure['AlreadyClaimedTaxInclusiveAmountCurr']);
        $this->assertEquals(9, $structure['DifferenceTaxExclusiveAmount']);
        $this->assertEquals(4.5, $structure['DifferenceTaxExclusiveAmountCurr']);
        $this->assertEquals(18, $structure['DifferenceTaxInclusiveAmount']);
        $this->assertEquals(9, $structure['DifferenceTaxInclusiveAmountCurr']);
        $this->assertEquals(1, $structure['PaidDepositsAmount']);
        $this->assertEquals(0.5, $structure['PaidDepositsAmountCurr']);
        $this->assertEquals(17, $structure['PayableAmount']);  
        $this->assertEquals(8.5, $structure['PayableAmountCurr']); 
        $this->assertEquals(0, $structure['PayableRoundingAmount']);  
        $this->assertEquals(0, $structure['PayableRoundingAmountCurr']);  
    }
}
