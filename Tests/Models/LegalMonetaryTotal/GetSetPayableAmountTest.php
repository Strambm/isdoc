<?php

namespace Isdoc\Tests\Models\LegalMonetaryTotal;

use Isdoc\Models\LegalMonetaryTotal;
use PHPUnit\Framework\TestCase;
use Cetria\Helpers\Reflection\Reflection;

class GetSetPayableAmountTest extends TestCase
{
    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function get(float|null $newValue): void
    {
        $model = new LegalMonetaryTotal();
        Reflection::setHiddenProperty($model, 'payableAmount', $newValue);
        $this->assertEquals($newValue, $model->getPayableAmount());
    }

    public static function getSetDataProvider(): array
    {
        return [
            [
                0
            ], [
                15
            ], [
                -18.5
            ]
        ];
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function set(float|null $newValue): void
    {
        $model = new LegalMonetaryTotal();
        $model->setPayableAmount($newValue);
        $this->assertEquals($newValue, Reflection::getHiddenProperty($model, 'payableAmount'));
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function getSet(float|null $newValue): void
    {
        $model = new LegalMonetaryTotal();
        $this->assertEquals($newValue, $model->setPayableAmount($newValue)->getPayableAmount());
    }

    /**
     * @test
     */
    public function automaticsCalc(): void
    {
        $model = new LegalMonetaryTotal();
        $model->setDifferenceTaxInclusiveAmount(49.5)
            ->setPayableRoundingAmount(0.5)
            ->setPaidDepositsAmount(20);
        $this->assertEquals(30, $model->getPayableAmount());
    }

    /**
     * @test
     */
    public function automaticsWithoutRoundingAmountCalc(): void
    {
        $model = new LegalMonetaryTotal();
        $model->setDifferenceTaxInclusiveAmount(50)
            ->setPaidDepositsAmount(20);
        $this->assertEquals(30, $model->getPayableAmount());
    }
}
