<?php

namespace Isdoc\Tests\Models\LegalMonetaryTotal;

use Isdoc\Enums\CurrencyCode;
use Isdoc\Models\ExchangeRate;
use PHPUnit\Framework\TestCase;
use Isdoc\Models\LegalMonetaryTotal;
use Cetria\Helpers\Reflection\Reflection;

class GetSetAlreadyClaimedTaxExclusiveAmountCurrTest extends TestCase
{
    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function get(float|null $newValue): void
    {
        $model = new LegalMonetaryTotal();
        Reflection::setHiddenProperty($model, 'alreadyClaimedTaxExclusiveAmountCurr', $newValue);
        $this->assertEquals($newValue, $model->getAlreadyClaimedTaxExclusiveAmountCurr());
    }

    public static function getSetDataProvider(): array
    {
        return [
            [
                0
            ], [
                15
            ], [
                -18.5
            ], [
                null
            ]
        ];
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function set(float|null $newValue): void
    {
        $model = new LegalMonetaryTotal();
        $model->setAlreadyClaimedTaxExclusiveAmountCurr($newValue);
        $this->assertEquals($newValue, Reflection::getHiddenProperty($model, 'alreadyClaimedTaxExclusiveAmountCurr'));
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function getSet(float|null $newValue): void
    {
        $model = new LegalMonetaryTotal();
        $this->assertEquals($newValue, $model->setAlreadyClaimedTaxExclusiveAmountCurr($newValue)->getAlreadyClaimedTaxExclusiveAmountCurr());
    }

    /**
     * @test
     */
    public function automaticsCalc(): void
    {
        $model = new LegalMonetaryTotal();
        $model->setExchangeRate(new ExchangeRate(CurrencyCode::CZK, CurrencyCode::EUR, 25, 20));
        $model->setAlreadyClaimedTaxExclusiveAmount(100);
        $this->assertEqualsWithDelta(4, $model->getAlreadyClaimedTaxExclusiveAmountCurr(), 0.001);
    }
}
