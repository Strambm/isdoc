<?php

namespace Isdoc\Tests\Models\LegalMonetaryTotal;

use Isdoc\Enums\CurrencyCode;
use Isdoc\Models\ExchangeRate;
use PHPUnit\Framework\TestCase;
use Isdoc\Models\LegalMonetaryTotal;
use Cetria\Helpers\Reflection\Reflection;

class GetSetTaxInclusiveAmountCurrTest extends TestCase
{
    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function get(float|null $newValue): void
    {
        $model = new LegalMonetaryTotal();
        Reflection::setHiddenProperty($model, 'taxInclusiveAmountCurr', $newValue);
        $this->assertEquals($newValue, $model->getTaxInclusiveAmountCurr());
    }

    public static function getSetDataProvider(): array
    {
        return [
            [
                0
            ], [
                15
            ], [
                -18.5
            ], [
                null
            ]
        ];
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function set(float|null $newValue): void
    {
        $model = new LegalMonetaryTotal();
        $model->setTaxInclusiveAmountCurr($newValue);
        $this->assertEquals($newValue, Reflection::getHiddenProperty($model, 'taxInclusiveAmountCurr'));
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function getSet(float|null $newValue): void
    {
        $model = new LegalMonetaryTotal();
        $this->assertEquals($newValue, $model->setTaxInclusiveAmountCurr($newValue)->getTaxInclusiveAmountCurr());
    }

    /**
     * @test
     */
    public function automaticsCalc(): void
    {
        $model = new LegalMonetaryTotal();
        $model->setExchangeRate(new ExchangeRate(CurrencyCode::CZK, CurrencyCode::EUR, 25, 20));
        $model->setTaxInclusiveAmount(100);
        $this->assertEqualsWithDelta(4, $model->getTaxInclusiveAmountCurr(), 0.001);
    }
}
