<?php

namespace Isdoc\Tests\Models\LegalMonetaryTotal;

use Isdoc\Models\LegalMonetaryTotal;
use PHPUnit\Framework\TestCase;
use Cetria\Helpers\Reflection\Reflection;

class GetSetDifferenceTaxExclusiveAmountTest extends TestCase
{
    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function get(float|null $newValue): void
    {
        $model = new LegalMonetaryTotal();
        Reflection::setHiddenProperty($model, 'differenceTaxExclusiveAmount', $newValue);
        $this->assertEquals($newValue, $model->getDifferenceTaxExclusiveAmount());
    }

    public static function getSetDataProvider(): array
    {
        return [
            [
                0
            ], [
                15
            ], [
                -18.5
            ]
        ];
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function set(float|null $newValue): void
    {
        $model = new LegalMonetaryTotal();
        $model->setDifferenceTaxExclusiveAmount($newValue);
        $this->assertEquals($newValue, Reflection::getHiddenProperty($model, 'differenceTaxExclusiveAmount'));
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function getSet(float|null $newValue): void
    {
        $model = new LegalMonetaryTotal();
        $this->assertEquals($newValue, $model->setDifferenceTaxExclusiveAmount($newValue)->getDifferenceTaxExclusiveAmount());
    }

    /**
     * @test
     */
    public function automaticsCalc(): void
    {
        $model = new LegalMonetaryTotal();
        $model->setTaxExclusiveAmount(100);
        $model->setAlreadyClaimedTaxExclusiveAmount(10);
        $this->assertEquals(90, $model->getDifferenceTaxExclusiveAmount());
    }
}
