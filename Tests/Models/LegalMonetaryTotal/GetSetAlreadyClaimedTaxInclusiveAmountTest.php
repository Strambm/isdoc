<?php

namespace Isdoc\Tests\Models\LegalMonetaryTotal;

use Isdoc\Models\LegalMonetaryTotal;
use PHPUnit\Framework\TestCase;
use Cetria\Helpers\Reflection\Reflection;

class GetSetAlreadyClaimedTaxInclusiveAmountTest extends TestCase
{
    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function get(float|null $newValue): void
    {
        $model = new LegalMonetaryTotal();
        Reflection::setHiddenProperty($model, 'alreadyClaimedTaxInclusiveAmount', $newValue);
        $this->assertEquals($newValue, $model->getAlreadyClaimedTaxInclusiveAmount());
    }

    public static function getSetDataProvider(): array
    {
        return [
            [
                0
            ], [
                15
            ], [
                -18.5
            ]
        ];
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function set(float|null $newValue): void
    {
        $model = new LegalMonetaryTotal();
        $model->setAlreadyClaimedTaxInclusiveAmount($newValue);
        $this->assertEquals($newValue, Reflection::getHiddenProperty($model, 'alreadyClaimedTaxInclusiveAmount'));
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function getSet(float|null $newValue): void
    {
        $model = new LegalMonetaryTotal();
        $this->assertEquals($newValue, $model->setAlreadyClaimedTaxInclusiveAmount($newValue)->getAlreadyClaimedTaxInclusiveAmount());
    }
}
