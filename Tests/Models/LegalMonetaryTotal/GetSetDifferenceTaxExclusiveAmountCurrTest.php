<?php

namespace Isdoc\Tests\Models\LegalMonetaryTotal;

use Isdoc\Enums\CurrencyCode;
use Isdoc\Models\ExchangeRate;
use PHPUnit\Framework\TestCase;
use Isdoc\Models\LegalMonetaryTotal;
use Cetria\Helpers\Reflection\Reflection;

class GetSetDifferenceTaxExclusiveAmountCurrTest extends TestCase
{
    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function get(float|null $newValue): void
    {
        $model = new LegalMonetaryTotal();
        Reflection::setHiddenProperty($model, 'differenceTaxExclusiveAmountCurr', $newValue);
        $this->assertEquals($newValue, $model->getDifferenceTaxExclusiveAmountCurr());
    }

    public static function getSetDataProvider(): array
    {
        return [
            [
                0
            ], [
                15
            ], [
                -18.5
            ], [
                null
            ]
        ];
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function set(float|null $newValue): void
    {
        $model = new LegalMonetaryTotal();
        $model->setDifferenceTaxExclusiveAmountCurr($newValue);
        $this->assertEquals($newValue, Reflection::getHiddenProperty($model, 'differenceTaxExclusiveAmountCurr'));
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function getSet(float|null $newValue): void
    {
        $model = new LegalMonetaryTotal();
        $this->assertEquals($newValue, $model->setDifferenceTaxExclusiveAmountCurr($newValue)->getDifferenceTaxExclusiveAmountCurr());
    }

    /**
     * @test
     */
    public function automaticsCalc(): void
    {
        $model = new LegalMonetaryTotal();
        $model->setExchangeRate(new ExchangeRate(CurrencyCode::CZK, CurrencyCode::EUR, 25, 20));
        $model->setDifferenceTaxExclusiveAmount(100);
        $this->assertEqualsWithDelta(4, $model->getDifferenceTaxExclusiveAmountCurr(), 0.001);
    }
}
