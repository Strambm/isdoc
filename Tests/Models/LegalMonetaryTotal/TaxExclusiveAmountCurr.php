<?php

namespace Isdoc\Tests\Models\LegalMonetaryTotal;

use Isdoc\Models\LegalMonetaryTotal;
use PHPUnit\Framework\TestCase;
use Cetria\Helpers\Reflection\Reflection;

class GetSetTaxExclusiveAmountCurrTest extends TestCase
{
    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function get(float|null $newValue): void
    {
        $model = new LegalMonetaryTotal();
        Reflection::setHiddenProperty($model, 'taxExclusiveAmountCurr', $newValue);
        $this->assertEquals($newValue, $model->getTaxExclusiveAmountCurr());
    }

    public static function getSetDataProvider(): array
    {
        return [
            [
                0
            ], [
                15
            ], [
                -18.5
            ], [
                null
            ]
        ];
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function set(float|null $newValue): void
    {
        $model = new LegalMonetaryTotal();
        $model->setTaxExclusiveAmountCurr($newValue);
        $this->assertEquals($newValue, Reflection::getHiddenProperty($model, 'taxExclusiveAmountCurr'));
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function getSet(float|null $newValue): void
    {
        $model = new LegalMonetaryTotal();
        $this->assertEquals($newValue, $model->setTaxExclusiveAmountCurr($newValue)->getTaxExclusiveAmountCurr());
    }
}
