<?php

namespace Isdoc\Tests\Models\LegalMonetaryTotal;

use Isdoc\Models\LegalMonetaryTotal;
use PHPUnit\Framework\TestCase;
use Cetria\Helpers\Reflection\Reflection;

class GetSetDifferenceTaxInclusiveAmountTest extends TestCase
{
    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function get(float|null $newValue): void
    {
        $model = new LegalMonetaryTotal();
        Reflection::setHiddenProperty($model, 'differenceTaxInclusiveAmount', $newValue);
        $this->assertEquals($newValue, $model->getDifferenceTaxInclusiveAmount());
    }

    public static function getSetDataProvider(): array
    {
        return [
            [
                0
            ], [
                15
            ], [
                -18.5
            ]
        ];
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function set(float|null $newValue): void
    {
        $model = new LegalMonetaryTotal();
        $model->setDifferenceTaxInclusiveAmount($newValue);
        $this->assertEquals($newValue, Reflection::getHiddenProperty($model, 'differenceTaxInclusiveAmount'));
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function getSet(float|null $newValue): void
    {
        $model = new LegalMonetaryTotal();
        $this->assertEquals($newValue, $model->setDifferenceTaxInclusiveAmount($newValue)->getDifferenceTaxInclusiveAmount());
    }

    /**
     * @test
     */
    public function automaticsCalc(): void
    {
        $model = new LegalMonetaryTotal();
        $model->setTaxInclusiveAmount(100);
        $model->setAlreadyClaimedTaxInclusiveAmount(10);
        $this->assertEquals(90, $model->getDifferenceTaxInclusiveAmount());
    }
}
