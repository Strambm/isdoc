<?php

namespace Isdoc\Tests\Models\LegalMonetaryTotal;

use Isdoc\Models\LegalMonetaryTotal;
use PHPUnit\Framework\TestCase;
use Cetria\Helpers\Reflection\Reflection;

class GetSetAlreadyClaimedTaxExclusiveAmountTest extends TestCase
{
    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function get(float|null $newValue): void
    {
        $model = new LegalMonetaryTotal();
        Reflection::setHiddenProperty($model, 'alreadyClaimedTaxExclusiveAmount', $newValue);
        $this->assertEquals($newValue, $model->getAlreadyClaimedTaxExclusiveAmount());
    }

    public static function getSetDataProvider(): array
    {
        return [
            [
                0
            ], [
                15
            ], [
                -18.5
            ]
        ];
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function set(float|null $newValue): void
    {
        $model = new LegalMonetaryTotal();
        $model->setAlreadyClaimedTaxExclusiveAmount($newValue);
        $this->assertEquals($newValue, Reflection::getHiddenProperty($model, 'alreadyClaimedTaxExclusiveAmount'));
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function getSet(float|null $newValue): void
    {
        $model = new LegalMonetaryTotal();
        $this->assertEquals($newValue, $model->setAlreadyClaimedTaxExclusiveAmount($newValue)->getAlreadyClaimedTaxExclusiveAmount());
    }
}
