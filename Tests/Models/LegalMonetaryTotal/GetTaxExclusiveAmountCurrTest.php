<?php

namespace Isdoc\Tests\Models\LegalMonetaryTotal;

use Isdoc\Enums\CurrencyCode;
use Isdoc\Models\ExchangeRate;
use PHPUnit\Framework\TestCase;
use Isdoc\Models\LegalMonetaryTotal;
use Cetria\Helpers\Reflection\Reflection;

class GetTaxExclusiveAmountCurrTest extends TestCase
{
    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function get(float|null $newValue): void
    {
        $model = new LegalMonetaryTotal();
        Reflection::setHiddenProperty($model, 'taxExclusiveAmountCurr', $newValue);
        $this->assertEquals($newValue, $model->getTaxExclusiveAmountCurr());
    }

    public static function getSetDataProvider(): array
    {
        return [
            [
                0
            ], [
                15
            ], [
                -18.5
            ], [
                null
            ]
        ];
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function set(float|null $newValue): void
    {
        $model = new LegalMonetaryTotal();
        $model->setTaxExclusiveAmountCurr($newValue);
        $this->assertEquals($newValue, Reflection::getHiddenProperty($model, 'taxExclusiveAmountCurr'));
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function getSet(float|null $newValue): void
    {
        $model = new LegalMonetaryTotal();
        $this->assertEquals($newValue, $model->setTaxExclusiveAmountCurr($newValue)->getTaxExclusiveAmountCurr());
    }

    /**
     * @test
     */
    public function automaticsCalc(): void
    {
        $model = new LegalMonetaryTotal();
        $model->setExchangeRate(new ExchangeRate(CurrencyCode::CZK, CurrencyCode::EUR, 25, 20));
        $model->setTaxExclusiveAmount(100);
        $this->assertEqualsWithDelta(4, $model->getTaxExclusiveAmountCurr(), 0.001);
    }
}
