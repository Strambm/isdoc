<?php

namespace Isdoc\Tests\Models\InvoiceLineReference;

use Isdoc\Models\InvoiceLineReference;
use PHPUnit\Framework\TestCase;
use Cetria\Helpers\Reflection\Reflection;

class ConstructorTest extends TestCase
{
    /**
     * @test
     */
    public function basic(): void
    {
        $model = new InvoiceLineReference('12345', '654789');
        $this->assertEquals('12345', $model->getRef());
        $this->assertEquals('654789', $model->getLineId());
        $this->assertEquals('12345' , Reflection::getHiddenProperty($model, 'ref'));
        $this->assertEquals('654789' , Reflection::getHiddenProperty($model, 'refLineId'));
    }
}
