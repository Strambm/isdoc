<?php

namespace Isdoc\Tests\Models\DocumentReference;

use Cetria\Helpers\Reflection\Reflection;
use Isdoc\Exceptions\MissingDeliveryNoteReferenceId;

class GetSetIdTest extends TestCase
{
    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function get(string $newValue): void
    {
        $model = $this->getModel('test');
        Reflection::setHiddenProperty($model, 'id', $newValue);
        $this->assertEquals($newValue, $model->getId());
    }

    public static function getSetDataProvider(): array
    {
        return [
            [
                ''
            ], [
                '123456'
            ], [
                'X123456'
            ]
        ];
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function set(string $newValue): void
    {
        $model = $model = $this->getModel('test');
        $model->setId($newValue);
        $this->assertEquals($newValue, Reflection::getHiddenProperty($model, 'id'));
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function getSet(string $newValue): void
    {
        $model = $model = $this->getModel('test');
        $this->assertEquals($newValue, $model->setId($newValue)->getId());
    }

    /**
     * @test
     */
    public function getWithoutValue(): void
    {
        $this->expectException(MissingDeliveryNoteReferenceId::class);
        $model = $model = $this->getModel('test');
        $model->getId();
    }
}
