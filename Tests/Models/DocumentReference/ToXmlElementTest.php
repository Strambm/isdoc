<?php

namespace Isdoc\Tests\Models\DocumentReference;

use Isdoc\Exceptions\MissingDeliveryNoteReferenceId;

class ToXmlElementTest extends TestCase
{
    /**
     * @test
     */
    public function missingIdAttribute(): void
    {
        $this->expectException(MissingDeliveryNoteReferenceId::class);
        $model = $this->getModel('test');
        $result = $model->toXmlElement();
    }

    /**
     * @test
     */
    public function onlyRequiredAttributes(): void
    {
        $id = '1234567dewe15';
        $model = $this->getModel('test');
        $model->setId($id);
        $result = $model->toString();
        $structure = json_decode(json_encode(simplexml_load_string($result)), true);
        $this->assertCount(2, $structure);
        $this->assertEquals($id, $structure['ID']);
        $this->assertEquals($id, $structure['@attributes']['id']);
    }

    /**
     * @test
     */
    public function withNecessaryParameters(): void
    {
        $id = '502065';
        $issueDate = '2021-06-26';
        $uuid = 'ABCDEF12-3486-7890-ABCD-EF0123456789';
        $model = $this->getModel('test');
        $model->setId($id)
            ->setIssueDate($issueDate)
            ->setUuid($uuid);
        $result = $model->toString();
        $structure = json_decode(json_encode(simplexml_load_string($result)), true);
        $this->assertCount(4, $structure);
        $this->assertEquals($id, $structure['ID']);
        $this->assertEquals($issueDate, $structure['IssueDate']);
        $this->assertEquals($uuid, $structure['UUID']);
        $this->assertEquals($id, $structure['@attributes']['id']);
    }
}
