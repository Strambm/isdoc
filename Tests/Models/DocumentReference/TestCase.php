<?php

namespace Isdoc\Tests\Models\DocumentReference;

use PHPUnit\Framework\TestCase as parentTestCase;
use Isdoc\Models\DocumentReference;
use PHPUnit\Framework\MockObject\MockObject;

abstract class TestCase extends parentTestCase
{
    protected function getModel(string $tagNameResult): DocumentReference|MockObject
    {
        $model = $this->getMockForAbstractClass(
            DocumentReference::class,
            [],
            '',
            true,
            true,
            true,
            [
                'getTagName'
            ]
        );
        $model->expects($this->any())
            ->method('getTagName')
            ->willReturn($tagNameResult);
        return $model;
    }
}
