<?php

namespace Isdoc\Tests\Models\DocumentReference;

use Isdoc\Exceptions\InvalidDateFormat;
use Cetria\Helpers\Reflection\Reflection;

class GetSetIssueDateTest extends TestCase
{
    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function get(string|null $newValue): void
    {
        $model = $this->getModel('test');
        Reflection::setHiddenProperty($model, 'issueDate', $newValue);
        $this->assertEquals($newValue, $model->getIssueDate());
    }

    public static function getSetDataProvider(): array
    {
        return [
            [
                null
            ], [
                '2020-02-02'
            ], [
                '2024-10-15'
            ]
        ];
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function set(string|null $newValue): void
    {
        $model = $this->getModel('test');
        $model->setIssueDate($newValue);
        $this->assertEquals($newValue, Reflection::getHiddenProperty($model, 'issueDate'));
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function getSet(string|null $newValue): void
    {
        $model = $this->getModel('test');
        $this->assertEquals($newValue, $model->setIssueDate($newValue)->getIssueDate());
    }

    /**
     * @test
     * @dataProvider invalidDateDataProvider
     */
    public function invalidDate(string $date): void
    {
        $this->expectException(InvalidDateFormat::class);
        $model = $this->getModel('test');
        $model->setIssueDate($date);
    }

    public static function invalidDateDataProvider(): array
    {
        return [
            [
                '2020-02-30'
            ], [
                '2011-13-01'
            ]
        ];
    }
}
