<?php

namespace Isdoc\Tests\Models\TaxedDeposit;

use Isdoc\Enums\CurrencyCode;
use Isdoc\Models\ExchangeRate;
use Isdoc\Models\TaxedDeposit;
use PHPUnit\Framework\TestCase;
use Cetria\Helpers\Reflection\Reflection;
use PHPUnit\Framework\MockObject\MockObject;
use Isdoc\Exceptions\ManuallySettedForeignCurrencyValueWithoutExchangeRate;

class GetSetTaxInclusiveDepositAmountCurrTest extends TestCase
{
    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function get(float|null $newValue): void
    {
        $model = $this->getMockedModel(true, $newValue, 2);
        Reflection::setHiddenProperty($model, 'taxInclusiveDepositAmountCurr', $newValue);
        $this->assertEquals($newValue, $model->getTaxInclusiveDepositAmountCurr());
    }

    private function getMockedModel(bool $hasForeignCurrency, float $valueInLocalCurrency, float $exchangeRate = 1): TaxedDeposit|MockObject
    {
        $foreignCurrencyCode = $hasForeignCurrency ? CurrencyCode::EUR : null;
        $exchangeRate = new ExchangeRate(CurrencyCode::CZK, $foreignCurrencyCode, $exchangeRate, 1);

        $mock = $this->getMockBuilder(TaxedDeposit::class)
            ->onlyMethods([
                'getTaxInclusiveDepositAmount',
            ])
            ->getMock();
        $mock->expects($this->any())
            ->method('getTaxInclusiveDepositAmount')
            ->willReturn($valueInLocalCurrency);

        Reflection::setHiddenProperty($mock, 'exchangeRate', $exchangeRate);

        return $mock;
    }

    public static function getSetDataProvider(): array
    {
        return [
            [
                12356
            ], [
                12.256
            ]
        ];
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function set(float|null $newValue): void
    {
        $model = $this->getMockedModel(true, $newValue, 2);
        $model->setTaxInclusiveDepositAmountCurr($newValue);
        $this->assertEquals($newValue, Reflection::getHiddenProperty($model, 'taxInclusiveDepositAmountCurr'));
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function getSet(float|null $newValue): void
    {
        $model = $this->getMockedModel(true, $newValue, 2);
        $this->assertEquals($newValue, $model->setTaxInclusiveDepositAmountCurr($newValue)->getTaxInclusiveDepositAmountCurr());
    }

    /**
     * @test
     */
    public function settedValueButDisabledForeignCurrency(): void
    {
        $this->expectException(ManuallySettedForeignCurrencyValueWithoutExchangeRate::class);
        $model = $this->getMockedModel(false, 50, 1);
        Reflection::setHiddenProperty($model, 'taxInclusiveDepositAmountCurr', 40);
        $model->getTaxInclusiveDepositAmountCurr();
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function automaticValue(float|null $value): void
    {
        $model = $this->getMockedModel(true, $value, 24);
        $this->assertEqualsWithDelta($value / 24, $model->getTaxInclusiveDepositAmountCurr(), 0.001);
    }

    /**
     * @test
     */
    public function withoutValue(): void
    {
        $model = $this->getMockedModel(false, 10, 1);
        $this->assertNull($model->getTaxInclusiveDepositAmountCurr());
    }
}
