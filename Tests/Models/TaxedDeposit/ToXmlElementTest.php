<?php

namespace Isdoc\Tests\Models\TaxedDeposit;

use Isdoc\Enums\CurrencyCode;
use Isdoc\Enums\VatCalculationMethod;
use Isdoc\Exceptions\MissingClassifiedTaxCategory;
use Isdoc\Models\ClassifiedTaxCategory;
use Isdoc\Models\ExchangeRate;
use Isdoc\Models\Price;
use Isdoc\Models\TaxedDeposit;
use PHPUnit\Framework\TestCase;

class ToXmlElementTest extends TestCase
{
    /**
     * @test
     */
    public function withoutTaxCategory(): void
    {
        $this->expectException(MissingClassifiedTaxCategory::class);
        $model = new TaxedDeposit();
        $model->setId('123')->setVariableSymbol('123');
        $model->toString();
    }

    /**
     * @test
     */
    public function onlyRequiredAttributes(): void
    {
        $classifiedTaxCategory = (new ClassifiedTaxCategory())->setPercent(10)->setVatCalculationMethod(VatCalculationMethod::ZDOLA);
        $price = (new Price(10, VatCalculationMethod::ZDOLA))->setValue(100);
        $model = new TaxedDeposit();
        $model->setClassifiedTaxCategory($classifiedTaxCategory)
            ->setTaxableDepositAmount($price->getValueWithoutVat())
            ->setId('123456')
            ->setVariableSymbol('123456');
        $result = $model->toString();
        $structure = json_decode(json_encode(simplexml_load_string($result)), true);
        $this->assertCount(5, $structure);
        $this->assertEquals($classifiedTaxCategory->getPercent(), $structure['ClassifiedTaxCategory']['Percent']);  
        $this->assertEquals($model->getVariableSymbol(), $structure['VariableSymbol']);  
        $this->assertEqualsWithDelta($price->getValueWithoutVat(), $structure['TaxableDepositAmount'], 0.001); 
        $this->assertEqualsWithDelta($price->getValueWithVat(), $structure['TaxInclusiveDepositAmount'], 0.001); 
        $this->assertEquals($model->getId(), $structure['ID']); 
    }

    /**
     * @test
     */
    public function withNecessaryParameters(): void
    {
        $classifiedTaxCategory = (new ClassifiedTaxCategory())->setPercent(10)->setVatCalculationMethod(VatCalculationMethod::ZDOLA);
        $price = (new Price(10, VatCalculationMethod::ZDOLA))->setValue(100);
        $model = new TaxedDeposit();
        $model->setClassifiedTaxCategory($classifiedTaxCategory)
            ->setTaxableDepositAmount($price->getValueWithoutVat())
            ->setId('1234567')
            ->setVariableSymbol('1234567')
            ->setExchangeRate(new ExchangeRate(CurrencyCode::CZK, CurrencyCode::CNY, 1, 1));
        $result = $model->toString();
        $structure = json_decode(json_encode(simplexml_load_string($result)), true);
        $this->assertCount(7, $structure);
        $this->assertEquals($classifiedTaxCategory->getPercent(), $structure['ClassifiedTaxCategory']['Percent']);  
        $this->assertEquals($model->getVariableSymbol(), $structure['VariableSymbol']);  
        $this->assertEqualsWithDelta($price->getValueWithoutVat(), $structure['TaxableDepositAmount'], 0.001); 
        $this->assertEqualsWithDelta($price->getValueWithoutVat(), $structure['TaxableDepositAmountCurr'], 0.001); 
        $this->assertEqualsWithDelta($price->getValueWithVat(), $structure['TaxInclusiveDepositAmount'], 0.001); 
        $this->assertEqualsWithDelta($price->getValueWithVat(), $structure['TaxInclusiveDepositAmountCurr'], 0.001); 
        $this->assertEquals($model->getId(), $structure['ID']);  
    }
}
