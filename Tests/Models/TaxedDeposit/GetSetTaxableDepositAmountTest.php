<?php

namespace Isdoc\Tests\Models\TaxedDeposit;

use Isdoc\Models\TaxedDeposit;
use PHPUnit\Framework\TestCase;
use Isdoc\Enums\VatCalculationMethod;
use Isdoc\Models\ClassifiedTaxCategory;
use Cetria\Helpers\Reflection\Reflection;
use PHPUnit\Framework\MockObject\MockObject;

class GetSetTaxableDepositAmountTest extends TestCase
{
    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function get(float|null $newValue): void
    {
        $model = new TaxedDeposit();
        Reflection::setHiddenProperty($model, 'taxableDepositAmount', $newValue);
        $this->assertEquals($newValue, $model->getTaxableDepositAmount());
    }

    private function getMockedModel(float $vatPercent, int $vatCalculationMethod, float|null $lineExtensionAmountTaxInclusive): TaxedDeposit|MockObject
    {
        $classifiedTaxCategory = (new ClassifiedTaxCategory())
            ->setPercent($vatPercent)
            ->setVatCalculationMethod($vatCalculationMethod);

        $mock = $this->getMockBuilder(TaxedDeposit::class)
            ->onlyMethods([
                'getClassifiedTaxCategory',
                'getTaxInclusiveDepositAmount',
            ])
            ->getMock();
        $mock->expects($this->any())
            ->method('getClassifiedTaxCategory')
            ->willReturn($classifiedTaxCategory);

        $mock->expects($this->any())
            ->method('getTaxInclusiveDepositAmount')
            ->willReturn($lineExtensionAmountTaxInclusive);

        return $mock;
    }

    public static function getSetDataProvider(): array
    {
        return [
            [
                12356
            ], [
                12.256
            ]
        ];
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function set(float|null $newValue): void
    {
        $model = new TaxedDeposit();
        $model->setTaxableDepositAmount($newValue);
        $this->assertEquals($newValue, Reflection::getHiddenProperty($model, 'taxableDepositAmount'));
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function getSet(float|null $newValue): void
    {
        $model = new TaxedDeposit();
        $this->assertEquals($newValue, $model->setTaxableDepositAmount($newValue)->getTaxableDepositAmount());
    }

    /**
     * @test
     */
    public function nullFromDown(): void
    {
        $model = $this->getMockedModel(10, VatCalculationMethod::ZDOLA, 50);
        $this->assertEquals(0, $model->getTaxableDepositAmount());
    }

    /**
     * @test
     */
    public function nullFromUp(): void
    {
        $model = $this->getMockedModel(10, VatCalculationMethod::SHORA, 50);
        $this->assertEquals(50/1.1, $model->getTaxableDepositAmount());
    }

    /**
     * @test
     */
    public function settedValueFromUp(): void
    {
        $model = $this->getMockedModel(10, VatCalculationMethod::SHORA, 50);
        Reflection::setHiddenProperty($model, 'taxableDepositAmount', 11);
        $this->assertEquals(11, $model->getTaxableDepositAmount());
    }
}
