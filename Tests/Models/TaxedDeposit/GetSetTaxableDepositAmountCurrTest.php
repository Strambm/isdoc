<?php

namespace Isdoc\Tests\Models\TaxedDeposit;

use Isdoc\Enums\CurrencyCode;
use Isdoc\Models\ExchangeRate;
use Isdoc\Models\TaxedDeposit;
use PHPUnit\Framework\TestCase;
use Cetria\Helpers\Reflection\Reflection;
use PHPUnit\Framework\MockObject\MockObject;
use Isdoc\Exceptions\ManuallySettedForeignCurrencyValueWithoutExchangeRate;

class GetSetTaxableDepositAmountCurrTest extends TestCase
{
    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function get(float|null $newValue): void
    {
        $model = $this->getMockedModel(true, $newValue, 2);
        Reflection::setHiddenProperty($model, 'taxableDepositAmountCurr', $newValue);
        $this->assertEquals($newValue, $model->getTaxableDepositAmountCurr());
    }

    private function getMockedModel(bool $hasForeignCurrency, float $valueInLocalCurrency, float $exchangeRate = 1): TaxedDeposit|MockObject
    {
        $foreignCurrencyCode = $hasForeignCurrency ? CurrencyCode::EUR : null;
        $exchangeRate = new ExchangeRate(CurrencyCode::CZK, $foreignCurrencyCode, $exchangeRate, 1);

        $mock = $this->getMockBuilder(TaxedDeposit::class)
            ->onlyMethods([
                'getTaxableDepositAmount',
            ])
            ->getMock();
        $mock->expects($this->any())
            ->method('getTaxableDepositAmount')
            ->willReturn($valueInLocalCurrency);

        Reflection::setHiddenProperty($mock, 'exchangeRate', $exchangeRate);

        return $mock;
    }

    public static function getSetDataProvider(): array
    {
        return [
            [
                12356
            ], [
                12.256
            ]
        ];
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function set(float|null $newValue): void
    {
        $model = $this->getMockedModel(true, $newValue, 2);
        $model->setTaxableDepositAmountCurr($newValue);
        $this->assertEquals($newValue, Reflection::getHiddenProperty($model, 'taxableDepositAmountCurr'));
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function getSet(float|null $newValue): void
    {
        $model = $this->getMockedModel(true, $newValue, 2);
        $this->assertEquals($newValue, $model->setTaxableDepositAmountCurr($newValue)->getTaxableDepositAmountCurr());
    }

    /**
     * @test
     */
    public function settedValueButDisabledForeignCurrency(): void
    {
        $this->expectException(ManuallySettedForeignCurrencyValueWithoutExchangeRate::class);
        $model = $this->getMockedModel(false, 50, 1);
        Reflection::setHiddenProperty($model, 'taxableDepositAmountCurr', 40);
        $model->getTaxableDepositAmountCurr();
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function automaticValue(float|null $value): void
    {
        $model = $this->getMockedModel(true, $value, 24);
        $this->assertEqualsWithDelta($value / 24, $model->getTaxableDepositAmountCurr(), 0.001);
    }

    /**
     * @test
     */
    public function withoutValue(): void
    {
        $model = $this->getMockedModel(false, 10, 1);
        $this->assertNull($model->getTaxableDepositAmountCurr());
    }
}
