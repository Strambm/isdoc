<?php

namespace Isdoc\Tests\Models\TaxedDeposit;

use Isdoc\Models\TaxedDeposit;
use PHPUnit\Framework\TestCase;
use Isdoc\Enums\VatCalculationMethod;
use Isdoc\Models\ClassifiedTaxCategory;
use Cetria\Helpers\Reflection\Reflection;
use PHPUnit\Framework\MockObject\MockObject;

class GetSetTaxInclusiveDepositAmountTest extends TestCase
{
    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function get(float|null $newValue): void
    {
        $model = new TaxedDeposit();
        Reflection::setHiddenProperty($model, 'taxInclusiveDepositAmount', $newValue);
        $this->assertEquals($newValue, $model->getTaxInclusiveDepositAmount());
    }

    private function getMockedModel(float $vatPercent, int $vatCalculationMethod, float|null $lineExtensionAmount): TaxedDeposit|MockObject
    {
        $classifiedTaxCategory = (new ClassifiedTaxCategory())
            ->setPercent($vatPercent)
            ->setVatCalculationMethod($vatCalculationMethod);

        $mock = $this->getMockBuilder(TaxedDeposit::class)
            ->onlyMethods([
                'getClassifiedTaxCategory',
                'getTaxableDepositAmount',
            ])
            ->getMock();
        $mock->expects($this->any())
            ->method('getClassifiedTaxCategory')
            ->willReturn($classifiedTaxCategory);

        $mock->expects($this->any())
            ->method('getTaxableDepositAmount')
            ->willReturn($lineExtensionAmount);

        return $mock;
    }

    public static function getSetDataProvider(): array
    {
        return [
            [
                12356
            ], [
                12.256
            ]
        ];
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function set(float|null $newValue): void
    {
        $model = new TaxedDeposit();
        $model->setTaxInclusiveAmount($newValue);
        $this->assertEquals($newValue, Reflection::getHiddenProperty($model, 'taxInclusiveDepositAmount'));
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function getSet(float|null $newValue): void
    {
        $model = new TaxedDeposit();
        $this->assertEquals($newValue, $model->setTaxInclusiveAmount($newValue)->getTaxInclusiveDepositAmount());
    }

    /**
     * @test
     */
    public function nullFromDown(): void
    {
        $model = $this->getMockedModel(10, VatCalculationMethod::ZDOLA, 50);
        $this->assertEqualsWithDelta(50 * 1.1 , $model->getTaxInclusiveDepositAmount(), 0.001);
    }

    /**
     * @test
     */
    public function nullFromUp(): void
    {
        $model = $this->getMockedModel(10, VatCalculationMethod::SHORA, 50);
        $this->assertEquals(0, $model->getTaxInclusiveDepositAmount());
    }

    /**
     * @test
     */
    public function settedValueFromDown(): void
    {
        $model = $this->getMockedModel(10, VatCalculationMethod::ZDOLA, 50);
        Reflection::setHiddenProperty($model, 'taxInclusiveDepositAmount', 11);
        $this->assertEquals(11, $model->getTaxInclusiveDepositAmount());
    }
}
