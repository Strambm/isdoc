<?php

namespace Isdoc\Tests\Models\BankAccount;

use Isdoc\Models\BankAccount;
use PHPUnit\Framework\TestCase;

class ToXmlElementTest extends TestCase
{
    /**
     * @test
     */
    public function onlyRequiredAttributes(): void
    {
        $model = (new BankAccount())
            ->setBankCode('123')
            ->setBic('BIC1')
            ->setIban('IBAN1')
            ->setId('1-123456')
            ->setName('NAME1');
        $result = $model->toString();
        $structure = json_decode(json_encode(simplexml_load_string($result)), true);
        $this->assertCount(5, $structure);
        $this->assertEquals('123', $structure['BankCode']);
        $this->assertEquals('BIC1', $structure['BIC']);
        $this->assertEquals('IBAN1', $structure['IBAN']);
        $this->assertEquals('1-123456', $structure['ID']);
        $this->assertEquals('NAME1', $structure['Name']);
    }
}
