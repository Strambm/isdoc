<?php

namespace Isdoc\Tests\Models\BankAccount;

use Isdoc\Exceptions\MissingBic;
use Isdoc\Models\BankAccount;
use PHPUnit\Framework\TestCase;
use Cetria\Helpers\Reflection\Reflection;

class GetSetBicTest extends TestCase
{
    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function get(string $newValue): void
    {
        $model = new BankAccount();
        Reflection::setHiddenProperty($model, 'bic', $newValue);
        $this->assertEquals($newValue, $model->getBic());
    }

    public static function getSetDataProvider(): array
    {
        return [
            [
                'KOMBCZPPXXX'
            ], [
                ''
            ]
        ];
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function set(string $newValue): void
    {
        $model = new BankAccount();
        $model->setBic($newValue);
        $this->assertEquals($newValue, Reflection::getHiddenProperty($model, 'bic'));
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function getSet(string $newValue): void
    {
        $model = new BankAccount();
        $this->assertEquals($newValue, $model->setBic($newValue)->getBic());
    }

    /**
     * @test
     */
    public function missingIban(): void
    {
        $this->expectException(MissingBic::class);
        $model = new BankAccount();
        $model->getBic();
    }
}
