<?php

namespace Isdoc\Tests\Models\BankAccount;

use Isdoc\Models\BankAccount;
use PHPUnit\Framework\TestCase;
use Isdoc\Exceptions\MissingBankId;
use Cetria\Helpers\Reflection\Reflection;

class GetSetIdTest extends TestCase
{
    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function get(string $newValue): void
    {
        $model = new BankAccount();
        Reflection::setHiddenProperty($model, 'id', $newValue);
        $this->assertEquals($newValue, $model->getId());
    }

    public static function getSetDataProvider(): array
    {
        return [
            [
                '123-12324'
            ], [
                '123456789'
            ]
        ];
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function set(string $newValue): void
    {
        $model = new BankAccount();
        $model->setId($newValue);
        $this->assertEquals($newValue, Reflection::getHiddenProperty($model, 'id'));
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function getSet(string $newValue): void
    {
        $model = new BankAccount();
        $this->assertEquals($newValue, $model->setId($newValue)->getId());
    }

    /**
     * @test
     */
    public function getWithoutValue(): void
    {
        $this->expectException(MissingBankId::class);
        $model = new BankAccount();
        $model->getId();
    }
}
