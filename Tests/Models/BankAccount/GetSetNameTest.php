<?php

namespace Isdoc\Tests\Models\BankAccount;

use Isdoc\Models\BankAccount;
use PHPUnit\Framework\TestCase;
use Cetria\Helpers\Reflection\Reflection;

class GetSetNameTest extends TestCase
{
    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function get(string $newValue): void
    {
        $model = new BankAccount();
        Reflection::setHiddenProperty($model, 'bankName', $newValue);
        $this->assertEquals($newValue, $model->getName());
    }

    public static function getSetDataProvider(): array
    {
        return [
            [
                'Fio banka, a.s.'
            ], [
                ''
            ]
        ];
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function set(string $newValue): void
    {
        $model = new BankAccount();
        $model->setName($newValue);
        $this->assertEquals($newValue, Reflection::getHiddenProperty($model, 'bankName'));
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function getSet(string $newValue): void
    {
        $model = new BankAccount();
        $this->assertEquals($newValue, $model->setName($newValue)->getName());
    }
}
