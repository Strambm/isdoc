<?php

namespace Isdoc\Tests\Models\BankAccount;

use Isdoc\Exceptions\MissingIban;
use Isdoc\Models\BankAccount;
use PHPUnit\Framework\TestCase;
use Cetria\Helpers\Reflection\Reflection;

class GetSetIbanTest extends TestCase
{
    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function get(string $newValue): void
    {
        $model = new BankAccount();
        Reflection::setHiddenProperty($model, 'iban', $newValue);
        $this->assertEquals($newValue, $model->getIban());
    }

    public static function getSetDataProvider(): array
    {
        return [
            [
                'CZ2001000000000661946821'
            ], [
                ''
            ]
        ];
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function set(string $newValue): void
    {
        $model = new BankAccount();
        $model->setIban($newValue);
        $this->assertEquals($newValue, Reflection::getHiddenProperty($model, 'iban'));
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function getSet(string $newValue): void
    {
        $model = new BankAccount();
        $this->assertEquals($newValue, $model->setIban($newValue)->getIban());
    }

    /**
     * @test
     */
    public function missingIban(): void
    {
        $this->expectException(MissingIban::class);
        $model = new BankAccount();
        $model->getIban();
    }
}
