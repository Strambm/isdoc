<?php

namespace Isdoc\Tests\Models\BankAccount;

use Isdoc\Exceptions\InvalidNumericFormat;
use Isdoc\Exceptions\MissingBankCode;
use Isdoc\Models\BankAccount;
use PHPUnit\Framework\TestCase;
use Cetria\Helpers\Reflection\Reflection;

class GetSetBankCodeTest extends TestCase
{
    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function get(string $newValue): void
    {
        $model = new BankAccount();
        Reflection::setHiddenProperty($model, 'bankCode', $newValue);
        $this->assertEquals($newValue, $model->getBankCode());
    }

    public static function getSetDataProvider(): array
    {
        return [
            [
                '0900'
            ], [
                '1234'
            ]
        ];
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function set(string $newValue): void
    {
        $model = new BankAccount();
        $model->setBankCode($newValue);
        $this->assertEquals($newValue, Reflection::getHiddenProperty($model, 'bankCode'));
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function getSet(string $newValue): void
    {
        $model = new BankAccount();
        $this->assertEquals($newValue, $model->setBankCode($newValue)->getBankCode());
    }

    /**
     * @test
     */
    public function getWithoutValue(): void
    {
        $this->expectException(MissingBankCode::class);
        $model = new BankAccount();
        $model->getBankCode();
    }

    /**
     * @test
     */
    public function setInvalidNumeric(): void
    {
        $this->expectException(InvalidNumericFormat::class);
        $model = new BankAccount();
        $model->setBankCode('/1234');
    }
}
