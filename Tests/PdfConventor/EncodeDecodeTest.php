<?php

namespace Isdoc\Tests\PdfConventor;

use Mpdf\Mpdf;
use Isdoc\PdfConventor;
use Isdoc\Models\Invoice;
use Mpdf\Output\Destination;
use PHPUnit\Framework\TestCase;
use Isdoc\Models\IsdocSimpleXMLElement;
use Cetria\Helpers\Reflection\Reflection;
use Isdoc\Tests\PdfConventor\FakeInvoiceParser;

class EncodeDecodeTest extends TestCase
{
    /**
     * @test
     */
    public function encodeDecode(): void
    {
        $invoice = $this->getInvoice();
        $conventor = new PdfConventor();
        Reflection::setHiddenProperty($conventor, 'invoiceParserClass', FakeInvoiceParser::class);
        $mpdf = new Mpdf();
        $mpdf->WriteHTML('<h1>Hello World</h1>');
        $mpdf = $conventor->encode($mpdf, $invoice);
        $fileContent = $mpdf->Output('tmp.php', Destination::STRING_RETURN);
        $invoiceResult = $conventor->decode($fileContent);
        $this->assertEquals($invoice->toString(true), $invoiceResult->xmlOriginal->asXML());
    }

    public function getInvoice(): Invoice
    {
        $model = $this->getMockBuilder(Invoice::class)
            ->onlyMethods(['toXmlElement', 'getId'])
            ->getMock();
        $model->expects($this->any())
            ->method('toXmlElement')
            ->willReturn(new IsdocSimpleXMLElement('<?xml version="1.0" encoding="UTF-8"?><Invoice><test>45</test></Invoice>'));
        $model->expects($this->any())
            ->method('getId')
            ->willReturn('');
        return $model;
    }
}
