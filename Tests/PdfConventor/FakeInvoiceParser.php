<?php

namespace Isdoc\Tests\PdfConventor;

use SimpleXMLElement;
use Isdoc\Parser\Invoice as Parser;
use Isdoc\Models\Invoice as ModelInvoice;

class FakeInvoiceParser extends Parser
{
    public function parseXml(SimpleXMLElement $xml): ModelInvoice
    {
        $result = new ModelInvoice();
        $result->xmlOriginal = $xml;
        return $result;
    }
}
