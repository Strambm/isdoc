<?php

namespace Isdoc\Tests\PdfConventor;

use Isdoc\Models\Invoice;
use Isdoc\Tests\Examples\ExampleProvider;
use Isdoc\PdfConventor;
use PHPUnit\Framework\TestCase;

class DecodeDocumentTest extends TestCase
{
    /**
     * @test
     * @dataProvider dataProvider
     */
    public function decodeTestFile(string $filePath): void
    {
        $content =  file_get_contents($filePath);
        $conventor = new PdfConventor();
        $result = $conventor->decode($content);
        $this->assertInstanceOf(Invoice::class, $result);
    }

    public static function dataProvider(): array
    {
        return [
            [
                ExampleProvider::getExamplePath('test001.isdoc.pdf'),
            ]
        ];
    }
}
