<?php

namespace Isdoc\Tests\Examples;

class ExampleProvider
{
    public static function getExamplePath(string $example): string
    {
        return __DIR__ . '/' . $example;
    }
}
