<?php

namespace Isdoc\Tests\Parser\TaxTotal;

use Isdoc\Models\TaxCategory;
use Isdoc\Models\TaxSubTotal;
use Isdoc\Models\TaxTotal;
use PHPUnit\Framework\TestCase;
use Isdoc\Parser\TaxTotal as Parser;

class ParseXmlTest extends TestCase
{
    /**
     * @test
     * @dataProvider parseDataProvider
     */
    public function parse(TaxTotal $original): void
    {
        $originalXmlString = $original->toString();
        $parser = (new Parser());
        $parsedXmlString = $parser->parseXml(simplexml_load_string($originalXmlString))->toString();
        $this->assertEquals($originalXmlString, $parsedXmlString);
    }

    public static function parseDataProvider(): array
    {
        return [
            [
                (new TaxTotal)
                    ->addTaxSubTotal((new TaxSubTotal)->setTaxCategory((new TaxCategory)->setPercent(0)))
            ], [
                (new TaxTotal)
                    ->addTaxSubTotal((new TaxSubTotal)->setTaxCategory((new TaxCategory)->setPercent(0)))
                    ->addTaxSubTotal((new TaxSubTotal)->setTaxCategory((new TaxCategory)->setPercent(20))->setTaxAmount(50))
            ], [
                (new TaxTotal)
                    ->addTaxSubTotal((new TaxSubTotal)->setTaxCategory((new TaxCategory)->setPercent(0)))
                    ->addTaxSubTotal((new TaxSubTotal)->setTaxCategory((new TaxCategory)->setPercent(20))->setTaxAmount(50)->setTaxableAmountCurr(25))
            ]
        ];
    }
}
