<?php

namespace Isdoc\Tests\Parser\BankAccount;

use Isdoc\Models\BankAccount;
use PHPUnit\Framework\TestCase;
use Isdoc\Parser\BankAccount as Parser;

class ParseXmlTest extends TestCase
{
    /**
     * @test
     * @dataProvider parseDataProvider
     */
    public function parse(BankAccount $original): void
    {
        $originalXmlString = $original->toString();
        $parser = (new Parser());
        $parsedXmlString = $parser->parseXml(simplexml_load_string($originalXmlString))->toString();
        $this->assertEquals($originalXmlString, $parsedXmlString);
    }

    public static function parseDataProvider(): array
    {
        return [
            [
                (new BankAccount)
                    ->setBankCode('0111')
                    ->setBic('BICHIEHF')
                    ->setIban('15641IBAN418')
                    ->setId('1548-4184841')
                    ->setName('BANKA')
            ]
        ];
    }
}
