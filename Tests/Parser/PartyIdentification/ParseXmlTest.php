<?php

namespace Isdoc\Tests\Parser\PartyIdentification;

use Isdoc\Models\PartyIdentification;
use PHPUnit\Framework\TestCase;
use Isdoc\Parser\PartyIdentification as ParserPartyIdentification;

class ParseXmlTest extends TestCase
{
    /**
     * @test
     * @dataProvider parseDataProvider
     */
    public function parse(PartyIdentification $original): void
    {
        $originalXmlString = $original->toString();
        $parser = (new ParserPartyIdentification());
        $parsedXmlString = $parser->parseXml(simplexml_load_string($originalXmlString))->toString();
        $this->assertEquals($originalXmlString, $parsedXmlString);
    }

    public static function parseDataProvider(): array
    {
        return [
            [
                (new PartyIdentification())
                    ->setId('123456')
            ], [
                (new PartyIdentification())
                    ->setId('15815')
                    ->setUserId('151515')
                    ->setCatalogFirmIdentification('2025-01-01')
            ]
        ];
    }
}
