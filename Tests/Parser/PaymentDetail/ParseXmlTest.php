<?php

namespace Isdoc\Tests\Parser\PaymentDetail;

use Isdoc\Models\BankAccount;
use Isdoc\Models\PaymentDetailBankTransaction;
use Isdoc\Models\PaymentDetailCash;
use Isdoc\Models\PaymentDetails;
use PHPUnit\Framework\TestCase;
use Isdoc\Parser\PaymentDetail as Parser;

class ParseXmlTest extends TestCase
{
    /**
     * @test
     * @dataProvider parseDataProvider
     */
    public function parse(PaymentDetails $original): void
    {
        $originalXmlString = $original->toString();
        $parser = (new Parser());
        $parsedXmlString = $parser->parseXml(simplexml_load_string($originalXmlString))->toString();
        $this->assertEquals($originalXmlString, $parsedXmlString);
    }

    public static function parseDataProvider(): array
    {
        return [
            [
                (new PaymentDetailCash())
                    ->setDocumentId('123456')
                    ->setIssueDate('2022-01-01')
            ], [
                (new PaymentDetailCash())
                    ->setIssueDate('2022-01-01')
            ], [
                (new PaymentDetailBankTransaction())
                    ->setBankAccount((new BankAccount)
                        ->setBankCode('0111')
                        ->setBic('BICHIEHF')
                        ->setIban('15641IBAN418')
                        ->setId('1548-4184841')
                        ->setName('BANKA')
                    )->setPaymentDueDate('2021-01-01')
            ], [
                (new PaymentDetailBankTransaction())
                    ->setBankAccount((new BankAccount)
                        ->setBankCode('0111')
                        ->setBic('BICHIEHF')
                        ->setIban('15641IBAN418')
                        ->setId('1548-4184841')
                        ->setName('BANKA')
                    )->setPaymentDueDate('2021-01-01')
                    ->setVs('8453')
                    ->setConstantSymbol('800')
                    ->setSpecificSymbol('777')
            ]
        ];
    }
}
