<?php

namespace Isdoc\Tests\Parser\PartyTaxScheme;

use Isdoc\Enums\TaxScheme;
use Isdoc\Models\PartyTaxScheme;
use PHPUnit\Framework\TestCase;
use Isdoc\Parser\PartyTaxScheme as ParserPartyTaxScheme;

class ParseXmlTest extends TestCase
{
    /**
     * @test
     * @dataProvider parseDataProvider
     */
    public function parse(PartyTaxScheme $original): void
    {
        $originalXmlString = $original->toString();
        $parser = (new ParserPartyTaxScheme());
        $parsedXmlString = $parser->parseXml(simplexml_load_string($originalXmlString))->toString();
        $this->assertEquals($originalXmlString, $parsedXmlString);
    }

    public static function parseDataProvider(): array
    {
        return [
            [
                (new PartyTaxScheme('123456', TaxScheme::TIN))
            ], [
                (new PartyTaxScheme('1', TaxScheme::VAT))
            ]
        ];
    }
}
