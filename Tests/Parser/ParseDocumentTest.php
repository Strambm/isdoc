<?php

namespace Isdoc\Tests\Parser;
use Isdoc\Parser\Invoice as Parser;
use PHPUnit\Framework\TestCase;
use Isdoc\Tests\Examples\ExampleProvider;

class ParseDocumentTest extends TestCase
{
    /**
     * @test
     * @dataProvider backtrackDataProvider
     */
    public function backtrack(string $filePath): void
    {
        $content = file_get_contents($filePath);
        $xml = simplexml_load_string($content);
        $parser = new Parser();
        $invoice = $parser->parseXml($xml);
        $resultXml = $invoice->toXmlElement();
        $this->assertCompareXmlValues(json_encode($xml), json_encode($resultXml));
    }

    public static final function backtrackDataProvider(): array
    {
        return [
            [
                ExampleProvider::getExamplePath('example001.isdoc'),
            ], [
                ExampleProvider::getExamplePath('exampleOptys001.isdoc'),
            ], [
                ExampleProvider::getExamplePath('exampleGiga001.isdoc')
            ]
        ];
    }

    private function assertCompareXmlValues(string $originalString, string $responseString): void
    {
        $array1 = json_decode($originalString, true);
        $array2 = json_decode($responseString, true);

        $this->compareDimension($array1, $array2);
    }

    private function compareDimension(array $xmlDimension1, array $xmlDimension2): void
    {
        foreach(array_keys($xmlDimension1) as $key) {
            if(!array_key_exists($key, $xmlDimension2)) {
                if(!$this->isEmptyValue($xmlDimension1[$key])) {
                    dd($key, $xmlDimension1[$key]);
                }
                continue;
            }
            $val1 = $xmlDimension1[$key];
            $val2 = $xmlDimension2[$key];
            if($key == 'version') {
                continue;
            } elseif($key == 'TaxSubTotal') {
                $this->compareTaxSubTotals($val1, $val2);
            } elseif(is_array($val1) && is_array($val2)) {
                $this->compareDimension($val1, $val2);
            } else {
                if($val1 != $val2 && $this->isEmptyValue($val1)) {
                    continue;
                }
                if($val1 !== $val2) {
                    dd($key, $xmlDimension1, $xmlDimension2);
                } 
                $this->assertEquals($val1, $val2, $key);
            }
        }
    }

    private function isEmptyValue(mixed $value): bool
    {
        if(is_array($value)) {
            if(count($value) > 0) {
                foreach($value as $val) {
                    if($this->isEmptyValue($val) == false) {
                        return false;
                    }
                }
            }
        } else {
            return is_null($value) || $value == '';
        }
        return true;
    }

    private function compareTaxSubTotals(array $taxSubTotalOriginal, array $taxSubTotalResult): void
    {
        if(array_key_exists('TaxCategory', $taxSubTotalOriginal)) {
            $taxSubTotalOriginal = [$taxSubTotalOriginal];
        }
        if(array_key_exists('TaxCategory', $taxSubTotalResult)) {
            $taxSubTotalResult = [$taxSubTotalResult];
        }
        foreach($taxSubTotalOriginal as $row) {
            if($row['TaxInclusiveAmount'] == 0) {
                continue;
            }
            $vatPercent = $row['TaxCategory']['Percent'];
            $filtered = current(array_filter($taxSubTotalResult, function(array $taxSubTotalXml) use ($vatPercent): bool {
                return $vatPercent == $taxSubTotalXml['TaxCategory']['Percent'];
            }));
            $this->compareDimension($row, $filtered);
        }
    }
}
