<?php

namespace Isdoc\Tests\Parser\PaymentMeans;

use Isdoc\Models\BankAccount;
use Isdoc\Models\Payment;
use Isdoc\Models\PaymentMeans;
use PHPUnit\Framework\TestCase;
use Isdoc\Enums\PaymentMeansCode;
use Isdoc\Models\PaymentDetailBankTransaction;
use Isdoc\Parser\PaymentMeans as Parser;

class ParseXmlTest extends TestCase
{
    /**
     * @test
     * @dataProvider parseDataProvider
     */
    public function parse(PaymentMeans $original): void
    {
        $originalXmlString = $original->toString();
        $parser = (new Parser());
        $parsedXmlString = $parser->parseXml(simplexml_load_string($originalXmlString))->toString();
        $this->assertEquals($originalXmlString, $parsedXmlString);
    }

    public static function parseDataProvider(): array
    {
        return [
            [
                (new PaymentMeans())
                    ->addPayment(
                        (new Payment())
                            ->setHasPartialPayment(true)
                            ->setPaidAmount(11)
                            ->setPaymentMeansCode(PaymentMeansCode::CASH)
                    )
            ], [
                (new PaymentMeans())
                    ->addPayment(
                        (new Payment())
                            ->setHasPartialPayment(true)
                            ->setPaidAmount(11)
                            ->setPaymentMeansCode(PaymentMeansCode::CASH)
                    )->addPayment(
                        (new Payment())
                            ->setHasPartialPayment(true)
                            ->setPaidAmount(11)
                            ->setPaymentMeansCode(PaymentMeansCode::BANK_TRANSFER)
                            ->setDetails(
                                (new PaymentDetailBankTransaction())
                                    ->setBankAccount(
                                        (new BankAccount())
                                            ->setBic('BIC1')
                                            ->setIban('IBAN1')
                                            ->setId('898-88888')
                                            ->setName('BANKA NAME')
                                            ->setBankCode('500')
                                    )->setPaymentDueDate('2022-01-01')
                            )
                    )->addAlternativeBankAccount(
                        (new BankAccount())
                            ->setBic('BIC2')
                            ->setIban('IBAN2')
                            ->setId('898-88888')
                            ->setName('BANKA NAME')
                            ->setBankCode('500')
                    )->addAlternativeBankAccount(
                        (new BankAccount())
                            ->setBic('BIC3')
                            ->setIban('IBAN3')
                            ->setId('898-88888')
                            ->setName('BANKA NAME')
                            ->setBankCode('500')
                    )
            ],
        ];
    }
}
