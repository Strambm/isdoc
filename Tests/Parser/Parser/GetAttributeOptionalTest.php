<?php

namespace Isdoc\Tests\Parser\Parser;

use SimpleXMLElement;
use Isdoc\Parser\Parser;
use PHPUnit\Framework\TestCase;

class GetAttributeOptionalTest extends TestCase
{
    /**
     * @test
     */
    public function hasValue(): void
    {
        $object = $this->getObject();
        $xml = new SimpleXMLElement('<test><value>5</value></test>');
        $object->getAttributeOptional($xml->value);
        $this->assertEquals(5, $object->getAttributeOptional($xml->value));
    }

    /**
     * @test
     */
    public function withoutValue(): void
    {
        $object = $this->getObject();
        $xml = new SimpleXMLElement('<test><value/></test>');
        $object->getAttributeOptional($xml->value);
        $this->assertNull($object->getAttributeOptional($xml->value));
    }

    /**
     * @test
     */
    public function emptyString(): void
    {
        $object = $this->getObject();
        $xml = new SimpleXMLElement('<test><value></value></test>');
        $object->getAttributeOptional($xml->value);
        $this->assertNull($object->getAttributeOptional($xml->value));
    }

    public function getObject(): Parser
    {
        return $this->getMockForAbstractClass(Parser::class);
    }
}
