<?php

namespace Isdoc\Tests\Parser\Parser;

use Isdoc\Exceptions\UnimplementedException;
use Isdoc\Parser\Invoice;
use Isdoc\Parser\Parser;
use PHPUnit\Framework\TestCase;
use Cetria\Helpers\Reflection\Reflection;

class CallTest extends TestCase
{
    /**
     * @test
     */
    public function getParserCorrect(): void
    {
        $object = $this->getObject();
        Reflection::setHiddenProperty($object, 'parsers', ['test' => Invoice::class]);
        $result = $object->getTestParser();
        $this->assertInstanceOf(Invoice::class, $result);
    }

    /**
     * @test
     */
    public function shortMethodName(): void
    {
        $this->expectException(UnimplementedException::class);
        $object = $this->getObject();
        $object->a();
    }

    /**
     * @test
     */
    public function getParserIncorrect(): void
    {
        $this->expectException(UnimplementedException::class);
        $object = $this->getObject();
        $object->getTestParser();
    }

    public function getObject(): Parser
    {
        return $this->getMockForAbstractClass(Parser::class);
    }
}
