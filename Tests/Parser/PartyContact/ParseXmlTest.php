<?php

namespace Isdoc\Tests\Parser\PartyContact;

use Isdoc\Enums\TaxScheme;
use Isdoc\Models\Contact;
use Isdoc\Models\PartyIdentification;
use Isdoc\Models\PartyContact;
use Isdoc\Models\PartyTaxScheme;
use Isdoc\Models\PostalAddress;
use PHPUnit\Framework\TestCase;
use Isdoc\Parser\PartyContact as Parser;

class ParseXmlTest extends TestCase
{
    /**
     * @test
     * @dataProvider parseDataProvider
     */
    public function parse(PartyContact $original): void
    {
        $originalXmlString = $original->toString();
        $parser = (new Parser());
        $model = $parser->parseXml(simplexml_load_string($originalXmlString));
        $parsedXmlString = $model->toString();
        $this->assertEquals($originalXmlString, $parsedXmlString);
        $this->assertEquals(get_class($original), get_class($model));
    }

    public static function parseDataProvider(): array
    {
        return [
            [
                (new PartyContact())
                    ->setContact(
                        (new Contact())
                            ->setName('Test Testovič')
                            ->setElectronicMail('test@isdoc.com')
                            ->setTelephone('+420 604 599 599')
                    )->setPartyIdentification(
                        (new PartyIdentification())
                            ->setId('12345789')
                    )->setName('Test Testovič')
                    ->setPostalAddress(
                        (new PostalAddress())
                            ->setBuildingNumber('2805/25')
                            ->setCityName('Brno')
                            ->setPostalZone('65982')
                            ->setStreetName('Tečstovací ulice')
                    )
            ], [
                (new PartyContact())
                    ->setContact(
                        (new Contact())
                            ->setName('Test Testovič')
                            ->setElectronicMail('test@isdoc.com')
                            ->setTelephone('+420 604 599 599')
                    )->setPartyIdentification(
                        (new PartyIdentification())
                            ->setId('12345789')
                    )->setName('Test Testovič')
                    ->setPostalAddress(
                        (new PostalAddress())
                            ->setBuildingNumber('2805/25')
                            ->setCityName('Brno')
                            ->setPostalZone('65982')
                            ->setStreetName('Tečstovací ulice')
                    )->addPartyTaxScheme(new PartyTaxScheme('123456', TaxScheme::VAT))
            ], [
                (new PartyContact())
                    ->setContact(
                        (new Contact())
                            ->setName('Test Testovič')
                            ->setElectronicMail('test@isdoc.com')
                            ->setTelephone('+420 604 599 599')
                    )->setPartyIdentification(
                        (new PartyIdentification())
                            ->setId('12345789')
                    )->setName('Test Testovič')
                    ->setPostalAddress(
                        (new PostalAddress())
                            ->setBuildingNumber('2805/25')
                            ->setCityName('Brno')
                            ->setPostalZone('65982')
                            ->setStreetName('Tečstovací ulice')
                    )->addPartyTaxScheme(new PartyTaxScheme('123456', TaxScheme::VAT))
                    ->addPartyTaxScheme(new PartyTaxScheme('123456', TaxScheme::TIN))
            ]
        ];
    }
}
