<?php

namespace Isdoc\Tests\Parser\NoteParser;

use Isdoc\Enums\LanguageCode;
use Isdoc\Models\Note;
use Isdoc\Models\NoteType;
use Isdoc\Models\VatNote;
use PHPUnit\Framework\TestCase;

class ParseXmlTest extends TestCase
{
    /**
     * @test
     * @dataProvider parseDataProvider
     */
    public function parse(NoteType $original, string $parserClass): void
    {
        $originalXmlString = $original->toString();
        $parser = (new $parserClass());
        $model = $parser->parseXml(simplexml_load_string($originalXmlString));
        $parsedXmlString = $model->toString();
        $this->assertEquals($originalXmlString, $parsedXmlString);
        $this->assertEquals(get_class($original), get_class($model));
    }

    public static function parseDataProvider(): array
    {
        return [
            [
                (new Note())
                    ->setLanguageId(LanguageCode::FI)
                    ->setValue('TEST'),
                \Isdoc\Parser\Note::class
            ], [
                (new VatNote())
                    ->setLanguageId(LanguageCode::CS)
                    ->setValue('§ 64 odst. 1 ZDPH'),
                \Isdoc\Parser\VatNote::class
            ], [
                (new VatNote())
                    ->setValue('§ 64 odst. 1 ZDPH'),
                \Isdoc\Parser\VatNote::class
            ]
        ];
    }
}
