<?php

namespace Isdoc\Tests\Parser\Payment;

use Isdoc\Models\BankAccount;
use Isdoc\Models\Payment;
use PHPUnit\Framework\TestCase;
use Isdoc\Enums\PaymentMeansCode;
use Isdoc\Models\PaymentDetailCash;
use Isdoc\Models\PaymentDetailBankTransaction;
use Isdoc\Parser\Payment as Parser;

class ParseXmlTest extends TestCase
{
    /**
     * @test
     * @dataProvider parseDataProvider
     */
    public function parse(Payment $original): void
    {
        $originalXmlString = $original->toString();
        $parser = (new Parser());
        $parsedXmlString = $parser->parseXml(simplexml_load_string($originalXmlString))->toString();
        $this->assertEquals($originalXmlString, $parsedXmlString);
    }

    public static function parseDataProvider(): array
    {
        return [
            [
                (new Payment())
                    ->setHasPartialPayment(true)
                    ->setPaidAmount(11)
                    ->setPaymentMeansCode(PaymentMeansCode::CASH)
            ], [
                (new Payment())
                    ->setHasPartialPayment(true)
                    ->setPaidAmount(11)
                    ->setPaymentMeansCode(PaymentMeansCode::CASH)
                    ->setDetails(
                        (new PaymentDetailCash())
                            ->setDocumentId('12')
                            ->setIssueDate('2020-01-01')
                    )
            ], [
                (new Payment())
                    ->setHasPartialPayment(true)
                    ->setPaidAmount(11)
                    ->setPaymentMeansCode(PaymentMeansCode::BANK_TRANSFER)
                    ->setDetails(
                        (new PaymentDetailBankTransaction())
                            ->setBankAccount(
                                (new BankAccount())
                                    ->setBic('BIC1')
                                    ->setIban('IBAN1')
                                    ->setId('898-88888')
                                    ->setName('BANKA NAME')
                                    ->setBankCode('500')
                            )->setPaymentDueDate('2022-01-01')
                    )
            ]
        ];
    }
}
