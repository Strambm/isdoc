<?php

namespace Isdoc\Tests\Parser\NonTaxedDeposit;

use Isdoc\Enums\CurrencyCode;
use Isdoc\Models\ExchangeRate;
use Isdoc\Models\NonTaxedDeposit;
use PHPUnit\Framework\TestCase;
use Isdoc\Parser\NonTaxedDeposit as Parser;

class ParseXmlTest extends TestCase
{
    /**
     * @test
     * @dataProvider parseDataProvider
     */
    public function parse(NonTaxedDeposit $original): void
    {
        $originalXmlString = $original->toString();
        $parser = (new Parser());
        $parsedXmlString = $parser->parseXml(simplexml_load_string($originalXmlString))->toString();
        $this->assertEquals($originalXmlString, $parsedXmlString);
    }

    public static function parseDataProvider(): array
    {
        return [
            [
                (new NonTaxedDeposit())
                    ->setId('123456')
                    ->setVariableSymbol('VS123456')
                    ->setDepositAmount(100)
                    
            ], [
                (new NonTaxedDeposit())
                    ->setId('55')
                    ->setVariableSymbol('55')
                    ->setDepositAmount(110)
                    ->setExchangeRate(new ExchangeRate(CurrencyCode::CZK, CurrencyCode::EUR, 1, 25.36))
            ], 
        ];
    }
}
