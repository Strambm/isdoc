<?php

namespace Isdoc\Tests\Parser\ClassifiedTaxCategory;

use PHPUnit\Framework\TestCase;
use Isdoc\Models\ClassifiedTaxCategory;
use Isdoc\Parser\ClassifiedTaxCategory as ParserClassifiedTaxCategory;

class ParseXmlTest extends TestCase
{
    /**
     * @test
     * @dataProvider parseDataProvider
     */
    public function parse(ClassifiedTaxCategory $original): void
    {
        $originalXmlString = $original->toString();
        $parser = (new ParserClassifiedTaxCategory());
        $parsedXmlString = $parser->parseXml(simplexml_load_string($originalXmlString))->toString();
        $this->assertEquals($originalXmlString, $parsedXmlString);
    }

    public static function parseDataProvider(): array
    {
        return [
            [
                (new ClassifiedTaxCategory())
                    ->setPercent(12.5)
                    ->setVatApplicable(true)
                    ->setVatCalculationMethod(0)
            ], [
                (new ClassifiedTaxCategory())
                    ->setPercent(10)
                    ->setVatCalculationMethod(1)
            ], [
                (new ClassifiedTaxCategory())
                    ->setLocalReverseCharge(false)
                    ->setPercent(8)
                    ->setVatCalculationMethod(1)
                    ->setVatApplicable(true)
            ]
        ];
    }
}
