<?php

namespace Isdoc\Tests\Parser\InvoiceLine;

use Isdoc\Models\Item;
use Isdoc\Models\Note;
use Isdoc\Models\VatNote;
use Isdoc\Enums\CurrencyCode;
use Isdoc\Enums\LanguageCode;
use Isdoc\Models\InvoiceLine;
use Isdoc\Models\ExchangeRate;
use PHPUnit\Framework\TestCase;
use Isdoc\Enums\VatCalculationMethod;
use Isdoc\Models\InvoiceLineReference;
use Isdoc\Models\ClassifiedTaxCategory;

class ParseXmlTest extends TestCase
{
    /**
     * @test
     * @dataProvider parseDataProvider
     */
    public function parse(InvoiceLine $original): void
    {
        $originalXmlString = $original->toString();
        $parser = (new \Isdoc\Parser\InvoiceLine());
        $parsedXmlString = $parser->parseXml(simplexml_load_string($originalXmlString))->toString();
        $this->assertEquals($originalXmlString, $parsedXmlString);
    }

    public static function parseDataProvider(): array
    {
        return [
            [
                (new InvoiceLine())
                    ->setId('123456')
                    ->setLineExtensionAmount(50)
                    ->setLineExtensionAmountTaxInclusive(500)
                    ->setLineExtensionTaxAmount(10)
                    ->setUnitPrice(5)
                    ->setUnitPriceTaxInclusive(11)
                    ->setClassifiedTaxCategory((new ClassifiedTaxCategory())->setPercent(1)->setVatCalculationMethod(VatCalculationMethod::SHORA))
            ], [
                (new InvoiceLine())
                    ->setId('1')
                    ->setLineExtensionAmount(100)
                    ->setLineExtensionAmountBeforeDiscount(150)
                    ->setInvoicedQuantity(2)
                    ->setOrderReference(new InvoiceLineReference('123', '123'))
                    ->setDeliveryNoteReference(new InvoiceLineReference('123', '123'))
                    ->setNote((new Note())->setLanguageId(LanguageCode::CS)->setValue('test'))
                    ->setVatNote((new VatNote())->setValue('test2')->setLanguageId(LanguageCode::BG))
                    ->setOriginalDocumentReference(new InvoiceLineReference('123', '123'))
                    ->setInvoiceQuantityUnitCode('dKg')
                    ->setClassifiedTaxCategory((new ClassifiedTaxCategory())->setPercent(12.5)->setVatCalculationMethod(VatCalculationMethod::ZDOLA))
                    ->setItem((new Item())->setDescription('ItemDescription'))
            ], [
                (new InvoiceLine())
                    ->setId('123456')
                    ->setLineExtensionAmount(50)
                    ->setLineExtensionAmountTaxInclusive(500)
                    ->setLineExtensionTaxAmount(10)
                    ->setUnitPrice(5)
                    ->setUnitPriceTaxInclusive(11)
                    ->setClassifiedTaxCategory((new ClassifiedTaxCategory())->setPercent(1)->setVatCalculationMethod(VatCalculationMethod::SHORA))
                    ->setExchangeRate(new ExchangeRate(CurrencyCode::CZK, CurrencyCode::CNY, 1, 0.5))
            ]
        ];
    }
}
