<?php

namespace Isdoc\Tests\Parser\Country;

use Isdoc\Enums\CountryCode;
use Isdoc\Models\Country;
use PHPUnit\Framework\TestCase;
use Isdoc\Parser\Country as ParserCountry;

class ParseXmlTest extends TestCase
{
    /**
     * @test
     * @dataProvider parseDataProvider
     */
    public function parse(Country $original): void
    {
        $originalXmlString = $original->toString();
        $parser = (new ParserCountry());
        $parsedXmlString = $parser->parseXml(simplexml_load_string($originalXmlString))->toString();
        $this->assertEquals($originalXmlString, $parsedXmlString);
    }

    public static function parseDataProvider(): array
    {
        return [
            [
                (new Country())
                    ->setName('Test')
                    ->setIdentificationCode(CountryCode::BE)
            ], [
                (new Country())
                    ->setIdentificationCode(CountryCode::AT)
            ]
        ];
    }
}
