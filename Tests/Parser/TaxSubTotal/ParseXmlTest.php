<?php

namespace Isdoc\Tests\Parser\TaxSubTotal;

use Isdoc\Enums\CurrencyCode;
use Isdoc\Enums\VatCalculationMethod;
use Isdoc\Models\ClassifiedTaxCategory;
use Isdoc\Models\ExchangeRate;
use Isdoc\Models\InvoiceLine;
use Isdoc\Models\TaxCategory;
use Isdoc\Models\TaxSubTotal;
use PHPUnit\Framework\TestCase;
use Isdoc\Parser\TaxSubTotal as Parser;

class ParseXmlTest extends TestCase
{
    /**
     * @test
     * @dataProvider parseDataProvider
     */
    public function parse(TaxSubTotal $original): void
    {
        $originalXmlString = $original->toString();
        $parser = (new Parser());
        $parsedXmlString = $parser->parseXml(simplexml_load_string($originalXmlString))->toString();
        $this->assertEquals($originalXmlString, $parsedXmlString);
    }

    public static function parseDataProvider(): array
    {
        return [
            [
                (new TaxSubTotal())
                    ->setTaxCategory((new TaxCategory)->setPercent(11))
            ], [
                (new TaxSubTotal())
                    ->setTaxCategory((new TaxCategory)->setPercent(11))
                    ->addInvoiceLine((new InvoiceLine)
                        ->setClassifiedTaxCategory((new ClassifiedTaxCategory)->setPercent(11)->setVatCalculationMethod(VatCalculationMethod::SHORA))
                        ->setLineExtensionAmountTaxInclusive(500)
                    )
            ], [
                (new TaxSubTotal())
                    ->setTaxCategory((new TaxCategory)->setPercent(11))
                    ->addInvoiceLine((new InvoiceLine)
                        ->setClassifiedTaxCategory((new ClassifiedTaxCategory)->setPercent(11)->setVatCalculationMethod(VatCalculationMethod::SHORA))
                        ->setLineExtensionAmountTaxInclusive(500)
                        ->setExchangeRate(new ExchangeRate(CurrencyCode::CZK, CurrencyCode::USD, 1, 24))
                    )
            ], [
                (new TaxSubTotal())
                    ->setTaxCategory((new TaxCategory)->setPercent(11))
                    ->addInvoiceLine((new InvoiceLine)
                        ->setClassifiedTaxCategory((new ClassifiedTaxCategory)->setPercent(11)->setVatCalculationMethod(VatCalculationMethod::SHORA))
                        ->setLineExtensionAmountTaxInclusive(500)
                    )
                    ->setAlreadyClaimedTaxableAmount(180.18)
                    ->setAlreadyClaimedTaxAmount(19.82)
                    ->setAlreadyClaimedTaxInclusiveAmount(200)
            ], [
                (new TaxSubTotal())
                    ->setTaxCategory((new TaxCategory)->setPercent(11))
                    ->addInvoiceLine((new InvoiceLine)
                        ->setClassifiedTaxCategory((new ClassifiedTaxCategory)->setPercent(11)->setVatCalculationMethod(VatCalculationMethod::SHORA))
                        ->setLineExtensionAmountTaxInclusive(500)
                        ->setExchangeRate(new ExchangeRate(CurrencyCode::CZK, CurrencyCode::USD, 1, 24))
                    )
                    ->setAlreadyClaimedTaxableAmount(180.18)
                    ->setAlreadyClaimedTaxAmount(19.82)
                    ->setAlreadyClaimedTaxInclusiveAmount(200)
                    ->setAlreadyClaimedTaxableAmountCurr(7.507)
                    ->setAlreadyClaimedTaxAmountCurr(0.825)
                    ->setAlreadyClaimedTaxInclusiveAmountCurr(8.333)
            ]
        ];
    }
}
