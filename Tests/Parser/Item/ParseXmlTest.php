<?php

namespace Isdoc\Tests\Parser\Item;

use Isdoc\Models\Item;
use PHPUnit\Framework\TestCase;

class ParseXmlTest extends TestCase
{
    /**
     * @test
     * @dataProvider parseDataProvider
     */
    public function parse(Item $original): void
    {
        $originalXmlString = $original->toString();
        $parser = (new \Isdoc\Parser\Item());
        $parsedXmlString = $parser->parseXml(simplexml_load_string($originalXmlString))->toString();
        $this->assertEquals($originalXmlString, $parsedXmlString);
    }

    public static function parseDataProvider(): array
    {
        return [
            [
                (new Item())
            ], [
                (new Item())
                    ->setBuyersItemIdentification('123456')
                    ->setCatalogueItemIdentification('54926378')
                    ->setSellersItemIdentification('ID123')
            ], [
                (new Item())
                    ->setBuyersItemIdentification('123')
                    ->setCatalogueItemIdentification('9999999')
                    ->setDescription('IDK')
                    ->setTertiarySellersItemIdentification('HHH456')
                    ->setSecondarySellersItemIdentification('JJ47')
                    ->setSellersItemIdentification('XZS8')
            ]
        ];
    }
}
