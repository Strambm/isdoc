<?php

namespace Isdoc\Tests\Parser\TaxCategory;

use Isdoc\Enums\TaxScheme;
use Isdoc\Models\TaxCategory;
use PHPUnit\Framework\TestCase;
use Isdoc\Parser\TaxCategory as Parser;

class ParseXmlTest extends TestCase
{
    /**
     * @test
     * @dataProvider parseDataProvider
     */
    public function parse(TaxCategory $original): void
    {
        $originalXmlString = $original->toString();
        $parser = (new Parser());
        $parsedXmlString = $parser->parseXml(simplexml_load_string($originalXmlString))->toString();
        $this->assertEquals($originalXmlString, $parsedXmlString);
    }

    public static function parseDataProvider(): array
    {
        return [
            [
                (new TaxCategory())
                    ->setPercent(12.5)
            ], [
                (new TaxCategory())
                    ->setPercent(10)
                    ->setTaxScheme(TaxScheme::TIN)
                    ->setVATAplicable(true)
            ], [
                (new TaxCategory())
                    ->setPercent(8)
                    ->setTaxScheme(TaxScheme::TIN)
                    ->setVATAplicable(false)
                    ->setLocalReverseChargeFlag(false)
            ]
        ];
    }
}
