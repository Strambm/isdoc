<?php

namespace Isdoc\Tests\Parser\PostalAddress;

use Isdoc\Enums\CountryCode;
use Isdoc\Models\Country;
use Isdoc\Models\PostalAddress;
use PHPUnit\Framework\TestCase;
use Isdoc\Parser\PostalAddress as ParserPostalAddress;

class ParseXmlTest extends TestCase
{
    /**
     * @test
     * @dataProvider parseDataProvider
     */
    public function parse(PostalAddress $original): void
    {
        $originalXmlString = $original->toString();
        $parser = (new ParserPostalAddress());
        $parsedXmlString = $parser->parseXml(simplexml_load_string($originalXmlString))->toString();
        $this->assertEquals($originalXmlString, $parsedXmlString);
    }

    public static function parseDataProvider(): array
    {
        return [
            [
                (new PostalAddress())
            ], [
                (new PostalAddress())
                    ->setPostalZone('78963')
                    ->setCountry((new Country)->setIdentificationCode(CountryCode::AT))
                    ->setCityName('město')
                    ->setStreetName('ulice')
                    ->setBuildingNumber('123')
            ]
        ];
    }
}
