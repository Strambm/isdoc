<?php

namespace Isdoc\Tests\Parser\Contact;

use Isdoc\Models\Contact;
use PHPUnit\Framework\TestCase;
use Isdoc\Parser\Contact as ParserContact;

class ParseXmlTest extends TestCase
{
    /**
     * @test
     * @dataProvider parseDataProvider
     */
    public function parse(Contact $original): void
    {
        $originalXmlString = $original->toString();
        $parser = (new ParserContact());
        $parsedXmlString = $parser->parseXml(simplexml_load_string($originalXmlString))->toString();
        $this->assertEquals($originalXmlString, $parsedXmlString);
    }

    public static function parseDataProvider(): array
    {
        return [
            [
                (new Contact())
                    ->setElectronicMail('mail@mail.co')
                    ->setName('name')
                    ->setTelephone('phone')
            ], [
                (new Contact())
            ]
        ];
    }
}
