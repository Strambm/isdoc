<?php

namespace Isdoc\Tests\Parser\DocumentReference;

use Isdoc\Models\DocumentReference;
use PHPUnit\Framework\TestCase;
use Isdoc\Models\DeliveryNoteReference;
use Isdoc\Models\OriginalDocumentReference;
use Isdoc\Parser\DeliveryNoteReference as ParserDeliveryNoteReference;
use Isdoc\Parser\OriginalDocumentReference as ParserOriginalReference;

class ParseXmlTest extends TestCase
{
    /**
     * @test
     * @dataProvider parseDataProvider
     */
    public function parse(DocumentReference $original, string $parserClass): void
    {
        $originalXmlString = $original->toString();
        $parser = (new $parserClass());
        $model = $parser->parseXml(simplexml_load_string($originalXmlString));
        $parsedXmlString = $model->toString();
        $this->assertEquals($originalXmlString, $parsedXmlString);
        $this->assertEquals(get_class($original), get_class($model));
    }

    public static function parseDataProvider(): array
    {
        return [
            [
                (new OriginalDocumentReference())
                    ->setId('123456'),
                ParserOriginalReference::class
            ], [
                (new OriginalDocumentReference())
                    ->setId('1515')
                    ->setIssueDate('2000-02-21')
                    ->setUuid('60e02d51-0768-4d71-a7a1-3e48e971c683'),
                ParserOriginalReference::class
            ], [
                (new DeliveryNoteReference())
                    ->setId('123456'),
                ParserDeliveryNoteReference::class
            ], [
                (new DeliveryNoteReference())
                    ->setId('1515')
                    ->setIssueDate('2000-02-21')
                    ->setUuid('60e02d51-0768-4d71-a7a1-3e48e971c683'),
                ParserDeliveryNoteReference::class
            ]
        ];
    }
}
