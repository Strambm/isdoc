<?php

namespace Isdoc\Tests\Parser\Invoice;

use Isdoc\Models\Note;
use Isdoc\Models\Contact;
use Isdoc\Models\Invoice;
use Isdoc\Models\Payment;
use Isdoc\Enums\TaxScheme;
use Isdoc\Enums\CurrencyCode;
use Isdoc\Enums\DocumentType;
use Isdoc\Models\InvoiceLine;
use Isdoc\Models\ExchangeRate;
use Isdoc\Models\PartyContact;
use Isdoc\Models\PaymentMeans;
use Isdoc\Models\TaxedDeposit;
use Isdoc\Models\PostalAddress;
use PHPUnit\Framework\TestCase;
use Isdoc\Models\OrderReference;
use Isdoc\Models\PartyTaxScheme;
use Isdoc\Enums\PaymentMeansCode;
use Isdoc\Models\NonTaxedDeposit;
use Isdoc\Parser\Invoice as Parser;
use Isdoc\Enums\VatCalculationMethod;
use Isdoc\Models\PartyIdentification;
use Isdoc\Models\ClassifiedTaxCategory;
use Isdoc\Models\DeliveryNoteReference;
use Isdoc\Models\OriginalDocumentReference;
use Isdoc\Models\ElectronicPossibilityAgreementReference;

class ParseXmlTest extends TestCase
{
    /**
     * @test
     * @dataProvider parseDataProvider
     */
    public function parse(Invoice $original): void
    {
        $originalXmlString = $original->toString();
        $parser = (new Parser());
        $parsedXmlString = $parser->parseXml(simplexml_load_string($originalXmlString))->toString();
        $this->assertEquals($originalXmlString, $parsedXmlString);
    }

    public static function parseDataProvider(): array
    {
        return [
            [
                (new Invoice())->setId('TEST-1')
                ->setIssueDate('2024-01-01')
                ->setVATApplicable(true)
                ->setExchangeRate(new ExchangeRate(CurrencyCode::CZK))
                ->setAccountingSupplierParty(
                    (new PartyContact())
                        ->setPartyIdentification(
                            (new PartyIdentification())
                                ->setId('123456')
                        )->setName('Společnost XYZ')
                        ->setPostalAddress(
                            (new PostalAddress())
                                ->setStreetName('UliceX')
                                ->setBuildingNumber('12/X')
                                ->setCityName('MěstoX')
                                ->setPostalZone('12345')
                        )->addPartyTaxScheme(new PartyTaxScheme('123456', TaxScheme::VAT))
                )->setAccountingCustomerParty(
                    (new PartyContact())
                    ->setPartyIdentification(
                        (new PartyIdentification())
                            ->setId('')
                    )->setName('')
                    ->setPostalAddress(
                        (new PostalAddress())
                            ->setStreetName('')
                            ->setBuildingNumber('')
                            ->setCityName('')
                            ->setPostalZone('')
                    )
                )->addInvoiceLine(
                    (new InvoiceLine())
                        ->setId('IL-0001')
                        ->setLineExtensionAmount(5000)
                        ->setUnitPrice(500)
                        ->setClassifiedTaxCategory(
                            (new ClassifiedTaxCategory())
                                ->setPercent(10)
                                ->setVatCalculationMethod(VatCalculationMethod::ZDOLA)
                        )
                )
            ], [
                (new Invoice())->setId('TEST-1')
            ->setDocumentType(DocumentType::CORRECTING_INVOICE)
            ->setElectronicPossibilityAgreementReference(
                (new ElectronicPossibilityAgreementReference())
                    ->setValue('ZPRAVA 1')
            )
            ->setNote(
                (new Note())
                    ->setValue('ZPRAVA 2')
            )
            ->setIssueDate('2024-01-01')
            ->setTaxPointDate('2024-02-01')
            ->setVATApplicable(true)
            ->setExchangeRate(new ExchangeRate(CurrencyCode::CZK, CurrencyCode::EUR, 2, 4))
            ->setAccountingSupplierParty(
                (new PartyContact())
                    ->setPartyIdentification(
                        (new PartyIdentification())
                            ->setId('123456')
                    )->setName('Společnost XYZ')
                    ->setPostalAddress(
                        (new PostalAddress())
                            ->setStreetName('UliceX')
                            ->setBuildingNumber('12/X')
                            ->setCityName('MěstoX')
                            ->setPostalZone('12345')
                    )->addPartyTaxScheme(new PartyTaxScheme('123456', TaxScheme::VAT))
            )->setAccountingCustomerParty(
                (new PartyContact())
                ->setPartyIdentification(
                    (new PartyIdentification())
                        ->setId('')
                )->setName('')
                ->setPostalAddress(
                    (new PostalAddress())
                        ->setStreetName('')
                        ->setBuildingNumber('')
                        ->setCityName('')
                        ->setPostalZone('')
                )
            )->addInvoiceLine(
                (new InvoiceLine())
                    ->setId('IL-0001')
                    ->setLineExtensionAmount(5000)
                    ->setUnitPrice(500)
                    ->setClassifiedTaxCategory(
                        (new ClassifiedTaxCategory())
                            ->setPercent(10)
                            ->setVatCalculationMethod(VatCalculationMethod::ZDOLA)
                    )
            )->addInvoiceLine(
                (new InvoiceLine())
                    ->setId('IL-0002')
                    ->setLineExtensionAmount(5000)
                    ->setUnitPrice(500)
                    ->setClassifiedTaxCategory(
                        (new ClassifiedTaxCategory())
                            ->setPercent(10)
                            ->setVatCalculationMethod(VatCalculationMethod::ZDOLA)
                    )
            )->addOriginalDocumentReference(
                (new OriginalDocumentReference())
                    ->setId('TEST-2')
            )->setPaymentMeans(
                (new PaymentMeans())
                    ->addPayment(
                        (new Payment())
                            ->setPaidAmount(10000)
                            ->setPaymentMeansCode(PaymentMeansCode::CASH_ON_DELIVERY)
                    )
            )->setIssuingSystem('SYSTEM123')
            ->addDeliveryNoteReference(
                (new DeliveryNoteReference())
                    ->setId('TEST-3')
            )->addOrderReference(
                (new OrderReference())
                    ->setSalesOrderId('ORDER-1')
            )->setDeliveryAddress(
                (new PartyContact())
                    ->setName('Test Testovič')
                    ->setContact(
                        (new Contact())
                            ->setName('Test Testovič')
                            ->setElectronicMail('test@isdoc.cz')
                            ->setTelephone('123 456 789')
                    )->setPostalAddress(
                        (new PostalAddress())
                            ->setBuildingNumber('1')
                            ->setCityName('Testovací město')
                            ->setPostalZone('78945')
                            ->setStreetName('Testovací ulice')
                    )->setPartyIdentification(
                        (new PartyIdentification())
                            ->setId('')
                    )
            )->addTaxedDeposit(
                (new TaxedDeposit())
                    ->setId('TEST-4')
                    ->setVariableSymbol('123')
                    ->setTaxableDepositAmount(2000)
                    ->setClassifiedTaxCategory(
                        (new ClassifiedTaxCategory())
                            ->setPercent(10)
                            ->setVatCalculationMethod(VatCalculationMethod::ZDOLA)
                    )
            )->addNonTaxedDeposit(
                (new NonTaxedDeposit())
                    ->setId('TEST-5')
                    ->setVariableSymbol('1234')
                    ->setDepositAmount(2000)
            )
            ] 
        ];
    }
}
