<?php

namespace Isdoc\Tests\Parser\LegalMonetaryTotal;

use PHPUnit\Framework\TestCase;
use Isdoc\Models\LegalMonetaryTotal;

class ParseXmlTest extends TestCase
{
    /**
     * @test
     * @dataProvider parseDataProvider
     */
    public function parse(LegalMonetaryTotal $original): void
    {
        $originalXmlString = $original->toString();
        $parser = (new \Isdoc\Parser\LegalMonetaryTotal());
        $parsedXmlString = $parser->parseXml(simplexml_load_string($originalXmlString))->toString();
        $this->assertEquals($originalXmlString, $parsedXmlString);
    }

    public static function parseDataProvider(): array
    {
        return [
            [
                (new LegalMonetaryTotal())
                    ->setTaxExclusiveAmount(100)
                    ->setTaxInclusiveAmount(100)
                    ->setAlreadyClaimedTaxExclusiveAmount(0)
                    ->setAlreadyClaimedTaxInclusiveAmount(0)
                    ->setPaidDepositsAmount(0)
            ], [
                (new LegalMonetaryTotal())
                    ->setTaxExclusiveAmount(1000)
                    ->setTaxExclusiveAmountCurr(100)
                    ->setTaxInclusiveAmount(1000)
                    ->setTaxInclusiveAmountCurr(100)
                    ->setAlreadyClaimedTaxExclusiveAmount(0)
                    ->setAlreadyClaimedTaxExclusiveAmountCurr(0)
                    ->setAlreadyClaimedTaxInclusiveAmount(0)
                    ->setAlreadyClaimedTaxInclusiveAmountCurr(0)
                    ->setPaidDepositsAmount(0)
                    ->setPaidDepositsAmountCurr(0)
                    ->setPayableRoundingAmount(0)
                    ->setPayableRoundingAmountCurr(0)
            ], [
                (new LegalMonetaryTotal())
                    ->setTaxExclusiveAmount(1000)
                    ->setTaxInclusiveAmount(1100)
                    ->setAlreadyClaimedTaxExclusiveAmount(500)
                    ->setAlreadyClaimedTaxInclusiveAmount(550)
                    ->setPaidDepositsAmount(200)
            ]
        ];
    }
}
