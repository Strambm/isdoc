<?php

namespace Isdoc\Tests\Parser\OrderReference;

use Isdoc\Models\OrderReference;
use PHPUnit\Framework\TestCase;
use Isdoc\Parser\OrderReference as ParserOrderReference;

class ParseXmlTest extends TestCase
{
    /**
     * @test
     * @dataProvider parseDataProvider
     */
    public function parse(OrderReference $original): void
    {
        $originalXmlString = $original->toString();
        $parser = (new ParserOrderReference());
        $parsedXmlString = $parser->parseXml(simplexml_load_string($originalXmlString))->toString();
        $this->assertEquals($originalXmlString, $parsedXmlString);
    }

    public static function parseDataProvider(): array
    {
        return [
            [
                (new OrderReference())
                    ->setSalesOrderId('123456')
            ], [
                (new OrderReference())
                    ->setSalesOrderId('15815')
                    ->setExternalOrderId('151515')
                    ->setExternalIssueDate('2025-01-01')
            ], [
                (new OrderReference())
                    ->setSalesOrderId('1515')
                    ->setExternalIssueDate('2022-01-01')
                    ->setExternalOrderId('559959')
                    ->setFileReference('7777')
                    ->setIsdsId('fefefe')
                    ->setIssueDate('2000-02-21')
                    ->setUuid('60e02d51-0768-4d71-a7a1-3e48e971c683')
                    ->setReferenceNumber('hhuhu4848')
            ]
        ];
    }
}
