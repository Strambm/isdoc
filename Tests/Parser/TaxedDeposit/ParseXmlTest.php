<?php

namespace Isdoc\Tests\Parser\TaxedDeposit;

use Isdoc\Enums\CurrencyCode;
use Isdoc\Enums\VatCalculationMethod;
use Isdoc\Models\ClassifiedTaxCategory;
use Isdoc\Models\ExchangeRate;
use Isdoc\Models\TaxedDeposit;
use PHPUnit\Framework\TestCase;
use Isdoc\Parser\TaxedDeposit as Parser;

class ParseXmlTest extends TestCase
{
    /**
     * @test
     * @dataProvider parseDataProvider
     */
    public function parse(TaxedDeposit $original): void
    {
        $originalXmlString = $original->toString();
        $parser = (new Parser());
        $parsedXmlString = $parser->parseXml(simplexml_load_string($originalXmlString))->toString();
        $this->assertEquals($originalXmlString, $parsedXmlString);
    }

    public static function parseDataProvider(): array
    {
        return [
            [
                (new TaxedDeposit())
                    ->setClassifiedTaxCategory(
                        (new ClassifiedTaxCategory())
                            ->setPercent(10)
                            ->setVatCalculationMethod(VatCalculationMethod::SHORA)
                            ->setVatApplicable(true)
                            ->setLocalReverseCharge(false)
                    )->setId('123456')
                    ->setVariableSymbol('VS123456')
                    ->setTaxableDepositAmount(100)
                    ->setTaxInclusiveAmount(110)
                    
            ], [
                (new TaxedDeposit())
                    ->setClassifiedTaxCategory(
                        (new ClassifiedTaxCategory())
                            ->setPercent(10)
                            ->setVatCalculationMethod(VatCalculationMethod::SHORA)
                            ->setVatApplicable(true)
                            ->setLocalReverseCharge(true)
                    )->setId('55')
                    ->setVariableSymbol('55')
                    ->setTaxInclusiveAmount(110)
            ], [
                (new TaxedDeposit())
                    ->setClassifiedTaxCategory(
                        (new ClassifiedTaxCategory())
                            ->setPercent(10)
                            ->setVatCalculationMethod(VatCalculationMethod::SHORA)
                            ->setVatApplicable(true)
                            ->setLocalReverseCharge(true)
                    )->setId('55')
                    ->setVariableSymbol('55')
                    ->setTaxInclusiveAmount(110)
                    ->setExchangeRate(new ExchangeRate(CurrencyCode::CZK, CurrencyCode::EUR, 1, 25.36))
            ], 
        ];
    }
}
