<?php

namespace Isdoc\Tests\Traits\DateValidation;

use Isdoc\Traits\DateValidation;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase as ParentTestCase;

abstract class TestCase extends ParentTestCase
{
    protected function getModel(): MockObject
    {
        return $this->getMockForTrait(DateValidation::class);
    }
}
