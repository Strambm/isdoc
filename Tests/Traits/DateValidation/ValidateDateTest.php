<?php

namespace Isdoc\Tests\Traits\DateValidation;

use Isdoc\Exceptions\InvalidDateFormat;
use Cetria\Helpers\Reflection\Reflection;
use TypeError;

class ValidateDateTest extends TestCase
{
    /**
     * @test
     * @dataProvider unvalidDateDataProvider
     */
    public function unvalidDate(string $value): void
    {
        $this->expectException(InvalidDateFormat::class);
        $this->act('validateDateNullable', $value);
    }

    public static function unvalidDateDataProvider(): array
    {
        return [
            [
                '2023-02-29',
            ], [
                '2024-02-30',
            ], [
                '2024-13-01',
            ]
        ];
    }

    /**
     * @test
     * @dataProvider validDateProvider
     * @doesNotPerformAssertions
     */
    public function validDate(string $value): void
    {
        $this->act('validateDateNullable', $value);
        $this->act('validateDate', $value);
    }

    public static function validDateProvider(): array
    {
        return [
            [
                '2024-02-02'
            ]
        ];
    }

    /**
     * @test
     */
    public function nullValueOnValidateDateMethod(): void
    {
        $this->expectException(TypeError::class);
        $this->act('validateDate', null);
    }

    /**
     * @test
     * @doesNotPerformAssertions
     */
    public function nullValueOnValidateDateNullable(): void
    {
        $this->act('validateDateNullable', null);
    }


    protected function act(string $methodName, string|null $value): string|null
    {
        $model = $this->getModel();
        $method = Reflection::getHiddenMethod($model, $methodName);
        return $method->invoke($model, $value);
    }
}
