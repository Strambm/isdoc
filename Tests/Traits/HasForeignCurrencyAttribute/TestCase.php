<?php

namespace Isdoc\Tests\Traits\HasForeignCurrencyAttribute;

use Isdoc\Traits\HasForeignCurrencyAttribute;
use PHPUnit\Framework\TestCase as ParentTestCase;

abstract class TestCase extends ParentTestCase
{
    protected function getModel(): object
    {
        return $this->getMockForTrait(HasForeignCurrencyAttribute::class);
    }
}
