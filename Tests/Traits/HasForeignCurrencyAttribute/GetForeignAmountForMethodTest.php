<?php

namespace Isdoc\Tests\Traits\HasForeignCurrencyAttribute;

use Isdoc\Enums\CurrencyCode;
use Isdoc\Models\ExchangeRate;
use Cetria\Helpers\Reflection\Reflection;
use Isdoc\Traits\HasForeignCurrencyAttribute;
use Isdoc\Exceptions\ManuallySettedForeignCurrencyValueWithoutExchangeRate;

class GetForeignAmountForMethodTest extends TestCase
{
    /**
     * @test
     */
    public function currAttributeSetted(): void
    {
        $model = $this->getModel();
        $model->testMethodCurr = 12;
        $model->testMethod = 1;
        $model->setThrowManuallySettedForeignCurrencyValueWithoutExchangeRate(false);
        $result = $this->act($model);
        $this->assertEquals(12, $result);
    }

    protected function getModel(): object
    {
        $object = new class()
        {   
            use HasForeignCurrencyAttribute;

            public $testMethodCurr =  null;
            public $testMethod = 1;

            public function getTestMethodCurr(): null|float
            {
                return $this->testMethodCurr;
            }

            public function getTestMethod(): float
            {
                return $this->testMethod;
            }
        };
        return $object;
    }
    
    protected function act(object $object): float|null
    {
        $method = Reflection::getHiddenMethod($object, 'getForeignAmountForMethod');
        return $method->invoke($object, 'getTestMethodCurr');
    }

    /**
     * @test
     */
    public function currAutomaticsCalculated(): void
    {
        $model = $this->getModel();
        $model->setExchangeRate(new ExchangeRate(CurrencyCode::CZK, CurrencyCode::EUR, 5, 24));
        $model->testMethodCurr = null;
        $model->testMethod = 10;
        $result = $this->act($model);
        $this->assertEquals(2, $result);
    }

    /**
     * @test
     */
    public function currManuallySetted(): void
    {
        $model = $this->getModel();
        $model->setExchangeRate(new ExchangeRate(CurrencyCode::CZK, CurrencyCode::EUR, 5, 24));
        $model->testMethodCurr = 2;
        $model->testMethod = 10;
        $result = $this->act($model);
        $this->assertEquals(2, $result);
    }

    /**
     * @test
     */
    public function currManuallySettedDifferently(): void
    {
        $model = $this->getModel();
        $model->setExchangeRate(new ExchangeRate(CurrencyCode::CZK, CurrencyCode::EUR, 5, 24));
        $model->testMethodCurr = 3;
        $model->testMethod = 10;
        $result = $this->act($model);
        $this->assertEquals(3, $result);
    }

    /**
     * @test
     */
    public function currAttributeSetted_throwExceprtion(): void
    {
        $this->expectException(ManuallySettedForeignCurrencyValueWithoutExchangeRate::class);
        $model = $this->getModel();
        $model->testMethodCurr = 12;
        $model->testMethod = 1;
        $this->act($model);
    }
}
