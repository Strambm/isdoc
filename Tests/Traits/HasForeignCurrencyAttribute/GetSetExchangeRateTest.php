<?php

namespace Isdoc\Tests\Traits\HasForeignCurrencyAttribute;

use Isdoc\Enums\CurrencyCode;
use Isdoc\Models\ExchangeRate;
use Cetria\Helpers\Reflection\Reflection;

class GetSetExchangeRateTest extends TestCase
{
    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function get(ExchangeRate|null $newValue): void
    {
        $model = $this->getModel();
        Reflection::setHiddenProperty($model, 'exchangeRate', $newValue);
        $this->assertEquals($newValue, $model->getExchangeRate());
    }

    public static function getSetDataProvider(): array
    {
        return [
            [
                new ExchangeRate(CurrencyCode::CZK)
            ], [
                null
            ]
        ];
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function set(ExchangeRate|null $newValue): void
    {
        $model = $this->getModel();
        $model->setExchangeRate($newValue);
        $this->assertEquals($newValue, Reflection::getHiddenProperty($model, 'exchangeRate'));
    }

    /**
     * @test
     * @dataProvider getSetDataProvider
     */
    public function getSet(ExchangeRate|null $newValue): void
    {
        $model = $this->getModel();
        $this->assertEquals($newValue, $model->setExchangeRate($newValue)->getExchangeRate());
    }
}
