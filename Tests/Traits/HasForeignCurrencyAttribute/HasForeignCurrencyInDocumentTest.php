<?php

namespace Isdoc\Tests\Traits\HasForeignCurrencyAttribute;

use Isdoc\Enums\CurrencyCode;
use Isdoc\Models\ExchangeRate;

class HasForeignCurrencyInDocumentTest extends TestCase
{
    /**
     * @test
     */
    public function nullExcahngeRate(): void
    {
        $model = $this->getModel();
        $model->setExchangeRate(null);
        $this->assertFalse($model->hasForeignCurrencyInDocument());
    }

    /**
     * @test
     */
    public function lokalExchangeRate(): void
    {
        $model = $this->getModel();
        $model->setExchangeRate(new ExchangeRate(CurrencyCode::CZK));
        $this->assertFalse($model->hasForeignCurrencyInDocument());
    }

    /**
     * @test
     */
    public function foreignExchangeRate(): void
    {
        $model = $this->getModel();
        $model->setExchangeRate(new ExchangeRate(CurrencyCode::CZK, CurrencyCode::USD, 24, 24.12));
        $this->assertTrue($model->hasForeignCurrencyInDocument());
    }
}
