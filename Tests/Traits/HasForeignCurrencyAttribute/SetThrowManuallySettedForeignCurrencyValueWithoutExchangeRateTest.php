<?php

namespace Isdoc\Tests\Traits\HasForeignCurrencyAttribute;

use Cetria\Helpers\Reflection\Reflection;

class SetThrowManuallySettedForeignCurrencyValueWithoutExchangeRateTest extends TestCase
{
    /**
     * @test
     * @dataProvider setDataProvider
     */
    public function setFalse(bool $value): void
    {
        $model = $this->getModel();
        $model->setThrowManuallySettedForeignCurrencyValueWithoutExchangeRate($value);
        $this->assertEquals($value, Reflection::getHiddenProperty($model, 'throwManuallySettedForeignCurrencyValueWithoutExchangeRateEnable'));
    }

    public static function setDataProvider(): array
    {
        return [
            [
                false,
            ], [
                true,
            ]
        ];
    }
}
