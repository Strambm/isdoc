<?php

namespace Isdoc\Tests\Traits\BoolValidation;

use Isdoc\Traits\BoolValidation;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase as ParentTestCase;

abstract class TestCase extends ParentTestCase
{
    protected function getModel(): MockObject
    {
        return $this->getMockForTrait(BoolValidation::class);
    }
}
