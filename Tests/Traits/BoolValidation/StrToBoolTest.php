<?php

namespace Isdoc\Tests\Traits\BoolValidation;

use Cetria\Helpers\Reflection\Reflection;
use Throwable;

class StrToBoolTest extends TestCase
{
    /**
     * @test
     */
    public function nullValue(): void
    {
        $this->assertNull($this->act(null));
    }

    /**
     * @test
     */
    public function emptyString(): void
    {
        $this->assertNull($this->act(''));
    }

    /**
     * @test
     */
    public function randomString(): void
    {
        $this->expectException(Throwable::class);
        $this->assertNull($this->act('rgrgrgrg'));
    }

    /**
     * @test
     */
    public function trueString(): void
    {
        $this->assertTrue($this->act('true'));
    }

    /**
     * @test
     */
    public function falseString(): void
    {
        $this->assertFalse($this->act('false'));
    }

    protected function act(string|null $value): bool|null
    {
        $model = $this->getModel();
        $methodName = 'strToBool';
        $method = Reflection::getHiddenMethod($model, $methodName);
        return $method->invoke($model, $value);
    }
}
