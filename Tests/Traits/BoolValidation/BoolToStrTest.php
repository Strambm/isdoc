<?php

namespace Isdoc\Tests\Traits\BoolValidation;

use Cetria\Helpers\Reflection\Reflection;

class BoolToStrTest extends TestCase
{
    /**
     * @test
     */
    public function nullValue(): void
    {
        $this->assertNull($this->act(null));
    }

    /**
     * @test
     */
    public function trueValue(): void
    {
        $this->assertEquals('true', $this->act(true));
    }

    /**
     * @test
     */
    public function falseString(): void
    {
        $this->assertEquals('false', $this->act(false));
    }

    protected function act(bool|null $value): string|null
    {
        $model = $this->getModel();
        $methodName = 'boolToStr';
        $method = Reflection::getHiddenMethod($model, $methodName);
        return $method->invoke($model, $value);
    }
}
