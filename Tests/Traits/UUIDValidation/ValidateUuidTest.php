<?php

namespace Isdoc\Tests\Traits\UUIDValidation;

use TypeError;
use Isdoc\Models\Invoice;
use Isdoc\Exceptions\InvalidUuidFormat;
use Cetria\Helpers\Reflection\Reflection;

class ValidateUuidTest extends TestCase
{
    /**
     * @test
     * @dataProvider unvalidNumericDataProvider
     */
    public function unvalidDate(string $value): void
    {
        $this->expectException(InvalidUuidFormat::class);
        $this->act('validateUuidNullable', $value);
    }

    public static function unvalidNumericDataProvider(): array
    {
        return [
            [
                '152-18-18196-fwefg1-efef',
            ], 
        ];
    }

    /**
     * @test
     * @dataProvider validDateProvider
     * @doesNotPerformAssertions
     */
    public function validNumeric(string $value): void
    {
        $this->act('validateUuid', $value);
        $this->act('validateUuidNullable', $value);
    }

    public static function validDateProvider(): array
    {
        return [
            [
                (new Invoice())->getUuid()
            ],
        ];
    }

    /**
     * @test
     */
    public function nullValueOnValidateUuidMethod(): void
    {
        $this->expectException(TypeError::class);
        $this->act('validateUuid', null);
    }

    /**
     * @test
     * @doesNotPerformAssertions
     */
    public function nullValueOnValidateUuidNullable(): void
    {
        $this->act('validateUuidNullable', null);
    }


    protected function act(string $methodName, string|null $value): string|null
    {
        $model = $this->getModel();
        $method = Reflection::getHiddenMethod($model, $methodName);
        return $method->invoke($model, $value);
    }
}
