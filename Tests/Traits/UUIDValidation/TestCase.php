<?php

namespace Isdoc\Tests\Traits\UUIDValidation;

use Isdoc\Traits\UUIDValidation;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase as ParentTestCase;

abstract class TestCase extends ParentTestCase
{
    protected function getModel(): MockObject
    {
        return $this->getMockForTrait(UUIDValidation::class);
    }
}
