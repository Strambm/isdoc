<?php

namespace Isdoc\Tests\Traits\StringConversion;

use Isdoc\Traits\StringConversion;
use Isdoc\Models\IsdocSimpleXMLElement;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase as ParentTestCase;

abstract class TestCase extends ParentTestCase
{
    protected function getModel(): MockObject
    {
        $model = $this->getMockForTrait(StringConversion::class);
        $model->expects($this->once())
            ->method('toXmlElement')
            ->willReturn(new IsdocSimpleXMLElement('<example></example>'));
        return $model;
    }
}
