<?php

namespace Isdoc\Tests\Traits\StringConversion;

class ToStringTest extends TestCase
{
    /**
     * @test
     */
    public function default(): void
    {
        $model = $this->getModel();
        $formattedXml = $model->toString();
        $expectedXml = '<example></example>';
        $this->assertEquals(trim($expectedXml), trim($formattedXml));
    }

    /**
     * @test
     */
    public function withHeader(): void
    {
        $model = $this->getModel();
        $formattedXml = $model->toString(true);
        $expectedXml = '<?xml version="1.0" encoding="utf-8"?>' . PHP_EOL .'<example></example>';
        $this->assertEquals(trim($expectedXml), trim($formattedXml));
    }
}
