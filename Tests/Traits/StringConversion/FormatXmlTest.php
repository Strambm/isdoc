<?php

namespace Isdoc\Tests\Traits\StringConversion;
use DOMDocument;
use Cetria\Helpers\Reflection\Reflection;

class FormatXmlTest extends TestCase
{
    /**
     * @test
     */
    public function basic(): void
    {
        $formattedXml = $this->act();
        $this->assertInstanceOf(DOMDocument::class, $formattedXml);
        $expectedXml = '<?xml version="1.0" encoding="utf-8"?><example/>';
        $this->assertXmlStringEqualsXmlString($expectedXml, $formattedXml->saveXML());
    }

    protected function act(): DOMDocument
    {
        $model = $this->getModel();
        $methodName = 'formatXml';
        $method = Reflection::getHiddenMethod($model, $methodName);
        return $method->invoke($model); 
    }
}
