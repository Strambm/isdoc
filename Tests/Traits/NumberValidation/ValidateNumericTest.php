<?php

namespace Isdoc\Tests\Traits\NumberValidation;

use TypeError;
use Cetria\Helpers\Reflection\Reflection;
use Isdoc\Exceptions\InvalidNumericFormat;

class ValidateNumericTest extends TestCase
{
    /**
     * @test
     * @dataProvider unvalidNumericDataProvider
     */
    public function unvalidDate(string $value): void
    {
        $this->expectException(InvalidNumericFormat::class);
        $this->act('validateNumericNullable', $value);
    }

    public static function unvalidNumericDataProvider(): array
    {
        return [
            [
                '152-18',
            ], [
                'AR123',
            ], [
                'ARfefe',
            ], [
                ' '
            ], [
                '8484.1'
            ]
        ];
    }

    /**
     * @test
     * @dataProvider validDateProvider
     * @doesNotPerformAssertions
     */
    public function validNumeric(string $value): void
    {
        $this->act('validateNumericNullable', $value);
        $this->act('validateNumeric', $value);
    }

    public static function validDateProvider(): array
    {
        return [
            [
                '123'
            ], [
                '1'
            ], 
        ];
    }

    /**
     * @test
     */
    public function nullValueOnValidateNumericMethod(): void
    {
        $this->expectException(TypeError::class);
        $this->act('validateNumeric', null);
    }

    /**
     * @test
     * @doesNotPerformAssertions
     */
    public function nullValueOnValidateDateNullable(): void
    {
        $this->act('validateNumericNullable', null);
    }


    protected function act(string $methodName, string|null $value): string|null
    {
        $model = $this->getModel();
        $method = Reflection::getHiddenMethod($model, $methodName);
        return $method->invoke($model, $value);
    }
}
