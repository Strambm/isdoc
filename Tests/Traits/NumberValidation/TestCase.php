<?php

namespace Isdoc\Tests\Traits\NumberValidation;

use Isdoc\Traits\NumberValidation;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase as ParentTestCase;

abstract class TestCase extends ParentTestCase
{
    protected function getModel(): MockObject
    {
        return $this->getMockForTrait(NumberValidation::class);
    }
}
